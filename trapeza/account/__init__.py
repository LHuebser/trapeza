from trapeza.account.base_account import BaseAccount
from trapeza.account.fx_account import FXAccount

# TODO: Account for stock shares: Based on FXAccount, but only allow integers for share volumes (respectively use kwarg
#       to additionally specify if float/ ratio share volumes are allowed)
