Go back: [Table of Content](table_of_content.md)  
- [Strategy](#strategy)
  - [Introduction and Purpose](#introduction-and-purpose)
  - [Instantiate Strategy Object](#instantiate-strategy-object)
  - [Running a Strategy](#running-a-strategy)
    - [Overall Concept](#overall-concept)
    - [Lookback](#lookback)
    - [Run!](#run)
  - [Input Data Format](#input-data-format)
  - [Defining a Custom Strategy Decision Function](#defining-a-custom-strategy-decision-function)
  - [Binding](#binding)  
  - [Ticking Behavior and Evaluation](#ticking-behavior-and-evaluation)
    - [The convenient Way: auto-ticking](#the-convenient-way-auto-ticking)
    - [The customized Way: user-defined ticking](#the-customized-way-user-defined-ticking)
    - [The complex Way: suppressed ticking](#the-complex-way-suppressed-ticking)
  - [Input Accounts](#input-accounts)
  - [Retrieving Results](#retrieving-results)
  - [Type Checking](#type-checking)
  
  
---------
# Strategy
Within the framework of _trapeza_, a strategy is defined by a user-defined function which is automatically evaluated on 
(multiple) account(s) with user-supplied market data.
________________________________
## Introduction and Purpose
In order to evaluate the performance (e.g. profitability, volatility, maximal drawdowns) of trading strategies or 
basically any set of decision rules regarding financial transaction, backtesting is applied to historic market data. 
Backtesting is performed under the assumption, that future events and market behavior will behave at least in a somehow 
similar manner as the historic data. Based on this assumption, strategies are evaluated regarding how they would have 
performed under realistic market conditions and whether they could be profitable in the future.  
  
_trapeza_ builds a framework to automate this process and to gain deeper insights into custom defined strategy performance. 
The main goal of _trapeza_ is to build realistic backtests and provide a high degree of flexibility with regard to 
modelling real-life situations than rather excel at computational speed. There a plethora of excellent backtest libraries, 
but most of them induce assumptions for simplification (i.e. neglecting slippage and fees). _trapeza_ on the other hand 
utilizes its account objects ([FXAccount](account.md)) to enable realistic, highly customizable and controllable mechanics 
for modelling trading strategies.  
  
The _FXStrategy_ object in _trapeza_ takes:  
  - a user defined strategy decision function (the actual 'trading strategy'),  
  - one or multiple accounts (_FXAccount_ objects), which can be used inside the user defined strategy decision function,  
  - historic market data,  

and automates the execution of the trading strategy, the calculation of results and serves as basis for a more sophisticated 
stochastic backtesting (see [_FXEngine_](engine.md)).  
[<img src="media/concept_strategy.png" alt="dashboard" width="800"/>](media/concept_strategy.png)  
<sup>*</sup>: [icon attribution](icon_attribution.md)
## Instantiate Strategy Object
Every strategy has to be named uniquely. The decision strategy function and a list of accounts are passed to FXStrategy 
within its constructor. Furthermore, a lookback value has to be defined:  
````python
from trapeza.account import FXAccount
from trapeza.strategy import FXStrategy

# instantiate an account
acc_0 = FXAccount('EUR')

# define function
def strategy_function(accounts, price_data_point, reference_currency, volume_data_point, fxstrategy):
    # accounts: list of account objects (inherited from BaseAccount)
    # price_data_point: dict,
    #                   key: tuple of currency strings
    #                   value: list of exchange rates (floats) as time series, 
    #                          last value in list is the most recent one (current time step), 
    #                          length=lookback+1, will be explained later on
    #                   e.g. {('USD','EUR'):[0.82, 0.81, 0.9, 0.82, ...], ('JPY','EUR'):[...]}
    # reference_currency: str
    # volume_data_point: same format as price_data, but with volumes instead of exchange rates
    # fxstrategy: FXStrategy object (such that strategy object can call itself, will be explained later on)
    # some code
    return

# instantiate strategy with one account
strategy = FXStrategy('my_awesome_strategy', acc_0, strategy_function, 5)

acc_1 = FXAccount('USD')

# instantiate strategy with multiple accounts
strategy = FXStrategy('my_awesome_strategy', [acc_0, acc_1], strategy_function, 5)
````  
## Running a Strategy
To better understand the parameters in the FXStrategy constructor and how to define custom strategy decision functions, 
we first have to take a look how the overall concept works:  
### Overall Concept  
Basically, the user supplies a list of exchange rates per currency pair. The data is structured in a Python dictionary: 
currency paris serve as dict keys, list of exchange rates (floats) serve as dict values (see [Input Data Format](#input-data-format)). 
Each float value within the list of exchange rates (historic market data) represents a distinct time step, e.g.:  
````
| t_0  | t_1  | t_2  | t_3  | t_4  |
| ---- | ---- | ---- | ---- | ---- |
| 0.82 | 0.81 | 0.85 | 0.78 | 0.82 |

with t_4 being the most recent value and t_0 the oldest
each step has the same time delta (can be any unit defined by the user, e.g. seconds, minutes, hours, etc.)
````  
The strategy object takes in those data and:  
  - loops over each time step, starting from t_0,  
  - takes all exchange rates of the current (loop) time step t_i from the lists of all currency pairs,  
  - feeds those current exchange rates into the strategy decision function,  
  - feeds additional data from previous time steps (defined by lookback) into the strategy decision function (e.g. if the 
    strategy decision function needs to calculate the average exchange rate of the past 5 time steps w.r.t the current 
    (loop) time step t_i),  
  - executes the strategy decision function for the current data,  
  - evaluates the results after executing the strategy decision function and stores them,  
  - repeats this process until all time steps have been looped through.  
  
[<img src="media/overall_concept_strategy.png" alt="dashboard" width="400"/>](media/overall_concept_strategy.png)
### Lookback
For some strategies it is necessary not just only look at the current time step, but to include data from previous time 
steps as well. E.g. a strategy might calculate the average of the previous 5 time steps in order to make a decision for 
buying or selling an asset at the current time step.  
The amount of data regarding previous time steps is defined by the argument _lookback_ during instantiation of a FXStrategy 
object.  

[<img src="media/lookback.png" alt="dashboard" width="800"/>](media/lookback.png)  
As shown in above figure, the actual __strategy execution does no longer start at index 0 but at index 5 (lookback=5)__ in 
order to provide enough lookback-data to the strategy function.  
  
Strategy execution starts at index 5 and provides data from t_0 to t_5 (with t_5 being the current time step) at its first 
loop iteration, then provides data from t_1 to t_6 (with t_6 being the new current time step) for second loop iteration, 
and so on and so forth.  
Therefore, the first index in the collected results will map to the first loop iteration (t_5), second index will map to 
the second loop iteration (t_6) and so on (see [_Retrieving Results_](#retrieving-results) for further details on how the 
data format of results is structured).  
  
Strategy execution always starts at index=lookback (in regard to input time series).
### Run!
````python
from trapeza.account import FXAccount
from trapeza.strategy import FXStrategy

# instantiate accounts
acc_0 = FXAccount('EUR')
acc_1 = FXAccount('USD')

# define function
def strategy_function(accounts, price_data_point, reference_currency, volume_data_point, fxstrategy):
    # some code
    return

# instantiate strategy with multiple accounts
strategy = FXStrategy('my_awesome_strategy', [acc_0, acc_1], strategy_function, 5)

# define data
price_data = {('USD', 'EUR'): [0.82, 0.82, 0.81, 0.85, 0.79, 0.83, 0.87, 0.78, 0.82]}
volume_data = {('USD', 'EUR'): [182, 282, 381, 285, 179, 283, 387, 278, 182]}

# RUN
strategy.run(price_data, 'USD', volume_data)
````
See [Retrieving Results](#retrieving-results) on how to get strategy results.  
## Input Data Format  
The input data for FXStrategy is structured as a Python dictionary.  
The dict keys are composed of string tuples which name the currency pair (or any other tradable asset pair).  
The dict values are composed of lists of floats representing time series of exchange rates for the respective key (string tuple).  
Example:  
````
data = {('USD', 'EUR'): [0.82, 0.81, 0.9, 0.82, ...],
        ('APPL', 'USD'): [... ... ...]
       }

each list must have the same length
````  
If a [_lookback_](#lookback) is defined, the user must manually append historic data (spanning lookback-number of time 
steps relative to the first list entry) to this above mentioned data structure:  
````
lookback: 5

data = {('USD', 'EUR'): [t_-5, t_-4, t_-3, t_-2, t_-1, 0.82, 0.81, 0.9, 0.82, ...],
        ('APPL', 'USD'): [t_-5, t_-4, t_-3, t_-2, t_-1, ... ... ...]
       }
````  
This concept is depicted in the figure below with a lookback size of 2:  
[<img src="media/data_format_strategy.png" alt="dashboard" width="600"/>](media/data_format_strategy.png)  
All lists in the dict must have the same length!  
  
__If input price data contains the keys (currency_1, currency_2) and its inverse (currency_2, currency_2), then the 
exchange rates must match with regard to their inverse values, e.g. exchange rate of first pair must equal the inverse 
of the exchange rates of the second (inverse) pair.__  
__This 'inverse rates'-check is not applied to volume data!__  
## Defining a Custom Strategy Decision Function  
The trading strategy is modelled as a strategy decision function, which gets fed with data and executed at each loop of 
FXStrategy. Therefore, this user-defined function must follow a specific function signature:  
````python
def strategy_function(accounts, price_data_point, reference_currency, volume_data_point, fxstrategy, **strategy_kwargs):
    # some code...
    return
````  
with the function arguments defined as follows:  
````
accounts:           list of FXAccount objects in the same order as supplied to FXStrategy during instantiation.
price_data_point:   dict with string tuples (naming the tradable pair) as dict keys and list of floats as time series of 
                    exchange rates as dict values. The length of the lists is defined by lookback + 1. If lookback is 
                    set 0, the list will only contain a single element (which is the exchange rate of the current time 
                    step within the FXStrategy execution loop).
reference_currency: string, name of reference currency to calculate the total value of the strategy (summarizes over all
                    accounts).
volume_data_point:  same as price_data_point but with volumes instead
fxstrategy:         current FXStrategy object, to which this function was passed as argument to during FXStrategy 
                    instantiation. This argument can be used e.g. to call fxstrategy.add_signal(...) or used for 
                    injection (see sections below).
strategy_kwargs:    keyword arguments which were passed at FXStrategy instantiation

accounts, price_data_point, reference_currency and fxstrategy are positional arguments
stratey_kwargs are **kwargs argument and will be called via kwargs inside FXStrategy
````  
The strategy decision function does not need to return any value.  
  
_____________________  
__Within the strategy decision function, all transaction methods of [_FXAccount_](account.md) can be invoked__ (this also 
includes using processing_durations)__. There are several ways how to incorporate _FXAccount.tick()_ into the concept 
of _FXStrategy___.  
  
___price\_data\_point_ always contains lists. If lookback=0, this list only contains a single element (current time step
of loop iteration during strategy execution), else the list length is lookback + 1.__
  
__accounts, price_data_point, reference_currency and fxstrategy are positional arguments (only placeholder, can be 
named whatever)__
____________________  
  
Use of _strategy_kwargs_:  
````python
def strategy_function(accounts, price_data_point, reference_currency, volume_data_point, fxstrategy, some_constant):
    # some code to calculate transaction volume
    calculated_transaction_volume += some_constant
    # some code...
    return


strategy = FXStrategy('my_awesome_strategy', [acc_0, acc_1], 3, some_constant=100)
````  
Additional function arguments for the strategy decision function can be defined via **kwargs as shown in the example 
above. The values for those arguments have to be passed to the FXStrategy constructor and are not changed during 
the execution of the loops inside FXStrategy.run() (even though this is possible by accessing _FXStrategy.strategy_kwargs_
from outside the strategy decision function).    
  
The strategy kwargs is accessible via the attribute __FXStrategy.strategy_kwargs__, which returns a protected Dictionary 
(see [_Account - Retrieving complete Depot_](account.md#retrieving-complete-depot)). The returned dict is therefore 
immutable. The attribute FXStrategy.strategy_kwargs can be changed by overwriting it with an entire dict.  
  
_strategy_kwargs_ serves for passing additional arguments to the strategy decision function. Those arguments are passed once 
during the instantiation of FXStrategy and are then passed to the strategy decision function. Therefore, those arguments 
will always be passed with the values that are set at instantiation, even if the user changes them within the strategy 
decision function (changes will not persist at the next internal loop/ iteration). If dynamic variables with memory shall be used, 
then either pass entire class objects (the values that shall be mutable can be implemented as class attributes, see 
[Smooth Moving Average Crossing - Example](examples.md#the-strategy---mac)) or use 
[Binding](#binding) in order to bind a new variable to the FXStrategy object, which can be either accessed via 
_self.variable_ or _fxstrategy.variable_ within the strategy decision function. Alternatively, those arguments can be 
changed from outside the strategy decision function via _FXStrategy.strategy_kwargs_ class attribute.  
## Binding
Similar to [binding/ injection at accounts](account.md#binding-injection), new variables can be bound to the FXStrategy object:  
````python
strat = strategy = FXStrategy('my_awesome_strategy', [acc_0, acc_1], strategy_function, 5)
strat.some_variable = 42    # can be int, float, str, object, etc.

def strategy_function(accounts, price_data_point, reference_currency, volume_data_point, fxstrategy):
    # access new variable
    self.some_variable += 1
    
    # alternatively, which is equal to above line
    fxstrategy.some_variable += 1
````  
The new variable is either accessible via _self_ or _fxstrategy_.  
  
Furthermore, instead of using the arguments of the call signature the user can also use the class objects directly in 
the strategy decision function:  
````python
def strategy_function(accounts, price_data_point, reference_currency, volume_data_point, fxstrategy):
    acc_0 = self.accounts[0]
    
    # alternatively, which is equal to above line
    acc_0 = accounts[0]
````  
Thereby, any FXStrategy class attribute is accessible within the strategy decision function.  
## Ticking Behavior and Evaluation
As described in the documentation section about [_FXAccount_](account.md), the main idea behind accounts is implemented 
as a stack automaton with discrete time steps. _FXStrategy_ continues with the idea of discrete time steps, which is 
described by the [overall concept](#overall-concept). This is implemented by looping over each time step in supplied 
market data during the execution of FXStrategy.  
Furthermore, there are several ways how to implement the idea of [_ticking accounts_](account.md#ticking-the-internal-clock) 
(moving forward in discrete time steps) in FXStrategy.
_suppress_ticking_ controls how ticking is performed and is set at instantiation of FXStrategy.  
### The convenient Way: auto-ticking  
__This option is only available if FXStrategy.suppress_ticking==False (set at constructor).__  
  
The easiest way is not bothering with FXAccount at all!  
FXStrategy will check the clock status of all accounts at each loop of its strategy execution and auto-detects, whether 
accounts need to invoke account.tick(). 
The user either can:  
  - call FXAccount.tick() explicitly in the user-defined strategy decision function, or  
  - just not carry about FXAccount.tick() at all.  
  
FXAccount.tick() can be explicitly but does not need to be called inside the user-defined strategy decision function. 
Either way, FXStrategy will appropriately take care of this. This is also the case if the account object has been 
patched by [Order Management](order_management.md).  
__To enable this, _suppress_ticking_ has to be set to False (default).__
### The customized Way: user-defined ticking
__This option is only available if FXStrategy.suppress_ticking==False (set at constructor).__  
  
Besides of letting FXStrategy tick the accounts by invoking FXAccount.tick(...), it is possible to define a custom tick 
function.  
This tick function does not directly affect FXAccount.tick(...) (i.e. FXAccount.tick() will not be overwritten), 
but can be used to customize what shall happen at each iteration of the strategy execution. Inside this function, the 
user can define how FXAccount shall be ticked at each iteration. In other words: This option sets the way how ticking 
within FXStrategy shall be handled at every iteration (i.e. customization) without overwriting FXAccount.tick() itself 
(which can and should be used within the new custom tick function) - the new custom tick function will be called after 
each iteration instead of the standard FXAccount.tick() (of each account contained in FXStrategy).  
  
The custom tick function must follow the function signature:  
````python
def tick_function(self, data_point, **kwargs):
    # some code...
    # perform custom ticking of accounts here
    # accounts are accessible via self.accounts
    # self is just a placeholder for the FXStrategy class object to which this ticking function is passed to
    # data_point is the data dict frame at time step t containing all trade pairs (e.g. currency pair, 
    #   stock-currency pair etc.) and their exchange rates (or price)
    # **kwargs are additional keyword arguments which are taken over from the FXStrategy constructor and are static
    #   (values do not change w.r.t. to the values passed to FXStrategy constructor and can not be overwritten from 
    #   inside tick_function())
    return
````  
_data\_point_ has the same structure as _price\_data\_point_ of the [_strategy decision function_](#defining-a-custom-strategy-decision-function) 
and _**kwargs_ is used in the same way. _self_ is just a placeholder to access internals of FXStrategy (see below).
_self_ and _data\_point_ are positional arguments (just placeholders, can be named whatever).  
__Only parameters after the first two positional arguments of the call signature of the custom tick function (_self_ and 
_data_point_, which can be named anyhow as they are only positional arguments...) will be handled as kwargs and will be 
listed in the [FXStrategy.tick_kwargs](api_reference.md#attribute-trapezastrategyfxstrategy) attribute.__  
  
___  
Function signatures of tick_function and strategy_function differ in the argument _self_!  
Consider using the method described in the [next section](#the-complex-way-suppressed-ticking) to customize ticking behavior, 
as this method here is rather unhandy...  
(_The difference in the function signatures is mainly for back compatibility reasons..._)  
  
__**kwargs of _strategy_function_ and _tick_function_ must be mutual exclusive (else name mangling).__
___  
  
As already mentioned, within the custom tick_function the user can decide how to tick the accounts. Nonetheless, accounts 
have to be ticked (if processing_durations shall be handled). Therefore, all accounts, which were passed to the FXStrategy 
constructor, are accessible via the FXStrategy.accounts attribute. This returns a protected List, which is ordered in the 
same way, as the accounts were passed to the constructor. This list is immutable. The FXStrategy.accounts attribute can 
be overwritten by supplying a complete list (see [_Account - Retrieving complete Depot_](account.md#retrieving-complete-depot)):
````python
strategy.accounts
# >>> [acc_0, acc_1]

# THIS WILL THROW AN EXCEPTION (AttributeError)
strategy.accounts[0] = acc_2

# this will work
strategy.accounts = [acc_2]

# this also works, as accessing the variable is possible as long as it does not change a single list element
strategy.accounts[0].tick()
````  
The FXStrategy.accounts attribute can be used to tick the accounts:  
````python
def tick_function(self, data_point, print_statement):
    # self is a positional argument and a placeholder for the FXStrategy class object to which tick_function is passed 
    #   to: can be named anyhow
    # data_point is a positional argument: can be named anyhow
    # print_statement is a keyword argument: name has to match exactly when passed at constructor
    # some nonsense code but makes clear how to use custom tick function and kwargs
    print(print_statement)
    
    # tick all accounts
    for acc in self.accounts:
        acc.tick()
    return

# taken from above example about strategy decision function to make clear how to use kwargs of strategy function and 
# kwargs of tick function
def strategy_function(accounts, price_data_point, reference_currency, volume_data_point, fxstrategy, some_constant):
    # some code to calculate transaction volume
    calculated_transaction_volume += some_constant
    # some code...
    return

strategy = FXStrategy('my_awesome_strategy', [acc_0, acc_1], strategy_function, 3, tick_func=tick_function,  
                      some_constant=100, print_statemment='tick-tack')
# custom tick function now will be automatically applied at each iteration of strategy execution
````  
Similar to the ticking logic in the previous section, account ticking can also be done in the strategy function. FXStrategy 
will automatically check, whether accounts have already been ticked inside the strategy decision function, and applies 
custom ticking function only when necessary (i.e. when account ticking has not been done within strategy decision function).  
  
__Note:__  
  - Ticking is only relevant, if transaction methods are used, which incur processing_duration. Else, ticking is not 
    relevant at all.  
  - Accounts can be patched by [Order Management](order_management.md). This changes the function signature from 
    FXAccount.tick() to FXAccount.tick(data_point). This has to be considered when defining a custom tick function for 
    FXStrategy by the user manually! The default ticking method handles this automatically.  
  - Ticking is only conducted inside FXStrategy if FXStrategy.suppress_ticking==False. This can be set in the constructor 
    of FXStrategy.  
  - kwargs passed to the FXStrategy constructor and used within the custom tick function are somehow 'static'. They will 
    be passed to the custom tick function with the same value as they were passed to the FXStrategy constructor. Overwriting 
    or manipulating those variables inside the custom tick function will not lead to any persistent changes within those 
    variables - instead they are passed with the values set at the FXStrategy constructor over and over again. This is similar 
    to the strategy kwargs, see [Defining a Custom Strategy Decision Function](#defining-a-custom-strategy-decision-function) 
    (one exception is to use the [FXStrategy.tick_kwargs](api_reference.md#attribute-trapezastrategyfxstrategy) attribute, 
    which returns a [ProtectedDict](account.md#retrieving-complete-depot) whose entries are immutable and can only be 
    manipulated or be overwritten by providing a complete dict).  
  
    
The default implementation of ticking in FXStrategy (i.e. when no custom ticking function is defined), looks like follows, 
which handles patched (by [Order Management](order_management.md)) and standard accounts:
````python
def _tick_method(self, data):   
    # note that we named it 'data' instead of 'data_point' as it is only a positional argument (and we do not use any
    # kwargs arguments here...)
    data_single_point = {k: data[k][-1] for k in data.keys()}
    for i in range(len(self.accounts)):
        if hasattr(self.accounts[i], '_is_order_manager_monkey_patch'): # used to identify order management patch
            # patched accounts
            self.accounts[i].tick(data_single_point)
        else:
            # standard account
            self.accounts[i].tick()
    return 1    # indicates successful execution
````  
(_The actual implementation differs from this one as it is implemented via Cython._)  
  
The possibility to implement custom ticking functions is especially handy, if the user has an own implementation of accounts. 
This implementation has to inherit from base class trapeza.accounts.BaseAccount in order to be fed to FXStrategy.  
### The complex Way: suppressed ticking
__This option is only available if FXStrategy.suppress_ticking==True (set at constructor).__  
  
This is a more convenient way of implementing custom and controllable ticking behavior than the method described in the 
section before.  
Ticking in FXStrategy can be suppressed by FXStrategy.suppress_ticking=True. This can be set at the constructor of FXStrategy. 
By doing so, FXStrategy will not attempt to call any of the above ticking options.  
  
By suppressing ticking in combination with setting the lookback to 'max length of time series - 1', we can force FXStrategy 
to execute exactly one iteration only. We then have to implement an own for-loop within the strategy decision function:  
````python
def strategy_function(accounts, price_data_point, reference_currency, volume_data_point, fxstrategy, some_constant):
    # price_data_point is essentially the entire market data if lookback is set to the above described size
    
    # get the length of the time series market data
    time_series_length = len(price_data_point[list(price_data_point.keys())[0]])

    # we have to implement a custom looping logic
    # loop through each time step in the market data, pay attention to lookback regarding where to start looping index
    for i in range(fxstrategy.lookback, time_series_length, 1):
        # current time step data
        USDEUR_rate_i = price_data_point['USD', 'EUR'][i]

        # some custom code...
        accounts[0].buy(10 + some_constant, USDEUR_rate_i, 'USD', 'EUR')
        # some more custom code...
        
        # tick all accounts, ticking has to be done before evaluation
        for acc in accounts:
            acc.tick()  # use an appropriate ticking method
            
        # evaluate by hand
        # data frame at time step i: {k: price_data_point[k][i] for k in price_data_point.keys()}
        fxstrategy.evaluate({k: price_data_point[k][i] for k in price_data_point.keys()}, reference_currency, True)

market_data = {('USD', 'EUR'): [0.82, 0.9, 0.65, 0.78, 0.81, 0.82]}

strategy = FXStrategy('my_awesome_strategy', [acc_0, acc_1], strategy_function,
                      lookback=len(market_data)-1, suppress_ticking=True, suppress_evaluation=True,
                      some_constant=100)
# strategy_func will now only be invoked once 
# note how lookback is set
# suppress_ticking avoids that accounts being ticked after strategy_func has executed (which would be 
#   double-ticking too much)
# note how kwargs of strategy_function ('some_constant') is used
````  
When using a custom for-loop structure, we have to evaluate our accounts by hand. This is done via _FXStrategy.evaluate()_. 
Make sure to set the _append_to_results_ argument to True, otherwise accounts will only be evaluated, but the results won't 
be appended to the FXStrategy's results. Be cautious when using _FXStrategy.evaluate()_ to just only evaluate without appending 
(_appending_to_results=False_), as every call to _FXStrategy.evaluate()_ deletes all previously added signals from the 
intermediate buffer, which then are lost and cannot be added to FXStrategy's results when calling 
_FXStrategy.evaluate(..., append_to_results=True) (cleaning the intermediate signal buffer is done at every call to 
FXStrategy.evaluate() independently of the _append_to_results_ argument).  
If only the terminal values of the accounts matter, then just leave out the call to _FXStrategy.evaluate()_. This is really 
just to get positions, signals and total balances at each time step and to append those to FXStrategy's [result attributes](#retrieving-results).  
_FXStrategy.evaluate()_ with _append_to_results=False_ can be useful when one is interested in the current status of the 
strategy regarding the positions and the total_balances. When implementing a custom loop, then it is generally the best 
to set _append_to_results=True_.  
  
__Ticking and adding signals have to be done before evaluation! Ticking is not included in evaluate!__  
(Remember: evaluate() clears the intermediate buffer such that current signals get cleared. If _append_to_results=False_, 
those signals are lost and not retrievable anymore)
  
See [Smooth Moving Average Crossing - Example](examples.md#the-strategy---mac) for an example where custom for-loops come 
in very handy and where _FXStrategy.evaluate()_ is used. See [API Reference - FXStrategy.evaluate()](api_reference.md#evaluate) 
for further information.  
___  
By default, FXStrategy will use a ticking method tailored to FXAccount.  
Order management patch is handled automatically.   
Auto-detection and ticking is turned on by default (_suppress_ticking=False_).  
  
Positional arguments in strategy decision function and custom tick function can be named whatever the user wants. 
Additional arguments are supplied as keyword arguments at function signature definition. Those keywords must be 
plugged into the FXStrategy constructor with the exact names of those keyword arguments.  
Keyword arguments must be mutual exclusive between strategy decision function and custom tick function (name mangling). 
This does not apply to positional arguments, as they are fed in via position and not via keyword when FXStrategy internally 
calls strategy decision function and custom tick function.  
___
When the user does not implement a custom loop but sticks to the standard way of defining a strategy decision function, 
then the whole evaluation scheme will be handled automatically and _FXStrategy.evaluate()_ does not need to be called 
explicitly by the user.  
  
__New since 0.0.6__:  
Since _trapeza 0.0.6_ the keyword argument _'suppress_evaluation'_ is available in the _FXStrategy_ constructor. When
set to True, FXStrategy.evaluate() will not be called automatically at every time step (iteration) during FXStrategy.run(). 
This comes especially handy when implementing custom strategy decision loops as described above. FXStrategy.evaluate() 
needs a dictionary of price data. If this price data is built dynamically within the strategy decision function and is 
not known upfront, then FXStrategy.evaluate() throws an exception (FXStrategy.evaluate() is called exactly once in the 
above described example - this is because we set lookback=len(data)-1 such that we force FXStrategy.run() to exactly one 
iteration). By passing _suppress_evaluation=True_, FXStrategy.evaluate() will not be automatically called, which enables 
e.g. the dynamic build of price data within the strategy decision function. When writing custom strategy decision loops
as in above example, the user needs to evaluate by hand anyways, such that the suppressed evaluation does not bring any 
additional disadvantages.  
  
#### Adding Signals
Inside the strategy decision function, _FXStrategy.add_signal_ can be used to add signals as descriptive strings. 
Signals are then stored in FXStrategy.signals.  
  
Adding signals can be useful to gather insights, which transactions happened at which time step during the execution 
of the strategy:  
````python
def strategy_function(accounts, price_data_point, reference_currency, volume_data_point, fxstrategy):
    # some code and transactions
    fxstrategy.add_signal(accounts[1], 'Account 1 reacted to condition XY')
    fxstrategy.add_signal(accounts[0], 'Account 0 did funny things.')
````  
Function signature of _add_signal_:  
````
add_signal(account, signal)

account:    account object accessible via 'accounts[i]' from inside the strategy decision function (order is the same 
            as passed to FXStrategy constructor)
signal:     string
````  
Signals will be stored at each loop iteration of FXStrategy's strategy execution. Multiple signals per account can be 
added at each iteration.  
See [Retrieving Results](#retrieving-results) on how to analyze signals.  
If distinct keywords are used and if strings are mutual exclusive, then those signal strings can also be used for 
visualization by FXEngine (see [Visualization](visualization.md), e.g. using symbols within the plots).
## Input Accounts
The FXStrategy constructor either takes a single account object or a list of account objects. If a list is passed, the 
order of accounts will be preserved throughout the entire framework of _FXStrategy_, i.e. when accessing in strategy 
decision function or in custom tick function, via FXStrategy.accounts attribute, or when retrieving results.  

__If depots of account shall be altered (e.g. depositing cash positions up-front before running a strategy), then this 
has to be done before passing them into the FXStrategy constructor. Otherwise, transactions on account won't take effect 
when already passed to FXStrategy (at least not inside FXStrategy).__    
  
After running a strategy via _FXStrategy.run(...)_, accounts will _not_ be reset automatically. Accounts will preserve 
the depot status of the last iteration of executing the iteration loops by FXStrategy. This means, accounts will not have 
the same depot positions compared to the ones before passing accounts to FXStrategy. Initial depot stati are stored in 
FXStrategy._init_acc_depots, which returns a protected list of depots (in the same order as the list of  accounts passed 
to FXStrategy; protected list is immutable, pass complete list if to overwrite attribute).  
  
Custom implementation of accounts (instead of FXAccount) can be passed to FXStrategy, if the implementation inherits from 
_trapeza.account.BaseAccount.
## Retrieving Results
Results can be retrieved after _FXStrategy.run(...)_:  
  1. FXStrategy.run(...) returns \[reference_currency, signals, positions, merged_positions, total_balances, merged_total_balances]  
  2. After calling FXStrategy.run, results will be stored in the attributes _FXStrategy.reference_currency, FXStrategy.signals, 
     FXStrategy.positions, FXStrategy.merged_positions, FXStrategy.total_balances, FXStrategy.merged_total_balances_ (recommended).  
     
#### __reference_currency:__  
````  
string, reference currency set at FXStrategy currency and used to calculate total_balances
````  
  
#### __signals:__  
````  
list of lists,
returns all signals per account and per time step, which are generated by strategy decision function

  | signals of account 0                | signals of account 1               |...
  V                                     V                                    V
[[[acc_0_sig_t_0, acc_0_sig_t_0, ...], [acc_1_sig_t_0, acc_1_sig_t_0, ...], [...]],   <-- time step 0
 [[acc_0_sig_t_1, acc_0_sig_t_1, ...], [acc_1_sig_t_1, acc_1_sig_t_1, ...], [...]],   <-- time step 1
...]

access data via: time_step, acc, signal_nr

e.g. accessing all signals at time step 4 emmitted by account 1:    signals[4][1]
     accessing third signal at time step 4 emmitted by account 1:   signals[4][1][3]
````  
  
#### __positions:__  
````
dict: currency (str) as key, list of lists as values of position of account n at time step t
returns all positions (currencies) and their value (size/ amount/ volume) per account and per time step

             | account 0  | account 1  ...  | account n
             V            V                 V
{currency: [[acc_0_val_0, acc_1_val_0, ..., acc_n_val_0],    <-- time step 0
            [acc_0_val_1, acc_1_val_1, ..., acc_n_val_1],    <-- time step 1
             ... ... ...
            [acc_0_val_t, acc_1_val_t, ..., acc_n_val_t]],   <-- time step n
 currency: ...}

access data via: position, time_step, acc_nr

e.g. accessing 'EUR'-position at time step 4 of account 1:  positions['EUR'][4][1]
````  
(see [account - depot and positions](account.md#retrieving-complete-depot))  
  
#### __merged_positions:__  
````
dict: currency (str) as key, list as values per time step
sums up all similar positions across all accounts to one composite position (currency) per time step

{currency: [val_0, ..., val_t],
currency: [val_0, ..., val_t]}

access data via: position, time_step

e.g. accesing composite 'EUR'-position summed up over all accounts at time step 4:  merged_positions['EUR'][4]
````  
  
#### __total_balances:__  
````
list of lists,
returns total account value (over all positions) in specified reference currency per each account and time step

  | account 0        | account 1        | ...
  V                  V                  V
[[balance_acc_0_t_0, balance_acc_1_t_0, ...],    <-- time step 0
 [balance_acc_0_t_1, balance_acc_1_t_1, ...],    <-- time step 1
...]

access data via: time_step, acc_nr

e.g. total_balance at time step 4 of account 1:     total_balances[4][1]
````  
(see [account - total balance](account.md#evaluating-the-total-account-balance))  
  
#### __merged_total_balances:__  
````
list,
sums up total balances of all accounts per each time step; total balances are all set to same reference currency; 
resembles total value of strategy with respect to all used accounts per time step

 |time step 0 |time step 1 |...
 V            V            V
[balance_t_0, balance_t_1, ....]

access data via: time_step

e.g. total strategy value at time step 4:   merged_total_balances[4]
````  
(_probably the most useful_)
  
## Type Checking  
Type Checking can be turned off in the same way as done on accounts (see [_account - type checking_](account.md#type-checking)):  
````python
from trapeza.strategy import FXStrategy

# default is ignore_type_checking=False (type checking being turned on)
strategy = FXStrategy('my_awesome_strategy', [acc_0, acc_1], strategy_function, 5, ignore_type_checking=True)
````  
  
  
---  
[Back to Top](#strategy)
