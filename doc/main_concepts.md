Go back: [Table of Content](table_of_content.md)
- [Main Concepts](#main-concepts)
  - [Introduction](#introduction)
  - [Classes](#classes)
  - [Base Classes](#base-classes)
  - [Processing Times and Fees](#processing-times-and-fees)
  - [Coverage Checks](#coverage-checks)
  - [Order Management](#order-management)
  - [Protected Attributes](#protected-attributes)
  - [Visualization](#visualization)
  - [Metrics](#metrics)
  - [Arithmetics](#arithmetics)
     
  
---------
# Main Concepts  
Main concepts within the framework of _trapeza_ are capsuled within separate classes.
________________________________  
## Introduction
_trapeza_'s main goal is to provide a software package which is capable of simulating real-life financial situations. 
Therefore, _trapeza_ focuses on customization and flexibility for accurate modelling instead of computational performance, 
nor simplicity.  
  
_trapeza_ can accurately model a diverse set of e.g. fees and processing durations of transactions (e.g. due to time 
taken for messaging via networks, etc.) and is capable of checking whether account coverage (i.e. debiting) occurs in the 
course of simulation.  
## Classes
_trapeza_ has the following main classes:  
  - [__FXAccount__](account.md): Class to model a bank account/ trading depot. Provides fee structure and processing durations modelling. 
    Performs basic transactions.  
  - [__FXStrategy__](strategy.md): Class to model trading strategies. Takes market data (price and volume), uses a custom-defined decision 
    function and uses (multiple) accounts to perform transactions like sell, buy, deposit and withdraw, which are subject 
    to fee structure and processing durations models of the very account. Performs classic backtesting.  
  - [__FXEngine__](engine.md): Class to perform Monte-Carlo-alike statistical analysis. Takes random sub-samples of market data and
    runs FXStrategy over each sub-sample to gather statistical insights e.g. distribution of metrics (instead of metrics only).
    
## Base Classes
Each of _trapeza_'s classes inherits from respective base classes (e.g. BaseAccount, BaseStrategy, etc.). Pre-implemented 
classes (FXAccount, FXStrategy, etc.) can easily be swapped for and be used in conjunction with custom implementations, 
which have to inherit from those base classes.  
## Processing Times and Fees
_FXAccount_ is capable of modelling [processing durations](account.md#processing-durations) and [fees](account.md#transaction-fees) 
for each of its transactions. _FXAccount_ then takes care of this automatically without any additional need for intervention.
## Coverage Checks
_FXAccount_ performs various [coverage checks](account.md#coverage) which ensure, that the account's depot positions do 
not drop into negative range. Those coverage checks can be turned off.  
## Order Management
The [_order_management_](order_management.md) module can patch additional, more complex order types e.g. stop loss orders 
onto single account objects (patches only objects, not whole classes). Handling and triggering those orders is handled 
automatically.  
## Protected Attributes
Most of _FXAccount_ and _FXStrategy_ is implemented in C/ C++ via Cython. Therefore, a couple of attributes are protected 
via custom data types _ProtectedDict_ and _ProtectedList_. Single elements of those data types are immutable (can not be 
manipulated), because manipulating those attributes would not change the underlying values of the C/ C++-implementations. 
Attributes can be overwritten by providing a complete Python list or dict. Attributes can not be changed in single elements 
of the ProtectedList or ProtectedDictionary directly. This only applies to a couple of attributes, see the documentation 
for further details.  
## Visualization
_trapeza_'s main goal is not to provide a superior visualization. There are a plethora of good visualization packages 
out there. Nonetheless. _trapeza_'s [_FXDashboard_](visualization.md#fxdashboard) class provides a very basic visualization 
and is especially designed to be used with _FXEngine_ (visualization of the Monte-Carlos-alike analysis). _FXDashboard_ 
is implemented via Python's _Dash_ package and can be viewed via the browser.  
## Metrics
_trapeza_ is not specialised on finance mathematics or on the most recent finance research. Nonetheless, _trapeza_ provides 
some [pre-implemented metrics](metric.md). Most metrics make assumptions such as normal distribution. _trapeza_'s 
metrics are customizable in a way, that the user can provide different assumptions in order to make metrics more adequate.  
  
Metrics can be plugged into _FXEngine_ in order to be used during the Monte-Carlo-alike analysis. Additionally, custom 
metrics can be used.  
  
__Disclaimer__:  
_trapeza is written for scientific, educational and research purpose only. The author of this software and accompanying 
materials makes no representation or warranties with respect to the accuracy, applicability, fitness, or completeness of
the contents. Therefore, if you wish to apply this software or ideas contained in this software, you are taking full 
responsibility for your action. All opinions stated in this software should not be taken as financial advice. Neither 
as an investment recommendation. This software is provided "as is", use on your own risk and responsibility._   
## Arithmetics
_trapeza_ provides [different methods for performing arithmetics](arithmetics.md). This is due to the floating point error:  
````python
print(1.1 * 3)
# >>> 3.3000000000000003
````  
This phenomenon occurs in every programming language, not only in Python, and is caused by the way how the computer 
represents numbers (binary representation). To handle this and to avoid error accumulation, _trapeza_ uses external 
software packages, which are specialized on decimal math and avoid the above phenomenon.  
If computational speed is more of a concern than accuracy, then _trapeza_ still can use floating point arithmetics instead 
of decimal arithmetics.  
  
  
---  
[Back to Top](#main-concepts)