Go back: [Table of Content](table_of_content.md)  
- [Engine](#engine)
  - [Introduction and Purpose](#introduction-and-purpose)
  - [How Sub-Sampling works](#how-sub-sampling-works)
  - [Input Data Format](#input-data-format)
  - [Instantiate an Engine Object](#instantiate-an-engine-object)
  - [Temporary Directory](#temporary-directory)
  - [Run the Engine](#run-the-engine)
  - [Analyze the Engine](#analyze-the-engine)
  - [Retrieving Results](#retrieving-results)
    - [Results from Runs](#results-from-runs)
    - [Results regarding Metrics](#results-regarding-metrics)
    - [Special Case - 'runthrough'](#special-case---runthrough)
  - [Save and Load the Engine](#save-and-load-the-engine)
  - [Register New Strategies](#register-new-strategies)
  - [Close the Engine](#close-the-engine)
  - [Type Checking](#type-checking)
  - [Disclaimer regarding Metrics](#disclaimer-regarding-metrics)  
    
  
---------  
# Engine
Whereas _FXStrategy_ performs classical backtesting for retrodiction by running an entire time-series of market data, 
_FXEngine_ emulates Monte-Carlo-Analysis and performs multiple strategy runs over randomly sub-sampled market data in 
order to gain statistical insights.  
________________________________  
## Introduction and Purpose
Within in the framework of _trapeza_, _FXStrategy_ performs backtesting in the classical and somehow statical sense, 
thereby takes a time-series of historic market data and evaluates the user-defined strategy on every time step to calculate 
a retrodiction.   
Many strategies incur a path-dependency (strategy decision is dependent on what happened n time steps before or in which 
state they have started). To better analyze them, _FXEngine_ emulates a Monte-Carlo-Analysis by performing multiple 
strategy runs with randomly sub-sampled market data. Thereby, different start points, path and market regimes can be 
emulated (given that enough market data with different market regimes is given) in order to analyze the strategy in a broader 
way. By doing so, statistical distributions can be gathered e.g. profitability, volatility, etc. The less variance occurs 
in those metrics, the more stable a strategy is against different market situations. Metrics then can not only be benchmarked 
against absolute values, but also their distribution can be inspected, which might give deeper insights.
_FXEngine_ does this by slicing the provided market data into sub-samples with different window lengths and different start 
indices. Within a sub-sample, the time order of the original market data is preserved (no random shuffling within a sub-sample). 
This methodology is inspired by Monte-Carlo-Analysis (even tough, to be precise this approach is not a 1:1 Monte-Carlo-Analysis!). 
Random sub-sampling also randomizes start states and paths for strategies such that they can be tested on a wider variety 
of market domains. Strategies can also be analyzed e.g. for time-being-invested etc.  
## How Sub-Sampling works
_FXEngine_ slices the supplied market data in to sub-samples of different window sizes and start indices. Price and volume 
data can be used. Subsequently, _FXEngine_ performs a run with _FXStrategy_ over each of the sliced sub-samples and stores 
the results. _FXEngine_ also calculates metrics on each run. Those metrics can be user-defined. _FXEngine_ also provides 
set of pre-implemented standard metrics.  
_FXEngine_ can take in multiple strategies at once. Sub-samples will be build for each strategy independently (which is 
necessary if e.g. strategies have different lookbacks). Minimal and maximal window sizes as well as maximal number of runs 
will apply to all strategies. Passing multiple strategies to _FXEngine_ is especially handy when used in conjunction with 
[_FXDashboard_](visualization.md) as all strategies will be visualized in the same dashboard.  
The minimal and maximal window size of the sub-samples can be defined by the user. Additionally, the user can enforce a 
complete run over the entire data set, independent of any window specifications.  
  
Each run of _FXEngine.run()_ is specified by its window size and its start index. The data is sliced according to:  
  
__\[start_index: start_index + window_size + lookback]__,  
  
where lookback is the [lookback size of the strategy](strategy.md#lookback).  
The start index marks the start of the data slice. Therefore, strategy execution starts at 'start_index + lookback'.  
Window size describes the number of time steps to be simulated. If e.g. the window size is 15, then the strategy will be 
run for 15 time steps. In order to supply enough data to ensure that 15 time steps can be simulated, we have to add 
the number of lookback size of time steps to the data slice. In short: start index is inclusive regarding the lookback 
size (strategy execution at start index + lookback), whereas window size is exclusive regarding lookback (sub-sample 
size is window size + lookback).  
If a complete run-trough is enforced, then the start index of this sub-sample will be named 'runthrough'.  

[<img src="media/random_sub_sample.png" alt="dashboard" width="800"/>](media/random_sub_sample.png)
## Input Data Format
The input data format is the [same as for FXStrategy](strategy.md#input-data-format):  
A Python dictionary with string tuples as dict keys (naming the tradable/ currency pair) and lists (time series of 
prices/ exchange rates) as dict values. All lists must have the same length and have to manually factor in the [lookback size](strategy.md#lookback).   
## Instantiate an Engine Object  
Every FXEngine takes in a unique name and a list of FXStrategy objects (or strategy objects derived from trapeza.strategy.base_strategy.BaseStrategy).
All strategies must have unique names (FXStrategy.name attribute) which also have to be mutually exclusive in their substrings 
(name of one strategy must not be a substring of any other strategy).
````
def __init__(name_id, strategies, cache_dir='__system__', loc_decision_functions=False, clean_cache_artifacts=False, 
             ignore_type_checking=False, n_jobs=-1)

name_id:............... str, unique name for the engine object
strategies:............ list or object, list of strategy objects or a single strategy object (must inherit from base 
                        class BaseStrategy)
cache_dir:............. str, path to directory in which a new temporary directory will be created.
                        This temporary dircetory is used to cache results from multi-processin and gets cleaned up and 
                        deleted afterwards.
                        cache_dir can be specified as:
                        '__system__': directs to platform specific default directory of Python's 'tempfile' (Python 
                                      package), under windows: cache_dir='C:/Users/[username]/AppData/Local/Temp'.
                                      Creates a new temp directory named '[cache_dir]/trapeza/FXEngine/[name]_[rand]/' 
                                      with [name] being the name of the engine and [rand] a random string sequence.
                                      Full path under windows:  
                                      'C:/Users/[username]/AppData/Local/Temp/trapeza/FXEngine/[name]_[rand]/'
                        '__cache__': directs to '[trapeza_path]/__cache__/' with [trapeza_path] being the path where
                                     pip installs trapeza (usually under 'site-packages') and creates a new temporary 
                                     directory named '/[name]_[rand]/' with [name] being the name of the engine and 
                                     [rand] a random string sequence.
                                     Full path: '[trapeza_path]/__cache__/[name]_[rand]/'
                        or any user-specific path: under which '.../[name]_[rand]' will be created. Can be either full 
                                                   absolute path or relative path w.r.t. current workin directory
                                                   Full path: '[my_path]/[name]_[rand]'
                        default: '__system__'
loc_decision_functions: bool, 
                        if True: line of codes (source code) of strategy decision function of each strategy will be 
                                 retrieved and written to temp file for later re-use (e.g. displaying).
                        if False: engine does not try to read source code of strategy decision functions.
                        default: False
                        -> Be careful using this option as it exposes code to temporary files which might be readable
                           for third parties!
clean_cache_artifacts:  bool,
                        if True: cleans up all residual subdirectories and files under cache_dir path 
                        if False: engine does not attempt to clean up anyting
                        default: False
                        -> If the engine's process gets killed unexpectedly (e.g. by exception), caches will not be 
                           cleaned up and temp directory with temp files will remain. This might lead to cluttering.
                           Therefore clean_cache_artificats can clean up all left over temp directories.
                        -> Be careful not to uninentionally delete any files from other programs! This is especially the 
                           case when cache_dir is not uniquely used for FXEngine!
ignore_type_checking:.. bool,
                        if True: type checking turned off
                        if False: type checking active
                        default: False (type checking active)
                        For further details see documentation regarding accounts.
                        This type checking is independent of type checking at FXAccount and FXStrategy.
n_jobs:................ int,
                        number of cores to use for multi-processing
                        -1: all available cores
                        0: no multi-processing, no extra process will be spwaned
                        default: -1
````  
Use:  
````python
from trapeza.account import FXAccount
from trapeza.strategy import FXStrategy
from trapeza.engine import FXEngine

# init accounts
acc_0 = FXAccount('USD')
acc_1 = FXAccount('EUR')

# init strategies
strat_0 = FXStrategy('strategy_0', acc_0, my_awesome_strategy_0, 2)
strat_1 = FXStrategy('strategy_1', acc_1, my_awesome_strategy_1, 8)

# init engine
engine = FXEngine('engine_0', [strat_0, strat_1], cache_dir='__system__', n_jobs=4)     # use for cores
````  
## Temporary Directory
FXEngine uses a temporary directory to dump multi-processing results in temporary files. Therefore, FXEngine creates a 
new temporary directory. The path under which this new temporary directory is created, can be specified by the argument 
_cache_dir_ during instantiation of FXEngine: 

  - '\_\_systems__': uses the platform specific path of the Python package 'tempfile' and creates a subdirectory named 
                   'trapeza/FXEngine/\[name]\_\[rand]/'.   
                   Under windows: 'C:/Users/\[username]/AppData/Local/Temp/trapeza/FXEngine/\[name]_\[rand]/'.  
  - '\_\_cache__': directs to '\[trapeza_path]/\_\_cache__/' with \[trapeza_path] being the path where pip installs trapeza 
                 (normally under 'site-packages') and creates a new subdirectory '/\[name]\_\[rand]/'.   
                 Full path: '\[trapeza_path]/\_\_cache__/\[name]_\[rand]/'.  
  - user-specified: creates subdirectory as '\[my_path]/\[name]_\[rand]/'  
  
with [name] being the engine's name and [rand] being a random string sequence (given by Python's 'tempfile' package).  
  
  
The temporary directory contains the following files:
  - _decision-function-loc\_\[rand]_  
      &nbsp;&nbsp;&nbsp;&nbsp; dict: decision_fnc_loc\[strategy.name] = inspect.getsource(strategy.strategy_func)  
  - _\[strategy_name]\_\[window_size]\_\[start_index]\_run\_\[rand]_  
      &nbsp;&nbsp;&nbsp;&nbsp; dict: {'signals': strategy.signals, 'positions': strategy.positions, 
             'merged_positions': strategy.merged_positions, 'total_balances': strategy.total_balances, 
             'merged_total_balances': strategy.merged_total_balances}  
  - _\[strategy_name]\_\[window_size]\_runthrough\_run\_\[rand]_  
      &nbsp;&nbsp;&nbsp;&nbsp; (window_size: len(data)-strategy.lookback)  
      &nbsp;&nbsp;&nbsp;&nbsp; same as \[strategy_name]\_\[window_size]\_\[start_index]\_run_\[rand], but this file is created if runthrough
      is enforced at FXEngine.run(),  
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; where start_index is replaced by the string 'runthrough'  
      &nbsp;&nbsp;&nbsp;&nbsp; dict: {'signals': strategy.signals, 'positions': strategy.positions, 'merged_positions': strategy.merged_positions,  
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 'total_balances': strategy.total_balances, 
      'merged_total_balances': strategy.merged_total_balances}  
  - _runs-total-balances-start-end\_\[rand]_  
      &nbsp;&nbsp;&nbsp;&nbsp; dict: run_balances\[(strategy.name, len_data-strategy.lookback, 'runthrough')] = \[start_balance, end_balance] (for runthrough)  
      &nbsp;&nbsp;&nbsp;&nbsp; dict: run_balances\[(strategy.name, window_size, start_index)] = \[start_balance, end_balance] (for normal runs)  
      
with [rand] being a random string sequence (given by Python's 'tempfile' package).  
  
  
FXEngine cleans and deletes the temporary directory used for caching automatically afterwards.  
If FXEngine's process(es) gets killed unexpectedly (e.g. by an exception), then the temporary director does _not_ get 
cleaned up. This may lead to cluttering. Therefore, the argument _clean_cache_artifacts_ can be set to True (default: False) 
in order to clean all residuals of previous FXEngine runs.  
  
___  
__Use with care! This deletes all files and subdirectory specified under _cache_dir___ _(if =='\_\_system\_\_': all files and sub-directories 
under \[platform_temp]/trapeza/FXEngine/, if =='\_\_cache\_\_': all files and sub-directories under \[trapeza_path]/\_\_cache\_\_/, 
if user-specified: all files and sub-directories under the user-specified path)___.  
This might also delete temp files of third party programs (especially when user-specified path is not exclusively used 
for trapeza)!__  
___  
  
__Check the _cache_dir_ from time to time in order to avoid cluttering up your disk space!__
___  
  
## Run the Engine  
After instantiating an engine object with strategies, the engine can run on supplied market data.  
````
def run(price_data, reference_currency, volume_data=None, 
        min_run_length=None, max_run_length=None, max_total_runs=None, run_through=True)

price_data:........ dict, market prices, see section 'Input Data Format'
                    (if (currency_1, currency_2) and its inverse pair (currency_2, currenc_1) are contained in 
                    price_data, then check is applied, whether their dict values match up the inverse relation - this
                    is done in the underlying FXStrategy object).
                    Same data format as provided to FXStrategy.run()
reference_currency: str, reference currency to calculate total_balance of accounts and strategies
volume_data:....... dict or None, market volumes, see section 'Input Data Format'
                    (if (currency_1, currency_2) and its inverse pair (currency_2, currency_1) are contained in 
                    volume_data, then no (!) check is applied, whether their dict values match up the inverse relation)
                    If None, then None will be passed to underlying strategies.
                    Same data format as param:price_data.
                    default: None
min_run_length:.... None or int,
                    Minimal window size (minimal sub-sample length: min_run_length + lookback)
                    If None, min_run_length = 1.
                    default: None
max_run_length:.... None or int > min_run_length,
                    Maximum window size (maximal sub-sample length: max_run_length + lookback)
                    If max_run_length > len(price_data[currency_1, currency_2])-lookback, then max_run_length is clipped 
                    to meet the condition.
                    If None: set to len(price_data[currency_1, currency_2])-lookback (maximal possible window length)
                    default: None
max_total_runs:.... None, int or 'all_once',
                    Maximum number of runs per each strategy (!) contained in FXEnine.
                    If None: number of runs unconstrained, runs all possible sub-samples which can be drawn given 
                             minimal and maximal window sizes (runs all possible start indices for each window size) per
                             each strategy (!) in FXEngine.
                    If int: number of runs (per each strategy (!) in FXEngine) is close but never greater than 
                            max_total_runs, some window lengths might be omitted (chooses randomly, which sub-samples 
                            to run).
                    If 'all_once: Each possible window size (defined by min_run_length and max_run_length) will be run 
                                  exactly once (per each strategy (!) in FXEngine).
                    default: None
run_through:....... bool,
                    if True: performs a run over the total data (time steps), independent from other settings
                    if False: does not perform a run over total data but only above specified settings
                    default: True
                    does not affect above settings and is just an addtional run (per each strategy (!) in FXEngine)
                    
return: dict,
        dict keys: tuple (strategy.name, window_size, start_index), 
                   see sections above regarding naming convention of window_size and start_index (which factors in 
                   lookback sizes)
        dict values: [start_balance, end_balance],
                     start_balance: total_balance of the strategy at start, in reference_currency
                     end_balance: total_balance of the strategy after run, in reference_currency
````  
Use:  
````python
# define data
price_data = {('USD', 'EUR'): [0.82, 0.82, 0.81, 0.85, 0.79, 0.83, 0.87, 0.78, 0.82]}
volume_data = {('USD', 'EUR'): [182, 282, 381, 285, 179, 283, 387, 278, 182]}

# run engine
run_results = engine.run(price_data, 'USD', volume_data)
````  
  
FXEngines drops the results of each run into the [temporary directory](#temporary-directory) and returns a synopsis in 
the form of a Python dictionary, which dict keys are formed by tuples (strategy.name, window_size, start_index) and which 
dict values contain the total value of the strategy at the start and at the end of each run in the given reference 
currency. Please see [How Sub-Sampling works](#how-sub-sampling-works) for the naming convention of 'window_size' and 'start_index'. 
Please see [Retrieve Results](#retrieving-results) on how to get detailed results.  
  
After call _FXEngine.run(...)_, all accounts and strategies will be reset to the exact same state as before (regarding 
e.g. strategy.positions, strategy.total_balances, account.depot, account.clock, etc.).  
## Analyze the Engine
After calling [_FXEngine.run()_](#run-the-engine), the results can be analyzed w.r.t. metrics.  
````
def analyze(metric_dict=None)

metric_dict: None or dict,
             dict: {metric_name: metric_fnc},
                   metric_name: str
                   metric_fnc: function object,
                               metric_fnc must follow the function signature:
                               float = metric_fnc(list_of_floats)
             None: only pre-implemented metrics will be analyzed - not annnualized:
                   {'total_rate_of_return': # total rate of return in percent, not annualized
                    'volatility': # volatility based on log returns and normal distribution assumption, not annualized
                    'downside_risk': # downside risk, risk-free: 3%, normal distribution assumption, assumes daily 
                                       frequency of data
                    'value_at_risk': # value at risk at 95% confidence based on log returns and normal distribution 
                                       assumptions, 'historic simulation' used as computation method
                    'expected_rate_of_return': # expected rate of excess return per time step, risk-free: 3%, based on 
                                                 log return and normal distribution assumption, assumes daily frequency 
                                                 of data
                    'sharpe_ratio': # sharpe ratio, risk-free: 3%, based on percentage return and normal distribution 
                                      assumption, not annualized
                    'sortino_ratio': # sortino ratio, risk-free: 3%, based on percentage return and normal distribution 
                                       assumption, not annualized
                    'max_drawdown': # max drawdown
                   }
             default: None

returns: None
````  
  
Use:  
````python
engine.analyze()
````
  
FXEngine evaluates the metrics based on strategy.[merged_total_balances](strategy.md#retrieving-results) for each run 
over the randomized sub-samples. Results are stored in _FXEngine.standard_analysis_results_ (for the pre-implemented metrics) 
and _FXEngine.analysis_results_ (for user-defined metrics) attributes as Python dictionary. The keys of this dict are 
the name strings of the underlying strategies, the dict values are dicts with tuple keys (window_size, start_index) and 
the metric's value as dict value, see [Retrieve Results](#retrieving-results) for further details.    
  
___FXEngine.standard_analysis_results_ are annualized to yearly and assume that data is in daily frequency (252 days per year)!  
The pre-implemented metrics in _FXEngine.analysis_results_ (i.e. _metric_dict=None_ argument in _FXEngine.analyze()_ which 
is the case if no metrics passed to _FXEngine.analyze()_) are NOT annualized (except: downside_risk and expected_rate_of_return 
are calculated against 0.0001172968 which is the log summing up to 3% p.a.)!__  
  
FXEngine can also analyze custom metrics. Those metrics must take a list of floats (strategy.merged_total_balances, see 
above) as inputs and must return a single float. If custom metric functions has more than the list input, then those 
inputs must be defined as keyword arguments!  
  
__FXEngine.standard_analysis_results__:  
````
standard_metric_dict = {'total_rate_of_return': _std_tot_rate_of_ret, 'volatility': _std_annual_vola,
                         'expected_rate_of_return': _std_annual_exp_rate_of_ret,
                         'expected_rate_of_log_return': _std_exp_rate_of_log_ret,
                         'sharpe_ratio': _std_annual_sharpe, 'sortino_ratio': _std_annual_sortino,
                         'value_at_risk': _std_var, 'max_drawdown': _std_max_drawdown}
````  
All metric results are annualized (!) and assume daily frequency of data.  
  
__pre-implemented FXEngine.analysis_results__ (i.e. no metrics supplied to FXEngine.analyze()):  
````
metric_dict = {'total_rate_of_return': metric.total_rate_of_return, 'volatility': metric.volatility,
               'downside_risk': _downside_risk, 'value_at_risk': metric.value_at_risk,
               'expected_rate_of_return': _expected_rate_of_return, 'sharpe_ratio': metric.sharpe_ratio,
               'sortino_ratio': metric.sortino_ratio, 'max_drawdown': _std_max_drawdown}
````
Metric results are NOT (!) annualized (except: downside_risk and expected_rate_of_return are calculated against 0.0001172968 
which is the log summing up to 3% p.a.). Those metrics are NOT the same as used in FXEngine.standard_analysis_results (see 
above). Pre-implemented metrics as described by _metric_dict_ are only used, if user does not supply any metrics i.e. 
_metric_dict=None_ in FXEngine.analyze().  
  
__Note__: VaR in _trapeza_ has a negative sign! Often, only the absolute value is used. Pay attention to those sign conventions!  
  

-----------------  
FXEngine.analyze() may take up some serious computation time (less performant and quite slow), consider just using 
FXEngine.run() (which is quite fast) and implement custom routines for analyzing metrics instead of FXEngine.analyze().
-----------------  
## Retrieving Results
Detailed results from _FXEngine.run()_ can be retrieved via _FXEngine.load_results()_, results from _FXEngine.analyze()_ 
can be retrieved via the attributes _FXEngine.standard_analysis_results_ and _FXEngine.analysis_results_.  
  
_FXEngine.run_register_ gives an overview which sub-samples have been drawn by the engine object:
````
dict of dicts,
{strategy_name_0: {window_size_0: [start_index_0, start_index_1, ...],
                   window_size_1: [start_index_0, start_index_1, ...],
                   ...
                  },
 strategy_name_1: {window_size_0: [start_index_0, start_index_1, ...],
                   window_size_1: [start_index_0, start_index_1, ...],
                   ...
                  },
 ...
}

strategy_name_x: str
window_size_x: int
start_index_x: int
````
````python
print(engine.run_register.keys())
# prints all strategy names

print(engine.run_register['my_awesome_strategy_0'].keys())
# prints all window sizes used for 'my_awesome_strategy_0' strategy object

print(engine.run_register['my_awesome_strategy_0'][4])
# prints all start indices of all sub-samples of window_size 4 used for 'my_awesome_strategy_0' strategy object
````  
  
Sub-samples are specified by window size and start index. The window size is the total number of simulation steps. The 
actual sub-sample length is window_size+lookback of the respective strategy. The start index denotes, where the sub-sample 
was sliced from. Strategy execution actually starts at lookback + start_index. See [How Sub-Sampling works](#how-sub-sampling-works) 
for further details. The sub-sample is sliced from \[start_index: start_index + window_size + lookback] (over all 
dict keys of the supplied input market data, see [Input Data Format](#input-data-format)).
### Results from Runs
The FXEngine.load_results() method loads the result from the [temporary directory](#temporary-directory):  
````
def load_result(strategy_name, window_size=None, start_index=None)

strategy_name: str, name of strategy to inspect
window_size: None or int, 
             if int: window size (number of simulation steps) of sub-sample
             if None: largest window size in self.run_register
             default: None
start_index: None or int, 
             if int: start index of sub-sample (step at which strategy execution starts - lookback, see above for 
                     details), index at where data slicing starts and therefore factors in lookback value (that is why
                     lookback has to be substracted from the actual start of the strategy execution time step, as data 
                     slicing starts factors in the lookback)
             if None: minimal start_index in self.run_register or 'runthrough' (prefered if present in 
                      self.run_register) 

returns: dict,
         keys: string, 'signals', 'positions', 'merged_positions', 'total_balances' or 'merged_total_balances'
         values: list, values of FXStrategy.signals, FXStrategy.positions, etc. of the run over the sub-sample specified 
                 by arguments window_size and start_index
````  
FXEngine.load_results() returns a Python dictionary with dict keys naming the result type and dict values with the 
respective list of values. See [Strategy - Retrieving Results](strategy.md#retrieving-results) for a detailed description 
of the returned results. See [How Sub-Sampling works](#how-sub-sampling-works) for a description of the naming convention 
of 'window_size' and 'start_index'. Results of the strategy execution are evaluated on the specified sub-sample.  
### Results regarding Metrics
Results from _FXEngine.analyze()_ are stored in the attributes _FXEngine.standard_analysis_results_ and 
_FXEngine.analysis_results_. Both attributes are Python dictionaries and are structured in the same way:  
````
dict of dicts,
{strategy_name_0: {[window_size_0, start_index_0]: {metric_0: val, metric_1: val...},
                   [window_size_0, start_index_1]: {metric_0: val, metric_1: val...},
                   ...
                   [window_size_1, start_index_0]: {metric_0: val, metric_1: val...},
                   ...
                  },
 strategy_name_1: {[window_size_0, start_index_0]: {metric_0: val, metric_1: val...},
                   [window_size_0, start_index_1]: {metric_0: val, metric_1: val...},
                   ...
                   [window_size_1, start_index_0]: {metric_0: val, metric_1: val...},
                   ...
                  },
 ...
}

strategy_name_x: str, name of strategy
window_size_x: int (or integer as string)
start_index_x: int or string
metric_0: str, name of metric
````  
Keys can be either int or string (in case of start_index='runthrough') to access (float valued) elements of 
_FXEngine.standard_analysis_results_ and _FXEngine.analysis_results_.  
````python
engine.standard_analysis_results['my_awesome_strategy_0'][4, 0]['sharpe_ratio']
# or with string tuple 
engine.standard_analysis_results['my_awesome_strategy_0']['4', '0']['sharpe_ratio']
# returns the pre-implemented sharpe ratio metric for 'my_awesome_strategy_0' strategy object for the sub-sample 
# which had 4 simulations steps and which was sliced starting at index 0 w.r.t. the supplied market data
# (simulation run/ strategy execution starts at 0 + strategy.lookback, see above sections)
````
  
A list of pre-implemented metrics can be found in the section [Analyze the Engine](#analyze-the-engine).  
  
___FXEngine.standard_analysis_results_ are annualized to yearly and assume that data is in daily frequency (252 days per year)!  
The pre-implemented metrics in _FXEngine.analysis_results_ (i.e. _metric_dict=None_ argument in _FXEngine.analyze()_ which 
is the case if no metrics passed to _FXEngine.analyze()_) are NOT annualized (except: downside_risk and expected_rate_of_return 
are calculated against 0.0001172968 which is the log summing up to 3% p.a.)!__  
  
__Note__: VaR in _trapeza_ has a negative sign! Often, only the absolute value is used. Pay attention to those sign conventions!  
### Special Case - 'runthrough'
If a [runthrough](#run-the-engine) was enforced, then the start_index is named 'runthrough' instead of an integer. Thereby, 
the runthrough sub-sample can be uniquely identified.
## Save and Load the Engine
FXEngine object can be saved and loaded via:
````python
engine.save(pth_dir)
engine.load(pth_dir)
````  
This creates a new folder under _pth_dir_ '\[pth_dir]/\[engine_name]_temp/' with [engine_name] being the name of the engine 
object set at instantiation. All files from temporary directory will be copied into this new directory, the engine object 
itself will be stored there as pickle object.  
  
Furthermore, the input market data are stored in the attributes _FXEngine.price_data_ and _FXEngine.volume_data_. This
is done because evaluated results only make sense in conjuncture with the supplied data. The data is accessible via those 
attributes and has the same structure as [Input Data Format](#input-data-format).  
Binding data to the engine object may bloat the pickle object's size. Therefore, it is possible to set those attributes 
to None in order to save some space. Even though, this is not recommended.
## Register New Strategies
New strategies can be registered via:
````
def register_strategies(strategies, loc_decision_functions=False)

strategies: list of strategies or single strategy object (has to inherit from base class BaseStrategy)
loc_decision_funcsion: bool, whether to retrieve source code of strategies, see section about instantiation for further
                       details
````  
This resets the complete engine object, deletes all existing strategies and assigns the new strategies to the engine object. 
This method also handles the strategies' lookback values automatically. All temporary results will be cleaned up, the 
attributes _FXEngine.standard_analysis_results_ and _FXEngine.analysis_results_ will be reset as well.  
Even though registering new strategies is possible, it is highly recommended using the constructor of FXEngine to bind 
strategies to the engine object.  
## Close the Engine
FXEngine normally should take care of cleaning up all temporary files and closing all resources automatically when it goes 
out of scope or gets garbage collected.  
__Nonetheless, it is highly recommended to explicitly close FXEngine by _FXEngine.close()_!__  
## Type Checking
FXEngine performs a type check at each of its methods. This checks if the input arguments are of the right data type. 
This can be turned off via _ignore_type_checking=True_ at instantiation of FXEngine. Default is False, therefore 
performing type checking.  
Turning this off increases runtime performance at the cost of safety.  
This type checking is done independently of the type checking at [account level](account.md#type-checking) or at 
[strategy level](strategy.md#type-checking).  
## Disclaimer regarding Metrics
Metrics provided in _trapeza.metric_ are not checked in a scientific manner and are not compared to the latest research or
to the up-to-date state-of-the-art. __Metrics implemented in this software do not state financial advice or investment 
recommendations, nor do they make any representation or warranties with respect to the accuracy, applicability, fitness, 
or completeness of the contents. Therefore, if you wish to apply this metrics or ideas contained in this software, you 
are taking full responsibility for your action.__
  
Furthermore:  
_trapeza_ is written for scientific, educational and research purpose only. The author of this software and accompanying 
materials makes no representation or warranties with respect to the accuracy, applicability, fitness, or completeness of
the contents. Therefore, if you wish to apply this software or ideas contained in this software, you are taking full 
responsibility for your action. All opinions stated in this software should not be taken as financial advice. Neither 
as an investment recommendation. This software is provided "as is", use on your own risk and responsibility.    
  
  
---  
[Back to Top](#engine)