Go back: [Table of Content](table_of_content.md)  
- [Metrics](#metrics)
  - [Pre-implemented Metrics](#pre-implemented-metrics)
    - [total_rate_of_return](#total_rate_of_return)
    - [volatility](#volatility)
    - [downside_risk](#downside_risk)
    - [value_at_risk](#value_at_risk)
    - [sharpe_ratio](#sharpe_ratio)
    - [sortino_ratio](#sortino_ratio)
  - [Special Metrics returning Non-Floats](#special-metrics-returning-non-floats)
    - [max_drawdown](#max_drawdown)
    - [high_water_marks](#high_water_marks)
    - [drawdown](#drawdown)
    - [_returns](#_returns)
  - [Special Metrics returning Floats](#special-metrics-returning-floats)
    - [continuous_lower_partial_moment](#continuous_lower_partial_moment)
    - [kde_lower_partial_moment](#kde_lower_partial_moment)
    - [expected_rate_of_return](#expected_rate_of_return)
  - [Custom Metrics](#custom-metrics)
  - [Legal Disclaimer](#legal-disclaimer)  
    
  
---------
# Metrics
Standard metrics make a lot of assumptions, e.g. normal distribution. _trapeza_'s metrics are somehow more flexible i.e. 
taking different distributions into account. Nonetheless, _trapeza_'s main focus does not lay on finance mathematics and 
therefore does not guarantee for meaningfulness of the pre-implemented metrics.  
________________________________  
## Pre-implemented Metrics
All pre-implemented metrics are located at _trapeza.metric_:
  - [total_rate_of_return](#total_rate_of_return)  
  - [volatility](#volatility)  
  - [downside_risk](#downside_risk)  
  - [value_at_risk](#value_at_risk)  
  - [sharpe_ratio](#sharpe_ratio)  
  - [sortino_ratio](#sortino_ratio)  
  
### total_rate_of_return
Calculates overall (historic) rate of return by only looking at the first and last value of a given time-series.  
Provides different types of return rates: logarithmic return (base e), percentage return, absolute return.  
Applies annualization if specified.

````
trapeza.metric.total_rate_of_return(data, reference='pct_return', period_length=None, reference_period_length=252)

data:................... 1D list or array
reference:.............. str, {'log_return', 'pct_return', 'abs_return'}
period_length:.......... int, float, or None,
                         None: no annualization
                         int or float: period spanned by data, 
                                       e.g. if hourly data, period_length can either represent number of days spanned 
                                       by data (e.g. 5 days) or number of hours spanned by data (in latter 
                                       case: len(data))
reference_period_length: int or float,
                         ignored if period_length=None
                         total length spanned by reference period in relation to period_length, e.g. if period_length 
                         is in days, then reference_period_length=252 (or 365)

            
return: float
````  
__Annualization__:
_Converts short-term calculation into an annual rate to better compare._
````
log_return:  log_return * (reference_period_length / period_length)
pct_return:  np.power(1 + pct_return, reference_period_length / period_length) - 1
abs_return:  abs_return * (reference_period_length / period_length)
````  
### volatility
Calculates volatility, which the standard deviation or an equivalent metric for a given probability model.
````
trapeza.metric.volatility(data, reference='log_return', prob_model='norm', ddof=0, annualization_factor=1, alpha=2.0)

data:................ 1D list or array
reference:........... str, {'log_return', 'pct_return', 'abs_return', 'abs_value', 'indexed_value'}
                      log_return: logarithmic returns (base e) per time step
                      pct_return: percentage return per time step
                      abs_return: absolute difference between two time step
                      abs_value: plain raw value used for calculation of deviation (no pre-processing, just calculates
                                 deviation)
                      indexed_value: values are indexed to 1-100 w.r.t first data point before calculating deviation
prob_model: ......... str, {'norm', '68_percentile', 'pareto', 'log_norm', 'chebyshev'}
                      probability model to calculate standard deviation or equivalent metric
                      norm: normal distribution
                      68_percentile: half width of the 68 symmetric percentile (in which 68% of all data points lie 
                                     within)
                      pareto: pareto distribution
                      log_norm: log norm distribution
                      chebyshev: NOT IMPLEMENTED YET
ddof:................ int,
                      CURRENTLY ONLY IMPLEMENTED FOR prob_model='norm'
                      delta degree of freedom, use ddof=1 for sample standard deviation and ddof=0 for population 
                      standard deviation
annualization_factor: int or float,
                      annualization: volatility * sqrt(annualization_factor)
                      provided that data is daily:
                         use annualization_factor=1 for daily standard deviation,
                         annualization_factor=252 for yearly (if no trading at weekends etc.,
                         cf. 365 for cryptos),
                         and annualization_factor=252/12 for monthly
alpha:............... float,
                      scaling factor for calculating annualization factor according to Levy stability exponent
                      
return float                     
````  
_chebyshev_ is not yet implemented. _ddof_ is only implemented for _prob_model='norm'_.  
  
__Annualization__:
_Calculated in slightly different way then total_rate_of_return. Annualization_factor is the multiplication factor to
scale data to the desired reference period._
````
volatility * sqrt(annualization_factor)
````  
  
__Alpha__:
_Annualization calculation of volatility implies the assumption of a random walk process whose steps have finite 
variance. To model more general natural stochastic processes the Levy stability exponent alpha is used.
alpha=2.0 equals the normal sqrt(annualization_factor) of a Wiener process._  
````
annualized_volatility = np.power(annualization_factor, 1/alpha) * volatility
````  
### downside_risk
Calculates the downside deviation, square root of downside variance.
````
trapeza.metric.downside_risk(data, target, reference='log_return', prob_model='norm', 
                              kde_kernel='gaussian', kde_bandwidth=0.75)
                           
data:......... 1D list or array
target:....... int or float,
               risk free rate
               if reference=log_return: use logarithm of risk free rate 
               if reference=pct_return: use percentage of risk free rate (5% --> 0.05)
reference:.... str, {'log_return', 'pct_return', 'abs_return', 'abs_value', 'indexed_value'}
               see volatility
prob_model:... str, {'norm', 'pareto', 'log_norm', 'kde', '68_percentile'}
               norm, pareto, log_norm, 68_percentile: see volatility
               kde: kernel density estimation
kde_kernel:... str,
               ignored if prob_model!='kde'
               see sklearn.neighbors.KernelDensity
kde_bandwidth: str,
               ignored if prob_model!='kde'
               see sklearn.neighbors.KernelDensity
               
return: float               
````  
### value_at_risk
Calculates value at risk, which is the maximal loss to be expected for given confidence.  
````
trapeza.metric.value_at_risk(data, confidence=0.95, reference='log_return', prob_model='norm', ddof=0,
                              method='historic_simulation', annualization_factor=1, alpha=2.0,
                              kde_kernel='gaussian', kde_bandwidth=0.75)
                  
data:......... 1D list or array
confidence:... float (0, 1),
               confidence level to calculate value at risk, takes 1-confidence as percentile for calculations
reference:.... str, {'log_return', 'pct_return', 'abs_return', abs_value', 'indexed_value'}, see volatility
prob_model:... str, {norm, log_norm, pareto, kde}, see downside_risk and volatility
ddof:......... int, see volatility
method:....... str, {'historic_simulation', 'var_cov'}
               method used for calculation
               'historic_simulation': calculation based on (1-confidence)-percentile of historic data
               'var_cov': fit probability model and calculate value at 1-confidence% of inverse cumulative distribution 
                          function (1-confidence% of all values of the fitted probability function are less or equal to 
                          calculated value)
annualization: int or float, see volatility
alpha:........ int or float, see volatility
kde_kernel: .. str, see downside_risk
kde_bandwidth: str, see downside_risk

return: float
````  
__VaR in _trapeza_ has a negative sign! Often, only the absolute value is used. Pay attention to those sign conventions!__  
### sharpe_ratio
Calculates the sharpe ratio (simplified: return to volatility).
````
trapeza.metric.sharpe_ratio(data, risk_free_rate=0.0001173037, reference='pct_return', prob_model_exp_ror='norm',
                             prob_model_vola='norm', ddof_vola=0, revised_denominator=True, annualization_factor=1, 
                             alpha=2.0, kde_kernel='gaussian', kde_bandwidth=0.75)
                             
data:................ 1D list or array
risk_free_rate:...... int, float or 1D array,
                      risk free rate for calculating shapre ratio
                      if reference='log_return': use logarithmic risk free rate
                      if reference='pct_return': use percentage risk free rate (e.g. 5% -> 0.05)
reference:........... str, {'log_return', 'pct_return'},
                      log_return: calculation based on logarithmic returns (base e) per time step
                      pct_return: calculation based on percentage returns per time step
prob_model_exp_ror:.. str, {'norm', 'log_norm', '50_percentile', 'pareto', 'kde', 'geometric'}
                      norm, log_norm, pareto: see volatility
                      kde: see downside_risk
                      50_percentile: median estimator
                      geometric: geometric mean as compound rate of return,
                                 this does not apply any probability distribution but calculates the mean estimator as
                                 compound excess rate of returns
prob_model_vola:..... str, {'norm', '68_percentile', 'pareto', 'log_norm', 'chebyshev'}, see volatility
ddof:................ int, see volatility
revised_denominator:  bool,
                      use revised definition of the sharpe ratio
                      revised_denominator=True:  std(total_rate_of_return - risk_free_rate)
                      revised_denominator=False: std(total_rate_of_return), original definition
                      ignored if risk_free_rate is constant: std(a-b) == std(a) if b=const
annualization_factor: int or float, see volatility
alpha:............... float, see volatility
kde_kernel:.......... str, see downside_risk
kde_bandwidth:....... str, see downside_risk

return: float
````  
If we assume a risk free rate of 3% per year, then the daily risk free rate is about 0.0001173037, which is the default 
value.  
Taking the logarithm of _1 + daily risk free rate_ gives 0.0001172968, which would be the logarithmic risk free rate in 
case of _reference='log_return'_.  
_prob_model_exp_ror=geometric_ actually does not denote the geometric distribution, but calculates the compound excess 
rate of returns as geometric mean (takes the n-th square root of the product of all n elements).  
### sortino_ratio
Calculates the sortino ratio. It is a modification of the Sharpe ratio which only penalizes returns below a user-specified 
target rate of return (measures risk-adjusted return). _sortino_ratio_ uses _downside_risk_ to calculate the downside 
deviation w.r.t. to the target rate.
````
trapeza.metric.sortino_ratio(data, target_rate=0.0001173037, reference='pct_return', prob_model_exp_ror='norm',
                              prob_model_dr='norm', annualization_factor=1, alpha=2.0, 
                              kde_kernel='gaussian', kde_bandwidth=0.75)
                              
data:................ 1D list or array
target_rate:......... int, float or 1D array,
                      target rate for calculating sortino ratio, see sharpe_ratio
reference:........... str, {'log_return', 'pct_return'}, see sharpe_ratio
prob_model_exp_ror:.. str, {'norm', 'log_norm', '50_percentile', 'pareto', 'kde', 'geometric'}, see sortino_ratio
prob_model_vola:..... str, {'norm', '68_percentile', 'pareto', 'log_norm', 'chebyshev'}, see volatility
annualization_factor: int or float, see volatility
alpha:............... float, see volatility
kde_kernel:.......... str, see downside_risk
kde_bandwidth:....... str, see downside_risk

return: float
````
## Special Metrics returning Non-Floats
The following metrics do not return a single float. Therefore, they cannot be plugged into _FXEngine_ directly.  
### max_drawdown
Calculates the maximum drawdown (MDD). MDD looks for the greatest movement from a high point (peak) to a low point (through) 
before a new peak is achieved. MDD only measures the largest possible loss, but not the time taken for recovery or if the 
investment even recovers at all.
````
trapeza.metric.max_drawdown(data, mode='relative')

data: 1D list or array
mode: str, {'relative', 'absolute'}
      'relative': divides the absolute peak-to-through-value by the accumulated value up to the MDD
      'absolute': only calculates MDD in absolute values
      
return: depth: int, absolute MDD peak-to-through value,
               depth is a absolute value even if mode='relative'. Nonetheless, this value might be different for 
               mode='relative' and mode='absolute' (peak_index and through_index at different index locations)
               depth = data[peak_index] - data[through_index]
               depth = 0, if only ascending values in data (no drawdown)
        peak_index: int or None, 
                    index in data,
                    None if only ascending values in data (no drawdown)
        thorugh_index: int or None, 
                       index in data,
                       None if only ascending values in data (no drawdown)
````  
Relative and absolute MDD might differ, e.g.:  
  
_S&P500_:  
2007-2008: drawdown = &nbsp;&nbsp;&nbsp;&nbsp; 888 points --> 57%  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2020: drawdown = 11,150 points --> 34%  
  
--> absolute and relative drawdown differ significantly  
_if drawdown analysis comprises a longer period, relative numbers might make more sense._  
  
If no through value was found, then _max_drawdown_ returns '0 ,None, None'.  
### high_water_marks
Calculates high watermarks.A high watermark is reached, if current value exceeds all past values. Every time a new 
high/ maximum is reached, it is called a high watermark.
````
trapeza.metric.high_water_marks(data)

data: 1D list or array

return: water_mark_values: 1D array of floats,
                           value of each high watermark
        water_mark_indices: 1D array of list, 
                            indices in data where high watermarks are located (shape matches data for masking)
````  
### drawdown
Calculates drawdown over the entire data. In contrast to [maximum drawdown](#max_drawdown), this function calculates all 
(local) drawdowns.  
````
trapeza.metric.drawdown(data, mode='relative')

data: 1D list or array
mode: str, {'relative', 'absolute'}, see max_drawdown

return: draw:........... 1D array, same length as data,
                         running (cummulative) drawdown at each point in data w.r.t. previous peak
                         can directly be used to draw under-water chart
        depth:.......... 1D array, depth as absolute value between peak and through value,
                         dpeth[c] is related to peak_indices[c] and thorugh_indices[c]
        peak_indices:... 1D array, indices of all peak values in data (shape matches data for masking), 
                         peak_indices[c] is related to thorugh_indices[c]
        through_indices: 1D array, indices of all through values in data (shape matches data for masking), 
                         peak_indices[c] is related to thorugh_indices[c]
````  
_depth[c]=data[peak_indices[c]]-data[through_indices[c]]_. _depth_, _peak_indices_ and _through_indices_ are structured 
such that at each of their index they will match up. If the peak value is searched, which corresponds to through_index c, 
then this peak value can be found at peak_indices[c].
### _returns
Calculates returns based on the same frequency as data (e.g. if data is on daily basis, then returned returns are on 
daily basis as well).  
_This function is only a helper function for the above metrics._  
````
trapeza.metric._returns(data, reference)

data: 1D list or array of shape(n,)
reference: str, {log_return, pct_return, abs_return}, see total_rate_of_return

return: 1D array of shape(n-1,)
````  
Returned array is one element shorter than the input list or array.
## Special Metrics returning Floats
The following metrics do return a float and could theoretically be plugged into _FXEngine_ but are rather meant as 
helper functions than for direct use.
### continuous_lower_partial_moment
Calculates the lower partial moment, which only factor in values below target with respect to the whole distribution.   
_This function is only a helper function for the above metrics._  
````
trapeza.metric.continuous_lower_partial_moment(data, target, order, scipy_distribution)

data:.............. 1D list or array
target:............ int or float (e.g. target rate)
order:............. int, tpye of moment
scipy_distribution: any scipy distribution class, e.g. scipy.stats.pareto, see scipy for further information

return: float
````  
### kde_lower_partial_moment
Basically does the same as [continuous_lower_partial_moment](#continuous_lower_partial_moment) but uses Kernel Density 
Estimation.  
_This function is only a helper function for the above metrics._  
````
trapeza.metric.kde_lower_partial_moment(data, target, order, kernel='gaussian', bandwidth=0.75)

data:.............. 1D list or array
target:............ int or float (e.g. target rate)
order:............. int, tpye of moment
kernal:............ str, see sklearn.neighbors.KernelDensity 
bandwidth:......... str, see sklearn.neighbors.KernelDensity

return: float
````  
### expected_rate_of_return
Calculates expected rate of return (rate of return at each time step which is to be expected on average). Whereas
total_rate_of_return calculates the total return over the entire period, expected_rate_of_return calculates the expected
return for a time step. Simpy speaking, this functions calculates the _expected value_.  
  
__This function was originally just meant as a helper function.__  
__Takes in a time-series of pre-calculated returns! Do not use e.g. raw price data.__  
  
Input data (argument _ret_) will not be pre-processed in any way but will be used as is. If e.g. log returns shall be used,
then pre-calculate log-return time-series beforehand and then plug it into expected_rate_of_return
(the interface slightly differs from total_rate_of_return as this function was actually not meant to be called directly
but was originally just a helper function - for back compatibility the function signature remained).  
````
trapeza.metric.expected_rate_of_return(ret, reference_rate, prob_model_exp_ror, kde_kernel, kde_bandwidth)


ret:............... 1D list or arrays,
                    must contain pre-calculated returns, not raw price data (!)
reference_rate:.... int or float,
                    reference rate to calculate excess rate: ret - reference_rate
prob_model_exp_ror: str, {'norm', 'log_norm', '50_percentile', 'pareto', 'kde', 'geometric'}
                    norm, log_norm, pareto: see volatility
                    kde: see downside_risk
                    50_percentile: median estimator
                    geometric: geometric mean as compound rate of return,
                               this does not apply any probability distribution but calculates the mean estimator as
                               compound excess rate of returns
kde_kernel:........ str, see downside_risk
kde_bandwith:...... str, see downside_risk

return: float
````
_prob_model_exp_ror=geometric_ actually does not denote the geometric distribution, but calculates the compound excess
rate of returns as geometric mean (takes the n-th square root of the product of all n elements).
## Custom Metrics
Custom metrics or metrics from external packages can be used as well. In order to be used with [_FXEngine_](engine.md#analyze-the-engine),
they must:  
  - Take a list (or array) of floats as input only (additional arguments can be implemented as keyword arguments tough),  
  - return a single float value.  
  
## Legal Disclaimer
Metrics provided in _trapeza.metric_ are not checked in a scientific manner and are not compared to the latest research or
to the up-to-date state-of-the-art. __Metrics implemented in this software do not state financial advice or investment 
recommendations, nor do they make any representation or warranties with respect to the accuracy, applicability, fitness, 
or completeness of the contents. Therefore, if you wish to apply this metrics or ideas contained in this software, you 
are taking full responsibility for your action.__  
  
Furthermore:  
_trapeza_ is written for scientific, educational and research purpose only. The author of this software and accompanying 
materials makes no representation or warranties with respect to the accuracy, applicability, fitness, or completeness of
the contents. Therefore, if you wish to apply this software or ideas contained in this software, you are taking full 
responsibility for your action. All opinions stated in this software should not be taken as financial advice. Neither 
as an investment recommendation. This software is provided "as is", use on your own risk and responsibility.    
  
  
---  
[Back to Top](#metrics)