Go back: [Table of Content](table_of_content.md)  
- [Order Management](#order-management)
  - [Implemented Order Types](#implemented-order-types)
    - [Stop Loss](#stop-loss)
    - [Trailing Stop Loss](#trailing-stop-loss)
    - [Buy Limit](#buy-limit)
    - [Sell Limit](#sell-limit)
    - [Start Buy](#start-buy) 
  - [Patch Accounts with Order Management](#patch-accounts-with-order-management)
    - [Monkey Patch](#monkey-patch)
    - [Ticking Behavior](#ticking-behavior)
    - [Use Order Types](#use-order-types)
    - [Coverage Checks](#coverage-checks) 
    - [Inner Workings: the Order Stack](#inner-workings-the-order-stack)
    - [Use patched Accounts with FXStrategy and FXEngine](#use-patched-accounts-with-fxstrategy-and-fxengine)
  - [Implement Custom Order Types](#implement-custom-order-types)
    - [Defining the Custom Order Type](#defining-the-custom-order-type)
    - [Apply the Patch to Custom Order Type](#apply-the-patch-to-custom-order-type)
    - [Things to note when Implementing Custom Order Types](#things-to-note-when-implementing-custom-order-types)
  - [Type Checking](#type-checking)
  
  
---------  
# Order Management  
FXAccount only implements very basic transactions. More complex trading orders are implemented in _Order Management_.  
Accounts can be patched by _Order Management_, afterwards order types can be directly used by accounts.  
________________________________  
## Implemented Order Types  
Following order types are implemented:  
  - [stop_loss(self, current_bid_rate, volume, stop_bid_price, base, quote, order_lifetime_duration, **kwargs)](#stop-loss)  
  - [trailing_stop_loss(current_bid_rate, volume, trailing_stop_bid_price, base, quote, order_lifetime_duration, percent=True, **kwargs)](#trailing-stop-loss)  
  - [buy_limit(current_ask_rate, volume, limit_ask_price, base, quote, order_lifetime_duration, **kwargs)](#buy-limit)  
  - [sell_limit(current_bid_rate, volume, limit_bid_price, base, quote, order_lifetime_duration, **kwargs)](#sell-limit)  
  - [start_buy(current_ask_rate, volume, start_ask_price, base, quote, order_lifetime_duration, **kwargs)](#start-buy)
  
  
All order types raise a trapeza.exception.OperatingError if an exception is thrown inside.  
  
### Stop Loss
Places a stop loss order. The stop loss order triggers a sell transaction when the current price drops under (or equal to) 
the stop price. The stop order expires automatically after _order_lifetime_duration_ time steps. After this duration 
the order gets deleted and will not be executable anymore. The concept of time steps is explained in 
[Account - Processing Durations](account.md#processing-durations). The last time step is inclusive, e.g. if _order_lifetime_duration=5_, 
then order is still executable in the 5th time step but not afterwards at the next time step.  
Additional keyword arguments for the underlying [sell transaction](account.md#transaction-sell) can be passed via _\*\*kwargs_.
````  
stop_loss(current_bid_rate, volume, stop_bid_price, base, quote, order_lifetime_duration, **kwargs)

current_bid_rate: ......float, current exchange rate of the tradable base|quote pair
volume: ................float, volume in base currency
stop_bid_price: ........float, exchange rate at which to place the stop loss
base: ..................string, base currency (identifier/ tick symbol)
quote: .................string, quote currency (identifier/ tick symbol)
oder_lifetime_duration: int, duration in time steps after which the order expires (all time steps inclusive)
**kwars: ...............key word arguments for trapeza.account.FXAccount.sell method

return: ................see return value of trapeza.account.FXAccount.sell method
                        -1: sell transaction not executed (yet), 
                         0: sell transaction delayed due to processing_duration, 
                         1: sell transaction executed
````  
Use:  
````python
from trapeza.account import FXAccount
from trapeza.account import order_management

acc = FXAccount('USD')
order_management.monkey_patch(acc)

# sell 100$ when the price of USD|EUR is less or equal to 0.8
# this order is only valid for 5 time steps
# the actual sell transaction needs 1 time step to be processed (e.g. messaging delay over networks etc.)
acc.stop_loss(0.82, 100, 0.8, 'USD', 'EUR', order_lifetime_duration=5, processing_duration=1)
````  
This order is commonly used to cut losses.  
_(This is a very simplified order model and does not account for lack of market liquidity or FOK/ IOC.)_  
### Trailing Stop Loss  
Places a trailing stop loss. Same as [Stop Loss](#stop-loss), but the stop price will be adjusted in case the price 
increases. The stop price will not follow downward movements of the price, only upward movements. Therefore, trailing 
stop loss preserves the distance between market price and stop price whenever the market price raises. This distance can 
either be defined as an absolute value or percentage value: 
````
trailing_stop_loss(current_bid_rate, volume, trailing_stop_bid_price, base, quote, order_lifetime_duration, 
                   percent=True, **kwargs)
                   
current_bid_rate: .......float, current exchange rate of the tradable base|quote pair
volume: .................float, volume in base currency
trailing_stop_bid_price: float, exchange rate at which to place the initial trailing stop loss
base: ...................string, base currency (identifier/ tick symbol)
quote: ..................string, quote currency (identifier/ tick symbol)
oder_lifetime_duration:  int, duration in time steps after which the order expires (all time steps inclusive)
percent: ................bool, True: adjust percentage distance in case of upward movement, 
         ......................False: adjust absolute distance in case of upward movement, 
         ......................default=True
**kwars: ................key word arguments for trapeza.account.FXAccount.sell method

return: .................see return value of trapeza.account.FXAccount.sell method
                         -1: sell transaction not executed (yet), 
                          0: sell transaction delayed due to processing_duration, 
                          1: sell transaction executed
````  
Use:  
````python
from trapeza.account import FXAccount
from trapeza.account import order_management

acc = FXAccount('USD')
order_management.monkey_patch(acc)

# sell 100$ when the price of USD|EUR is less or equal to 0.8
# if USD|EUR price increases, then stop loss will be adjusted accordingly with an absolute distance of 0.02
# this order is only valid for 5 time steps
# the actual sell transaction needs 1 time step to be processed (e.g. messaging delay over networks etc.)
acc.trailing_stop_loss(0.82, 100, 0.8, 'USD', 'EUR', percent=False, order_lifetime_duration=5, processing_duration=1)
````  
This order is commonly used to cut losses.  
_(This is a very simplified order model and does not account for lack of market liquidity or FOK/ IOC.)_  
### Buy Limit  
Places a buy limit order. The buy limit order triggers a buy transaction when the current price drops under (or equal to) 
the buy limit price ([_ask price_](account.md#transaction-buy)). The buy limit order expires automatically after 
_order_lifetime_duration_ time steps. After this duration the order gets deleted and will not be executable anymore. 
The concept of time steps is explained in [Account - Processing Durations](account.md#processing-durations). 
The last time step is inclusive, e.g. if _order_lifetime_duration=5_, then order is still executable in the 5th time step 
but not afterwards at the next time step.  
Additional keyword arguments for the underlying [buy transaction](account.md#transaction-buy) can be passed via _\*\*kwargs_.
````  
buy_limit(current_ask_rate, volume, limit_ask_price, base, quote, order_lifetime_duration, **kwargs)

current_ask_rate: ......float, current exchange rate of the tradable base|quote pair
volume: ................float, volume in base currency
limit_ask_price: .......float, exchange rate at which to place the buy limit
base: ..................string, base currency (identifier/ tick symbol)
quote: .................string, quote currency (identifier/ tick symbol)
oder_lifetime_duration: int, duration in time steps after which the order expires (all time steps inclusive)
**kwars: ...............key word arguments for trapeza.account.FXAccount.buy method

return: ................see return value of  trapeza.account.FXAccount.buy method
                        -1: buy transaction not executed (yet), 
                         0: buy transaction delayed due to processing_duration, 
                         1: buy transaction executed
````  
Use:  
````python
from trapeza.account import FXAccount
from trapeza.account import order_management

acc = FXAccount('USD')
order_management.monkey_patch(acc)

# buy 100$ when the price of USD|EUR is less or equal to 0.8
# this order is only valid for 5 time steps
# the actual buy transaction needs 1 time step to be processed (e.g. messaging delay over networks etc.)
acc.buy_limit(0.82, 100, 0.8, 'USD', 'EUR', order_lifetime_duration=5, processing_duration=1)
````  
This order is commonly used to ensure not to buy at a too high price.  
_(This is a very simplified order model and does not account for lack of market liquidity or FOK/ IOC.)_  
### Sell Limit  
Places a sell limit order. The sell limit order triggers a sell transaction when the current price raises above (or equal 
to) the sell limit price ([_bid price_](account.md#transaction-sell)). The sell limit order expires automatically after 
_order_lifetime_duration_ time steps. After this duration the order gets deleted and will not be executable anymore. 
The concept of time steps is explained in [Account - Processing Durations](account.md#processing-durations). 
The last time step is inclusive, e.g. if _order_lifetime_duration=5_, then order is still executable in the 5th time step 
but not afterwards at the next time step.  
Additional keyword arguments for the underlying [sell transaction](account.md#transaction-sell) can be passed via _\*\*kwargs_.  
````  
sell_limit(current_bid_rate, volume, limit_bid_price, base, quote, order_lifetime_duration, **kwargs)

current_bid_rate: ......float, current exchange rate of the tradable base|quote pair
volume: ................float, volume in base currency
limit_bid_price: .......float, exchange rate at which to place the sell limit
base: ..................string, base currency (identifier/ tick symbol)
quote: .................string, quote currency (identifier/ tick symbol)
oder_lifetime_duration: int, duration in time steps after which the order expires (all time steps inclusive)
**kwars: ...............key word arguments for trapeza.account.FXAccount.sell method

return: ................see return value of  trapeza.account.FXAccount.sell method
                        -1: sell transaction not executed (yet), 
                         0: sell transaction delayed due to processing_duration, 
                         1: sell transaction executed
````  
Use:  
````python
from trapeza.account import FXAccount
from trapeza.account import order_management

acc = FXAccount('USD')
order_management.monkey_patch(acc)

# sell 100$ when the price of USD|EUR is above or equal to 0.84 in order to take profits
# this order is only valid for 5 time steps
# the actual sell transaction needs 1 time step to be processed (e.g. messaging delay over networks etc.)
acc.sell_limit(0.82, 100, 0.84, 'USD', 'EUR', order_lifetime_duration=5, processing_duration=1)
````  
This order is commonly used take profits.  
_(This is a very simplified order model and does not account for lack of market liquidity or FOK/ IOC.)_  
### Start Buy
Places a normal start_buy order. The start buy order triggers a buy transaction when the current price raises above 
(or equal to) the start buy price ([_ask price_](account.md#transaction-buy)). The buy limit order expires automatically after 
_order_lifetime_duration_ time steps. After this duration the order gets deleted and will not be executable anymore. 
The concept of time steps is explained in [Account - Processing Durations](account.md#processing-durations). 
The last time step is inclusive, e.g. if _order_lifetime_duration=5_, then order is still executable in the 5th time step 
but not afterwards at the next time step.  
Additional keyword arguments for the underlying [buy transaction](account.md#transaction-buy) can be passed via _\*\*kwargs_.  
````
start_buy(current_ask_rate, volume, start_ask_price, base, quote, order_lifetime_duration, **kwargs)

current_ask_rate: ......float, current exchange rate of the tradable base|quote pair
volume: ................float, volume in base currency
start_ask_price: .......float, exchange rate at which to place the start buy limit
base: ..................string, base currency (identifier/ tick symbol)
quote: .................string, quote currency (identifier/ tick symbol)
oder_lifetime_duration: int, duration in time steps after which the order expires (all time steps inclusive)
**kwars: ...............key word arguments for trapeza.account.FXAccount.buy method

return: ................see return value of  trapeza.account.FXAccount.buy method
                        -1: buy transaction not executed (yet), 
                         0: buy transaction delayed due to processing_duration, 
                         1: buy transaction executed
````  
Use:  
````python
from trapeza.account import FXAccount
from trapeza.account import order_management

acc = FXAccount('USD')
order_management.monkey_patch(acc)

# buy 100$ when the price of USD|EUR is greater or equal to 0.85
# this order is only valid for 5 time steps
# the actual buy transaction needs 1 time step to be processed (e.g. messaging delay over networks etc.)
acc.start_buy(0.82, 100, 0.85, 'USD', 'EUR', order_lifetime_duration=5, processing_duration=1)
````  
This order type is used to buy a position as soon as the course exceeds a certain limit, which might indicate a
sustainable recovery point and is used e.g. to take advantage of sustainable trends.  
_(This is a very simplified order model and does not account for lack of market liquidity or FOK/ IOC.)_  
## Patch Accounts with Order Management
To make orders accessible to accounts, they have to be patched. This is implemented as monkey patch, which surely is not 
the most elegant way, but ensures that only single elements and not whole classes get patched.  
### Monkey Patch
To make this order types accessible, accounts have to be patched by trapeza.account.order_management.monkey_patch(...). 
After patching, order types can be used directly by the patched account. This only patches single objects but not the 
entire class, so e.g. if only one of two accounts of the class FXAccount gets patched, then the other remains unpatched.  
````
monkey_patch(account, only_basics=False)

account:        account object, has to inherit from trapeza.account.base_account.BaseAccount base class, e.g. FXAccount
only_basics:    bool, default=False
                True: Only methods necessary to implement the patch will be patched, but not the above mentioned
                      order types.
                False: The above mentioned order types will be patched along all helper functions
````  
The _only_basics_ argument controls, whether order types shall be patched. This is useful e.g. when the user wants to 
write custom order types but does not want to patch the above mentioned order types (i.e. when other custom implementations 
of the above mentioned order types is desired - this avoids name mangling in this case). Default is patching the above 
mentioned order types.  
Use:  
````python
from trapeza.account import FXAccount
from trapeza.account.order_management import monkey_patch

acc_0 = FXAccount('USD')
acc_1 = FXAccount('EUR')

monkey_patch(acc_0)
# acc_1 stays untouched
````  
After patching, the patched account has an additional attribute call _\_is_order_manager_monkey_patch_ (alongside some 
other new internal attributes). This new attribute can be used to check, whether an account has been patched.
### Ticking Behavior  
Order types compare the current market price with their order limits in order to decide whether some transactions shall 
be triggered. Therefore, we have to continuously tell the account, what the current market prices are.  
After patching, the [FXAccount.tick()](account.md#ticking-the-internal-clock) method get overwritten to take current market 
prices into consideration:  
````
tick(dict_current_data=None, *args_base_tick, **kwargs_base_tick)

dict_current_data:    dict or None,
                      dict keys: string tuple of base and quote ('base_currency', 'quote_currency')
                      dict value: float representing current exchange rate of respective dict key (base quote tuple)
                      e.g.:
                        {('USD', 'EUR'): 0.82,
                         ('JPY', 'EUR'): 0.0075,
                        }
                      if None: oder management will not be checked for executable orders, orders may then expire at the
                        next tick if order lifetime expires (missed triggers will not be catched up but are just 
                        ignored). This does not affect basic transactions implemented at account level such as sell, 
                        buy, deposit or withdraw.
*args_base_tick:      positional arguments of the unpatched tick method, e.g. FXAccount.tick(clock=None),
                      useful when using custom account implementation (inherit from 
                      trapeza.account.base_account.BaseAccount)
**kwargs_base_tick:   keyword arguments of the unpatched tick method, e.g. FXAccount.tick(clock=None),
                      useful when using custom account implementation (inherit from 
                      trapeza.account.base_account.BaseAccount)
````  
The input market data uses the [direct base quote notation](account.md#base-quote-notation).  
The input market data uses a Python dictionary format. The dict keys consist of string tuples which names the tradable pair 
(e.g. currency pair). The dict values represent the respective exchange rate as float value. 

If None is passed instead of a data dict, then orders will not be checked whether they might trigger transactions (order 
management is just omitted and the account uses the tick method as it would not have been patched at all). The counter 
for the order lifetime still counts up accordingly to the time steps passed, even if no data was passed (and no order management 
was checked). Internally the order management logic is implemented as a second stack automaton, which shares the same internal 
clock as the [stack automaton](account.md#inner-workings-how-the-stack-automaton-logic-works) of the underlying account.  
The same effect occurs when the tick method uses the _clock_ argument to fast-forward the internal clock. By skipping some 
time steps, orders will not be evaluated backwards/ retrospective (they just get not checked whether they may trigger).  
See below code example for a more tangible explanation.  
See [Inner Workings: The Order Stack](#inner-workings-the-order-stack) for detailed information about the order stack 
implementation.

Furthermore, arguments of the underlying account tick function can be passed. The underlying tick function gets overwritten 
by the patch but is internally still invoked. The FXAccount.tick() method has the keyword argument _clock_ which can be 
passed via those positional _\*args_base_tick_ or keyword _\*\*kwargs_base_tick_ arguments. This especially useful when 
custom account implementations (which nonetheless have to inherit from trapeza.account.base_account.BaseAccount) with 
custom tick functions shall be used in conjunction with order management.  
  
````python
from trapeza.account import FXAccount
from trapeza.account.order_management import monkey_patch

# initialize an account and deposit some cash
acc = FXAccount('USD')
acc.deposit(10_000, 'USD')

# patch the account with order management
monkey_patch(acc)

# lets place some orders
# stop loss orders will sell 10 USD if the USD|EUR drops to or below 0.7 and expires after 3 time steps
acc.stop_loss(0.82, 10, 0.7, 'USD', 'EUR', 3)
# stop loss orders will sell 10 USD if the USD|EUR drops to or below 0.6 and expires after 5 time steps
acc.stop_loss(0.82, 10, 0.6, 'USD', 'EUR', 5)
# stop loss orders will sell 10 USD if the USD|EUR drops to or below 0.5 and expires after 8 time steps
acc.stop_loss(0.82, 10, 0.5, 'USD', 'EUR', 8)
# stop loss orders will sell 10 USD if the USD|EUR drops to or below 0.5 and expires after 100 time steps
acc.stop_loss(0.82, 10, 0.5, 'USD', 'EUR', 100)

print(acc.depot)
# >>> {'USD': 10000.0}

acc.tick({('USD', 'EUR'): 0.8})
print(acc.depot)
# >>> {'USD': 10000.0}
# internal clock is now 1, nothing happened

acc.tick()  # internal clock = 2
print(acc.depot)
# >>> {'USD': 10000.0}

# last chance for the first order to get triggered
acc.tick({('USD', 'EUR'): 0.65})
print(acc.depot)
# >>> {'USD': 9990.0, 'EUR': 6.5}
# order got executed as expected: we sold 10$ for 6.5€

# we also can use the args of the underlying account tick method, e.g. to fast forward the internal clock time
acc.tick({('USD', 'EUR'): 0.55}, clock=6)   # set internal clock to 6
print(acc.depot)
# >>> {'USD': 9990.0, 'EUR': 6.5}
# even though the current market price is below the stop limit of 0.6, this order did not get triggered because 
# during fast forward we skipped all of order management and the order expired in the meantime

acc.tick({('USD', 'EUR'): 0.55})    # internal clock = 7
acc.tick({('USD', 'EUR'): 0.55})    # internal clock = 8
print(acc.depot)
# >>> {'USD': 9990.0, 'EUR': 6.5}
# the third order expired ordinarily as price just did not drop to/ below stop limit

acc.tick({('USD', 'EUR'): 0.5})    # internal clock = 9
print(acc.clock)    # just to be sure, that we are really at internal clock = 9
# >>> 9
print(acc.depot)
# >>> {'USD': 9980.0, 'EUR': 11.5}
# fourth order was untouched from the previous hustle and got executed as expected
````  
Code: [script_order_management_example.py](script_order_management_example.py)  
### Use Order Types  
After patching an account with trapeza.account.order_management.monkey_patch, the patched account can invoke the above 
order types directly.  
Nevertheless, note that _Order Management_ is a pure Python implementation, whereas FXAccount is transpiled into C++ via 
Cython. So there might be slight performance decreases by patching accounts.  
  
As explained in the [next section](#inner-workings-the-order-stack), _Order Management_ is implemented via stack automaton 
logic just like FXAccount. The order stack is implemented via a min heap data structure (pure Python) and can be inspected 
via the _\_order_heap_ attribute. All _Order Management_ attributes (opposed to FXAccount) return mutable data types and 
can be manipulated directly. They are not [protected such as at FXAccount](account.md#retrieving-complete-depot):  
````python
from trapeza.account import FXAccount
from trapeza.account.order_management import monkey_patch

 # initialize an account and deposit some cash
acc = FXAccount('USD')
acc.deposit(10_000, 'USD')

# patch the account with order management
monkey_patch(acc)

# lets place some orders
# stop loss orders will sell 10 USD if the USD|EUR drops to or below 0.7 and expires after 3 time steps
acc.stop_loss(0.82, 10, 0.7, 'USD', 'EUR', 3)
# stop loss orders will sell 10 USD if the USD|EUR drops to or below 0.6 and expires after 5 time steps
acc.stop_loss(0.82, 10, 0.6, 'USD', 'EUR', 5)
# stop loss orders will sell 10 USD if the USD|EUR drops to or below 0.5 and expires after 8 time steps
acc.stop_loss(0.82, 10, 0.5, 'USD', 'EUR', 8)
# stop loss orders will sell 10 USD if the USD|EUR drops to or below 0.5 and expires after 100 time steps
acc.stop_loss(0.82, 10, 0.5, 'USD', 'EUR', 100)

# inspect order stack
print(acc._order_heap)
# >>> {('USD', 'EUR'): 
#           [['stop_loss', {'volume': 10, 'stop_bid_price': 0.7, 'base': 'USD', 'quote': 'EUR', 
#             'order_lifetime_duration': 3, '_internal_order_lifetime': 3}], 
#            ['stop_loss', {'volume': 10, 'stop_bid_price': 0.6, 'base': 'USD', 'quote': 'EUR', 
#             'order_lifetime_duration': 5, '_internal_order_lifetime': 5}], 
#            ['stop_loss', {'volume': 10, 'stop_bid_price': 0.5, 'base': 'USD', 'quote': 'EUR', 
#             'order_lifetime_duration': 8, '_internal_order_lifetime': 8}], 
#            ['stop_loss', {'volume': 10, 'stop_bid_price': 0.5, 'base': 'USD', 'quote': 'EUR', 
#             'order_lifetime_duration': 100, '_internal_order_lifetime': 100}]
#            ]
#     }
````  
The order stack is structured as a Python dictionary. String tuples, which name the tradable (currency) pair, serve as 
dict keys, min heaped lists are used as dict values. The min heaped list contains all orders tied to the specific tradable 
(currency) pair. The _\_internal_order_lifetime_ is defined as 'current internal clock + order_lifetime_duration'. 
In theory, the user can manually overwrite this stack, but __it is highly recommended not to do that manually__.  
  
All order types raise a trapeza.exception.OperatingError if an exception is thrown inside.  
  
Transactions like _sell_, _buy_, _deposit_ and _withdraw_, as well as the internal (transaction) stack, of the original 
account will stay untouched by the patch and can be used as usual.  
### Coverage Checks
_trapeza_ does basic coverage checks of normal transactions, i.e. sell, buy, transfer etc. This prevents that positions 
might fall into a negative range (debts). The coverage checking behavior can be controlled on [FXAccount](account.md) level, 
such as allowing negative positions (debts). For further explanations see [FXAccount - Coverage](account.md#coverage).  
  
_Order Management_ does not check coverage at the time of placing the order (this wouldn't make too much sense as positions 
might change significantly until e.g. a stop-loss-order gets triggered and executed). Coverage is only checked at the time 
of execution, i.e. when the order gets triggered, but not before. Make sure, that there is enough coverage when the order 
will be executed.  
  
Orders placed onto the [order stack](#inner-workings-the-order-stack), i.e. placed but not yet triggered 
orders (waiting e.g. until a certain price level of the underlying is hit), will not be factored into the coverage check. 
That means, that not yet triggered orders will not be calculated into the coverage, which is needed to meet a sufficient 
account coverage. Therefore, orders might throw a coverage exception at time of execution (when e.g. triggered by a certain 
price level of the underlying).  
### Inner Workings: the Order Stack
As mentioned in the previous sections, _Order Management_ implements a second stack automaton with a separate stack called 
order stack in this context.  
Both stack automaton share the same internal clock of account.  
When an order is placed, it is first checked, whether current market price will trigger the order immediately. If not, then 
the order is pushed down to the order stack.  
At each tick of the patched account, the internal clock is counted up (by 1 time step normally), then first the transaction 
stack is checked whether transactions are executable.  
Next the trigger of the order stack automaton checks whether orders are triggered w.r.t. the current market data (supplied 
via the tick(data) method of the patched account, see [_Ticking Behavior_](#ticking-behavior)). If so, orders are popped 
of the order stack and are decomposed into basic transaction (e.g. buy, sell, etc.). If those basic transaction incur a 
processing duration (e.g. delayed by messaging over network, broker execution, etc., see [Account - Processing Durations](account.md#processing-durations)), 
then those delayed transactions are pushed to the transaction stack. Otherwise, the basic transactions are executed directly. 
The trigger can also dump orders if they expired.  
[<img src="media/order_stack.png" alt="dashboard" width="650"/>](media/order_stack.png)  
__Note:__ The transaction stack is executed before the order stack. This may affect [coverage checks](account.md#coverage).
### Use patched Accounts with FXStrategy and FXEngine
FXStrategy and FXEngine automatically take care of whether the account was patched or not. Just use them as usual.  
The patch only does affect single accounts, but not its entire class. So if one account is patched, all other accounts of 
the same class stay untouched.  
  
___  
__Note:__ Transactions placed directly via the account (e.g. buy, sell, etc.) are executed first. This also applies for 
the stack of delayed transactions. Orders get checked and executed second. This might affect coverage checks, especially the 
['Coverage at Order Time'](account.md#coverage-at-order-time)-check. As an example: if you directly withdraw via a transaction 
and simultaneously deposit via a sell order at the same time step, the withdrawal gets executed first and might cause a 
coverage violation (not sufficiently cash/ assets on account's depot). If you had done it the other way around, then 
the action pattern might have been executable (deposit via transaction and withdraw via buy order) due to the cash inflow 
of the deposit transaction, which would get executed first (all done in one time step, but even tough, there is a sequence 
of operations even within a time step...).  
Further, note that orders are not included in the ['Marked Coverage'](account.md#marked-coverage)-check and do not affect 
this check.  
___  
  
## Implement Custom Order Types  
Custom order types can be easily implemented by the user.  
### Defining the Custom Order Type
The user-defined code must be bracketed by firstly a call to _setup_order_method(...)_ and lastly by a call to 
_tear_down_order_method(...)_:  
````  
setup_order_method(func, order_lifespan_arg_name, **kwargs)

func:.....................function object, order function defined by user from which this function is called (comparable 
                          to 'this' in other programming languages)
order_lifespan_arg_name:..string, the name of the argument that denotes the order lifetime duration (e.g. in the pre-
                          implemented order types from above: 'order_lifetime_duration'). This is to avoid name mangling 
                          if the basic transaction methods from the underlying account have variables that assign a 
                          processing duration or similar. This variable must denote the lifespan in time steps, not the
                          expiration time step.
**kwargs:.................keyword argument dict, argument passed through by keywords to the underlying transactions used
                          in the custom order implementation, same as kwargs in the user-define function signature of 
                          the order type (which the user whishes to implement), see below for further information

returns bool,
        True: if order is expired
        False: else
````  
and  
````
tear_down_order_method(func, confirmation)

func:........ function object, same as in setup_order_method(...)
confirmation: int, just pass in the confirmation value of the underyling transaction of the underlying account
              All transactions must return a confirmation value. This is ensured by required by inheritance from the 
              base class trapeza.account.base_clase.BaseAccount. See FXAccount.sell, FXAccount.buy, etc. for further 
              information.
              
returns int, exaktly same as confirmation
````  
The custom order type must return the value, which is returned by _tear_down_order_method_.  
  
The simplest way to show how to implement a custom order type is to give the implementation of the stop loss order:
````python
from trapeza import exception as tpz_exception

def stop_loss(self, current_bid_rate, volume, stop_bid_price, base, quote, order_lifetime_duration, **kwargs):
    # self --> account to be patched
  
    # >>>get expiry date and do basic setup
    _ = self.setup_order_method(stop_loss, 'order_lifetime_duration', volume=volume, stop_bid_price=stop_bid_price,
                                base=base, quote=quote, order_lifetime_duration=order_lifetime_duration,
                                **kwargs)
    # notes: 'stop_loss' is just the name of our custom implementation
    #        'order_lifetime_duration' argument of the function signature is passed as string to setup_order_method
    #        kwargs are just passed through, kwargs can be e.g. processing_duration, instant_withdrawal, etc.
    #          basically all keyword arguments used in FXAccount.sell

    # #########################
    # # # start user code # # #
    # #########################
    try:  # try except is just an extra bonus and not a must have for custom implementation
        # >>>execute sell order if price drops below (or equal) stop price
        if current_bid_rate <= stop_bid_price:
            confirmation = self.sell(volume, current_bid_rate, base, quote, **kwargs)   # account.sell(...)
        else:
            confirmation = 0
    except Exception as e:
        raise tpz_exception.OperatingError(e)
    # ########################
    # # # end user code # # #
    # ########################

    # >>>handle heap depending on execution status and return confirmation status
    return self.tear_down_order_method(stop_loss, confirmation)
````   
  
  - setup_order_method() and tear_down_order_method() are patched onto the account object after invoking trapeza.account.order_management.monkey_patch 
    and are accessible via 'self'.
  - self.setup_order_method(...) has to be called before the user-defined code in order to register the custom order type  
  - self.tear_down_order_method(...) has to be called after the user-defined code to signal the end of the custom order type. 
    The return value of self.tear_down_order_method(...) has to be returned by the custom order type.  
  - All argument names used in the function signature of the custom order type must be passed to setup_order_method() as 
    keyword argument. The function itself has to be passed to setup_order_method() as well. Furthermore, the variable name, 
    which denotes the order lifespan, has to be passed as string (and as keyword argument). \*\*kwargs has to be passed as 
    \*\*kwargs to setup_order_method().  
  - The custom order type function itself must be passed to tear_down_order_method(), as well as the confirmation returned 
    by the account's transaction. According to the base class trapeza.account.base_account.BaseAccount, all transactions 
    must return a confirmation state (e.g. -1: ignored, 0: processing (processing_duration), 1: executed/ billed)  
  - 'self' represent the instance of the class and is used to access the account's methods (and those of _Order Management_ 
    if needed: self.setup_order_method and self.tear_down_order_method), e.g. self.sell, self.buy, etc. and is also used 
    in the function signature of the custom order type.  
  - \*\*kwargs can be used for all keyword arguments of the underlying transactions, e.g. FXAccount.sell has the keyword 
    (default) arguments _processing_duration_, _instant_withdrawal_, _float_fee_currency_, etc., which we do not have to 
    explicitly include into the function signature of the customer order type. Instead, we just use a \*\*kwargs in the 
    function signature and pass it to the transaction itself. We then call the function with 
    our_custom_order_type(..., processing_duration=10, instant_withdrawal=True, ...) and the named arguments will be passed 
    according to their names to the underlying self.transaction.  
  - __The first argument after 'self' in the function signature must be the current exchange rate (price)!__
  
    
### Apply the Patch to Custom Order Type
To patch custom order types, first patch the account with trapeza.account.order_management.monkey_patch. That ensures, that 
all inner mechanisms are patched onto the account (e.g. renaming the original account.tick() method into _tick() such that
the new tick method can be patched, setting up the new order stack with its automaton logic, making setup_order_method() 
and tear_down_order-method() accessible, etc.) that are needed for the 
custom order patch.  
  
The custom order type can be patched in two ways:  
  1. Doing it yourself with Python's setattr function, or  
  2. use the trapeza.account.order_management.patch() function.
  
__The latter one is recommended.__   
````  
def patch(list_funcs, safe_patch=True)

list_funcs: list of function objects (custom order types)
safe_patch: bool, if True: patch() checks if any element of list_funcs overwrites any allready existing attribute (or 
                           method). Raises an AttributeError in case any existing attribute (or method) of the account
                           would get overwritten.
                  if False: Over-patching is allowed (use with care!)
                  default: True
````  
_trapeza.account.order_management.patch()_ takes in a list of function objects (custom order types) and patches them onto 
the account. Furthermore, _patch()_ can be controlled via the argument _safe_patch_. If _safe_patch_ is set to True, then 
before patching the function checks whether any already existing attribute or method name would be overwritten and throws 
an AttributeError in this case. This ensures, that the user does not break anything. If nevertheless over-patching any 
inner attributes or methods is required to e.g. change any inner mechanisms (like the [stack automaton logic](#inner-workings-the-order-stack)), 
then _safe_patch_ can be set to False (but it is highly recommended not to do so...).  
  
After _trapeza.account.order_management.monkey_patch()_, _trapeza.account.order_management.patch()_ is directly accessible 
by the account by account.patch() as an object method.  
  
Use:
````python
from trapeza.account import FXAccount
from trapeza.account.order_management import monkey_patch, patch

# instantiate an account
acc = FXAccount('USD')

# first apply the monkey patch
monkey_patch(acc)

# then patch custom order types
acc.patch([my_custom_order_type], safe_patch=True)

# atlernatively:
patch(acc, [my_custom_order_type], safe_patch=True)
````  
_patch()_ can be either called by the account directly (_acc.patch(...)_ as an object method) or can be invoked as a 
stand-alone function. In the latter case, the account is passed as the 'self' argument (see implementation).   
If pre-implemented order types like _stop_loss_, _sell_limit_, e.g. shall not be patched by default, then _monkey_patch_ 
can be called with _only_basics=True_, which only patches all necessary stuff needed for the stack automaton logic 
of _Order Management_ (but no pre-implemented order types). Default is _monkey_patch=False_, which will also patch the 
above mentioned order types e.g. _stop_loss_ etc.  
  
__The first argument after 'self' in the function signature of the custom order type must be the current exchange rate 
(price)!__  
_Remember: custom order types have to return a confirmation state (which is prescribed by the base class BaseAccount, see 
FXAccount.sell or FXAccount.buy for concrete examples)._
### Things to note when Implementing Custom Order Types
When patching customer order types onto the account object, there are some points to note:
  - Do not overwrite any existing attributes and methods. Do not use e.g. the following names:  
    - _order_heap
    - _is_order_manager_heap
    - _is_setup_order_called
    - _is_tear_down_order_called
    - _temp_kwargs_order
    - _internal_acc_methods
    - _is_order_manager_monkey_patch
    - any method names of e.g. already implemented order types
    - any attributes or method names of the underlying account, in case of FXAccount e.g.:
      - exec_heap
      - clock
      - _ignore_type_checking
      - etc.  
      - but also all attributes and methods of the underlying Cython implementation cFXAccount e.g.:
        - _c_is_heap
        - _c_reference_currency
        - etc.  
      
    (see the concrete implementations or [API Reference](api_reference.md) for a full list of all attributes and methods)  
    When using _trapeza.account.order_management.patch()_ with the argument _safe_patch=True_, then this will be taken care 
    of automatically.
  - The account's original tick function will be renamed to _tick() in order to implement the _Order Management's_ modified 
    version of the tick method. Do not overwrite _tick().  
  - If custom account implementation are used, then they have to inherit from trapeza.account.base_account.BaseAccount.
  - Be careful with __multiprocessing__. The stack automaton logic uses internal attributes for temporarily storage and 
    internal states, which are used across different (patched internally) methods. They might get messed up by 
    multiprocessing, so always check twice!  
  
__Remember: The first argument after 'self' in the function signature must be the current exchange rate (price)!__
## Type Checking
FXAccount performs a type check at each of its methods. This checks if the input arguments are of the right data type.  
Patched functions will use the same setting for type checking like the one's of the underlying account object.  
If type checking shall be ignored, then type checking has to be turned off at the account level, e.g. instantiating 
FXAccount with _ignore_type_checking=True_. Do this before patching any account object. Turning type checking off increases 
runtime performance at the cost of safety.  
  
  
---
[Back to Top](#order-management)