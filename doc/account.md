Go back: [Table of Content](table_of_content.md)  
- [Account](#account)
  - [Introduction and Purpose](#introduction-and-purpose)
  - [Instantiate Account Object](#instantiate-account-object)
  - [Base Quote Notation](#base-quote-notation)
  - [Transaction Types](#transaction-types)
    - [Transaction: _deposit_](#transaction-deposit)
    - [Transaction: _withdraw_](#transaction-withdraw)
    - [Transaction: _buy_](#transaction-buy)
    - [Transaction: _sell_](#transaction-sell)
    - [Transaction: _transfer_](#transaction-transfer)
    - [Transaction: _collect_](#transaction-collect)
  - [Depot Status](#depot-status)
    - [Retrieving complete Depot](#retrieving-complete-depot)
    - [Retrieving single Positions](#retrieving-single-positions)
    - [Evaluating the Total Account Balance](#evaluating-the-total-account-balance)
  - [Processing Durations](#processing-durations)
    - [Specifying a Processing Duration](#specifying-a-processing-duration)
    - [Ticking the Internal Clock](#ticking-the-internal-clock)
    - [Special Case: Processing Durations at _buy_ and _sell_](#special-case-processing-durations-at-buy-and-sell)
    - [Special Case: Processing Durations at _transfer_ and _collect_](#special-case-processing-durations-at-transfer-and-collect)
    - [Inner Workings: How the stack automaton logic works](#inner-workings-how-the-stack-automaton-logic-works)
  - [Transaction Fees](#transaction-fees)
    - [Global Transaction Fees](#global-transaction-fees)
    - [Transaction-specific Fees](#transaction-specific-fees)
    - [Special Case: Fees at _transfer_ and _collect_](#special-case-fees-at-transfer-and-collect)
    - [Binding/ Injection](#binding-injection)
  - [Coverage](#coverage)
    - [Additional Debt Check at Stack Execution](#additional-debt-check-at-stack-execution)
    - [Coverage at Order Time](#coverage-at-order-time)
    - [Marked Coverage](#marked-coverage)
    - [Visualization of Marked Coverage Concepts](#visualization-of-marked-coverage-concepts)
  - [Debiting Behavior](#debiting-behavior)
  - [Stack Corruption](#stack-corruption)
  - [Type Checking](#type-checking)
  - [Transactions regarding Stocks](#transactions-regarding-stocks)
     
  
---------
# Account
Within the framework of _trapeza_, an account models a bank account/ trading account.  
________________________________  
## Introduction and Purpose
Trading accounts normally consist of a depot, where all assets (e.g. stocks, options, etc.) are stored, and an additional 
clearing account (often an instant access savings account), where all cash positions are placed to buy assets etc.  
Accounts in _trapeza_ are somehow a bit simplified in that, that there is only one depot, which holds assets and cash.    
The main functionalities of _trapeza's_ account are:  
1. __Depot__: Holds and keeps track of all positions, which can be cash or assets (or really anything else you want).  
2. __Transactions__: Financial transactions, exactly like a trading account/ bank account, to e.g. sell an asset or deposit cash. 
3. __Coverage__: Detect coverage violations (i.e. when positions would go into debt due to transactions).  
## Instantiate Account Object
Every account has a reference currency, which is defined at instantiation. This reference currency is used to calculate 
the total value of all depot positions within an account (see [Depot Status](#depot-status)). Any string can serve as 
ticker symbol for the reference currency (even fictional ones):  
````python
from trapeza.account import FXAccount
account = FXAccount('USD')
````
## Base Quote Notation  
_trapeza_ uses the direct notation (opposed to volume notation), which is common in the foreign exchange ('fx') market:  
- A currency pair is defined as __base|quote pair__ with a corresponding exchange rate.
- The exchange rate denotes how many __units of quote currency__ are exchanged __against one unit of the base currency__. 
- The mathematical unit of exchange rate is __$`[\frac{QuoteCurrency}{BaseCurrency}]`$__.
  
$`Q = B * e`$  
$`B = \frac{Q}{e}`$  
  
_Q: volume in quote currency_  
_B: volume in base currency_  
_e: exchange rate_  
  
__Example:__  
_currency pair_ &nbsp; -> &nbsp; USD|EUR  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; base: USD  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; quote: EUR  
_exchange rate_ &nbsp; -> &nbsp; e<sub>$|€</sub> = 0.82<sup>€</sup>&frasl;<sub>$</sub>  
_Q = B * e<sub>$|€</sub>_ &nbsp; -> &nbsp; 205€ = 250$ * 0.82<sup>€</sup>&frasl;<sub>$</sub>  
_B = Q : e<sub>$|€</sub>_ &nbsp; -> &nbsp; 250$ = 205€ : 0.82<sup>€</sup>&frasl;<sub>$</sub>   
  
## Transaction Types
Following transaction types are available:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.[FXAccount.deposit(volume, currency)](#transaction-deposit)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.[FXAccount.withdraw(volume, currency)](#transaction-withdraw)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.[FXAccount.buy(volume_base, ask_price, base, quote)](#transaction-buy)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;4.[FXAccount.sell(volume_base, bid_price, base, quote)](#transaction-sell)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;5.[FXAccount.transfer(payee_account, volume, currency)](#transaction-transfer)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;6.[FXAccount.collect(payer_account, volume, currency)](#transaction-collect)  
  
For the full call signature with all arguments see the [API Reference - method:trapeza.account.FXAccount](api_reference.md#method-trapezaaccountfxaccount).  
### Transaction: _deposit_
FXAccount.deposit(...) deposits a certain amount of a given currency (or asset):
````python
account.deposit(100, 'USD')     # deposit 100$ onto account
print(account.depot)            # check account's depot
# >>> {'USD': 100}

account.deposit(100, 'EUR')     # deposit 100€
print(account.position('EUR'))  # check account's €-position size
# >>> 100
print(account.depot)
# >>> {'USD': 100, 'EUR': 100}  # two positions in depot now

account.deposit(100, 'APPL')    # deposit 100 shares of Apple stocks
print(account.depot)
# >>> {'USD': 100, 'EUR': 100, 'APPL': 100}
````
### Transaction: _withdraw_
FXAccount.withdraw(...) withdraws a certain amount of a given currency (or asset):
````python
print(account.depot)
# >>> {'USD': 100, 'EUR': 100, 'APPL': 100}

account.withdraw(50, 'EUR')
account.withdraw(90, 'APPL')

print(account.depot)
# >>> {'USD': 100, 'EUR': 50, 'APPL': 10}
````
### Transaction: _buy_
  
___
__Notation:__  
&nbsp;&nbsp;&nbsp; Buy the base currency and sell the quote currency.  
&nbsp;&nbsp;&nbsp; (in other words: receive units in base currency in exchange for quote currency)  
&nbsp;&nbsp;&nbsp; In market context, buy transactions are performed at the __ask price__.  
___  
FXAccount.buy(...) buys the base currency (or asset). The base position in the account's depot will be increased, the quote 
position will be reduced. The _ask price_ is the current exchange rate at which the market is willing to exchange against 
the base currency:  
````python
print(account.depot)
# >>> {'USD': 100, 'EUR': 50, 'APPL': 10}

account.buy(10, 0.82, 'USD', 'EUR')     # buy 10$ and give €s in exchange at an ask price of 0.82[€/$]
print(account.depot)
# >>> {'USD': 110, 'EUR': 41.8, 'APPL': 10}

account.buy(1, 104.78, 'APPL', 'USD')   # buy one Apple stock at an ask price of 104.78$ (104.78[$/APPL])
print(account.depot)
# >>> {'USD': 5.22 'EUR': 41.8, 'APPL': 11}
````  
When handling indivisible assets like stocks, rounding the volume argument to next integral number must be done manually 
before invoking FXAccount.buy(...).  
### Transaction: _sell_
  
___
__Notation:__  
&nbsp;&nbsp;&nbsp; Sell the base currency and buy the quote currency.  
&nbsp;&nbsp;&nbsp; (in other words: give units in base currency in exchange for quote currency and receive quote currency)  
&nbsp;&nbsp;&nbsp; In market context, sell transactions are performed at the __bid price__.  
___  
FXAccount.sell(...) sells the base currency (or asset). The base position in the account's depot will be reduced, the quote 
position will be increased. The _bid price_ is the current exchange rate at which the market is willing to exchange in flavor of 
receiving the base currency:  
````python
print(account.depot)
# >>> {'USD': 5.22 'EUR': 41.8, 'APPL': 11}

account.sell(3, 0.82, 'USD', 'EUR')     # sell 3$ and receive €s at a bid price of 0.82[€/$]
print(account.depot)                    # 1$ is worth 0.82€ --> 3$ is worth 2.46€
# >>> {'USD': 2.22, 'EUR': 44.26, 'APPL': 11}

account.sell(2, 104.78, 'APPL', 'USD')  # sell two Apple stocks at a bid price of 104.78$ (104.78[$/APPL])
print(account.depot)
# >>> {'USD': 211.78, 'EUR': 44.26, 'APPL': 9}
````  
When handling indivisible assets like stocks, rounding the volume argument to next integral number must be done manually 
before invoking FXAccount.sell(...).  
### Transaction: _transfer_
FXAccount.transfer(...) transfers a certain volume of currency (or asset) from own account to another account. 
This reduces the position size in one's own account and increases the same position size in the account of the counterpart.  
The currency (or asset) has to be in one's own account depot, and the other account has to be an object of the class 
_trapeza.account.FXAccount_ or has to inherit from _trapeza.account.BaseAccount_:  
````python
print(account.depot)
# >>> {'USD': 211.78, 'EUR': 44.26, 'APPL': 9}

counter_account = FXAccount('EUR')                  # counterpart's account can be in any reference currency

account.transfer(counter_account, 100, 'USD')       # transfer 100 USD, no automatic conversion into counterpart's reference currency
print(account.depot)
# >>> {'USD': 111.78, 'EUR': 44.26, 'APPL': 9}
print(counter_account.depot)
# >>> {'USD': 100}

account.transfer(counter_account, 1, 'APPL')        # transfer 1 share of Apple stocks
print(account.depot)
# >>> {'USD': 111.78, 'EUR': 44.26, 'APPL': 8}
````  
Any position hold in one's own account is transferable (it's really just operating on string IDs for currencies and assets, 
they even can be fictional...).  
### Transaction: _collect_
FXAccount.collect(...) collects a certain amount of currency (or asset) for another account. The currency (or asset) has 
to be in the counterpart's account depot. The counterpart's depot has to be an object of _trapeza.account.FXAccount_ or 
has to inherit from _trapeza.account.BaseAccount_:  
````python
print(account.depot)
# >>> {'USD': 111.78, 'EUR': 44.26, 'APPL': 8}
print(counter_account.depot)
# >>> {'USD': 100, 'APPL': 1}

# exchange some USD at the counterpart's side
counter_account.sell(50, 0.82, 'USD', 'EUR')
print(counter_account.depot)
# >>> {'USD': 50, 'EUR': 41, 'APPL': 1}

account.collect(counter_account, 20, 'EUR')         # collect 20€
print(account.depot)
# >>> {'USD': 111.78, 'EUR': 64.26, 'APPL': 8}
print(counter_account.depot)
# >>> {'USD': 50, 'EUR': 21, 'APPL': 1}
````
## Depot Status
<a id="depot_status"></a>
### Retrieving complete Depot
An account object keeps track of all its position within its _depot_ attribute:
````python
print(account.depot)
# >>> {'USD': 111.78, 'EUR': 64.26, 'APPL': 8}
````   
The attribute _FXAccount.depot_ returns a protected dictionary. This dictionary is immutable (this has something to do, 
how the underlying Cython class is implemented, but we won't bother with this here...). Nonetheless, the depot attribute 
can be set by providing an entire dict:
````python
print(account.depot)
# >>> {'USD': 111.78, 'EUR': 64.26, 'APPL': 8}

account.depot['USD'] = 42       # THIS WILL THROW AN ERROR!!!

depot_copy = account.depot
depot_copy['USD'] = 42          # THIS WILL STILL THROW AN ERROR!!! we basically just copied the ProtectedDict...

depot_copy = dict(depot_copy)   # this now is mutable as we have casted it to a normal dict
depot_copy['USD'] = 42          # >>> {'USD': 42, 'EUR': 64.26, 'APPL': 8}
account.depot = depot_copy      # now the account's depot is set
print(account.depot)
# >>> {'USD': 42, 'EUR': 64.26, 'APPL': 8}

# let's get back to the old depot status
account.deposit(111.78-42, 'USD')
print(account.depot)
# >>> {'USD': 111.78, 'EUR': 64.26, 'APPL': 8}
````
### Retrieving single Positions
Furthermore, single positions can be retrieved via the FXAccount.position(...) method:
````python
print(account.position('USD'))
# >>> 111.78
````  
This only returns a single float. If the currency (or asset) specified by the string argument is not existent in the 
account's depot, then 0 will be returned.  
### Evaluating the Total Account Balance  
The total balance of an account can be easily evaluated in a given reference currency. In order to let the method know 
the current exchange rates, we have to supply a Python dict, which has to include all relevant base|quote pairs. Those 
pairs have to be either ref_currency|quote or base|ref_currency|base (which of them doesn't matter, they get converted 
in the right manner internally), wherein quote and base have to cover all positions within the account's depot:
````python
print(account.depot)
# >>> {'USD': 111.78, 'EUR': 64.26, 'APPL': 8}

total_balance = account.total_balance() # THIS WILL THROW AN ERROR: method does not know exchange rates and therefore 
#                                         not how to convert positions within depot - we have to provide proper rates

exchange_rates = {('USD', 'EUR'): 0.82,
                  ('APPL', 'USD'): 104.78}
# all account's positions are covered in the dict keys

total_balance = account.total_balance(exchange_rates=exchange_rates, reference_currency='USD')

print(total_balance)
# >>> 1028.3858536585
#   111.78$                = 111.78$
# + 64.26€ / 0.82[€/$]     =  78.3658536...$
# + 8APPL * 104.78[$/APPL] = 838.24$
#--------------------------------------
# =                         1028.3858536585$
````  
If reference_currency=None (default), then the account's reference currency (_FXAccount.reference_currency_) will be used 
for calculations.  
  
It is also possible to leave exchange_rates=None (default), but then it is necessary to call _FXAccount.batch_update_exchange_rates(...)_:  
````python
account.batch_update_exchange_rates([0.82, 104.78], [('USD', 'EUR'), ('APPL', 'USD')])
total_balance = account.total_balance()
````  
It is also possible to update just one exchange rate via _FXAccount.update_exchange_rate()_:
````python
account.update_exchange_rate(0.82, 'USD', 'EUR')
total_balance = account.total_balance()
````  
Nonetheless, this is in general not recommended as this only updates a single exchange rate and tempts to leave the other 
rates outdated by accident...
## Processing Durations 
<a id="processing_durations"></a>
In real-life situation, transaction are generally not executed instantly, but at least take some amount of time. This 
might be caused by e.g. sending/ broadcasting the order message to one's broker or bank, by time taken by the broker/ bank 
to execute the order, etc.  
_trapeza_ has the capability to model those situations by:
    - implementing an internal clock,  and
    - by specifying a processing duration for each transaction.
### Specifying a Processing Duration
Each of the aforementioned transaction methods have the keyword _'processing_duration'_:
````python
account.sell(double volume_base, double bid_price, str base, str quote, processing_duration=5)
````  
The default is processing_duration=None (=0), which means, that transactions are executed instantly.  
If processing_duration is set to an integer, the order will be executed when the internal clock reaches:
````
internal_clock_at_time_of_placing_order + processing_duration = time_of_execution
````  
In the above example, the current clock time of the account is at 0. The sell order will be executed as soon as the internal 
clock is set to 5 after 5 time steps have elapsed.  
### Ticking the Internal Clock
An FXAccount object has an internal clock, which is accessed via the _FXAccount.clock_ attribute:
````python
print(account.clock)
# >>> 0
````  
This returns the current clock time as an integer.  
FXAccount works in discrete time steps. The unit of those time steps can be anything that the user wants to (e.g. milliseconds, 
hours, days, etc.). The internal clock will elapse a discrete time step everytime the _FXAccount.tick()_ method is invoked:
````python
print(account.clock)
# >>> 0

account.tick()
print(account.clock)
# >>> 1
````  
At each tick, all previous orders, which were called with a specified processing_duration, will be checked and executed if 
the execution time step is reached.  
It is also possible to set the internal clock to a specific time step. By doing so, all transactions will be executed, which 
execution time steps are less or equal to the current internal clock:
````python
account.tick(8)
# >>> above sell order with processing_duration=5 called at account.clock=0 got executed
````  
[<img src="media/internal_clock.png" alt="dashboard" width="650"/>](media/internal_clock.png)    
___  
__DO NOT FORGET TO CALL FXAccount.tick() IF PROCESSING_DURATION IS DEFINED. OTHERWISE, TRANSACTIONS WILL NOT BE EXECUTED__  
___  
### Special Case: Processing Durations at _buy_ and _sell_
Buy and sell transactions can be decomposed into a deposit and a withdraw transaction.  
A buy transaction deposits the base currency (or asset) and withdraws the quote currency (or assets), whereas a sell 
transaction does the opposite.  
There's an addition keyword argument to manipulate the withdrawal transaction independently of the deposit transaction:  
````
instant_withdrawal: if True  --> withdrawal is executed instantly whereas depositing is done after processing_duration
                    if False --> withdrawal and depositing are executed simultaneously after processing_duration
                    default is False
````  
Usage:  
````python
# buy 1$ at an ask price of 0.82[€/$]
# 0.82€ will be withdrawn instantly
# 1$ will be deposited after 5 time steps
acc.buy(1, 0.82, 'USD', 'EUR', processing_duration=5, instant_withdrawal=True)
````  
Default is: _instant\_withdrawal=False_.
### Special Case: Processing Durations at _transfer_ and _collect_
For _transfer_ and _collect_, separate processing_durations for the payee, and the payer can be defined. Under the hood, 
those transactions get split into a _withdraw_ and a _deposit_ transaction, and therefore, two separate processing_durations 
can be defined.  
If a processing_duration is specified, the other account has to implement the following method signature with __positional (!)__ 
arguments:  
````python
def transaction(volume, currency, fee, payer_processing_duration):  # deposit or withdraw
    # some code ....
    return confirmation     # confirmation states --> 0: processing (processing_duration), 1: executed/ billed
````  
If this implementation is not available on the other account, then a trapeza.exception.AccountError will be raised (see 
[_Exceptions_](exceptions.md))  
  
If processing_duration is not specified (=None or =0), the other account is called with:  
````python
def transaction(volume, currency):  # deposit or withdraw
    # some code ...
    return confirmation     # confirmation states --> 0: processing (processing_duration), 1: executed/ billed
````  
  
The _transfer_ and _collect_ methods do not provide the keyword argument _instant\_withdrawal_. Withdrawals will be executed 
at the same time as defined by processing_duration (simultaneously with the deposit transaction at the time defined by 
processing_duration).  
Defining _instant\_withdrawal_ just wouldn't make much sense for this two transaction types:  
&nbsp;&nbsp; The withdrawal at the other account during a _collect_ transaction is highly dependent on the implementation of the _deposit()_  
&nbsp;&nbsp; method at the other account (which does not need to be FXAccount, but can also be a custom implementation, which only  
&nbsp;&nbsp; needs to inherit from BaseAccount base class). Besides that, payee_processing_duration and payer_processing_duration  
&nbsp;&nbsp; already provide a good way to control the timings of the underlying _deposit_ and _withdraw_ transactions.  
&nbsp;&nbsp; The withdrawal at the own account during a _transfer_ transaction is already defined by the keyword argument payer_processing_duration.  
&nbsp;&nbsp; The deposit at the other account during _transfer_ is highly dependent on the implementation of the _deposit()_ method at  
&nbsp;&nbsp; the other account.  
___
In order to take effect on the depot's positions, both accounts (payee and payer) have to invoke their self.tick() method 
on their own (there is no coupling of the tick()-methods between those two accounts...).
___  
### Inner Workings: How the stack automaton logic works  
Internally the logic of discrete time steps is implemented via so called _stack automaton_, also known as _pushdown automaton_.  
At each call to a transaction method, which specifies a processing_duration, the time step at which to execute the order 
is calculated. Then, the transaction order is placed onto the internal stack (_'pushdown'_). At each call to _FXAccount.tick()_, 
the internal stack will be inspected and all orders, which execution time step is less or equal to the current internal clock, 
will be executed (_'pop'_).  
If no processing_time is specified (=None or =0), then the transaction will be executed instantly without pushing it to the 
stack.  
_Sell_, _buy_, _transfer_ and _collect_ transactions are split into a basic _withdraw_ and _deposit_ order. If a fee is 
incurred, then this fee will be converted to a _withdraw_ order (see section below for details on fees). This means, that 
only _deposit_ and _withdraw_ orders are pushed down to the stack.  
The stack is implemented as a min heap data structure sorted by execution time steps (internally this heap is implemented 
as C++ vector of structs...).  
[<img src="media/stack_automaton.png" alt="dashboard" width="650"/>](media/stack_automaton.png)  
The current heap can be inspected via:  
````python
account.exec_heap
````  
which returns a protected list. Similar to the _depot_ attribute, this is an immutable view. If the heap shall be altered, 
set the _exec_heap_ attribute with a complete list, even though it is highly recommended not to do so.  
The heap structure is as follows:
````
[[int:execution_time, str:transaction_type, float:volume, str:currency, str:coverage],
 [int:execution_time, str:transaction_type, float:volume, str:currency, str:coverage],
 ...
]

with transaction_type being either 'withdraw' or 'deposit' (respectively 'c_withdraw' and 'c_deposit', but this will be 
   auto-parsed, so nay bother with this when setting the exec_heap)
with coverage being 'partial', 'except' or 'debit'
````  
## Transaction Fees
_trapeza_ is capable of modelling fees as they also might occur in real-life.  
There are two ways of defining fees:  
1. __Global Fees:__ Those fees are applied for each transaction. Global fees can be defined per transaction type separately.  
2. __Transaction-specific Fees:__ Those fees are only applied once for a single transaction.  
  
All transactions have a keyword argument _fee_, e.g.:  
````
FXAccount.buy(volume_base, ask_price, base, quote, fee=None, processing_duration=None)
FXAccount.withdraw(volume, currency, fee=None, processing_duration=None)
````  
The behavior of this keyword argument _fee_ is defined as follows:  
````
fee=None  -->  apply globally defined fee (specified for eacht transaction type)
fee=False -->  do not apply any fee, even if global fees are defined (will be skipped for this very transaction)
fee=float -->  apply a fee specified by a float value and ignore global fee for this transaction call
fee=func  -->  use a custom function to evaluate fees for this transaction call, global fees will be ignored once for 
               this transaction call
````  
### Global Transaction Fees
Global fees can be defined separately for:  
  1. _sell_ transaction  
  2. _buy_ transaction  
  3. _deposit_ transaction  
  4. _withdraw_ transaction  
  
Those fees are applied automatically at each transaction if the keyword argument _fee_ is set to _None_ (default).  
Default for globally defined fees is not applying any fees. This can be changed by providing custom fee functions at 
object instantiation of FXAccount, which then will be applied globally.
#### Function Signature for Custom Fees
In order to define globally applicable fees, custom fee functions must implement following function signature:
````python
def custom_fee(volume, price, base, quote, processing_duration):
    # some code...
    return fee_amount, fee_currency, fee_processing_duration
````  
When invoking a transaction method, the above function gets called from inside this transaction method. Therefore, 
the arguments __volume__, __price__, __base__, __quote__ and __processing_duration__ are used as keyword arguments and have 
to __match exactly regarding their naming__ when implementing custom fee functions.  
__volume__, __price__, __base__, __quote__ and __processing_duration__ are exactly the __same variables as used to call 
the transaction method__ (i.e. _FXAccount.buy(volume_base, ask_price, base, quote, fee=None, processing_duration=None)_).  
___  
__volume argument is always given in base currency!__  
__For deposit and withdraw: base and quote are the same ('currency' argument of deposit/ withdraw method).__
___  
The return values are defined as follows:  
````
fee_amount: float value, total volume of fees
fee_currency: str, specifying which currency is used to bill fee_amount (it is possible to freely choose this string and 
                   is not restricted to just only use base or quote, can really be anything)
fee_processing_duration: int, specifing a processing_duration for billing fee_amount
````  
_fee_processing_duration_ follows the same logic as _processing_duration_ of transaction methods and specifies how many 
time steps are needed to execute the fee billing on one's account. _fee_processing_duration_ and _processing_duration_ are 
handled independently (e.g. fee_processing_duration=10 and processing_duration=5 means that fees are billed 5 time steps 
after transaction has taken place, which in turn needs 5 time steps to be executed).  
#### Setting Global Fees at Instantiation  
A fee structure might be:  
````
buy:        - 1.5% of transaction volume in the quote currency, nearly instant execution
sell:       - free of charge, takes 1min for execution
deposit:    - free of charge, takes 1 day
withdraw:   - 0.5% of transaction volume takes 3 days
````
Global fee functions can be set at instantiation:  
````python
from trapeza.account import FXAccount

def custom_buy_fee(volume, price, base, quote, processing_duration):
    # volume is given in base
    # price is given as base|quote notation has the mathematical unit [quote/base]
    # volume * base converts the volume to quote
    return volume*price * 0.015, quote, 0

def custom_sell_fee(volume, price, base, quote, processing_duration):
    # fee_currency can be base or quote, does not matter as fee_amount==0
    # base unit for time steps is minutes
    return 0, base, 1

def custom_deposit_fee(volume, price, base, quote, processing_duration):
    # base unit for time steps is minutes
    # 1day = 1min * 60 * 24
    return 0, base, 1*60*24

def custom_withdraw_fee(volume, price, base, quote, processing_duration):
    # base unit for time steps is minutes
    # base==quote==currency, currency argument of withdraw method
    return volume * 0.005, base, 60*24*3


# pass function objects of custom fees to constructor
acc = FXAccount('USD',  
                fee_buy=custom_buy_fee, 
                fee_sell=custom_sell_fee, 
                fee_deposit=custom_deposit_fee, 
                fee_withdraw=custom_withdraw_fee)
# reference_currency does not interfere with base and quote arguments of fees and is only 
# used when calling self.total_balance()
````
  
After instantiation and setting global fees:  
````python
# this will apply global fees: 1.5% of the quote volume (in €) will be charged instantly, whereas the transaction volume
# of 1$ will be deposited after 2min
acc.buy(1, 0.82, 'USD', 'EUR', fee=None, processing_duration=2)

# this will ignore global fees, no fees at all will be charged. The transaction volume of 1$ will be deposited 
# after 2min.
acc.buy(1, 0.82, 'USD', 'EUR', fee=False, processing_duration=2)

# this will ignore global fees and apply the alternative fee function instead. The alternative function has to implement 
# the same function signature as explained above. The transaction volume of 1$ will be deposited after 2min.
acc.buy(1, 0.82, 'USD', 'EUR', fee=alternative_function, processing_duration=2)

# this will ignore global fees and will charge 0.01$ after the same processing_duration as the transaction volume.
# 1$ will be deposited after 2min and 0.01$ will be charged after 2min (see section about transaction-specific fees)
acc.buy(1, 0.82, 'USD', 'EUR', fee=0.01, processing_duration=2)
````  
#### Setting new Global Fees 
The current global fee setting can be inspected via the _FXAccount.\_fee_ attribute:  
````python
print(acc._fee)
# >>> {'sell': custom_sell_fee,
#      'buy': custom_buy_fee,
#      'withdraw': custom_withdraw_fee,
#      'deposit': custom_deposit_fee}
````  
This returns a protected dictionary, which entries are immutable (cf. section [_Retrieving complete Depot_](#retrieving-complete-depot)).  
Global fees can be re-set by providing an entire dict:  
````python
acc._fee = {'sell': alt_sell_fee, 'withdraw': alt_withdraw_fee}

print(acc._fee)
# >>> {'sell': alt_sell_fee,
#      'buy': custom_buy_fee,
#      'withdraw': alt_withdraw_fee,
#      'deposit': custom_deposit_fee}
````  
Not all keys (_'sell'_, _'buy'_, _'withdraw'_, _'deposit'_) have to be specified. Not-specified keys indicate to leave the 
existing entries untouched (re-use existing fee functions). Using other keys than those will throw an exception.  
### Transaction-specific Fees  
Besides defining global fees, transaction-specific fees can be used. Those transaction-specific fees will ignore globally 
set fees for once and apply their specified fees. Those transaction-specific fees can be:  
  1. an alternative function,  
  2. a float value, or  
  3. False  
#### Alternative function as fee
An alternative function can be supplied to the _fee_ keyword argument of the transaction method. This alternative function 
must implement the same function signature as global fees (see [_Function Signature for Custom Fees_](#function-signature-for-custom-fees)).  
````python
acc.buy(1, 0.82, 'USD', 'EUR', fee=alternative_function)
````  
#### Float value as fee
A float value can be passed to the keyword argument _fee_, which will ignore current global fees once for the very transaction 
method call. This is handy to model fixed fees.  
The behavior of the float fee can be set via additional keywords:
```
instant_float_fee: if True  --> float fee will be withdrawn instantly and independently from processing_duration
                   if False --> float fee will be withdrawn at the same time as the transaction gets executed specified 
                                by the keyword argument processing_duration
                   default is False
float_fee_currency: str, defines in which currency to bill the float fee
                    if None --> take base currency for billing float fee
                    default is None
```  
Usage:
````python
# buy 1$ at an ask price of 0.82[€/$]
# pay 0.01 in Japanese Yen as fees, fees are withdrawn instantly
# 0.82€ will be withdrawn instantly
# 1$ will be deposited after 5 time steps
acc.buy(1, 0.82, 'USD', 'EUR', 
        fee=0.01, instant_float_fee=True, float_fee_currency='JPY', 
        processing_duration=5, instant_withdrawal=True)
````  
#### False as fee
Setting the keyword argument _fee_ to False is equivalent to billing no fees at all. Globally set fees will be ignored 
once for this very transaction method call.  
### Special Case: Fees at _transfer_ and _collect_
The _transfer_ and _collect_ methods do not provide the keyword arguments _instant\_float\_fee_ and _float\_fee\_currency_ 
(and _instant\_withdrawal_, see [_Processing Durations - Special Case: transfer and collect_](#special-case-processing-durations-at-transfer-and-collect)).   
Float fees are processed at the same time as the underlying _deposit_ and _withdraw_ transactions. Their execution time is 
defined by the keyword argument processing_duration. Defining those keyword arguments (_instant\_float\_fee_ and _float\_fee\_currency_) 
just wouldn't make much sense for _transfer_ and _collect_ transactions.  
### Binding/ Injection 
Custom fee functions can be misused for injection. E.g. inside a custom fee function it is possible to access the account's 
functions via the keyword _self_:  
````python
def custom_fee(volume, price, base, quote, processing_duration):
    # some code...
    self.deposit(100, 'EUR')    # deposit some money
    # some code...
    return fee_amount, fee_currency, fee_processing_duration
````  
  
  
---
  
__IT IS HIGHLY RECOMMENDED NOT TO USE INJECTION!__  
This kind of code might lead to unexpected bugs, corrupt the internal stack (which holds all delayed orders; implemented 
as min heap data structure), might break in case of multi-threading (FXEngine uses multi-threading for stochastic 
backtesting), etc.  
  
---
  
  
Nevertheless, this still might be useful in some situations. Consider the following fee structure:  
````
buy:    - 1.5% of the transaction in base currency
        - 5€ additional fixed fees
        - max. 50€ fees
        - total fees shall be returned in €
        - fee is executed instantly
        
        - buy USD|BTC pair
````  
The function signature for custom fees:  
````python
def custom_fee(volume, price, base, quote, processing_duration):
    # some code...
    return fee_amount, fee_currency, fee_processing_duration
````  
The _volume_ is given in USD, the _price_ is given in \[BTC/$] (unit), _base_=='USD' and _quote_=='BTC. This means, that 
the function has no possibility to convert $ to €. In this case, we might consider to using the _FXAccount.exchange_rates_ 
attribute inside our custom fee function:  
````python
from trapeza.account import FXAccount

# define custom buy fee
def custom_fee(volume, price, base, quote, processing_duration):
    var_fee_amount = volume * 0.015     # this is in USD, volume is always in base currency
    
    # convert var_fee_amount from $ to €
    try:
        USD_EUR = self.exchange_rates['USD', 'EUR']     # unit: [€/$]
        var_fee_amount *= USD_EUR   # unit check: $ * [€/$] = €
    except KeyError:
        EUR_USD = self.exchange_rates['EUR', 'USD']     # unit: [$/€]
        var_fee_amount /= EUR_USD   # unit check: $ / [$/€] = $ * [€/$] = €
    
    # add fix fee
    fee_amount = var_fee_amount + 5     # in €
    
    # clip fee
    if fee_amount > 50:
        fee_amount = 50
        
    # price argument is not in use in this example, because its the price of USD|BTC pairs (unit [BTC/$])
    return fee_amount, 'EUR', 0

# instantiate account with globally defined buy fee
acc = FXAccount('USD', fee_buy=custom_fee, depot={'EUR': 1_000, 'BTC': 100})

# we need to update our exchange rate EVERYTIME before invoking a transaction
acc.update_exchange_rate(0.82, 'USD', 'EUR')

# call transaction method: buy 100 USD in exchange of BTC
acc.buy(100, 0.000021, 'USD', 'BTC')    # custom fee is applied automatically

# everytime before invoking buy transaction method, we have to update the exchange rate:
acc.update_exchange_rate(0.75, 'USD', 'EUR')
acc.buy(100, 0.000021, 'USD', 'BTC')    # custom fee is applied automatically
````  
  
A second possibility would be to assign a custom attribute to the account object (like a monkey patch):  
````python
from trapeza.account import FXAccount

# define custom buy fee
def custom_fee(volume, price, base, quote, processing_duration):
    var_fee_amount = volume * 0.015     # this is in USD, volume is always in base currency
    
    # convert var_fee_amount from $ to €
    var_fee_amount *= self.USD_EUR      # unit check: $ * [€/$] = €
    
    # add fix fee
    fee_amount = var_fee_amount + 5     # in €
    
    # clip fee
    if fee_amount > 50:
        fee_amount = 50
        
    # price argument is not in use in this example, because its the price of USD|BTC pairs (unit [BTC/$])
    return fee_amount, 'EUR', 0

# instantiate account with globally defined buy fee
acc = FXAccount('USD', fee_buy=custom_fee, depot={'EUR': 1_000, 'BTC': 100})

# assign custom attribute, which is used in custom_fee and stores current exchange rate
# we need to update our exchange rate EVERYTIME before invoking a transaction
acc.USD_EUR = 0.82  # unit: [€/$]

# call transaction method: buy 100 USD in exchange of BTC
acc.buy(100, 0.000021, 'USD', 'BTC')    # custom fee is applied automatically

# everytime before invoking buy transaction method, we have to update the exchange rate:
acc.USD_EUR = 0.75
acc.buy(100, 0.000021, 'USD', 'BTC')    # custom fee is applied automatically
````  
  
The above is an example on how to inject external objects, functions, etc. More complex modelling is possible through 
external injection into FXAccount object. This design pattern i.e. can be used for general callbacks. 
  
  
---  
  
As stated above:  
  
__IT IS NOT RECOMMENDED TO USE INJECTION. THIS DESIGN PATTERN IS NOT YET UNIT TESTED IN _trapeza_.  
INJECTION MIGHT CAUSE BOGUS BEHAVIOR!  
USE WITH CARE.__
  
---  
  
  
## Coverage
_trapeza_ performs coverage checks automatically. Basically, a coverage check analysis, if a transaction could go 
negative/ into debt and how to react, if this case occurs.  
There two kinds of coverage checks performed by FXAccount:
  1. __Coverage at Order Time:__ This a very basic check. At the time of invoking a transaction method, this checks whether 
     depot positions would be in theory sufficient to perform withdrawals instantly and neglecting any processing_duration. 
     Keep in mind, every transaction in _trapeza_ is decomposed into underlying _deposit()_ and _withdraw()_ transactions, 
     e.g. _sell()_ is decomposed into depositing of quote and withdrawal of base currency. It is ver basic in a sense, 
     that it does not factor in processing_durations but only checks 'baseline' coverage.  
  2. __Marked Coverage:__ This is a more complex and sophisticated check. If a transaction needs some processing_duration 
     to get executed, then this transaction does not alter the account's depot instantly but at a future time step
     (see [_Processing Durations_](#processing-durations)). If multiple transactions overlay their processing_durations, 
     then the 'Coverage at Order Time'-check might fail, and a coverage violation might occur at some future time step, 
     even if no transaction method is called at this very (future) time step (only by calling FXAccount.tick()).  
     The 'Marked Coverage'-check is capable of detecting such situations. It therefore inspects the internal stack (heap 
     data structure), where all delayed transactions are placed (thereby calling them 'marked' but not yet billed transactions).  
     
Some banks/ brokers allow debiting of accounts, some do not. For some financial products it is highly disadvantageous to 
let a position fall into negative.  
The exact behavior in case of coverage violation detection can be controlled via the keyword argument _coverage_ for 
'Coverage at Order Time' when calling a transaction method, or via the keyword _marked\_coverage_ for 'Marked Coverage' 
in the FXAccount constructor when initializing an account object.
### Additional Debt Check at Stack Execution
Furthermore, when the internal stack is executed (see stack automaton logic [_Processing Durations_](#processing-durations) 
and [_Processing Durations - Inner Workings: How the stack automaton logic works_](#inner-workings-how-the-stack-automaton-logic-works)) an additional check 
can be applied. It checks if any positions falls into a negative range during order execution. If so, a trapeza.exception.CoverageError 
(see [_Exceptions_](exceptions.md)) will be thrown. This is done independently of the above coverage checks 'Coverage 
at Order Time' and 'Marked Coverage' and represents some kind of final check-up. This check is turned off by default. 
If this check is turned on by setting _prohibit_debiting=True_ at FXAccount initialization, then at each tick all 
positions within the depot will be checked and a trapeza.exception.CoverageError will be raised if any position is 
in a negative range.  
### Coverage at Order Time
Following options are available for the keyword argument _coverage_:  
|     | 'except' | 'partial' | 'debit' | 'ignore' |
|----:|:--------:|:---------:|:-------:|:--------:|
| deposit<sup>\*</sup> | x |   | x |   |
| withdraw<sup>\*\*</sup> | x | x | x |   |
| collect<sup>\*\*\*</sup> |   |   |   |   |
| transfer<sup>\*\*\*</sup> |   |   |   |   |
| buy | x | x | x | x |
| sell | x | x | x | x |  
  
  
__<sup>\*</sup> :__ 'partial' is not available (opposed to withdraw), because it does not make much sense in the context of depositing. 
This is because, if fees are in the same currency as volume, then they will get totaled up anyways. If they are not in 
the same currency, then there is no way to adapt fees as we do not know if they are variable or fixed fees.  
__<sup>\*\*</sup> :__ The option 'ignore' is not available for _deposit_ and _withdraw_ on purpose. This is done to make 
sure that those transactions do not get overlooked by accident, as they are inherently important.  
__<sup>\*\*\*</sup> :__ _collect_ and _transfer_ use the default of _withdraw_ and _deposit_, which is 'except' (and by 
the way instant_float_fee=False as well).  
  
Their behavior is defined as follows:  
````
'except' -->   Raises trapeza.exception.CoverageError if coverage is violated, which is the case if withdrawal 
               (including fees) is greater than current depot position (in same currency as volume).
               
'partial' -->  Partial fullfilment as far as possible. Fees will be left untouched, but the withdrawal will be reduced 
               if possible. The withdrawal volume will be reduced such that withdrawal_volume + fee >= depot position. 
               As mentioned, the fee amount stays untouched. That means, that a coverage violation still might occur if
               fees are too high (i.e. theoretical reduction of withdrawal would fall into negative. Or if fees are 
               billed in a different currency then withdrawal, then there is no possibility to reduce withdrawal 
               volume). 
               Sell and buy transaction are composed of a withdrawal and a depositing transaction. The withdrawal part 
               is handeled as described above. The deposit volume will be reduced accordingly to the reduction of the 
               withdrawal volume (they'll stay in balance relative to each other). Neverthelesse, that might cause 
               paying higher fees than expected. This is caused due to the fact, that there's no way to adapt fees as we
               do not know if fees are billed as fixed fees or as variable volume-dependent fees.
               This option does not guarantee transaction fulfillment.
               
'debit' -->    Allows depot position to fall into negative range (debiting). Debiting can be caused either by the 
               withdrawal transaction or by fees. This option temporarly deactivates the 'Marked Coverage'-check. It 
               does not deactivate the internal debt check mentioned above (set by prohibit_debiting) such that even if 
               this option is set, transactions with processing_duration may not throw a trapeza.exception.CoverageError 
               at call time of this function but at least when FXAccount.tick() is called and detects a negative 
               positions. This internal debt check is turned off by default.
               
'ignore' -->   If a coverage violation occurs, the transaction is just quietly ignored instead of throwing a 
               trapeza.exception.CoverageError.

default is 'except' for all transactions methods.
````  
Usage:
````python
# buy 1$ at a ask price of 0.82[€/$]
# bill 0.01 Japanese Yen as fees
# fees are billed instantly
# if not enough € in depot, buy volume will be reduced accordingly (meaning that we receive less $ and depot position 
#   regarding €s will be 0)
# €s are withdrawn instantly
# $s will be deposited after 5 time steps
acc.buy(1, 0.82, 'USD', 'EUR', 
        fee=0.01, 
        processing_duration=5, 
        coverage='partial', 
        instant_withdrawal=True, instant_float_fee=True, float_fee_currency='JPY')
````  
'Coverage at Order Time' is checked independently of 'Marked Coverage'. A 'Marked Coverage' violation is still possible 
even if 'Coverage at Order Time' passes. So the latter one is no guarantee that the transaction will be executed.  
If a coverage violation leads to an exception (according to above described options), then a trapeza.exception.CoverageError 
will be thrown (see [_Exceptions_](exceptions.md)).  
### Marked Coverage
As mentioned above, 'Marked Coverage' checks the internal stack. This really comes into play whenever a new transaction 
is going to be placed onto the internal stack (caused by processing_duration). This avoids coverage violations for future 
executions of stacked (delayed) transactions. Long story short, this checks all marked but not yet executed transactions.  
'Marked Coverage' is set via the FXAccount constructor during initialization.  
  
Following options are available:  
````
None or False  --> No checking of marked but not yet executed transactions. This suppresses 
                   trapeza.exception.CoverageError thrown by 'Marked Coverage'. Nonetheless, 'Coverage at Order Time'
                   still might throw a trapeza.exception.CoverageError. Furthermore, during execution of the internal 
                   stack an addtional check can be applied (off by default). This checks whether debiting occurs and 
                   will throw a trapeza.exception.CoverageError.
                   
'max_forward'  --> Takes the maximum execution time step of all transaction orders, which are located on the internal 
                   stack, calculates the sum of all future withdrawal and all future depositings and checks whether the 
                   insertion of the new transaction (delayed by processing_duration) into the stack (heap data 
                   structure) will lead to a coverage violation at any given time stepe. Throws a 
                   trapeza.exception.CoverageError in case of coverage violation.
                   As we look forward up to the max time step, we call it 'max_forward'.
                   
'min_backward' --> Takes the execution time step of the transaction order (delayed by processing_duration) which shall 
                   be inserted into the internal stack. Calculates the sum of all future withdrawals and all future 
                   depositings up to this execution time step and checks whether the insertion of the new transaction 
                   into the stack will lead to a coverage violation at any given time step up to the aforementioned 
                   execution time step of the transaction which shall be inserted into the stack. Throws a 
                   trapeza.exception.CoverageError in case of coverage violation.
                   Therefore, coverage is only checked whether the new transaction can be filled without coverage 
                   violation, but does not check if transaction might lead to future coverage violation.
                   As we take the execution time step of transaction as look up horizon and effectively only evaluating 
                   backwards, we call it 'min_backward'.

default is 'min_backward'
````  
This check is done independently of the 'Coverage at Order Time'-check.  
  
Usage:
````python
acc = FXAccount('USD', marked_coverage='max_forward')
````
  
### Visualization of Marked Coverage Concepts
The following figure shows the overall concept of 'Marked Coverage' (valid for both, _'max_forward'_ and _'min_backward'_):  
[<img src="media/concept_coverage.png" alt="dashboard" width="800"/>](media/concept_coverage.png)
  
#### 'min_backward'
Next, the following figure shows, how _'min_backward'_ detects a coverage violation caused by marked but not yet executed 
transactions:  
[<img src="media/min_backward_1.png" alt="dashboard" width="800"/>](media/min_backward_1.png)  
There are cases, were _'min_backward'_ fails to detect coverage violations upfront (but as we'll see, this is expected 
behavior and on purpose): 
[<img src="media/min_backward_2.png" alt="dashboard" width="800"/>](media/min_backward_2.png)  
Up to the expected execution time of transaction Nr. 5, coverage is fully met, which makes transaction Nr. 5 executable 
at its expected execution time. Coverage is not violated until time step 10, when transaction Nr. 4 is about to be executed.  
  
The reason, why _'min_backward_' fails to detect the coverage violation on purpose is, that we want to make it possible 
that transactions arriving in the meantime (between inserting the transaction order into the internal stack, and the 
actual execution) may resolve the coverage situation:  
[<img src="media/min_backward_3.png" alt="dashboard" width="800"/>](media/min_backward_3.png)  
  
#### 'max_forward'
If we take the second figure of [_min_backward_](#min_backward) from the previous section, we can clearly see the difference 
between _min_backward_ and _max_forward_:  
[<img src="media/max_forward.png" alt="dashboard" width="800"/>](media/max_forward.png)  
_max_forward_ scans the entire internal stack and is capable of detecting future coverage complications early on. On the 
other side, _max_forward_ is noticeably more restrictive than _min_backward_.

#### 'min_backward' vs 'max_forward'
_max_forward_ guarantees, that no coverage violation will occur at any time step by scanning the entire internal stack. 
_min_backward_ is more flexible, but there might be cases, where future coverage violations will occur. Nonetheless, 
the latter provides the opportunity to resolve such future coverage violations by further incoming future transactions, 
whereas the first option might throw a premature trapeza.exception.CoverageError (and thereby clearly restricts the 
flexibility regarding the 'action space' when defining trading strategies, see main concept [_Strategy_](strategy.md)).  
  
## Debiting Behavior
There are three checks to detect and control debiting:
  1. 'Coverage at Order Time': Checks whether transaction leads to debiting if the transaction was executed immediately 
     (therefore checking against current position in depot).
  2. 'Marked Coverage': Checks whether transaction might lead to debiting according to its processing_duration (checking 
     against future depot status).
  3. Internal debt check: Checks at each FXAccount.tick() if any position is in the negative range.  
  
When set _coverage='debit'_, 'Coverage at Order Time' allows placing the transaction even if the current position in depot 
is not sufficient. This option might get intercepted by 'Marked Coverage' and the internal debt check.  
Depending on the option set for 'Marked Coverage', it is allowed to place a transaction which leads to future debts. This 
option still might get intercepted by the internal debt check.  
The internal debt check (_prohibit_debiting_ in FXAccount constructor) checks at every tick, if debts occur and can 
intercept the former two checks. This option is set off by default.  
  
All three checks are done independently of each other and only depend on their specific argument setting.  
  
Usage of _prohibit_debiting_:
````python
from trapeza.account import FXAccount
acc = FXAccount('EUR', prohibit_debiting=True)      # default =False
````  
  
To enable debiting behavior fully, always call transaction methods with _coverage='debit'_, set _marked_coverage=None_ and 
_prohibit_debiting=False_ at FXAccount instantiation.  
## Stack Corruption
If a trapeza.exception.CoverageError is thrown:  
  - by 'Coverage at Order Time', then the internal stack is still intact as no actions were taken.  
  - by 'Marked Coverage', then the internal stack is probably corrupted. _Sell_ and _buy_ transactions are split into 
    corresponding withdrawals and deposits. Only the later two will be placed on the internal stack (if processing_duration is defined). 
    This means, that when 'Marked Coverage' detects a coverage violation, transactions are already on stack are not back 
    traceable. This is especially the case for _'min_backward'_, whereas _'max_forward'_ might preserve an uncorrupted stack
    (no guarantee).  
  - by the internal debt check (_prohibit_debiting=True_), then the stack is always corrupted.  
  
A corrupted stack means, that the account will experience unexpected behavior if FXAccount.tick() is called. It is safest to 
assume a coverage error as a hard fail!  
  
If 'Marked Coverage' is turned off and a coverage violation is detected, then it is certain, that the internal stack is 
always corrupted.  
## Type Checking
FXAccount performs a type check at each of its methods. This checks if the input arguments are of the right data type. 
This can be turned off via _ignore_type_checking=True_ at instantiation of FXAccount. Default is False, therefore 
performing type checking.  
Turning this off increases runtime performance at the cost of safety:  
````python
from trapeza.account import FXAccount
acc = FXAccount('EUR', ignore_type_checking=True)      # default =False
````  
## Transactions regarding Stocks  
Even though, _trapeza_'s FXAccount class strongly focuses currencies (or any other continuously divisible assets), it is 
possible to model stock transactions (or any discrete asset). Therefore, the user has to make sure, that the transaction 
volumes are rounded appropriately to the next integral number.  
  
  
---  
[Back to Top](#account)
