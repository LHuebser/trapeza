from trapeza.account import FXAccount
from trapeza.account.order_management import monkey_patch

if __name__ == "__main__":
    # initialize an account and deposit some cash
    acc = FXAccount('USD')
    acc.deposit(10_000, 'USD')

    # patch the account with order management
    monkey_patch(acc)

    # lets place some orders
    # stop loss orders will sell 10 USD if the USD|EUR drops to or below 0.7 and expires after 3 time steps
    acc.stop_loss(0.82, 10, 0.7, 'USD', 'EUR', 3)
    # stop loss orders will sell 10 USD if the USD|EUR drops to or below 0.6 and expires after 5 time steps
    acc.stop_loss(0.82, 10, 0.6, 'USD', 'EUR', 5)
    # stop loss orders will sell 10 USD if the USD|EUR drops to or below 0.5 and expires after 8 time steps
    acc.stop_loss(0.82, 10, 0.5, 'USD', 'EUR', 8)
    # stop loss orders will sell 10 USD if the USD|EUR drops to or below 0.5 and expires after 100 time steps
    acc.stop_loss(0.82, 10, 0.5, 'USD', 'EUR', 100)

    print(acc._order_heap)

    print(acc.depot)
    # >>> {'USD': 10000.0}

    acc.tick({('USD', 'EUR'): 0.8})
    print(acc.depot)
    # >>> {'USD': 10000.0}
    # internal clock is now 1, nothing happened

    acc.tick()  # internal clock = 2
    print(acc.depot)
    # >>> {'USD': 10000.0}

    # last chance for the first order to get triggered
    acc.tick({('USD', 'EUR'): 0.65})
    print(acc.depot)
    # >>> {'USD': 9990.0, 'EUR': 6.5}
    # order got executed as expected: we sold 10$ for 6.5€

    # we also can use the args of the underlying account tick method, e.g. to fast forward the internal clock time
    acc.tick({('USD', 'EUR'): 0.55}, clock=6)  # set internal clock to 6
    print(acc.depot)
    # >>> {'USD': 9990.0, 'EUR': 6.5}
    # even though the current market price is below the stop limit of 0.6, this order did not get triggered because
    # during fast forward we skipped all of order management and the order expired in the meantime

    acc.tick({('USD', 'EUR'): 0.55})  # internal clock = 7
    acc.tick({('USD', 'EUR'): 0.55})  # internal clock = 8
    print(acc.depot)
    # >>> {'USD': 9990.0, 'EUR': 6.5}
    # the third order expired ordinarily as price just did not drop to/ below stop limit

    acc.tick({('USD', 'EUR'): 0.5})  # internal clock = 9
    print(acc.clock)
    # >>> 9
    print(acc.depot)
    # >>> {'USD': 9980.0, 'EUR': 11.5}
    # fourth order was untouched from the previous hustle and got executed as expected
