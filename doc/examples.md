Go back: [Table of Content](table_of_content.md)  
- [Examples](#examples)
  - [Legal Disclaimer](#legal-disclaimer)
  - [Relative Strength Index](#relative-strength-index)
    - [The Indicator - RSI](#the-indicator-rsi)
    - [The Strategy - RSI](#the-strategy-rsi)
    - [Fee Structure - RSI](#fee-structure-rsi)
    - [Getting the data - RSI](#getting-the-data-rsi)
    - [Putting it all together - RSI](#putting-it-all-together-rsi)
    - [Insights via Stochastic Analysis](#insights-via-stochastic-analysis)
    - [Alternative Strategies and Benchmark](#alternative-strategies-and-benchmark)
  - [Moving Average Crossings](#moving-average-crossings)
    - [The Indicator - MAC](#the-indicator-mac)
    - [The Strategy - MAC](#the-strategy-mac)
    - [Fee Structure - MAC](#fee-structure-mac)
    - [Additional Real-World Constraints - MAC](#additional-real-world-constraints-mac)
    - [Getting the data - MAC](#getting-the-data-mac)
    - [Putting it all together - MAC](#putting-it-all-together-mac)
    - [Performance Analysis](#performance-analysis)
    - [Final Remarks on SMAC Strategy Definition](#final-remarks-on-smac-strategy-definition)
  - [Floating Point Error](#floating-point-error)
  
    
---------
# Examples
In order to demonstrate how _trapeza_ can be and how _trapeza_ is supposed to be used, we now will take a look on some 
code examples:  
  1. [Relative Strength Index trading strategy](#relative-strength-index):  
     - trade only one stock,  
     - implement the Relative Strength Index indicator,  
     - define global fees to represent real-life broker/ trading conditions,  
     - use strategy key word arguments to expand the capabilities of FXStrategy class,  
     - a shallow analysis on how to improve the strategy,  
     - and benchmark different strategies.
  2. [Moving Average Crossing trading strategy](#moving-average-crossings)  
    - trade multiple stocks,  
    - implement a moving average crossing 50 and 200 days indicator and 
      bind/ assign it as an additional attribute to the FXStrategy object,  
    - define global fees in conjuncture with additional fee condition (such as every 10th trade for free),  
    - formulate additional trading constraints, which would be hard to capture with conventional backtesting frameworks 
      (i.e. only trade 4 out of 5 days),  
    - suppress the [automated ticking logic](account.md#ticking-the-internal-clock) and implement a custom discrete-time-step-loop 
      and tick routine instead,
    - utilize external Python packages to visualize our backtesting results,  
    - place trades with stop loss and start buy orders instead of simple market orders,  
    - use a second account (as reservoir to borrow cash and stocks),  
    - and use virtual positions to capture cash and stock borrowing movement from the second account (reservoir) to the 
      first account (active trading account) as well as account for interests paid on own and on borrowed positions.  
     
________________________________     
## Legal Disclaimer
_trapeza_ is written for scientific, educational and research purpose only. The author of this software and accompanying 
materials makes no representation or warranties with respect to the accuracy, applicability, fitness, or completeness of
the contents. Therefore, if you wish to apply this software or ideas contained in this software, you are taking full 
responsibility for your action. All opinions stated in this software should not be taken as financial advice. Neither 
as an investment recommendation. This software is provided "as is", use on your own risk and responsibility.  
  
Metrics provided in _trapeza.metric_ are not checked in a scientific manner and are not compared to the latest research or
to the up-to-date state-of-the-art. __Metrics implemented in this software do not state financial advice or investment 
recommendations, nor do they make any representation or warranties with respect to the accuracy, applicability, fitness, 
or completeness of the contents. Therefore, if you wish to apply this metrics or ideas contained in this software, you 
are taking full responsibility for your action.__  
  
## Relative Strength Index
The complete sample code is located at [script_example_RSI.py](script_example_RSI.py).
### The Indicator: RSI
The _Relative Strength Index_ (RSI) is a technical indicator (momentum oscillator class) and is commonly used in trading. 
The RSI puts the upwards movements of an asset in ratio to the downwards movements over a given period (commonly 14 days), 
and therefore measures the velocity and magnitude of price movements. Its values range from 0 to 100. Values below 30 are 
commonly considered as an asset being oversold, values above 70 are considered as overbought.  
_Calculation:_  
````
1. Calculate all price differences over a given time period (e.g. the last 14 days) as: 
   (price - previous price) / previous price,
   commonly daily percentage changes are used
2. Average all positive differences and all negative differences separately. Take the absolute value of the average of 
   all negative differences ('remove the sign from the negative average').
3. Calculate the final value as: RSI = avg_up / (avg_up + avg_down)
````  
The Python implementation looks as follows:
````python
from trapeza import metric
import numpy as np

def rsi(x, reference):
    """
    :param x: 1D list or array (commonly 15 elements long; 14 days lookback + current price)
    :param reference: str, {'pct_return', 'abs_return', 'log_return'},
                      used to describe how to calculate price differences/ returns
    """
    # calculate differences/ absolute changes per time step -> this is the same as calculating stock returns
    differences = metric._returns(x, reference)

    # calculate average of positive gains
    avg_up = np.sum(differences[differences > 0]) / len(differences)

    # calculate average of negative gains
    avg_down = np.abs(np.sum(differences[differences < 0]) / len(differences))

    return avg_up / (avg_up + avg_down)
````
### The Strategy: RSI
The strategy is really simple regarding the interpretation of the RSI indicator:  
  - if an asset is oversold we assume a recovery,  
  - if an asset is overbought we assume a regression of the price.  
  
To reduce false positive signals, we adjust our thresholds to 20 and 80 instead of the commonly used 30 and 70 values.  
  
Regarding the position management, we will take the following approach:  
  - The stronger oversold the asset is, the stronger the recovery will be. Therefore, we enter the buy position and scale 
    the buy volume according to the level of 'oversoldness':   
    __[(0.2 - RSI) / 0.2] * current cash position__.  
  - We take a similar approach when selling a position and scale the sell volume according to the level of 'overboughtness':   
    __[(RSI - 0.8) / 0.2] * current asset position__.
    
Basically, we just take our current cash or asset position and scale it according to how strong the RSI below 20 or over 80.  
We will use a time period of 14 days to calculate the RSI, therefore we have a lookback value of 14 when defining our strategy.  
  
The Python implementation using FXStrategy looks as follows:
````python
import numpy as np

def rsi_strategy(accounts, price_data_pt, reference_currency, volume_data_pt, fxstrategy, rsi_method, rsi_reference,
                 tick_symbol):
    """
    :param accounts: list of account objects (inherited from BaseAccount)
    :param price_data_pt: dict,
                          key: tuple of currency strings
                          value: list of exchange rates (floats) as time series, 
                                 last value in list is the most recent one (current time step), 
                                 length=lookback+1, will be explained later on
    :param reference_currency: str, 'EUR'
    :param volume_data_pt: same format as price_data, but with volumes instead of exchange rates, not used in this 
                           example
    :param fxstrategy: FXStrategy object, basically used to imitate 'self'
    :param rsi_method: function object, rsi
    :param rsi_reference: str, {'pct_return', 'abs_return', 'log_return'},
    :param tick_symbol: str, 'ADS.DE'
    """
    # first calculate RSI
    rsi_signal = rsi_method(price_data_pt[tick_symbol, reference_currency], rsi_reference)

    # read current asset position and cash position
    asset = accounts[0].position(tick_symbol)   # in number of stocks
    cash = accounts[0].position(reference_currency)     # in reference currency (€)

    current_price = price_data_pt[tick_symbol, reference_currency][-1]

    # make trading decision and add signal
    if rsi_signal > 0.8 and asset > 0:    # over-bought, only sell if we have a valid position size
        # we sell proportional to the signal strength
        strength = (rsi_signal-0.8) / 0.2
        # we have to round to the next integral number of stocks
        sell_volume = np.floor(asset*strength)
        accounts[0].sell(sell_volume, current_price, tick_symbol, reference_currency)
        fxstrategy.add_signal(accounts[0], 'sell_{}_{}'.format(tick_symbol, reference_currency))
    elif rsi_signal < 0.2 and cash > 70:  # over-sold, only buy if we have enough cash to cover fees
        # we buy proportional to the signal strength
        strength = (0.2-rsi_signal) / 0.2
        # we have to round the number of stocks to integral number
        # we hold out 70€ to be sure that we can cover fees
        buy_volume = np.floor((strength * (cash-70)) / current_price)
        accounts[0].buy(buy_volume, current_price, tick_symbol, reference_currency)
        fxstrategy.add_signal(accounts[0], 'buy_{}_{}'.format(tick_symbol, reference_currency))
````
Things to note:  
  - We only buy the asset if we have at least 70€ cash in order to cover our fees  
  - We round to the next integral number as we can only trade whole stocks and no partial stocks  
  - We will use a lookback of 14 days, the most current price is located at index -1  
  - We add signals for latter visualization. All signals contain the tag words 'sell/ buy', the ticker symbol ('ADS.DE') 
    and the quote currency ('EUR')  
  - We do not need to use volume data for this strategy (simplifying assumption: there is enough market liquidity to fill 
    our trades such that we have to care about volumes...)  
    
  
__Use of strategy kwargs:__
In the call signature of _rsi_strategy()_, we use additional keyword arguments (rsi_method, rsi_reference, tick_symbol). 
Those keyword arguments will be passed to the constructor of _FXStrategy_, which in turn passes them to our _rsi_strategy()_ 
function. Note: Those keyword arguments can be variables (e.g. tick_symbol), function object (e.g. rsi_method) or anything 
else, but are static in a sense, that _FXStrategy_ will always pass the values received at the constructor. Changes to 
those keyword arguments within the strategy decision function will not persist in the next iterations and will be overwritten 
by the original values (passed at constructor) as soon as _FXStrategy_ call the strategy decision function internally again.  
### Fee Structure: RSI
We define a fee function which is passed to FXAccount at instantiation. Therefore, the fee function will automatically be 
applied at every sell and buy transaction (without setting any arguments explicitly). That is why we call them 'global fees'.  
  
The fee structure at ING DiBa is e.g.:
````
- 0.25% of trade volume as variable costs
- 4.90€ fix costs
- 69.90€ at maximum
````  
_FXAccount_ needs a specific [function signature for fees](account.md#function-signature-for-custom-fees).  
The Python implementation for this:
````python
def fee(volume, price, base, quote, processing_duration):
    """
    :param volume: float, trading volume (transaction) of asset
    :param price: float
    :param base: str, asset ticker symbol
    :param quote: str, 'EUR'
    :param processing_duration: int, processing duration passed from FXAccount.sell or FXAccount.buy, which we will not
           use for this example
    """
    # calculate volume in currency (quote)
    cash_equivalent_volume = volume * price

    fee_amount = 0.0025 * cash_equivalent_volume + 4.9
    fee_amount = min([fee_amount, 69.9])

    return fee_amount, quote, 0
````  
Things to note:  
  - We assume that transactions are processed immediately  
  - Our quote (and therefore also our reference currency) is in 'EUR'  
  - The volume argument is always in base (asset/ stock), see [function signature for fess in FXAccount](account.md#function-signature-for-custom-fees)  
### Getting the data: RSI
We will use the package _pandas_datareader_ in order retrieve course data from _yahoo finance_.  
To simplify latter code, we will write a separate function for this:  
````python
from pandas_datareader import data

def build_data(ticker_symbols, quote, start_data, end_date):
    """
    :param ticker_symbols: list of ticker symbols
    :param quote: quote currency (in our case 'EUR')
    :param start_data: str, YYYY-MM-DD date format
    :param end_date: str, YYYY-MM-DD date format
    """
    _price_data = dict()
    _dates = False

    for _ticker in ticker_symbols:
        ticker_price_data = data.DataReader(_ticker, start=start_data, end=end_date,
                                            data_source='yahoo')['Close'].reset_index().drop_duplicates('Date', 'last')
        if _dates is False:     # only set _dates once
            _dates = ticker_price_data['Date']
        ticker_price_data = ticker_price_data['Close'].to_numpy()
        _price_data[_ticker, quote] = ticker_price_data

    return _dates, _price_data
````  
Things to note:  
  - We convert the price data to numpy arrays for easier handling  
  - We leave the dates as pandas series for latter visualization purposes  
  - We only use the close prices  
### Putting it all together: RSI
We will trade the stocks of the sport equipment manufacturer 'Adidas' (ADS.DE) over a time period of two years with the 
above defined strategy and fees. We will start with an initial depot of 5,000€. We define a lookback of 14 day in order 
to obtain a RSI value, which is calculated over the past 14 days:  
````python
# we have to manipulate context first before doing anything else
from trapeza import context
context.ARBITRARY_QUANTIZE_SIZE = 3     # restrict to three after decimal places

from trapeza.account import FXAccount
from trapeza.strategy import FXStrategy
from trapeza.engine import FXEngine
from trapeza.dashboard import FXDashboard
from trapeza import metric

import numpy as np
from matplotlib import pyplot as plt
# noinspection PyPackageRequirements
import seaborn as sns
from pandas_datareader import data
import time

if __name__ == "__main__":
    # get the data
    ticker = 'ADS.DE'
    dates, price_data = build_data([ticker], 'EUR', '2019-05-12', '2021-05-12')
    dates = dates.to_numpy()

    # init account object and deposit 5,000€
    acc = FXAccount('EUR', fee_buy=fee, fee_sell=fee, ignore_type_checking=True)
    acc.deposit(5000, 'EUR')

    # init strategy object
    strat = FXStrategy('rsi-14-pct', acc, rsi_strategy, 14,
                       rsi_method=rsi, rsi_reference='pct_return', tick_symbol=ticker, ignore_type_checking=True)

    # run strategy
    start_time = time.perf_counter()
    strat.run(price_data, 'EUR')
    end_time = time.perf_counter()
    print('Elapsed time for single strategy execution: {}'.format(end_time-start_time))
````  
__Note__: We import the [context](arithmetics.md) first and set the number of decimal places (numbers after the radix point) 
to 3. This has to be done before any other imports of _trapeza_ happen!  
We use a lookback of 14 days in order to obtain a RSI value for the last 14 days.  
The constructor uses additional keywords 'rsi_method', 'rsi_reference' and 'tick_symbol', which will be passed to our 
strategy decision function _rsi_strategy()_. Every time _rsi_strategy()_ is called internally, the values of those 
keyword arguments will be passed, which were passed to the constructor. Changes within _rsi_strategy()_ to those 
keyword arguments are not persistent and will be overwritten by the original values (passed to constructor) at the next 
time step (which is when _FXStrategy_ calls _rsi_strategy()_ iteratively).  
  
To run the strategy over a time period of two years of daily data, we need around _0.02571 seconds_. Not toooooo bad...  
  
Next we visualize the results:  
````python
    # visualize results
    # underlying
    plt.figure()
    plt.plot(dates, price_data[ticker, 'EUR'], color='black')
    plt.title('Underlying')
    plt.xticks(rotation=90)
    plt.show()
    # total value of strategy
    plt.figure()
    plt.plot(dates[14:], strat.merged_total_balances, label='strategy', color='blue')
    plt.ylabel('EUR')
    plt.legend()
    plt.xticks(rotation=90)
    # total value of strategy performance, price data performance
    plt.figure()
    plt.plot(dates[14:], (np.array(strat.merged_total_balances)/5000)*100, label='strategy', color='blue')
    plt.ylabel('% indexed to strategy start [t_0=14]')
    plt.plot(dates, (price_data[ticker, 'EUR']/price_data[ticker, 'EUR'][14])*100, label='ads|EUR', color='black')
    plt.legend(loc='upper right')
    plt.xticks(rotation=90)
    # positions
    plt.figure()
    plt.stackplot(dates[14:],
                  strat.merged_positions['EUR'],
                  np.array(strat.merged_positions[ticker])*np.array(price_data[ticker, 'EUR'][14:]),
                  baseline='zero', labels=['EUR', 'ads'], colors=['yellow', 'indigo'])
    plt.legend()
    plt.xticks(rotation=90)
    # price data, signals, rsi
    _, ax1 = plt.subplots()
    ax1.plot(dates, price_data[ticker, 'EUR'], label='ads|EUR', color='black')
    # get all indices of buy and sell transactions
    buy_indices = [i + 14 for i in range(len(strat.signals)) for s in strat.signals[i][0] if 'buy' in s]
    sell_indices = [i + 14 for i in range(len(strat.signals)) for s in strat.signals[i][0] if 'sell' in s]
    ax1.scatter(dates[buy_indices], price_data[ticker, 'EUR'][buy_indices], label='buy', color='green', s=20)
    ax1.scatter(dates[sell_indices], price_data[ticker, 'EUR'][sell_indices], label='sell', color='red', s=20)
    ax1.set_ylabel('EUR', color='black')
    plt.legend(loc='upper left')
    plt.xticks(rotation=90)
    ax2 = ax1.twinx()
    rsi_vals = np.array([rsi(price_data[ticker, 'EUR'][i-15:i], 'pct_return')*100 for i in range(15, len(dates)+1)])
    ax2.plot(dates[14:], rsi_vals,
             label='rsi', color='grey', linestyle=':')
    ax2.fill_between(dates[14:], rsi_vals, y2=20, where=rsi_vals <= 20, interpolate=True, facecolor='green', alpha=0.25)
    ax2.fill_between(dates[14:], rsi_vals, y2=80, where=rsi_vals >= 80, interpolate=True, facecolor='red', alpha=0.25)
    ax2.hlines([80, 20], colors=['grey', 'grey'], linestyles=['-.', '-.'], xmin=dates[14], xmax=dates[-1])
    ax2.set_ylabel('%', color='grey')
    plt.legend(loc='upper right')
    plt.xticks(rotation=90)
````  
_Plotting via matplotlib is a bit tedious, feel free to use any other external package for visualization._  
See [FXStrategy - Retrieving Results](strategy.md#retrieving-results) for details on how FXStrategy's attributes, which 
hold all results, are structured.  
  
We get the following results:  
Adidas Stock over the last two years:  
[<img src="media/example_rsi_underlying.png" alt="underlying" width="650"/>](media/example_rsi_underlying.png)  
  
Our strategy (RSI 14 days using daily percentage returns):  
[<img src="media/example_rsi_total_balance.png" alt="total_balance" width="650"/>](media/example_rsi_total_balance.png)  

As we can see, our strategy makes some money. But how much better is our strategy against the sole stock? To compare both, 
we index all values to the start date of the strategy execution (strategy total balance divided by 5,000€ and all stock 
prices divided by the stock price when the strategy execution started -> this at index 14 because of the lookback value 
defined at FXStrategy instantiation):  
[<img src="media/example_rsi_performance_indexed.png" alt="performance_indexed" width="650"/>](media/example_rsi_performance_indexed.png)  
  
As we can see, our strategy performs quite okayish against the stock.  
Let's take a look when and where we sold and bought the stock:  
[<img src="media/example_rsi_signals.png" alt="signals" width="650"/>](media/example_rsi_signals.png)  
The resulting portfolio allocation:  
[<img src="media/example_rsi_asset_allocation.png" alt="allocation" width="650"/>](media/example_rsi_asset_allocation.png)  
We see, that most of the time, we are just holding cash. This is an indication, that our strategy is a bit too passive. 
The low number of portfolio re-allocations (buy and sell) probably will cause that the performance will vary quite strongly 
for short evaluation time spans (meaning that our strategy is not very reliable for short term trading...).  
### Insights via Stochastic Analysis  
In order to validate our above assumptions, we can use _FXEngine_ to run a simulation over different time spans of being 
invested in our strategy. We therefore use a larger data basis of 22 years (_'build_data()'_). We will simulate 100 runs 
and define, that each run must at least contain 60 days (and at max 22 years...):  
````python
    # perform stochastic analysis
    # first get more data
    dates, price_data = build_data([ticker], 'EUR', '1999-05-12', '2021-05-12')

    # reset strategy
    strat.reset()

    # init an engine object
    engine = FXEngine('rsi_engine', strat, ignore_type_checking=True, cache_dir='__cache__')
    # alternative cache_dir='__system__' or custom path

    # run the engine
    start_time = time.perf_counter()
    engine.run(price_data, 'EUR', min_run_length=60, max_total_runs=100)

    # analyze the engine
    engine.analyze()
    end_time = time.perf_counter()
    print('Elapsed time for 100 simulation runs and metrics calculations: {}'.format(end_time-start_time))
````  
__Note__: As in the code examples above, we set _ignore_type_checking=True_ in order to increase performance and avoid 
extra checks. Please use this option with care.  
  
Simulating 100 runs of our strategy takes _8 seconds_.  
  
We will now visualize the distribution of total returns of our strategy:  
````python
    # we are interested in the total return
    total_returns = [engine.standard_analysis_results['rsi-14-pct'][w, s]['total_rate_of_return']
                     for w in engine.run_register['rsi-14-pct'].keys()
                     for s in engine.run_register['rsi-14-pct'][w]]
    window_lengths = [w for w in engine.run_register['rsi-14-pct'].keys() for _ in engine.run_register['rsi-14-pct'][w]]

    # visualize
    # histogram and density
    plt.figure()
    sns.distplot(total_returns, hist=True, kde=True, kde_kws={'linewidth': 3})
    plt.title('Distribution of total return')
    plt.ylabel('Density')
    plt.xlabel('Total return')
    print('Average annualized total return: {} %'.format(np.average(total_returns)*100))
````  
__Note__: We use FXEngine.standard_analysis_results' _total_rate_of_return_ metric. This metric is annualized (given that 
data is daily) and is returned in percentage. This makes the results regarding time periods of differing lengths comparable 
to each other.  
See [FXEngine - Analyze the Engine](engine.md#analyze-the-engine) for further details on available metrics.  
  
[<img src="media/example_rsi_distribution_total_return.png" alt="distrib" width="650"/>](media/example_rsi_distribution_total_return.png)  
The figure shows that on average we get a total return of 2.62% (not too good...). This average is calculated over different 
dates at which we entered the market, as well as different time spans of how long we stick to our strategy.  
  
We can also visualize, whether the total return of our strategy is dependent on how long we follow our strategy and whether 
the total return converges to a certain value if we execute our strategy for a longer time frame (this is the window length 
of the data sub-sample on which we run our strategy):  
````python
    # yield curve
    plt.figure()
    plt.scatter(window_lengths, total_returns, s=15)
    plt.title('Total return over window length')
    plt.xlabel('Window length (sample length for strategy execution)')
    plt.ylabel('Total return')
    plt.show()
````
[<img src="media/example_rsi_yield_curve.png" alt="yield" width="650"/>](media/example_rsi_yield_curve.png)  
As the figure shows, our strategy gives quite mixed results when executed for only a couple of days but converges towards 
2.62% quite consistently when we execute our strategy over a longer time period (which doesn't make our 2.62% less poor 
performing...).  
  
The above visualization examples show, how we can retrieve and interpret results from _FXEngine_ without using the dashboard.  
See also [FXEngine - Retrieve Results](engine.md#retrieving-results).  
  
If we take a deeper look at the portfolio allocation, we might conclude that our initial strategy is somehow slightly 
too aggressive when selling a position. On the long run, stock tend to increase about 5-7% p.a. on average. Based on this 
assumption we might formulate a strategy, which is a bit more inert regarding a selling signal (biased strategy so to speak).  
We also might try, whether calculating the RSI based on logarithmic returns instead of percentage returns might lead to 
increased performance (i.e. better signals).  
### Alternative Strategies and Benchmark
We formulate three alternative strategies, which we will benchmark against each other:
  - __Biased RSI strategy__: we will factor in some inertia w.r.t. the sell signal (RSI above 80). This done by choosing a 
    different scaling factor for our sell volume: __[(RSI - 0.8) / 0.27] * current asset position__. We change the 
    denominator from _0.2 (20)_ to _0.27 (27)_ to factor in the 7% average price gain of the stock market.  
    To implement this strategy, we have to re-write our strategy decision function.  
  - __Log return based RSI strategy__: instead of percentage returns, we will calculate the RSI based on logarithmic returns 
    in order to see if this gives better signals.  
    To implement this strategy, we just have to change the keyword arguments during FXStrategy instantiation, but no need 
    to re-write anything.  
  - __Buy and hold strategy__: As a baseline for our benchmark, we will also analyze a simple buy-and-hold strategy, which 
    buys the stock exactly once at the first time step with the total of the cash position.  
    
#### Biased RSI strategy
Our new strategy decision function looks like follows:  
````python
def rsi_strategy_bias(accounts, price_data_pt, reference_currency, volume_data_pt, fxstrategy, rsi_method,
                      rsi_reference, tick_symbol):
    # first calculate RSI
    rsi_signal = rsi_method(price_data_pt[tick_symbol, reference_currency], rsi_reference)

    # read current asset position and cash position
    asset = accounts[0].position(tick_symbol)   # in number of stocks
    cash = accounts[0].position(reference_currency)     # in reference currency (€)

    current_price = price_data_pt[tick_symbol, reference_currency][-1]

    # make trading decision and add signal
    # noinspection DuplicatedCode
    if rsi_signal > 0.8 and asset > 0:    # over-bought, only sell if we have a valid position size
        # we sell proportional to the signal strength
        strength = (rsi_signal-0.8) / 0.27  # --> !
        # we have to round to the next integral number of stocks
        sell_volume = np.floor(asset*strength)
        accounts[0].sell(sell_volume, current_price, tick_symbol, reference_currency)
        fxstrategy.add_signal(accounts[0], 'sell_{}_{}'.format(tick_symbol, reference_currency))
    elif rsi_signal < 0.2 and cash > 70:  # over-sold, only buy if we have enough cash to cover fees
        # we buy proportional to the signal strength
        strength = (0.2-rsi_signal) / 0.2
        # we have to round the number of stocks to integral number
        # we hold out 70€ to be sure that we can cover fees
        buy_volume = np.floor((strength * (cash-70)) / current_price)
        accounts[0].buy(buy_volume, current_price, tick_symbol, reference_currency)
        fxstrategy.add_signal(accounts[0], 'buy_{}_{}'.format(tick_symbol, reference_currency))

# ...
# ...
# ...

strat_bias = FXStrategy('rsi-bias', acc, rsi_strategy_bias, 14,
                        rsi_method=rsi, rsi_reference='pct_return', tick_symbol=ticker, ignore_type_checking=True)
````  
#### Log Return RSI Strategy
We do not need to re-write our strategy decision function. We just change the keyword arguments when instantiating 
_FXStrategy_:
````python
strat_log = FXStrategy('rsi-14-log', acc, rsi_strategy_bias, 14,
                       rsi_method=rsi, rsi_reference='log_return', tick_symbol=ticker, ignore_type_checking=True)
````  
_rsi_reference='log_return'_ gets passed through to _rsi_strategy(accounts, price_data_pt, reference_currency, 
volume_data_pt, fxstrategy, rsi_method, rsi_reference, tick_symbol)_ which then uses logarithmic returns instead of 
percentage returns to calculate the RSI.
#### Buy-and-Hold Strategy
Our buy-and-hold strategy is rather simple to define:
````python
def buy_and_hold_strategy(accounts, price_data_pt, reference_currency, volume_data_pt, fxstrategy, tick_symbol):
    cash = accounts[0].position(reference_currency)  # in reference currency (€)
    current_price = price_data_pt[tick_symbol, reference_currency][-1]
    if cash > 70:
        buy_volume = np.floor((cash - 70) / current_price)
        accounts[0].buy(buy_volume, current_price, tick_symbol, reference_currency)
        fxstrategy.add_signal(accounts[0], 'buy_{}_{}'.format(tick_symbol, reference_currency))

# ...
# ...
# ...
strat_buy_and_hold = FXStrategy('buy-and_hold', acc, buy_and_hold_strategy, 14, tick_symbol=ticker)
````  
As we use the entire cash position (minus max fees), the buy transaction should only be conducted exactly once at the 
first time step.  
#### Putting it all together: RSI Alternatives
````python
    # benchmark other strategies
    strat_bias = FXStrategy('rsi-bias', acc, rsi_strategy_bias, 14,
                            rsi_method=rsi, rsi_reference='pct_return', tick_symbol=ticker, ignore_type_checking=True)
    strat_log = FXStrategy('rsi-14-log', acc, rsi_strategy_bias, 14,
                           rsi_method=rsi, rsi_reference='log_return', tick_symbol=ticker, ignore_type_checking=True)
    strat_buy_and_hold = FXStrategy('buy-and_hold', acc, buy_and_hold_strategy, 14, tick_symbol=ticker)

    engine_new = FXEngine('benchmark_engine', [strat, strat_bias, strat_log, strat_buy_and_hold], cache_dir='__cache__',
                          ignore_type_checking=True, n_jobs=-1)

    start_time = time.perf_counter()
    engine_new.run(price_data, 'EUR', min_run_length=60, max_total_runs=300)
    engine_new.analyze()
    end_time = time.perf_counter()
    print('Elapsed time to benchmark strategies over 300 simulations: {}'.format(end_time-start_time))

    dash = FXDashboard(engine_new, {'buy': ('triangle-up', 'Green'), 'sell': ('triangle-down', 'Red')}, dates, 'full')
    dash.run(debug=False)
    engine.close()      # this the engine from our initial experiments which have to close at the end
    engine_new.close()
````  
We put all strategies into a new engine object and use 300 simulations, from which each run has to be at least 60 time 
steps long. Afterwards we use _FXDashboard_ to visualize the simulation results. Even tough _trapeza_ is not primarily 
designed for sophisticated visualization, this is somehow more convenient than writing all the matplotlib code...  
The code snippet above creates a dashboard which can be accessed via any browser (at localhost e.g. http://127.0.0.1:8050/, 
the exact address should be output at your console).  
  
This code snippet takes _64.22 seconds_. _trapeza_ is not primarily designed for computational speed (there are quite a lot 
of good backtesting packages which focus on exactly that, but may fall short at other aspects, where _trapeza_ shines), 
but considering, that we run 12,000 simulations in total (4 strategies * 300 runs per strategy) over an average period of 
approximately 2,740 days (approx. 11 years, note min run length of 60 days) this seems to be okayish performance-wise 
(that are 3.3 million time steps of simulation..., which is 54,800 time step per second or 217 years per second of simulation). 
Also consider, that _trapeza_ handles fees automatically and performs coverage checks to see if we would debit our account 
\- this is all done while running the actual simulations!  
  
Let's first compare the RSI strategies and leaving the buy-and-hol strategy out for a moment:  
[<img src="media/example_rsi_benchmark_rsi.png" alt="benchmark_rsi" width="650"/>](media/example_rsi_benchmark_rsi.png)  
We can indeed see, that our bias idea is working out! Staying on the buying side seems to increase the overall strategy 
performance (measured in the total value of the account's depot).  
Our RSI calculation based on logarithmic returns seems to work out even better. This is not too surprising though. Stock returns 
are usually roughly better fitted to log-norm distribution than to a normal gaussian distribution, which might explain better 
RSI signals when using log returns:  
[<img src="media/example_rsi_daily_rets.png" alt="daily_returns" width="650"/>](media/example_rsi_daily_rets.png)  
The above figure depicts the distribution of daily returns. We can see, that our RSI strategies have a 'sharper' (more 
narrow) distribution and therefore a lower volatility than the pure buy-and-hold strategy.  
  
Buuuut, is a RSI strategy really that better than a simple buy-and-hold strategy?  
The answer is.... NO!  
[<img src="media/example_rsi_benchmark_buy_and_hold.png" alt="benchmark_buy_and_hold" width="650"/>](media/example_rsi_benchmark_buy_and_hold.png)  
Weeeell, that did not turn out that well... A simple buy-and-hold is way more profitable on the long run than a rather 
complex (and more time-consuming regarding managing transactions as well as more costly due to transaction fees) RSI 
strategy. The performance is multiple times better, but the volatility is higher as well:  
[<img src="media/example_rsi_benchmark_exp_ret_vola.png" alt="benchmark_exp_ret_vola" width="650"/>](media/example_rsi_benchmark_exp_ret_vola.png)  
The above figure shows, that a buy-and-hold strategy is better in performance but also much more volatile. We also can 
see that our volatility and return metrics seem to converge to steady value the longer we run a strategy (radius). 
At this point, there is only one thing left to note: 'Greetings from the efficient market theory' ;-)  
  
All in all, we were able to backtest, benchmark and compare different strategies against each other. Our analysis factors 
in realistic circumstances such as fees and coverage checks. We were able to identify the best alternatives regarding the 
design of a RSI strategy and to check whether we could beat the market (which we could not... jaaaaa no free lunch here...).  
  
The console output of the script can be found at: [output_script_example_RSI.txt](media/output_script_example_RSI.txt).  
  
## Moving Average Crossings  
The complete code is located at [script_example_smac.py](script_example_smac.py).
### The Indicator: MAC
The _Moving Average Indicator_ (MA) is commonly used in technical analysis. This indicator is also called rolling average, moving mean 
or running average. It smooths out the underlying's price by constantly updating the average price over a specific period. 
The _Simple Moving Average_ (SMA) takes the unweighted arithmetic mean over the specific number of days (in contrast to e.g. 
an exponentially weighted mean). In this example we will use the SMA.  
Usually a fast and a slow SMA's crossover is used to identify appropriate trading signals. If e.g. the SMA line with a 
window length (number of days) of 50 (SMA_50) crosses a SMA line with a window length of 200 (SMA_200) from above, then 
this is interpreted as a _Death Cross_ (sell signal). When SMA_50 crosses SMA_200 from below, then this called a _Golden 
Cross_ (buy signal). In general, the shorter the window length of a SMA, the faster it reacts to current price changes and 
the lesser it is smoothed out.  
  
For demonstration purpose, we will implement our own class for continuously calculating SMAs and an own class for the 
detection of SMAs crossings (SMAC).  
Implementing a continuous SMA class is pretty straight-forward (doing continuously calculation based on a per-time-step-logic 
sticks to the overall logic of _trapeza_ of ticking an internal clock in discrete time steps. On the other hand, calculations 
could be speed up by using e.g. numpy convolutions to make use of vectorized computations and to compute all values at 
once...):  
````python
import numpy as np

class SMA:
    def __init__(self, window_length, ticker_symbol, reference_currency, price_data, t_current=None):
        self.window_length = window_length
        self.ticker = ticker_symbol
        self.reference_currency = reference_currency

        if t_current is None:
            self.t = self.window_length - 1
        else:
            self.t = t_current

        self.sma = np.average(price_data[self.ticker,
                                         self.reference_currency][self.t - self.window_length + 1: self.t + 1])

        # this is for latter visualization purpose only
        self.sma_data = list()
        for i in range(self.window_length, len(price_data[self.ticker, self.reference_currency]) + 1):
            self.sma_data.append(np.average(price_data[self.ticker, self.reference_currency][i - self.window_length:i]))
        self.sma_data = np.array(self.sma_data)

    def update(self, price_data, t=None):
        if t is None:
            self.t += 1
            self.sma = self.sma + ((price_data[self.ticker, self.reference_currency][self.t]
                                    - price_data[self.ticker, self.reference_currency][
                                        self.t - self.window_length]) / self.window_length)
        else:
            self.t = t
            self.sma = np.average(price_data[self.ticker,
                                             self.reference_currency][self.t - self.window_length + 1: self.t + 1])
````
The class for detecting SMAC uses the above SMA class, on class object for a fast SMA_50 and on class object for a slower 
SMA_200. It returns -1 as sell signal, 1 as buy signal and 0 as neutral at every time step when calling the _update()_ 
method (which calculates the SMAs for the next time step and checks for crossings):  
````python
class SMAC:
    def __init__(self, window_length_fast, window_length_slow, ticker_symbol, reference_currency, price_data,
                 t_current):
        self.sma_fast = SMA(window_length_fast, ticker_symbol, reference_currency, price_data, t_current)
        self.sma_slow = SMA(window_length_slow, ticker_symbol, reference_currency, price_data, t_current)

        self.ticker = ticker_symbol
        self.reference_currency = reference_currency
        self.t = t_current

        # this is for latter visualization purpose only
        self.signals = list()
        for i in range(t_current + 1, len(price_data[ticker_symbol, reference_currency])):
            self.signals.append(self.update(price_data))

        # we have to reset our internal variables (which were altered in the above snippet, which is used for 
        #  latter visualization purpose only)
        self.sma_fast = SMA(window_length_fast, ticker_symbol, reference_currency, price_data, t_current)
        self.sma_slow = SMA(window_length_slow, ticker_symbol, reference_currency, price_data, t_current)
        self.t = t_current

    def update(self, price_data, t=None):
        if t is None:
            self.t += 1
        else:
            self.t = t

            self.sma_fast.update(price_data, t - 1)
            self.sma_slow.update(price_data, t - 1)

        sma_fast_prev = self.sma_fast.sma
        sma_slow_prev = self.sma_slow.sma

        self.sma_fast.update(price_data)
        self.sma_slow.update(price_data)

        if sma_fast_prev > sma_slow_prev and self.sma_fast.sma < self.sma_slow.sma:
            # sma_fast crossing from above: Death Cross --> sell signal
            return -1
        elif sma_fast_prev < sma_slow_prev and self.sma_fast.sma > self.sma_slow.sma:
            # sma_fast crossing from below: Golden Cross --> buy signal
            return 1
        else:
            return 0
````
In order to make our SMAC class accessible to our FXStrategy object, we will bind/ assign it as 
an extra attribute to our FXStrategy object:  
````python
from trapeza.strategy import FXStrategy
from trapeza.account import FXAccount, order_management

SMAC_ADS = SMAC(50, 200, 'ADS.DE', 'EUR', price_data, 199)
SMAC_MRK = SMAC(50, 200, 'MRK', 'EUR', price_data, 199)

acc = FXAccount('EUR')
acc_res = FXAccount('EUR')
# monkey patch order management before passing accounts to FXStrategy
order_management.monkey_patch(acc)
order_management.monkey_patch(acc_res)

strat = FXStrategy('SMAC', [acc, acc_res], strategy_function, len(price_data['ADS.DE', 'EUR']) - 1, 
                   suppress_ticking=True)
# bind SMAC classes to FXStrategy object
strat.SMAC_ADS = SMAC_ADS
strat.SMAC_MRK = SMAC_MRK
````  
We will get to the variables such as 'strategy_function' and all the other settings later on.  
### The Strategy: MAC
#### Signals
The overall strategy is fairly simple. We use the SMAC's Golden Cross and Death Cross as trading signals. This is already 
implemented in the above SMAC class.  
#### Position Size Management
Furthermore, at each signal we will either buy or sell assets equal to 1,000 €. We will trade two stocks: Adidas and Merck. 
This asset choice is simply based on the low correlation factor between these two stocks, but there's really no more sophisticated 
reason for trading these two example stocks.  
To avoid false positive signals, we will use stop loss and start buy order types (c.f. [order management](order_management.md)) 
at a 1 % distance to the actual signal. This is done to avoid over-sensitive and noisy signals.  
#### Depot Management
We will use a second account as a virtual reservoir. Whenever we run out of cash for buying stocks or whenever we run out 
of stocks for selling our position, we will transfer cash and/ or stocks from this second reservoir account. Therefore, 
the second account holds a large amount of stocks and cash (quasi near infinite amounts). (__This is mainly done to 
demonstrate how different accounts can interact. The easier way would be just to use the deposit() method of FXAccount...__)   
In order to keep track of borrowed cash and stocks, we will open 'virtual positions' at our main account. Those virtual 
position will be priced with the negative (price x -1) rates of the actual stock prices (or in case of cash just -1). In 
particular, they are just named '-EUR', '-ADS.DE' and '-MRK' and can be seen as short selling.
#### Implementation Details
For demonstration purpose, we will suppress the [automated ticking logic](strategy.md#ticking-behavior-and-evaluation).  
This suppresses the call to _FXAccount.tick()_ and the automated evaluation within _FXStrategy_.  
  
In order to have a more granular control over the looping behavior within _FXStrategy_, we only want _FXStrategy.run()_ 
to perform one looping iteration step over the data (run() shall only run once), the actual looping logic is implemented 
by hand.     
This is achieved by setting the lookback to _len(price_data) - 1_. Thereby, _FXStrategy_ will only call our strategy decision 
function withing _FXStrategy.run()_ and furthermore passes all the data at once to our strategy decision function.  
Having all the data at once, we can implement our own loop over the entire data frame (and over all time steps), such that 
we have a more granular control (e.g. only trading 4 out of 5 work days), but we also have to tick and evaluate all accounts 
by hand as well when doing so.  
We then have to implement our own custom discrete-time-step-loop within the strategy decision function:  
````python
def strategy_function(accounts, price_data, reference_currency, volume_data_pt, fxstrategy):
    for t in range(200, len(price_data['ADS.DE', 'EUR'])):
        # some code...
    return
````
We start from time step 200, as we need the first 200 time steps (0 to 199) to initialize our SMA_200 indicator and at 
least two SMA_200 values (at time step 199 and time step 200) to detect a potential crossing with the SMA_50. The constructor 
has to look like the following in order to force _FXStrategy.run()_ to only call our strategy decision function once
internally:  
````python
# suppress ticking and set lookback to force only one iteration
strat = FXStrategy('SMAC', [acc, acc_res], strategy_function, len(price_data['ADS.DE', 'EUR']) - 1,
                       suppress_ticking=True)
````
  
Furthermore, we have assigned our SMAC classes as new attributes to our FXStrategy _'strat'_ object, which means, that 
we can access them in our strategy decision function _strategy_function_ via the argument _fxstrategy_ as 
_fxstrategy.SMAC_ADS_.  
Our strategy decision function looks as follows so far:  
````python
import math 

def strategy_function(accounts, price_data, reference_currency, volume_data_pt, fxstrategy):
    for t in range(200, len(price_data['ADS.DE', 'EUR'])):
        # get current stock prices of time step t
        ads_current_price = price_data['ADS.DE', 'EUR'][t]
        mrk_current_price = price_data['MRK', 'EUR'][t]
        
        # first handle ADS.DE
        # get trading signal
        ads_signal = fxstrategy.SMAC_ADS.update(price_data)
        if ads_signal == -1:  # sell signal
            # calculate the amount of stocks to be sold
            # we subtract 1% as we place our stop loss 1 % below our signal to avoid false positive noise
            ads_amount = math.floor(1000 / (ads_current_price * 0.99))
            
            # as we are sure that our stop loss will not trigger immediately, we do not need to check if our position 
            #  is sufficient to make the trade - we will check position coverage later on
            
            # place stop loss: current_bid_rate, volume, stop_bid_price, base, quote, order_lifetime_duration
            # order is valid for 20 work days
            accounts[0].stop_loss(ads_current_price, ads_amount, ads_current_price * 0.99, 'ADS.DE', 'EUR', 20)
            
            # we could add some signal using self.add_signal(self.accounts[0], 'ads_stop_loss'), but that would be the 
            #  same signal as already generated within our SMAC class, so we just leave it out...
        elif ads_signal == 1:  # buy signal
            # calculate the amount of stocks to be bought
            # we add 1% when placing our start buy order to avoid false positive noise
            ads_amount = math.floor(1000 / (ads_current_price * 1.01))
            
            # as we are sure that our stop loss will not trigger immediately, we do not need to check if our position 
            #  is sufficient to make the trade - we will check position coverage later on
            
            # place start buy order: we set our buy start limit 1 % above the current signal to avoid 
            #  false positive noise
            accounts[0].start_buy(ads_current_price, ads_amount, ads_current_price * 1.01, 'ADS.DE', 'EUR', 20)
        elif ads_signal == 0:  # no signal
            # do nothing...
            pass
        
        # handle MRK the same way as ADS
        mrk_signal = fxstrategy.SMAC_MRK.update(price_data)
        if mrk_signal == -1:  # sell signal
            mrk_amount = math.floor(1000 / (mrk_current_price * 0.99))
            accounts[0].stop_loss(mrk_current_price, mrk_amount, mrk_current_price * 0.99, 'MRK', 'EUR', 20)
        elif mrk_signal == 1:  # buy signal
            mrk_amount = math.floor(1000 / (mrk_current_price * 1.01))
            accounts[0].start_buy(mrk_current_price, mrk_amount, mrk_current_price * 1.01, 'MRK', 'EUR', 20)
        elif mrk_signal == 0:  # no signal
            pass
    
        # to avoid running into coverage exceptions, we have to check the current order heap and determine, if we have
        #  to collect cash or stocks from our second reservoir account
        # This is done by checking how many open orders are on the order heap and sum up their volume. This gives an 
        #  upper approximation. In case, that all orders may get triggered, then this is the amount of stocks/ cash 
        #  that we need to avoid coverage exceptions. Even though not all orders may get triggered (as we have a 1% gap 
        #  between our signals and our order limits), we take this as an approximation to be on the safe side
        
        # get all open ADS.DE orders with their order volume and estimate cash volume needed to cover fees
        if ('ADS.DE', 'EUR') in accounts[0]._order_heap.keys():
            open_ads_stop_loss = np.sum([i[1]['volume'] for i in accounts[0]._order_heap['ADS.DE', 'EUR']
                                         if i[0] == 'stop_loss'])   # volume in amount of stocks
            open_ads_start_buy = np.sum([i[1]['volume'] for i in accounts[0]._order_heap['ADS.DE', 'EUR']
                                         if i[0] == 'start_buy'])   # volume in amount of stocks
            number_of_open_trades = len(accounts[0]._order_heap['ADS.DE', 'EUR'])
        else:
            open_ads_stop_loss = 0
            open_ads_start_buy = 0
            number_of_open_trades = 0
            
        # do the same for MRK
        if ('MRK', 'EUR') in accounts[0]._order_heap.keys():
            open_mrk_stop_loss = np.sum([i[1]['volume'] for i in accounts[0]._order_heap['MRK', 'EUR']
                                         if i[0] == 'stop_loss'])
            open_mrk_start_buy = np.sum([i[1]['volume'] for i in accounts[0]._order_heap['MRK', 'EUR']
                                         if i[0] == 'start_buy'])
            number_of_open_trades += len(accounts[0]._order_heap['MRK', 'EUR'])
        else:
            open_mrk_stop_loss = 0
            open_mrk_start_buy = 0
            
        # buy orders withdraw cash in order to buy stocks
        # this is why we have to convert the amount of open start buy orders to EUR as their currently in 
        #  amount of stocks
        open_ads_start_buy = open_ads_start_buy * price_data['ADS.DE', 'EUR'][t]
        open_mrk_start_buy = open_mrk_start_buy * price_data['MRK', 'EUR'][t]
        # to calculate the amount of fees in EUR, we have to multiply the number of open trades with the max fee 
        #  (upper approximation to be on the safe side)
        cash_fee = number_of_open_trades * 70
        
        # we now have to transfer cash and/ or stocks from our second reservoir account and deposit the corresponding
        #  amounts into our virtual positions at our main trading account
        # this way we can model the borrowing process
        # first handle ADS
        if open_ads_stop_loss > accounts[0].position('ADS.DE'):
            # borrow stocks if position is not sufficient
            # first deposit into virtual position which keeps track of borrowings
            accounts[0].deposit(open_ads_stop_loss - accounts[0].position('ADS.DE'), '-ADS.DE')
            # now do the actual borrowing
            accounts[0].collect(accounts[1], open_ads_stop_loss - accounts[0].position('ADS.DE'), 'ADS.DE')
        # now handle MRK
        if open_mrk_stop_loss > accounts[0].position('MRK'):
            accounts[0].deposit(open_mrk_stop_loss - accounts[0].position('MRK'), '-MRK')
            accounts[0].collect(accounts[1], open_mrk_stop_loss - accounts[0].position('MRK'), 'MRK')
        # handle cash: cash is needed for our start buy orders in order to avoid coverage exceptions
        # open_ads_start_buy and open_mrk_start_buy are already converted to EUR
        if open_ads_start_buy + open_mrk_start_buy + cash_fee > accounts[0].position('EUR'):
            # borrow cash if cash position is not sufficient to buy stocks and to cover fees
            accounts[0].deposit(open_ads_start_buy + open_mrk_start_buy + cash_fee, '-EUR')
            accounts[0].collect(accounts[1], open_ads_start_buy + open_mrk_start_buy + cash_fee, 'EUR')
            
        # now we have to do some housekeeping
        # custom data frame for ticking accounts by hand
        # we have to evaluate by hand: we construct a separate data frame dict which also includes our virtual positions
        data_frame = {('ADS.DE', 'EUR'): ads_current_price,
                      ('MRK', 'EUR'): mrk_current_price,
                      ('-EUR', 'EUR'): -1,
                      ('-ADS.DE', 'EUR'): -ads_current_price,
                      ('-MRK', 'EUR'): -mrk_current_price}
        
        # tick all accounts to make orders take effect and to count up internal clock of accounts
        for _acc in accounts:
            # be aware: monkey patched accounts need a custom data frame for ticking to check if stop loss and start
            #  buy order are triggered
            _acc.tick(data_frame)
            
        # we have to evaluate by hand to append results to FXStrategy's results
        fxstrategy.evaluate(data_frame,
                            reference_currency=reference_currency,
                            append_to_results=True)
````  
An alternative to implementing a custom for-loop would be to make use of the accounts' clocks. As we will see later on, 
we somehow need a clock reference and in principle we either can implement a custom for-loop as above, make use of the 
accounts' clocks, use an additionally assigned attribute to the FXStrategy object (in the same way, we assigned SMAC, 
e.g. strat.extra_clock via binding an attribute to a class object which is then counted up at every call within the 
decision function via _fxstrategy.extra_clock += 1_) or use a counter class which is passed via 
[_strategy_kwargs_ of the call signature](strategy.md#defining-a-custom-strategy-decision-function) of the strategy 
decision function (which is then counted up at every call within the decision function - this has to be a class and 
cannot be a variable, because _strategy_kwargs_ are static such that we need a counter class with a _count method_ which 
alters internal class attributes. Every change to a normal variable will be lost at the next iteration when _FXStrategy.run()_ 
calls the strategy decision function internally, so use a class with a class attribute/ method).    
  
For further details regarding the for-loop structure see: [strategy - The complex Way: suppressed ticking](strategy.md#the-complex-way-suppressed-ticking)
### Fee Structure: MAC
We will use the exact same fee structure as [in the previous RSI example](#fee-structure---rsi) with only one exception:
every 10th trade is for free (because our broker is just a very nice guy...).
The implementation changes to:
````python
def strategy_function(accounts, price_data, reference_currency, volume_data_pt, fxstrategy):
    trade_counter = 0
    
    for t in range(200, len(price_data['ADS.DE', 'EUR'])):
        # first handle ADS.DE
        # ...
        # every 10th trade is for free
        if trade_counter % 10 == 0:
            fee = False
        else:
            fee = None
        
        # ...
        if ads_signal == -1: # sell signal
            # ...
            accounts[0].stop_loss(ads_current_price, ads_amount, ads_current_price * 0.99, 'ADS.DE', 'EUR', 20,
                                  fee=fee)
            trade_counter += 1
        elif ads_signal == 1: # buy signal
            # ...
            accounts[0].start_buy(ads_current_price, ads_amount, ads_current_price * 1.01, 'ADS.DE', 'EUR', 20,
                                  fee=fee)
            trade_counter += 1
        # ...
        
        # handle MRK the same way as ADS
        # ...
        # every 10th trade is for free
        if trade_counter % 10 == 0:
            fee = False
        else:
            fee = None
        # ...
        if mrk_signal == -1: # sell signal
            # ...
            accounts[0].stop_loss(mrk_current_price, mrk_amount, mrk_current_price * 0.99, 'MRK', 'EUR', 20,
                                  fee=fee)
            trade_counter += 1
        elif mrk_signal == 1: # buy signal
            accounts[0].start_buy(mrk_current_price, mrk_amount, mrk_current_price * 1.01, 'MRK', 'EUR', 20,
                                  fee=fee)
            trade_counter += 1
        # ...
````  
### Additional Real-World Constraints: MAC
Too further complicate our example for demonstration purposes, we state the following additional constraints:
  - trade on only 4 out of 5 week days, the 5th day is a day off  
  - we get interest on our cash position of about 0.012% per day  
  - we pay negative interest on borrowed cash positions of about 0.024% per day as well as on the cash equivalent of 
    borrowed stocks   
  - we pay back borrowed cash and stocks if we are sure, that our positions have enough liquidity to theoretically cover 
    all open stop loss and start buy orders as well as fee coverage.  
  
The implementation changes to:
````python
def strategy_function(accounts, price_data, reference_currency, volume_data_pt, fxstrategy):
    trade_counter = 0

    for t in range(200, len(price_data['ADS.DE', 'EUR'])):
        ads_current_price = price_data['ADS.DE', 'EUR'][t]
        mrk_current_price = price_data['MRK', 'EUR'][t]
        if (t % 5) == 0:
            # we only work 4 out of 5 work days
            pass
        else:
            if trade_counter % 10 == 0:
                fee = False
            else:
                fee = None

            ads_signal = fxstrategy.SMAC_ADS.update(price_data)
            if ads_signal == -1:  # sell signal
                ads_amount = math.floor(1000 / (ads_current_price * 0.99))
                accounts[0].stop_loss(ads_current_price, ads_amount, ads_current_price * 0.99, 'ADS.DE', 'EUR', 20,
                                      fee=fee)
                trade_counter += 1
            elif ads_signal == 1:  # buy signal
                ads_amount = math.floor(1000 / (ads_current_price * 1.01))
                accounts[0].start_buy(ads_current_price, ads_amount, ads_current_price * 1.01, 'ADS.DE', 'EUR', 20,
                                      fee=fee)
                trade_counter += 1
            elif ads_signal == 0:  # no signal
                pass

            if trade_counter % 10 == 0:
                fee = False
            else:
                fee = None
            mrk_signal = fxstrategy.SMAC_MRK.update(price_data)
            if mrk_signal == -1:  # sell signal
                mrk_amount = math.floor(1000 / (mrk_current_price * 0.99))
                accounts[0].stop_loss(mrk_current_price, mrk_amount, mrk_current_price * 0.99, 'MRK', 'EUR', 20,
                                      fee=fee)
                trade_counter += 1
            elif mrk_signal == 1:  # buy signal
                mrk_amount = math.floor(1000 / (mrk_current_price * 1.01))
                accounts[0].start_buy(mrk_current_price, mrk_amount, mrk_current_price * 1.01, 'MRK', 'EUR', 20,
                                      fee=fee)
                trade_counter += 1
            elif mrk_signal == 0:  # no signal
                pass

        if ('ADS.DE', 'EUR') in accounts[0]._order_heap.keys():
            open_ads_stop_loss = np.sum([i[1]['volume'] for i in accounts[0]._order_heap['ADS.DE', 'EUR']
                                         if i[0] == 'stop_loss'])
            open_ads_start_buy = np.sum([i[1]['volume'] for i in accounts[0]._order_heap['ADS.DE', 'EUR']
                                         if i[0] == 'start_buy'])
            number_of_open_trades = len(accounts[0]._order_heap['ADS.DE', 'EUR'])
        else:
            open_ads_stop_loss = 0
            open_ads_start_buy = 0
            number_of_open_trades = 0
        if ('MRK', 'EUR') in accounts[0]._order_heap.keys():
            open_mrk_stop_loss = np.sum([i[1]['volume'] for i in accounts[0]._order_heap['MRK', 'EUR']
                                         if i[0] == 'stop_loss'])
            open_mrk_start_buy = np.sum([i[1]['volume'] for i in accounts[0]._order_heap['MRK', 'EUR']
                                         if i[0] == 'start_buy'])
            number_of_open_trades += len(accounts[0]._order_heap['MRK', 'EUR'])
        else:
            open_mrk_stop_loss = 0
            open_mrk_start_buy = 0

        cash_fee = number_of_open_trades * 70
        open_ads_start_buy = open_ads_start_buy * price_data['ADS.DE', 'EUR'][t]
        open_mrk_start_buy = open_mrk_start_buy * price_data['MRK', 'EUR'][t]

        if open_ads_stop_loss > accounts[0].position('ADS.DE'):
            accounts[0].deposit(open_ads_stop_loss - accounts[0].position('ADS.DE'), '-ADS.DE')
            accounts[0].collect(accounts[1], open_ads_stop_loss - accounts[0].position('ADS.DE'), 'ADS.DE')
        elif open_ads_stop_loss < accounts[0].position('ADS.DE') and accounts[0].position('-ADS.DE') > 0:
            # payback stocks if position has enough liquidity
            pay_back_ads = min(accounts[0].position('ADS.DE')-open_ads_stop_loss, accounts[0].position('-ADS.DE'))
            accounts[0].transfer(accounts[1], pay_back_ads, 'ADS.DE')
            accounts[0].withdraw(pay_back_ads, '-ADS.DE')
        if open_mrk_stop_loss > accounts[0].position('MRK'):
            accounts[0].deposit(open_mrk_stop_loss - accounts[0].position('MRK'), '-MRK')
            accounts[0].collect(accounts[1], open_mrk_stop_loss - accounts[0].position('MRK'), 'MRK')
        elif open_mrk_stop_loss < accounts[0].position('MRK') and accounts[0].position('-MRK') > 0:
            # payback stocks if position has enough liquidity
            pay_back_mrk = min(accounts[0].position('MRK')-open_mrk_stop_loss, accounts[0].position('-MRK'))
            accounts[0].transfer(accounts[1], pay_back_mrk, 'MRK')
            accounts[0].withdraw(pay_back_mrk, '-MRK')
        if open_ads_start_buy + open_mrk_start_buy + cash_fee > accounts[0].position('EUR'):
            accounts[0].deposit(open_ads_start_buy + open_mrk_start_buy + cash_fee, '-EUR')
            accounts[0].collect(accounts[1], open_ads_start_buy + open_mrk_start_buy + cash_fee, 'EUR')
        elif (open_ads_start_buy + open_mrk_start_buy + cash_fee < accounts[0].position('EUR')
              and accounts[1].position('EUR') < 1_000_000_000):
            # pay back cash if cash position has enough liquidity
            pay_back_cash = min(accounts[0].position('EUR')-(open_ads_start_buy + open_mrk_start_buy + cash_fee),
                                1_000_000_000-accounts[1].position('EUR'))
            accounts[0].transfer(accounts[1], pay_back_cash, 'EUR')
            accounts[0].withdraw(pay_back_cash, '-EUR')

        # factor in interests
        accounts[0].deposit(accounts[0].position('EUR') * 0.00012, 'EUR')
        accounts[0].deposit(accounts[0].position('-EUR') * 0.00024, '-EUR')
        accounts[0].deposit(accounts[0].position('-ADS.DE') * price_data['ADS.DE', 'EUR'][t] * 0.00024, '-EUR')
        accounts[0].deposit(accounts[0].position('-MRK') * price_data['MRK', 'EUR'][t] * 0.00024, '-EUR')

        data_frame = {('ADS.DE', 'EUR'): ads_current_price,
                      ('MRK', 'EUR'): mrk_current_price,
                      ('-EUR', 'EUR'): -1,
                      ('-ADS.DE', 'EUR'): -ads_current_price,
                      ('-MRK', 'EUR'): -mrk_current_price}

        for _acc in accounts:
            _acc.tick(data_frame)

        fxstrategy.evaluate(data_frame,
                            reference_currency=reference_currency,
                            append_to_results=True)
````
### Getting the data: MAC
We will use the same function as already implemented in [the previous RSI example](#getting-the-data---rsi). We will 
pass _['ADS.DE', 'MRK']_ as arguments to _build_data()_ to retrieve stock prices for Adidas and Merck.  
Unfortunately, yahoo finance data is inconsistent for 'ADS.DE' and 'MRK' regarding missing dates such that we have to 
slightly adjust the function in order to clean up the data:
````python
def build_data(ticker_symbols, quote, start_data, end_date):
    _price_data = dict()
    _dates = list()
    _orig_dates = list()

    for _ticker in ticker_symbols:
        ticker_price_data = data.DataReader(_ticker, start=start_data, end=end_date,
                                            data_source='yahoo')['Close'].reset_index().drop_duplicates('Date', 'last')
        _dates.append(set(ticker_price_data['Date'].to_numpy()))
        _orig_dates.append(ticker_price_data['Date'].to_numpy())
        ticker_price_data = ticker_price_data['Close'].to_numpy()
        _price_data[_ticker, quote] = ticker_price_data

    missing_dates = set.union(*_dates) - set.intersection(*_dates)

    for i in range(len(_orig_dates)):
        for missing_date in missing_dates:
            if missing_date in _orig_dates[i]:
                delete_index = np.nonzero(_orig_dates[i] == missing_date)[0][0]
                _price_data[ticker_symbols[i], quote] = [p for n, p in enumerate(_price_data[ticker_symbols[i], quote])
                                                         if n != delete_index]
                _orig_dates[i] = [p for n, p in enumerate(_orig_dates[i]) if n != delete_index]

    _dates = _orig_dates[0]
    for ticker in ticker_symbols:
        _price_data[ticker, quote] = np.array(_price_data[ticker, quote])
    return _dates, _price_data
````
### Putting it all together: MAC
We will test our SMAC strategy on Adidas and Merck over a period of 5 years. We will start with 5,000 € cash, 5,000 € in
ADS stocks and 5,000 € MRK stocks at our main trading account. We will set the lookback value to _len(price_data) - 1_ as 
described in [implementation details](#implementation-details) in order to force _FXStrategy.run()_ to only call our 
strategy decision function once and to pass the entire data frame at once:
````python
# we have to manipulate context first before doing anything else
from trapeza import context
context.ARBITRARY_QUANTIZE_SIZE = 3  # restrict to three after decimal places

from trapeza.account import FXAccount, order_management
from trapeza.strategy import FXStrategy
from trapeza import metric

import math
import time

import numpy as np
import pandas as pd
from pandas_datareader import data
# for visualization
from matplotlib import pyplot as plt
import quantstats as qs

if __name__ == "__main__":
    dates, price_data = build_data(['ADS.DE', 'MRK'], 'EUR', '2016-05-12', '2021-05-12')
    dates = np.array(dates)
    price_data['-ADS.DE', 'EUR'] = -price_data['ADS.DE', 'EUR']
    price_data['-MRK', 'EUR'] = -price_data['MRK', 'EUR']
    price_data['-EUR', 'EUR'] = np.array([-1 for _ in range(len(dates))])

    SMAC_ADS = SMAC(50, 200, 'ADS.DE', 'EUR', price_data, 199)
    SMAC_MRK = SMAC(50, 200, 'MRK', 'EUR', price_data, 199)

    acc = FXAccount('EUR')
    acc_res = FXAccount('EUR')
    # monkey patch order management before passing accounts to FXStrategy
    order_management.monkey_patch(acc)
    order_management.monkey_patch(acc_res)

    # start with 5,000 € cash, 5,000 € in ADS and 5,000 € in MRK
    # for calculating the amount of stocks we take the stock prices at time step 199 as reference as our strategy
    #  execution starts at time step 200
    acc.deposit(5000, 'EUR')
    acc.deposit(math.floor(1000 / price_data['ADS.DE', 'EUR'][200]), 'ADS.DE')
    acc.deposit(math.floor(1000 / price_data['MRK', 'EUR'][200]), 'MRK')

    # we fill our reservoir account with a ver large amount
    acc_res.deposit(1_000_000_000, 'EUR')
    acc_res.deposit(1_000_000_000, 'ADS.DE')
    acc_res.deposit(1_000_000_000, 'MRK')

    # suppress ticking and set lookback to force only one iteration
    strat = FXStrategy('SMAC', [acc, acc_res], strategy_function, len(price_data['ADS.DE', 'EUR']) - 1,
                       suppress_ticking=True)
    # bind SMAC classes to FXStrategy object
    strat.SMAC_ADS = SMAC_ADS
    strat.SMAC_MRK = SMAC_MRK

    start_time = time.perf_counter()
    strat.run(price_data, 'EUR')
    end_time = time.perf_counter()
    print('Strategy execution time for {} time steps: {} s'.format(len(dates)-199, end_time-start_time))

    print('Sharpe Ratio SMAC 50 200: ',
          metric.sharpe_ratio(np.array(strat.total_balances)[:, 0], 0, annualization_factor=252,
                              revised_denominator=False))
````  
This prints:  
````
>>> Strategy execution time for 1036 time steps: 0.2646331000000002 s
>>> Sharpe Ratio SMAC 50 200:  0.6522295034666459
````
### Performance Analysis
We won't go into details regarding the performance details such as we did in the [above RSI example](#insights-via-stochastic-analysis).  
But nevertheless, let's inspect our SMA_50, SMA_200 and SMAC indicators:  
[<img src="media/example_smac_indicator.png" alt="underlying" width="850"/>](media/example_smac_indicator.png)  
As we can see, our indicator classes seem to work properly.  
  
Let's see the portfolio allocation over time:  
[<img src="media/example_smac_portfolio.png" alt="portfolio_allocation" width="850"/>](media/example_smac_portfolio.png)  
As the virtual negative positions (borrowings) are hard to read, we scale our axis with a symmetric log:  
[<img src="media/example_smac_portfolio_log.png" alt="log_portfolio_allocation" width="850"/>](media/example_smac_portfolio_log.png)  
Now we see, that we actually did short MRK stocks and had to borrow some cash to buy stocks.  
_All above figures were made using matplotlib. See [script_exampe_smac.py](script_example_smac.py) for implementation, as 
this is pretty straight forward and a lot of boilerplate code..._  
  
As we didn't use FXEngine and any sophisticated stochastic analysis methods, we will make use of an external visualization 
package named _quantstats_:  
````python
# visualize total balance of trading account with external package
pd_dates = pd.to_datetime(dates[199:])
tot_bal = pd.Series(np.array(strat.total_balances)[:, 0], index=pd_dates, name='SMAC_50_200')
qs.plots.snapshot(tot_bal, title='SMAC 50 200 Performance')

# visualize ADS performance
ads = pd.Series(price_data['ADS.DE', 'EUR'][199:], index=pd_dates, name='ADS')
qs.plots.snapshot(ads, title='ADS Performance')

# generate html report
qs.reports.html(tot_bal, ads, output='media/script_example_smac_results.html')
````  
Our SMAC strategy:  
[<img src="media/example_smac_performance.png" alt="example_smac_performance" width="850"/>](media/example_smac_performance.png)  
  
ADS as benchmark:    
[<img src="media/example_smac_ads_performance.png" alt="example_smac_ads_performance" width="850"/>](media/example_smac_ads_performance.png)    
  
_quantstats_ also generates a full report. The html version can be found [here](media/script_example_smac_results.html).  
In this report, 'strategy' denotes our SMAC strategy and ADS is used as 'benchmark'.  
[<img src="media/example_smac_results.png" alt="example_smac_results" width="850"/>](media/example_smac_results.png)  

Our strategy doesn't seem to be more profitable (oooh wonder... greetings from the efficient market theory once again....) 
than a simple buy and hold of ADS, but it significantly reduces volatility.  
_quantstats_ reports a Sharpe Ratio of 0.65 which is pretty much consistent with our calculations (yeaaahhh.....).    
  
### Final Remarks on SMAC Strategy Definition
The above SMAC example is over-complicated on purpose. This is done to show the modelling flexibility which comes with 
_trapeza_.  
There are several options to simplify the above code without making changes to the overall trading logic:  
  - we could have make use of _FXStrategy's_ automated ticking behavior, such that we wouldn't have to define our own 
    custom for-loop, which also would have make evaluation of the accounts easier,  
  - the second reservoir account could have been modeled simpler such that we wouldn't need two _FXAccounts_,  
  - as we barely use _FXStrategy_, we could have modeled the example without the use of _FXStrategy_ by simply just 
    using _FXAccount_ and manually evaluate and store the total balance of the account(s) at each time step.  
    
  
## Floating Point Error
As described in the chapter about _trapeza's_ [arithmetics](arithmetics.md), the float data type is prone to accumulating 
error during arithmetic operations. To mitigate this, _trapeza_ uses decimal math libraries, such that arithmetic 
operations performed by _trapeza_ do not incur floating point errors.  
Nonetheless, in the above example _numpy_ was used to calculate e.g. indicator signals. Those calculations might be 
affected by floating point error. Be aware, which calculations are done outside _trapeza_ regarding floating point 
error!  
  
  
---  
[Back to Top](#examples)
