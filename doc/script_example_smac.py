# we have to manipulate context first before doing anything else
from trapeza import context

context.ARBITRARY_QUANTIZE_SIZE = 3  # restrict to three after decimal places

from trapeza.account import FXAccount, order_management
from trapeza.strategy import FXStrategy
from trapeza import metric

import math
import time

import numpy as np
import pandas as pd
from pandas_datareader import data
import yfinance as yfin
from matplotlib import pyplot as plt
import quantstats as qs


yfin.pdr_override()     # patch pandas-datareader as it sometimes has problems when yahoo makes changes to its website


class SMA:
    def __init__(self, window_length, ticker_symbol, reference_currency, price_data, t_current=None):
        self.window_length = window_length
        self.ticker = ticker_symbol
        self.reference_currency = reference_currency

        if t_current is None:
            self.t = self.window_length - 1
        else:
            self.t = t_current

        self.sma = np.average(price_data[self.ticker,
                                         self.reference_currency][self.t - self.window_length + 1: self.t + 1])

        # this is for latter visualization purpose only
        self.sma_data = list()
        for i in range(self.window_length, len(price_data[self.ticker, self.reference_currency]) + 1):
            self.sma_data.append(np.average(price_data[self.ticker, self.reference_currency][i - self.window_length:i]))
        self.sma_data = np.array(self.sma_data)

    def update(self, price_data, t=None):
        if t is None:
            self.t += 1
            self.sma = self.sma + ((price_data[self.ticker, self.reference_currency][self.t]
                                    - price_data[self.ticker, self.reference_currency][
                                        self.t - self.window_length]) / self.window_length)
        else:
            self.t = t
            self.sma = np.average(price_data[self.ticker,
                                             self.reference_currency][self.t - self.window_length + 1: self.t + 1])


class SMAC:
    def __init__(self, window_length_fast, window_length_slow, ticker_symbol, reference_currency, price_data,
                 t_current):
        self.sma_fast = SMA(window_length_fast, ticker_symbol, reference_currency, price_data, t_current)
        self.sma_slow = SMA(window_length_slow, ticker_symbol, reference_currency, price_data, t_current)

        self.ticker = ticker_symbol
        self.reference_currency = reference_currency
        self.t = t_current

        # this is for latter visualization purpose only
        self.signals = list()
        for i in range(t_current + 1, len(price_data[ticker_symbol, reference_currency])):
            self.signals.append(self.update(price_data))

        # we have to reset our internal variables (which were altered in the above snippet, which is used for
        #  latter visualization purpose only)
        self.sma_fast = SMA(window_length_fast, ticker_symbol, reference_currency, price_data, t_current)
        self.sma_slow = SMA(window_length_slow, ticker_symbol, reference_currency, price_data, t_current)
        self.t = t_current

    def update(self, price_data, t=None):
        if t is None:
            self.t += 1
        else:
            self.t = t

            self.sma_fast.update(price_data, t - 1)
            self.sma_slow.update(price_data, t - 1)

        sma_fast_prev = self.sma_fast.sma
        sma_slow_prev = self.sma_slow.sma

        self.sma_fast.update(price_data)
        self.sma_slow.update(price_data)

        if sma_fast_prev > sma_slow_prev and self.sma_fast.sma < self.sma_slow.sma:
            # sma_fast crossing from above: Death Cross --> sell signal
            return -1
        elif sma_fast_prev < sma_slow_prev and self.sma_fast.sma > self.sma_slow.sma:
            # sma_fast crossing from below: Golden Cross --> buy signal
            return 1
        else:
            return 0


# noinspection PyProtectedMember
def strategy_function(accounts, price_data, reference_currency, volume_data_pt, fxstrategy):
    trade_counter = 0

    for t in range(200, len(price_data['ADS.DE', 'EUR'])):
        ads_current_price = price_data['ADS.DE', 'EUR'][t]
        mrk_current_price = price_data['MRK', 'EUR'][t]
        if (t % 5) == 0:
            # we only work 4 out of 5 work days
            pass
        else:
            # every 10th trade is for free
            if trade_counter % 10 == 0:
                fee = False
            else:
                fee = None

            # first handle ADS.DE
            ads_signal = fxstrategy.SMAC_ADS.update(price_data)
            if ads_signal == -1:  # sell signal
                ads_amount = math.floor(1000 / (ads_current_price * 0.99))
                accounts[0].stop_loss(ads_current_price, ads_amount, ads_current_price * 0.99, 'ADS.DE', 'EUR', 20,
                                      fee=fee)
                trade_counter += 1
            elif ads_signal == 1:  # buy signal
                ads_amount = math.floor(1000 / (ads_current_price * 1.01))
                accounts[0].start_buy(ads_current_price, ads_amount, ads_current_price * 1.01, 'ADS.DE', 'EUR', 20,
                                      fee=fee)
                trade_counter += 1
            elif ads_signal == 0:  # no signal
                pass

            # every 10th trade is for free
            if trade_counter % 10 == 0:
                fee = False
            else:
                fee = None
            # handle MRK the same way as ADS
            mrk_signal = fxstrategy.SMAC_MRK.update(price_data)
            if mrk_signal == -1:  # sell signal
                mrk_amount = math.floor(1000 / (mrk_current_price * 0.99))
                accounts[0].stop_loss(mrk_current_price, mrk_amount, mrk_current_price * 0.99, 'MRK', 'EUR', 20,
                                      fee=fee)
                trade_counter += 1
            elif mrk_signal == 1:  # buy signal
                mrk_amount = math.floor(1000 / (mrk_current_price * 1.01))
                accounts[0].start_buy(mrk_current_price, mrk_amount, mrk_current_price * 1.01, 'MRK', 'EUR', 20,
                                      fee=fee)
                trade_counter += 1
            elif mrk_signal == 0:  # no signal
                pass

        # get all open orders with their order volume and estimate cash volume needed to cover fees
        if ('ADS.DE', 'EUR') in accounts[0]._order_heap.keys():
            open_ads_stop_loss = np.sum([i[1]['volume'] for i in accounts[0]._order_heap['ADS.DE', 'EUR']
                                         if i[0] == 'stop_loss'])
            open_ads_start_buy = np.sum([i[1]['volume'] for i in accounts[0]._order_heap['ADS.DE', 'EUR']
                                         if i[0] == 'start_buy'])
            number_of_open_trades = len(accounts[0]._order_heap['ADS.DE', 'EUR'])
        else:
            open_ads_stop_loss = 0
            open_ads_start_buy = 0
            number_of_open_trades = 0
        if ('MRK', 'EUR') in accounts[0]._order_heap.keys():
            open_mrk_stop_loss = np.sum([i[1]['volume'] for i in accounts[0]._order_heap['MRK', 'EUR']
                                         if i[0] == 'stop_loss'])
            open_mrk_start_buy = np.sum([i[1]['volume'] for i in accounts[0]._order_heap['MRK', 'EUR']
                                         if i[0] == 'start_buy'])
            number_of_open_trades += len(accounts[0]._order_heap['MRK', 'EUR'])
        else:
            open_mrk_stop_loss = 0
            open_mrk_start_buy = 0

        # convert to cash positions
        cash_fee = number_of_open_trades * 70
        open_ads_start_buy = open_ads_start_buy * price_data['ADS.DE', 'EUR'][t]
        open_mrk_start_buy = open_mrk_start_buy * price_data['MRK', 'EUR'][t]

        # handle borrowing and pay backs
        if open_ads_stop_loss > accounts[0].position('ADS.DE'):
            # borrow stocks if position is not sufficient
            accounts[0].deposit(open_ads_stop_loss - accounts[0].position('ADS.DE'), '-ADS.DE')
            accounts[0].collect(accounts[1], open_ads_stop_loss - accounts[0].position('ADS.DE'), 'ADS.DE')
        elif open_ads_stop_loss < accounts[0].position('ADS.DE') and accounts[0].position('-ADS.DE') > 0:
            # payback stocks if position has enough liquidity
            pay_back_ads = min(accounts[0].position('ADS.DE')-open_ads_stop_loss, accounts[0].position('-ADS.DE'))
            accounts[0].transfer(accounts[1], pay_back_ads, 'ADS.DE')
            accounts[0].withdraw(pay_back_ads, '-ADS.DE')
        if open_mrk_stop_loss > accounts[0].position('MRK'):
            accounts[0].deposit(open_mrk_stop_loss - accounts[0].position('MRK'), '-MRK')
            accounts[0].collect(accounts[1], open_mrk_stop_loss - accounts[0].position('MRK'), 'MRK')
        elif open_mrk_stop_loss < accounts[0].position('MRK') and accounts[0].position('-MRK') > 0:
            pay_back_mrk = min(accounts[0].position('MRK')-open_mrk_stop_loss, accounts[0].position('-MRK'))
            accounts[0].transfer(accounts[1], pay_back_mrk, 'MRK')
            accounts[0].withdraw(pay_back_mrk, '-MRK')
        if open_ads_start_buy + open_mrk_start_buy + cash_fee > accounts[0].position('EUR'):
            # borrow cash if cash position is not sufficient to buy stocks and to cover fees
            accounts[0].deposit(open_ads_start_buy + open_mrk_start_buy + cash_fee, '-EUR')
            accounts[0].collect(accounts[1], open_ads_start_buy + open_mrk_start_buy + cash_fee, 'EUR')
        elif (open_ads_start_buy + open_mrk_start_buy + cash_fee < accounts[0].position('EUR')
              and accounts[1].position('EUR') < 1_000_000_000):
            # pay back cash if cash position has enough liquidity
            pay_back_cash = min(accounts[0].position('EUR')-(open_ads_start_buy + open_mrk_start_buy + cash_fee),
                                1_000_000_000-accounts[1].position('EUR'))
            accounts[0].transfer(accounts[1], pay_back_cash, 'EUR')
            accounts[0].withdraw(pay_back_cash, '-EUR')

        # factor in interests
        accounts[0].deposit(accounts[0].position('EUR') * 0.00012, 'EUR')
        accounts[0].deposit(accounts[0].position('-EUR') * 0.00024, '-EUR')
        accounts[0].deposit(accounts[0].position('-ADS.DE') * price_data['ADS.DE', 'EUR'][t] * 0.00024, '-EUR')
        accounts[0].deposit(accounts[0].position('-MRK') * price_data['MRK', 'EUR'][t] * 0.00024, '-EUR')

        # custom data frame for ticking accounts by hand
        data_frame = {('ADS.DE', 'EUR'): ads_current_price,
                      ('MRK', 'EUR'): mrk_current_price,
                      ('-EUR', 'EUR'): -1,
                      ('-ADS.DE', 'EUR'): -ads_current_price,
                      ('-MRK', 'EUR'): -mrk_current_price}

        # tick all accounts to make orders take effect and to count up internal clock of accounts
        for _acc in accounts:
            # be aware: monkey patched accounts need a custom data frame for ticking to check if stop loss and start
            #  buy order are triggered
            _acc.tick(data_frame)

        # we have to evaluate by hand to append results to FXStrategy's results
        fxstrategy.evaluate(data_frame,
                            reference_currency=reference_currency,
                            append_to_results=True)


# noinspection DuplicatedCode
def build_data(ticker_symbols, quote, start_data, end_date):
    _price_data = dict()
    _dates = list()
    _orig_dates = list()

    for _ticker in ticker_symbols:
        ticker_price_data = data.DataReader(_ticker, start=start_data, end=end_date,
                                            data_source='yahoo')['Close'].reset_index().drop_duplicates('Date', 'last')
        _dates.append(set(ticker_price_data['Date'].to_numpy()))
        _orig_dates.append(ticker_price_data['Date'].to_numpy())
        ticker_price_data = ticker_price_data['Close'].to_numpy()
        _price_data[_ticker, quote] = ticker_price_data

    missing_dates = set.union(*_dates) - set.intersection(*_dates)

    for i in range(len(_orig_dates)):
        for missing_date in missing_dates:
            if missing_date in _orig_dates[i]:
                delete_index = np.nonzero(_orig_dates[i] == missing_date)[0][0]
                _price_data[ticker_symbols[i], quote] = [p for n, p in enumerate(_price_data[ticker_symbols[i], quote])
                                                         if n != delete_index]
                _orig_dates[i] = [p for n, p in enumerate(_orig_dates[i]) if n != delete_index]

    _dates = _orig_dates[0]
    for ticker in ticker_symbols:
        _price_data[ticker, quote] = np.array(_price_data[ticker, quote])
    return _dates, _price_data


if __name__ == "__main__":
    dates, price_data = build_data(['ADS.DE', 'MRK'], 'EUR', '2016-05-12', '2021-05-12')
    dates = np.array(dates)
    price_data['-ADS.DE', 'EUR'] = -price_data['ADS.DE', 'EUR']
    price_data['-MRK', 'EUR'] = -price_data['MRK', 'EUR']
    price_data['-EUR', 'EUR'] = np.array([-1 for _ in range(len(dates))])

    SMAC_ADS = SMAC(50, 200, 'ADS.DE', 'EUR', price_data, 199)
    SMAC_MRK = SMAC(50, 200, 'MRK', 'EUR', price_data, 199)

    acc = FXAccount('EUR')
    acc_res = FXAccount('EUR')
    # monkey patch order management before passing accounts to FXStrategy
    order_management.monkey_patch(acc)
    order_management.monkey_patch(acc_res)

    # start with 5,000 € cash, 5,000 € in ADS and 5,000 € in MRK
    # for calculating the amount of stocks we take the stock prices at time step 199 as reference as our strategy
    #  execution starts at time step 200
    acc.deposit(5000, 'EUR')
    acc.deposit(math.floor(1000 / price_data['ADS.DE', 'EUR'][200]), 'ADS.DE')
    acc.deposit(math.floor(1000 / price_data['MRK', 'EUR'][200]), 'MRK')

    # we fill our reservoir account with a ver large amount
    acc_res.deposit(1_000_000_000, 'EUR')
    acc_res.deposit(1_000_000_000, 'ADS.DE')
    acc_res.deposit(1_000_000_000, 'MRK')

    # suppress ticking and set lookback to force only one iteration
    strat = FXStrategy('SMAC', [acc, acc_res], strategy_function, len(price_data['ADS.DE', 'EUR']) - 1,
                       suppress_ticking=True)
    # bind SMAC classes to FXStrategy object
    strat.SMAC_ADS = SMAC_ADS
    strat.SMAC_MRK = SMAC_MRK

    start_time = time.perf_counter()
    strat.run(price_data, 'EUR')
    end_time = time.perf_counter()
    print('Strategy execution time for {} time steps: {} s'.format(len(dates)-199, end_time-start_time))

    # plot indicator for ADS
    signals_ads = np.concatenate((np.array([0 for _ in range(200)]), SMAC_ADS.signals), axis=0)
    signals_mrk = np.concatenate((np.array([0 for _ in range(200)]), SMAC_MRK.signals), axis=0)
    fig, ax = plt.subplots(1, 2)
    fig.set_figwidth(15)
    ax[0].plot(dates, price_data['ADS.DE', 'EUR'], color='black', label='ADS')
    ax[0].plot(dates[49:], SMAC_ADS.sma_fast.sma_data, color='firebrick', label='SMA_50')
    ax[0].plot(dates[199:], SMAC_ADS.sma_slow.sma_data, color='darkorange', label='SMA_200')
    ax[0].scatter(dates[signals_ads == 1], price_data['ADS.DE', 'EUR'][signals_ads == 1], color='green', s=180,
                  label='buy')
    ax[0].scatter(dates[signals_ads == -1], price_data['ADS.DE', 'EUR'][signals_ads == -1], color='red', s=180,
                  label='sell')
    ax[0].legend()
    ax[1].plot(dates, price_data['MRK', 'EUR'], color='black', label='MRK')
    ax[1].plot(dates[49:], SMAC_MRK.sma_fast.sma_data, color='firebrick', label='SMA_50')
    ax[1].plot(dates[199:], SMAC_MRK.sma_slow.sma_data, color='darkorange', label='SMA_200')
    ax[1].scatter(dates[signals_mrk == 1], price_data['MRK', 'EUR'][signals_mrk == 1], color='green', s=180,
                  label='buy')
    ax[1].scatter(dates[signals_mrk == -1], price_data['MRK', 'EUR'][signals_mrk == -1], color='red', s=180,
                  label='sell')
    ax[1].legend()
    plt.tight_layout()
    plt.suptitle('Stock prices and SMAC indicators')
    plt.show()

    # plot asset allocation
    plt.figure()
    pos_EUR = np.array(strat.positions['EUR'])
    pos_nEUR = np.array(strat.positions['-EUR'])
    pos_ADS = np.array(strat.positions['ADS.DE'])
    pos_nADS = np.array(strat.positions['-ADS.DE'])
    pos_MRK = np.array(strat.positions['MRK'])
    pos_nMRK = np.array(strat.positions['-MRK'])
    plt.stackplot(dates[199:],
                  pos_EUR[:, 0],
                  pos_ADS[:, 0]*price_data['ADS.DE', 'EUR'][199:],
                  pos_MRK[:, 0]*price_data['MRK', 'EUR'][199:],
                  baseline='zero', labels=['EUR', 'ADS', 'MRK'],
                  colors=['gold', 'dimgray', 'royalblue'])
    plt.stackplot(dates[199:],
                  pos_nEUR[:, 0] * -1,
                  pos_nADS[:, 0] * price_data['-ADS.DE', 'EUR'][199:],
                  pos_nMRK[:, 0] * price_data['-MRK', 'EUR'][199:],
                  baseline='zero', labels=['-EUR', '-ADS', '-MRK'],
                  colors=['maroon', 'rosybrown', 'lightcoral'])
    plt.legend()
    plt.title('Positions of active trading account')
    plt.tight_layout()
    # noinspection DuplicatedCode
    plt.show()

    plt.figure()
    pos_EUR = np.array(strat.positions['EUR'])
    pos_nEUR = np.array(strat.positions['-EUR'])
    pos_ADS = np.array(strat.positions['ADS.DE'])
    pos_nADS = np.array(strat.positions['-ADS.DE'])
    pos_MRK = np.array(strat.positions['MRK'])
    pos_nMRK = np.array(strat.positions['-MRK'])
    plt.stackplot(dates[199:],
                  pos_EUR[:, 0],
                  pos_ADS[:, 0] * price_data['ADS.DE', 'EUR'][199:],
                  pos_MRK[:, 0] * price_data['MRK', 'EUR'][199:],
                  baseline='zero', labels=['EUR', 'ADS', 'MRK'],
                  colors=['gold', 'dimgray', 'royalblue'])
    plt.stackplot(dates[199:],
                  pos_nEUR[:, 0] * -1,
                  pos_nADS[:, 0] * price_data['-ADS.DE', 'EUR'][199:],
                  pos_nMRK[:, 0] * price_data['-MRK', 'EUR'][199:],
                  baseline='zero', labels=['-EUR', '-ADS', '-MRK'],
                  colors=['maroon', 'rosybrown', 'lightcoral'])
    plt.legend()
    plt.title('Symlog scaled - Positions of active trading account')
    plt.yscale('symlog')
    plt.tight_layout()
    plt.show()

    # visualize total balance of trading account with external package
    pd_dates = pd.to_datetime(dates[199:])
    tot_bal = pd.Series(np.array(strat.total_balances)[:, 0], index=pd_dates, name='SMAC_50_200')
    qs.plots.snapshot(tot_bal, title='SMAC 50 200 Performance')

    # visualize ADS performance
    ads = pd.Series(price_data['ADS.DE', 'EUR'][199:], index=pd_dates, name='ADS')
    qs.plots.snapshot(ads, title='ADS Performance')

    # generate html report
    qs.reports.html(tot_bal, ads, output='media/script_example_smac_results.html')

    print('Sharpe Ratio SMAC 50 200: ',
          metric.sharpe_ratio(np.array(strat.total_balances)[:, 0], 0, annualization_factor=252,
                              revised_denominator=False))
