Go back: [Table of Content](table_of_content.md)  
- [API](#api)
  - [FXAccount](#fxaccount)
    - [class: trapeza.account.FXAccount()](#class-trapezaaccountfxaccount)
    - [attribute: trapeza.account.FXAccount](#attribute-trapezaaccountfxaccount)
      - [exec_heap: list of lists](#exec_heap-list-of-lists)
      - [check_marked_coverage: {None, False, 'min_backward', 'max_forward'}](#check_marked_coverage-none-false-min_backward-max_forward)
      - [clock: int](#clock-int)
      - [_ignore_type_checking: bool](#_ignore_type_checking-bool)
      - [depot: dict](#depot-dict)
      - [_fee: dict](#_fee-dict)
      - [exchange_rates: dict](#exchange_rates-dict)
      - [reference_currency: str](#reference_currency-str)
      - [is_exchange_rate_updated: bool](#is_exchange_rate_updated-bool)
      - [prohibit_debiting: bool](#prohibit_debiting-bool)
    - [method: trapeza.account.FXAccount](#method-trapezaaccountfxaccount)
      - [deposit()](#deposit)
      - [withdraw()](#withdraw)
      - [collect()](#collect)
      - [transfer()](#transfer)
      - [buy()](#buy)
      - [sell()](#sell)
      - [tick()](#tick)
      - [total_balance()](#total_balance)
      - [position()](#position)
      - [batch_update_exchange_rates()](#batch_update_exchange_rates)
      - [update_exchange_rate()](#update_exchange_rate)
      - [_reset()](#_reset)
      - [_full_reset()](#_full_reset)
      - [_eval_fee()](#_eval_fee)
  - [Order Management](#order-management)
    - [attributes: trapeza.account.order_management](#attributes-trapezaaccountorder_management)
    - [method: trapeza.account.order_management](#method-trapezaaccountorder_management)
      - [monkey_patch()](#monkey_patch)
      - [stop_loss()](#stop_loss)
      - [trailing_stop_loss()](#trailing_stop_loss)
      - [start_buy()](#start_buy)
      - [buy_limit()](#buy_limit)
      - [sell_limit()](#sell_limit)
      - [setup_order_method()](#setup_order_method)
      - [tear_down_order_method()](#tear_down_order_method)
      - [patch()](#patch)
      - [tick()](#tick-1)
      - [_reset()   (order_management)](#_reset---order_management)
  - [FXStrategy](#fxstrategy)
    - [class: trapeza.strategy.FXStrategy()](#class-trapezastrategyfxstrategy)
    - [attribute: trapeza.strategy.FXStrategy](#attribute-trapezastrategyfxstrategy)
      - [name: str](#name-str)
      - [accounts: list](#accounts-list)
      - [lookback: int](#lookback-int)
      - [suppress_ticking: bool](#suppress_ticking-bool)
      - [strategy_kwargs: dict](#strategy_kwargs-dict)
      - [tick_kwargs: dict](#tick_kwargs-dict)
      - [strategy_func: func](#strategy_func-func)
      - [tick_method: func](#tick_method-func)
      - [\_init_accs_depots: list](#_init_accs_depots-list)
      - [\_accs_clocks: list](#_accs_clocks-list)
      - [positions: dict](#positions-dict)
      - [merged_positions: dict](#merged_positions-dict)
      - [total_balances: list](#total_balances-list)
      - [merged_total_balances: list](#merged_total_balances-list)
      - [signals: list](#signals-list)
      - [\_ignore_type_checking: bool](#_ignore_type_checking-bool-1)
    - [method: trapeza.strategy.FXStrategy](#method-trapezastrategyfxstrategy)
      - [run()](#run)
      - [add_signal()](#add_signal)
      - [reset()](#reset)
      - [evaluate()](#evaluate)
  - [FXEngine](#fxengine)
    - [class: trapeza.engine.FXEngine()](#class-trapezaenginefxengine)
    - [attribute: trapeza.engine.FXEngine](#attribute-trapezaenginefxengine)
      - [strategies: list](#strategies-list)
      - [name: str](#name-str-1)
      - [lookbacks: dict](#lookbacks-dict)
      - [reference_currency: str](#reference_currency-str-1)
      - [_tmp_dir: str](#_tmp_dir-str)
      - [run_register: dict of dicts](#run_register-dict-of-dicts)
      - [metrics: list](#metrics-list)
      - [standard_analysis_results: dict of dicts](#standard_analysis_results-dict-of-dicts)
      - [analysis_results: dict of dicts](#analysis_results-dict-of-dicts)
      - [price_data: dict](#price_data-dict)
      - [volume_data: dict](#volume_data-dict)
      - [n_jobs: int](#n_jobs-int)
      - [_ignore_type_checking: bool](#_ignore_type_checking-bool-2)
      - [is_loaded_from_file: bool](#is_loaded_from_file-bool)
      - [has_run: bool](#has_run-bool)
      - [is_analyzed: bool](#is_analyzed-bool)
      - [Further private attributes](#further-private-attributes)
    - [method: trapeza.engine.FXEngine](#method-trapezaenginefxengine)
      - [run()   (FXEngine)](#run---fxengine)
      - [analyze()](#analyze)
      - [load_results()](#load_results)
      - [close()   (FXEngine)](#close---fxengine)
      - [save()](#save)
      - [load()](#load)
      - [reset()   (FXEngine)](#reset---fxengine)
      - [register_strategies()](#register_strategies)
      - [_clean_artifacts()](#_clean_artifacts)
      - [run_strategy()](#run_strategy)
      - [analyze_strategy()](#analyze_strategy)
  - [FXDashboard](#fxdashboard)
    - [class: trapeza.dashboard.FXDashboard()](#class-trapezadashboardfxdashboard)
    - [attribute: trapeza.dashboard.FXDashboard](#attribute-trapezadashboardfxdashboard)
      - [engine: object](#engine-object)
      - [app: object](#app-object)
      - [signal_symbols: dict or None](#signal_symbols-dict-or-none)
      - [overview_page_layout: str](#overview_page_layout-str)
      - [strategy_page_layout: str](#strategy_page_layout-str)
      - [Further Privat Attributes](#further-privat-attributes)
    - [method: trapeza.dashboard.FXDashboard](#method-trapezadashboardfxdashboard)
      - [run()](#run-1)
      - [max_window_min_start_index()](#max_window_min_start_index)
      - [Drawing Methods](#drawing-methods)
  - [Context](#context)
  - [Metrics](#metrics)
  - [Exceptions](#exceptions)
    - [CoverageError](#coverageerror)
    - [AccountingError](#accountingerror)
    - [PositionNAError](#positionnaerror)
    - [AccountError](#accounterror)
    - [StrategyError](#strategyerror)
    - [OperatingError](#operatingerror)
  
  
---------
# API    
This an overview of all available classes and methods of _trapeza_. 

------------------------
------------------------
## FXAccount
For further details see the [documentation - FXAccount](account.md) or the [docstring and implementation](../trapeza/account/fx_account.pyx#L1045).    

------------------------
### class: trapeza.account.FXAccount()
- Account for managing buy, sell, withdrawal, deposit, transfer (between accounts derived from trapeza.account.base_account.BaseAccount) 
  and collect (between accounts derived from trapeza.account.base_account.BaseAccount) of FX trading transactions.    
- Customizable fee structure.  
- Accounts for transaction processing durations by implementing time-state-logic (comparable to stack automaton).    
- Call self.tick() for proceeding one time unit and to make transactions take effect if they incur transaction 
  processing duration.  
- Base class: [BaseAccount](../trapeza/account/base_account.py), Cython implementation: [cFXAccount](../trapeza/account/fx_account.pxd).  
   
  
__trapeza.account.FXAccount(reference_currency, depot=None, fee_buy=False, fee_sell=False, fee_deposit=False, 
                          fee_withdraw=False, exec_heap=None, long clock=0, exchange_rates=None, 
                          marked_coverage='min_backward', prohibit_debiting=False, ignore_type_checking=False)__
````
:param reference_currency: str
                           reference currency for calculating fees, total balance, etc.
:param depot: dict{currency: volume} or None, default=None
              dictionary which keeps track of depot assets.
              depot=None initializes empty dictionary.

              currency: str (key)
              volume: float (value)
:param exec_heap: heapq (list) of lists: [time, action_func, volume, currency, coverage], default=None
                  heap that stores transactions and is executed at each clock cycle (self.tick()).
                  exec_heap=None initializes an empty heap.

                  time: int, in clock cycles/ time base units (starting from 0)
                  action_func: str, {'c_withdraw', 'c_deposit'} (or {'withdraw', 'deposit'})
                  volume: float
                  currency: str
                  coverage: str
:param clock: int, default=0
              internal clock time. Increased by one unit when self.tick() is called.
:param fee_buy: {func, False}, default=False
                Fee function to apply automatically to every FXAccount.buy() transaction
                False: no fee applied
                func: function with signature as follows:
                      (fee_amount,fee_currency, fee_exec_time) = fee(volume, price, base, quote, processing_duration)
                      
                      volume, price, base, quote, processing_duration are the same as used within respective
                      transaction call (in this case FXAccount.buy()). Those arguments are just passed through
                      from there.
:param fee_sell: see fee_buy
:param fee_withdraw: see fee_buy,
                     if func: fee_amount, fee_currency, fee_exec_time
                                = fee(volume, price, base, quote, processing_duration)
                                = fee(volume, None, currency, currency, processing_duration)
                              base input is pre-set to/ called with hardcoded param:currency of FXAccount.withdraw()
                              price input is pre-set to/ called with hardcoded None
                              quote input is pre-set to/ called with hardcoded param:currency of FXAccount.withdraw()
                     also applied at transfer
:param fee_deposit: see fee_buy
                     if func: fee_amount, fee_currency, fee_exec_time
                                = fee(volume, price, base, quote, processing_duration)
                                = fee(volume, None, currency, currency, processing_duration)
                              base input is pre-set to/ called with hardcoded param:currency of FXAccount.deposit()
                              price input is pre-set to/ called with hardcoded None
                              quote input is pre-set to/ called with hardcoded param:currency of FXAccount.deposit()
                     also applied at collect
:param exchange_rates: {dict, None}, default=None
                       Python dictionary containing price information/ exchange rates
                       if None, self.exchange_rates is initialized as empty dict

                       key: (base, quote) --> (from_currency, to_currency)
                            Should contain all currency pairs of depot positions w.r.t. reference currency.
                            Doesn't matter if keys are supplied as base|quote or quote|base as rates will be
                            inverted automatically to match currency|reference_currency. See documentation 'account.md' 
                            regarding FX direct base|quote notation.
                            base: str
                            quote: str
                       value: float,
                              exchange_rate of base|quote FX pair
:param marked_coverage: {False, None, 'max_forward', 'min_backward'}, 
                        default = 'min_backward'
                        whether to check if marked but not yet booked transactions might lead to future
                        coverage violation.

                        None or False: No coverage checking
                        'max_forward': Get the maximum time_step in current heap and check if insertion
                                       of a new transaction into internal heap leads to coverage violation
                                       at any time_step. This checks, if any insertion of a new transaction might lead 
                                       to a coverage exception at any given time_step.
                        'min_backward': Take time_step given by processing_duration of transaction's method call, and 
                                        check if coverage is intact up to this time_step. Therefore coverage is only 
                                        checked whether specific transaction can be filled without coverage violation, 
                                        but does not check, if transaction might lead to future coverage violations.

                        - if {'max_forward', 'min_backward'}: throws trapeza.exception.CoverageError
                        - if None or False: no coverage check of marked but not yet booked transactions. This is 
                                            independent from the 'coverage' of a transaction's method argument of 
                                            transaction methods which only checks coverage at the time of calling the 
                                            transaction method (e.g. whether depot is covered just at the moment).
                        see documentation 'account.md' for more details.
:param prohibit_debiting: {False, True}, default=False
                          if True: no negative positions allowed at time of a transaction's execution
                                   !!! CoverageError is only thrown at _exec_heap(). This is not a good and proper 
                                   warning, but just a safety backup !!!
                          if False: negative positions are allowed. If self.check_marked_coverage is set to None, then 
                                    debiting might really occur.
:param ignore_type_checking: bool,
                             if True, type checking is omitted, which increases performance but is less safe
````    

------------------------
### attribute: trapeza.account.FXAccount
#### exec_heap: list of lists
Internal heap of delayed transactions.  
Getter returns [ProtectedExecHeap](account.md#retrieving-complete-depot) which is immutable and has the following structure:  
````
(heap) list of lists,
each sub-list is structured as follows:    
  [int:execution_time, str:transaction_type, float:volume, str:currency, str:coverage]
transaction_type is {'c_deposit', 'c_withdraw'} (or {'deposit', 'withdraw'}, gets automatically parsed)
coverage: see docstring of FXAccount.deposit() or .withdraw()
````
Use list of lists for Setter.
#### check_marked_coverage: {None, False, 'min_backward', 'max_forward'}
Status of self.check_marked_coverage: {False, 'min_backward', 'max_forward'}
#### clock: int
Current internal clock status.
Setter: must be greater equal to 0, not recommended setting by hand.
#### _ignore_type_checking: bool
Whether type checking is ignored or not, can only be set at \_\_init__().
#### depot: dict
Current depot and it's positions.  
Getter returns [ProtectedDict](account.md#retrieving-complete-depot) which is immutable and has the following structure:  
````
Dict,
keys: currencies/ assets as str
values: position size as float
````  
Use a complete dict of all new positions as Setter, see [ProtectedDict](account.md#retrieving-complete-depot) for further 
details.  
#### _fee: dict
Current [global fees](account.md#global-transaction-fees).
Getter returns [ProtectedDict](account.md#retrieving-complete-depot) which is immutable and has the following structure:  
````
Dict,
keys: {{'sell', 'buy', 'withdraw', 'deposit'}
values: function
````   
Set new fees by providing a dict. Dict can contain keys:{'sell', 'buy', 'withdraw', 'deposit'}. If one of the keys is 
missing in new dictionary, then the current internal setting for this key will be taken over and re-used.
#### exchange_rates: dict
Current exchange rates (which was previously supplied to self.tick(...), self.update_exchange_rate(...) or 
self.batch_update_exchange_rates(...)).  
Getter returns [ProtectedDict](account.md#retrieving-complete-depot) which is immutable and has the following structure:  
````
Dict,
keys: tuple of strings (currency pair)
values: exchange rate as float
````
Set new exchange rates as complete dict of all currency pairs. Must contain all currency pairs which are also contained 
in self.depot, otherwise self.total_balance will throw an exception. Doesn't matter if keys are noted as base|quote or 
quote|base (as long as [FX direct notation](account.md#base-quote-notation) is used) as rates are automatically inverted 
to base|self.reference_currency (when calling FXAccount.total_balance).
#### reference_currency: str
Reference currency of account object to calculate e.g. total balance, etc.
#### is_exchange_rate_updated: bool
Set internal status whether exchange rates have been updated and self.total_balance() is ready to be called. It is not 
recommended setting this value manually.
#### prohibit_debiting: bool
Whether debiting of account is allowed or not.    

------------------------
### method: trapeza.account.FXAccount
#### deposit()
Deposits volume given in currency onto account.  
__deposit(volume, currency, fee=None, processing_duration=None, coverage='except', instant_float_fee=False, float_fee_currency=None)__  
````
:param volume: float, >= 0
               volume given in currency
:param currency: str
:param processing_duration: int or None, default=None
                            processing time (delay) until transaction takes effect on account's depot
                            time of execution/ billing = self.clock + processing_duration
                            None is equivalent to 0 in behavior
:param fee: {func, False, None, float}, default=None
            fee setting for depositing

            None: self._fee['deposit'] is applied which is set at __init__.
            False: no fees at all applied, even if self._fee['deposit'] is set (overrides globally
                   set fees: self._fee['deposit'] will be temporarily ignored for this one
                   transaction)
            func: alternative function to self._fee is applied (self._fee['deposit'] will be ignored
                  for this one transaction and func will be applied instead)
                  Must follow call signature:
                      fee_volume, fee_currency, fee_exec_time = fee(volume, price, base, quote, processing_duration)
                                                              = fee(volume, 1, currency, currency, processing_duration)
                  base input is pre-set to/ called with hardcoded param:currency
                  price input is pre-set to/ called with hardcoded 1
                  quote input is pre-set to/ called with hardcoded param:currency
            float: constant fee is applied and billed in param:float_fee_currency
                   if instant_float_fee = False: fee is billed at the same time as set by param:processing_duration 
                                                 (same time step when transaction takes effect on account's depot)
                   if instant_float_fee = True: fee is billed instantly independent from param:processing_duration of 
                                                transaction
:param coverage: {'except', 'debit'}, default='except'
                 Controls exception behavior in case of coverage violation. This coverage analysis checks whether 
                 transaction is conductible in principal (e.g. if account's depot position is sufficient right now) but 
                 does not check against marked but not yet billed transactions.

                 'except': raises trapeza.exception.CoverageError if coverage is violated, which is the case if fee is 
                           greater than current depot position (in same currency as fee).
                 'debit': temporarily deactivates coverage check of current depot position as well as coverage check of 
                          marked but not yet booked transactions. If prohibit_debiting is set to False at __init__(), 
                          then negative (debt) positions are possible, else a trapeza.exception.CoverageError will be 
                          thrown at time of execution.

                 Coverage checking of current depot position is done independently of self.check_marked_coverage, which 
                 additionally checks against marked but not yet filled future transactions.
:param instant_float_fee: bool, default=False
                          ONLY APPLIES if fee = float, else ignored
                          if True: fees are withdrawn instantly independent of processing_duration. This does
                                   not affect processing duration of withdrawing param:volume.
                          if False: fees are billed at the same time as withdrawing param:volume given by the
                                    processing duration.
:param float_fee_currency: {str, None}, default=None
                           currency type to bill fee if fee=float, else ignored
                           if None or ignored, float_fee_currency is set to param:currency
:return: int
         confirmation states --> 0: processing (processing_duration greater 0), 1: executed/ billed
````
#### withdraw()
Withdraws volume given in currency from account.  
__withdraw(volume, currency, fee=None, processing_duration=None, coverage='except', instant_float_fee=False, float_fee_currency=None)__
````                 
:param volume: float, >= 0
               volume given in currency
:param currency: str
:param processing_duration: int or None, default=None
                            processing time (delay) until transaction takes
                            effect on account's depot
                            time of execution/ billing = self.clock + processing_duration
                            None is equivalent to 0 in behavior
:param fee: {func, False, None, float}, default=None
             fee setting for withdrawal
  
             None: self._fee['withdraw'] is applied which is set at __init__.
             False: no fees at all applied, even if self._fee['withdraw'] is set (overrides globally
                    set fees: self._fee['withdraw'] will be temporarily ignored for this one transaction)
             func: alternative function to self._fee is applied (self._fee['withdraw'] will be ignored 
                   for this one transaction and func will be applied instead)
                   Must follow call signature:
                       fee_volume, fee_currency, fee_exec_time = fee(volume, price, base, quote, processing_duration)
                                                               = fee(volume, 1, currency, currency, processing_duration)
                   base input is pre-set to/ called with hardcoded param:currency
                   price input is pre-set to/ called with hardcoded 1
                   quote input is pre-set to/ called with hardcoded param:currency
             float: constant fee is applied and billed in param:float_fee_currency
                    if instant_float_fee = False: fee is billed at the same time as set by
                                                  param:processing_duration (same time step when
                                                  transaction takes effect on account's depot)
                    if instant_float_fee = True: fee is billed instantly independent from
                                                 param:processing_duration of transaction
:param coverage: {'partial', 'except', 'debit'}, default='except'
                 Controls exception behavior in case of coverage violation. This coverage analysis checks whether 
                 transaction is conductible in principal (e.g. if account's depot position is sufficient right now) but 
                 does not check against marked but not yet billed transactions.

                 'partial': partial fulfilment as far as possible (including fees).
                            Withdrawal volume will be reduced s.t. that coverage conditions are met. This means, if 
                            withdrawal volume + fees is greater than current depot position, then withdrawal volume will 
                            be reduced, such that no debiting in account's depot positions occurs. If volume and fees 
                            are not in the same currency, this reduction is only performed on the volume (fees might
                            still cause a coverage violation), amount of fees stays untouched then. volume is reduced to 
                            meet initially calculated fees, fees are not adapted to reduced volume (guessing 
                            interdependency would be hard), s.t. more fees might be payed than according to the reduced 
                            volume. This does not guarantee transaction fulfillment, as this just checks current depot 
                            but does not check against marked but not yet billed (delayed) transactions, which is done 
                            independently of this coverage check.
                 'except': raises trapeza.exception.CoverageError if coverage is violated, which is the case if 
                           withdrawal (including fees) is greater than current depot position.
                 'debit': temporarily deactivates coverage check of current depot position as well as coverage check of 
                          marked but not yet booked transactions. If prohibit_debiting is set to False at __init__(), 
                          then negative (debt) positions are possible, else a trapeza.exception.CoverageError will be 
                          thrown at time of execution.

                 Coverage checking of current depot position is done independently of self.check_marked_coverage, 
                 which additionally checks against marked but not yet filled future transactions.
:param instant_float_fee: bool, default=False
                          ONLY APPLIES if fee = float, else ignored
                          if True: fees are withdrawn instantly independent of processing_duration. This does
                            not affect processing duration of withdrawing param:volume.
                          if False: fees are billed at the same time as withdrawing param:volume given by the
                            processing duration.
:param float_fee_currency: {str, None}, default=None
                           currency type to bill fee if fee=float, else ignored
                           if None or ignored, float_fee_currency is set to param:currency
:return: int
         confirmation states --> 0: processing (processing_duration greater 0), 1: executed/ billed
````
#### collect()
Collects payment volume in given currency from payer account to this account.  
__collect(payer_account, volume, currency, payee_fee=False, payer_fee=False, payee_processing_duration=None, payer_processing_duration=None)__
````
:param payer_account: class trapeza.account.base_account.BaseAccount (or derived class) object
:param volume: float, >= 0
               volume of currency
:param currency: str
:param payee_fee: {None, False, func, float}, default=False
                  fee setting for payee account

                  None: self._fee['deposit'] is applied which is set at __init__.
                        Call signature will be parsed to:
                            fee(volume, 1, currency, currency, processing_duration)
                  False: no fees at all applied, even if self._fee['deposit'] is set (overrides globally
                         set fees: self._fee['deposit'] will be temporarily ignored for this one transaction)
                  func: alternative function to calculate fees is applied with signature (self._fee['deposit']
                        will be ignored for this one transaction and func will be applied instead)
                        fee_volume, fee_currency, fee_exec_time = fee(volume, price, base, quote, processing_duration)
                                                               = fee(volume, 1, currency, currency, processing_duration)
                        price is set/ hard coded to 1 
                        base and quote are set/ hard coded to param:currency (induced by withdraw()/ deposit())
                  float: constant fee is applied and billed in param:currency at the same time as the collect
                         transaction takes effect
:param payer_fee: dependent on payer class implementation of payer_account.withdraw()
                  default=False

                  (!) if not {None, False, 0}:
                    payer_account.withdraw(volume, currency, payer_fee, payer_processing_duration) has to be 
                    implementend and is called to withdraw transaction volume and fees
:param payee_processing_duration: int or None, default=None
                                  processing time (delay) until transaction takes effect on account's depot
                                  time of execution/ billing = self.clock + processing_duration
                                  None is equivalent to 0 in behavior
:param payer_processing_duration: int or None, default=None
                                  processing time (delay) until transaction takes effect on account's depot
                                  time of execution/ billing = self.clock + processing_duration
                                  None is equivalent to 0 in behavior

                                 (!) if not {None, 0}:
                                    payer_account.withdraw(volume, currency, payer_fee, payer_processing_duration)
                                    is called (make sure, this method is implemented at payer_account)
                                 else:
                                    payer_account.withdraw(volume, currency) is called
:return: confirmation states --> 0: processing (processing_duration), 1: executed/ billed
````
#### transfer()
Transfers payment volume in given currency from this:account to payee account.  
__transfer(payee_account, volume, currency, payee_fee=False, payer_fee=False, payee_processing_duration=None, payer_processing_duration=None)__  
````
:param payee_account: class trapeza.account.base_account.BaseAccount (or derived class) object
:param volume: float, >= 0
               volume of currency
:param currency: str
:param payee_fee: dependent on payee class implementation of payee_account.deposit()
                  default=False

                  (!) if not {None, False, 0}:
                    payee_account.deposit(volume, currency, payee_fee, payee_processing_duration) has to be implemented 
                    and is called to deposit transaction volume and to bill fees if necessary
:param payer_fee: {None, False, func, float}, default=False
                  fee setting for payer account

                  None: self._fee['withdraw'] is applied which is set at __init__.
                        Call signature will be parsed to:
                            fee(volume, 1, currency, currency, processing_duration)
                  False: no fees at all applied, even if self._fee['withdraw'] is set (overrides globally
                         set fees: self._fee['withdraw'] will be temporarily ignored for this one transaction)
                  func: alternative function to calculate fees is applied with signature (self._fee['withdraw']
                        will be ignored for this one transaction and func will be applied instead)
                        fee_volume, fee_currency, fee_exec_time = fee(volume, price, base, quote, processing_duration)
                                                               = fee(volume, 1, currency, currency, processing_duration)
                        price is set/ hard coded to 1
                        base and quote are set/ hard coded to param:currency (induced by withdraw()/ deposit())
                  float: constant fee is applied and billed in param:currency at the same time as the transfer
                         transaction takes effect
:param payee_processing_duration: int or None, default=None
                                  processing time (delay) until transaction takes effect on account's depot
                                  time of execution/ billing = self.clock + processing_duration
                                  None is equivalent to 0 in behavior

                                 (!) if not {None, 0}:
                                    payee_account.deposit(volume, currency, payee_fee, payee_processing_duration)
                                    is called (make sure, this method is implemented at payee_account)
                                 else:
                                    payee_account.deposit(volume, currency) is called
:param payer_processing_duration: int or None, default=None
                                  processing time (delay) until transaction takes effect on account's depot
                                  time of execution/ billing = self.clock + processing_duration
                                  None is equivalent to 0 in behavior
:return: confirmation states --> 0: processing (processing_duration), 1: executed/ billed
````
#### buy()
Buy base|quote pair at ask_price (buy base). Give quote and receive base.  
__buy(volume_base, ask_price, base, quote, fee=None, processing_duration=None, coverage='except', instant_withdrawal=False, instant_float_fee=False, float_fee_currency=None)__  
````
:param volume_base: float, >= 0
                    volume in BASE currency
:param base: str,
             base currency
:param ask_price: float
                  current base|quote bid (FX direct notation)
:param quote: str,
              quote currency
:param fee: {func, False, None, float}, default=None
            fee setting for buy transaction

            None: self._fee['buy'] is applied which is set at __init__.
            False: no fees at all applied, even if self._fee['buy'] is set (overrides globally set fees:
                   self._fee['buy'] will be temporarily ignored for this one transaction)
            func: alternative function to self._fee is applied (self._fee['buy'] will be ignored
                  for this one transaction and func will be applied instead)
                  Must follow call signature:
                      fee_volume, fee_currency, fee_exec_time = fee(volume, price, base, quote, processing_duration)
            float: constant fee is applied and billed in param:float_fee_currency
                   if instant_float_fee = False: fee is billed at the same time as set by param:processing_duration 
                                                 (same time step when transaction takes effect on account's depot)
                   if instant_float_fee = True: fee is billed instantly independent from param:processing_duration of 
                                                transaction
:param processing_duration: int or None, default=None
                            processing time (delay) until transaction takes effect on account's depot
                            time of execution/ billing = self.clock + processing_duration
                            None is equivalent to 0 in behavior
:param coverage: {'partial', 'except', 'ignore', 'debit'}, default='except'
                 Controls exception behavior in case of coverage violation. This coverage analysis checks whether 
                 transaction is conductible in principal (e.g. if account's depot position is sufficient right now) but 
                 does not check against marked but not yet billed transactions.

                 'partial': partial fulfilment as far as possible (including fees).
                            Withdrawal volume of buying transaction will be reduced s.t. that coverage conditions are 
                            met. This means, if withdrawal volume + fees is greater than current depot position, then 
                            withdrawal volume will be reduced, such that no debiting in account's depot positions 
                            occurs. If volume and fees are not in the same currency, this reduction is only performed on 
                            the volume (fees might still cause a coverage violation), amount of fees stays untouched 
                            then. volume is reduced to meet initially calculated fees, fees are not adapted to reduced 
                            volume (guessing interdependency would be hard), s.t. more fees might be payed than 
                            according to the reduced volume. This does not guarantee transaction fulfillment, as this 
                            just checks current depot but does not check against marked but not yet billed (delayed) 
                            transactions, which is done independently of this coverage check. If withdrawal volume has 
                            to be reduced, then depositing volume is adapted accordingly (buy transaction is split into
                            the withdrawal of quote and depositing of base).
                 'except': raises trapeza.exception.CoverageError if coverage is violated, which is the case if 
                           withdrawal (including fees) is greater than current depot position
                 'debit': temporarily deactivates coverage check of current depot position as well as coverage check of 
                          marked but not yet booked transactions. If prohibit_debiting is set to False at __init__(), 
                          then negative (debt) positions are possible, else a trapeza.exception.CoverageError will be 
                          thrown at time of execution.
                 'ignore': buy order is not fulfilled but quietly ignored (without any exceptions) if coverage violation 
                           occurs

                  Coverage checking of current depot position is done independently of self.check_marked_coverage, 
                  which additionally checks against marked but not yet filled future transactions.
:param instant_withdrawal: bool, default=False
                           if True, volume in quote currency is withdrawn instantly,
                                but depositing volume in base currency is done after processing_duration.
                                This argument does not affect at which time step fees will be billed (which
                                is only affected via processing_time (when fee is a func or self._fee['buy']
                                is applied), fee and instant_float_fee).
                           if False, processing_time is applied to both withdrawal and depositing
:param instant_float_fee: bool, default=False
                          ONLY APPLIES if fee = float, else ignored
                          if True, fees are withdrawn instantly independent of processing_duration. This does
                            not affect processing duration of withdrawing param:volume.
                          if False, fees are billed at the same time as withdrawing param:volume given by the
                            processing duration.
:param float_fee_currency: {str, None}, default=None
                           currency type to bill fee if fee=float, else ignored
                           if None or ignored, float_fee_currency is set to param:base,
                           else float_fee_currency is set to param:base if fee is False, None or func
:return: int
         confirmation states --> -1: ignored, 0: processing (processing_duration), 1: executed/ billed
````
#### sell()
Sell base|quote pair at bid_price (sell base). Give base and receive quote.  
__sell(volume_base, bid_price, base, quote, fee=None, processing_duration=None, coverage='except', instant_withdrawal=False, instant_float_fee=False, float_fee_currency=None)__  
````
:param volume_base: float, >= 0
                    volume in BASE currency
:param base: str,
             base currency
:param bid_price: float
                  current base|quote ask (FX direct notation)
:param quote: str,
              quote currency
:param fee: {func, False, None, float}, default=None
            fee setting for sell transaction

            None: self._fee['sell'] is applied which is set at __init__.
            False: no fees at all applied, even if self._fee['sell'] is set (overrides globally set fees:
                   self._fee['sell'] will be temporarily ignored for this one transaction)
            func: alternative function to self._fee is applied (self._fee['sell'] will be ignored
                  for this one transaction and func will be applied instead)
                  Must follow call signature:
                      fee_volume, fee_currency, fee_exec_time = fee(volume, price, base, quote, processing_duration)
            float: constant fee is applied and billed in param:float_fee_currency
                   if instant_float_fee = False: fee is billed at the same time as set by param:processing_duration 
                                                 (same time step when transaction takes effect on account's depot)
                   if instant_float_fee = True: fee is billed instantly independent from param:processing_duration of 
                                                transaction
:param processing_duration: int or None, default=None
                            processing time (delay) until transaction takes effect on account's depot
                            time of execution/ billing = self.clock + processing_duration
                            None is equivalent to 0 in behavior
:param coverage: {'partial', 'except', 'ignore', 'debit'}, default='except'
                 Controls exception behavior in case of coverage violation. This coverage analysis checks whether 
                 transaction is conductible in principal (e.g. if account's depot position is sufficient right now) but 
                 does not check against marked but not yet billed transactions.

                 'partial': partial fulfilment as far as possible (including fees).
                            Withdrawal volume of sell transaction will be reduced s.t. that coverage conditions are met.
                            This means, if withdrawal volume + fees is greater than current depot position, then 
                            withdrawal volume will be reduced, such that no debiting in account's depot positions 
                            occurs. If volume and fees are not in the same currency, this reduction is only performed on 
                            the volume (fees might still cause a coverage violation), amount of fees stays untouched 
                            then. volume is reduced to meet initially calculated fees, fees are not adapted to reduced 
                            volume (guessing interdependency would be hard), s.t. more fees might be payed than 
                            according to the reduced volume. This does not guarantee transaction fulfillment, as this 
                            just checks current depot but does not check against marked but not yet billed (delayed) 
                            transactions, which is done independently of this coverage check. If withdrawal volume has 
                            to be reduced, then depositing volume is adapted accordingly (sell transaction is split into 
                            the withdrawal of base and depositing of quote).
                 'except': raises trapeza.exception.CoverageError if coverage is violated, which is the case if 
                           withdrawal (including fees) is greater than current depot position
                 'debit': temporarily deactivates coverage check of current depot position as well as coverage check of 
                          marked but not yet booked transactions. If prohibit_debiting is set to False at __init__(), 
                          then negative (debt) positions are possible, else a trapeza.exception.CoverageError will be 
                          thrown at time of execution.
                 'ignore': sell order is not fulfilled but quietly ignored (without any exceptions) if coverage 
                           violation occurs

                  Coverage checking of current depot position is done independently of self.check_marked_coverage, 
                  which additionally checks against marked but not yet filled future transactions.
:param instant_withdrawal: bool, default=False
                           if True, volume in base currency is withdrawn instantly,
                                but depositing volume in quote currency is done after processing_duration.
                                This argument does not affect at which time step fees will be billed (which
                                is only affected via processing_time (when fee is a func or self._fee['sell'] 
                                is applied), fee and instant_float_fee).
                           if False, processing_time is applied to both withdrawal and depositing
:param instant_float_fee: bool, default=False
                          ONLY APPLIES if fee = float, else ignored
                          if True, fees are withdrawn instantly independent of processing_duration. This does
                            not affect processing duration of withdrawing param:volume.
                          if False, fees are billed at the same time as withdrawing param:volume given by the
                            processing duration.
:param float_fee_currency: {str, None}, default=None
                           currency type to bill fee if fee=float, else ignored
                           if None or ignored, float_fee_currency is set to param:base,
                           else float_fee_currency is set to param:base if fee is False, None or func
:return: int
         confirmation states --> -1: ignored, 0: processing (processing_duration), 1: executed/ billed
````
#### tick()
Sets/ counts up internal clock.  
Executes exec_heap (see [stack automaton logic](account.md#inner-workings-how-the-stack-automaton-logic-works)) after 
new clock time is set, e.g. executing all due transactions, which were delayed due to processing
times. All transactions on heap, whose execution times (defined as time step of instructing transaction +
processing duration) are less than the new clock time, will be executed from oldest to newest up to the new
clock time. Transactions allocated in the future w.r.t. the new clock time will of course not be executed but
remain on the heap until they are due (w.r.t internal clock).  
__tick(clock=None)__  
````
:param clock: {int >= 0, None}, default=None
              if clock=None, then count up by one. If clock is integer, set internal clock to param:clock.
:return: int, current (freshly set) clock time
````
#### total_balance()
Calculates current value of all assets within [self.depot](#depot-dict) in reference currency.  
If exchange_rate=None, then [self.batch_update_exchange_rates()](#batch_update_exchange_rates) has to be called beforehand.  
__total_balance(exchange_rates=None, reference_currency=None)__  
````
:param exchange_rates: {dict, None}, default=None
                       if None, self.exchange_rates will be used (if set at initialization)

                       !!! call self.batch_update_exchange_rate before calling this
                           function if param:exchange_rate=None !!!

                       exchange_rates are given in base|quote FX direct notation, see documentation 'account.md' for
                       further information on notation convention (simply speaking: just plug in official
                       exchange rates from base|quote pair which one can find on any source providing financial data. 
                       Instead of currencies, stock/ assets can be used as well, see documentation 'account.md')

                       key: tuple of strings (base, quote)
                            base: str
                            quote: str

                            It does not matter if data is provided as (base, quote) or (quote, base) as long as proper 
                            FX direct notation is used - inverting of exchange rate is done automatically internally.
                            Supplying exchange rate data as one of the two tuple variants is sufficient (user
                            does not have to supply base|quote and quote|base, but just one of them).
                       value: float,
                              exchange_rate of base|quote pair
:param reference_currency: {str, None}, default=None
                           if None: self.reference_currency is used
:return: float (total balance)
````
#### position()
Returns position size of given currency (or stock/ asset).  
_(Does not need any update of exchange rate previous to calling this method)_    
__position(currency)__  
````
:param currency: str
:return: float, volume of currency
````
#### batch_update_exchange_rates()
Performs batch update of exchange rates of multiple currency pairs (or assets), whereas self.update_exchange_rate() only 
updates one exchange rate pair at once. It's sufficient, to just provide base|quote ratio (as long as proper FX direct 
notation for exchange rates is used, see documentation 'account.md' for notation conventions). quote|base ratio does not 
have to be added separately and will be calculated automatically.  
  
If one wants to call [self.total_balance()](#total_balance) without arguments, then this method has to be called beforehand.  
  
It is highly recommended, that all currencies/ stocks/ assets, which are present within depot, are also present in the 
update arguments of this method as respective pairs against the reference currency. Otherwise, self.total_balance() 
might throw an exception in certain circumstances! The safest way is to not use this method at all but call
[self.total_balance()](#total_balance) with appropriate input data argument!  
  
This method updates the class attribute self.exchange_rates.  
  
__batch_update_exchange_rates(list_rates, list_base_quote_pairs)__  
````
:param list_rates: list of floats,
                   exchange rate of base|quote pair
                   Rate is given in FX direct notation of base|quote, see documentation 'account.md' for further
                   information on notation convention (simply speaking: just plug in official exchange rates
                   from base|quote pair which one can find on any source providing financial data)
:param list_base_quote_pairs: list of tuples(base, quote),
                             base|quote pair in FX direct notation, see documentation 'account.md'
                             base: str
                             quote: str
:return: None
````
#### update_exchange_rate()
It is highly recommended not to use this class method, but use [self.batch_update_exchange_rates](#batch_update_exchange_rates) instead or call
[self.total_balance()](#total_balance) with appropriate input data argument!  
  
Update and/ or add exchange rate (within self.exchange_rates dictionary) for base|quote pair in 
self.exchange_rates dictionary. Use FX direct notation, see documentation 'account.md' for further information on 
notation convention.  
  
It's sufficient, to just provide base|quote ratio (as long as proper FX direct notation for exchange rates is used, 
see documentation 'account.md' for notation conventions). quote|base ratio does not have to be added separately
and will be calculated automatically.  
  
This method updates the class attribute self.exchange_rates (just one entry at a time).  

__update_exchange_rate(rate, base, quote)__  
````
:param rate: float,
             use FX direct notation of base|quote pair, see documentation 'account.md' for further information
:param base: str
:param quote: str
:return: None
````
#### _reset()
Resets account but retains reference_currency, check_marked_coverage, prohibit_debiting, ignore_type_checking and 
fee_x (e.g. fee_buy). Depot, heap and exchange_rate dict are set to empty, clock is set to 0. See [self.\_\_init__()](#class-trapezaaccountfxaccount) 
for further information.  
__\_reset()__
````
:return: None
````
#### _full_reset()
Resets account with depot=None, fee_x=False (e.g. fee_buy), check_marked_coverage='min_backward', empty heap, clock=0 
and empty exchange_rate dict. Only initial reference_currency is retained. See [self.\_\_init__()](#class-trapezaaccountfxaccount) 
for further information.  
__\_full_reset()__
````
:return: None
````
#### _eval_fee()
Evaluates fee without processing any transaction. Takes processing_duration into account. Used to predict fee costs.  
__\_eval_fee(action_type, alt_fee, volume, current_rate, base, quote, processing_duration)__
````
:param action_type: {'buy', 'sell', 'deposit', 'withdraw'},
                    ignored if alt_fee != None
:param alt_fee: alternative fee, {func, False, None, float}
:param volume: float
:param base: str
:param current_rate: float
:param quote: str
:param processing_duration: int
:return: fee_amount: float, fee amount
         fee_currency: str, fee currency
         fee_exec_duration: int, execution time of fee (e.g. if processing delay occurs)
````    

------------------------
## Order Management
- Monkey patch account of BaseAccount or derived subclass with methods for implementing order management (additional 
  data structure and methods), such as stop loss.   
- Patched methods are then accessible by class instance directly, e.g. account.stop_loss().   
- Be aware of possible side effects.  
- Only patches single instance, not entire classes.  
- Additionally custom methods can be patched onto account instance via patch() method.  
- Overwrites tick() method of account instance of BaseAccount/ derived subclass.
- Pure Python implementation without any base class.  
  
  
Be cautious when multi-processing, internal states might get messed up, as internal states/ attributes
store temporarily values, which are then used across different methods across patched structures.    

------------------------
### attributes: trapeza.account.order_management
Patching accounts will introduce new attributes to FXAccount object (only object not class!).  
All new attributes should not be accessed by the user directly (therefore, we only list those attributes, but we 
will not be explaining them in further detail here...):  
````
    _order_heap: dict, stack automaton logic for oder management, see documentation 'order_management.md'
    _is_order_manager_heap: bool, state variable to handle stack automaton logic
    _is_setup_order_called: bool, state variable to handle stack automaton logic
    _is_tear_down_order_called: bool, state variable to handle stack automaton logic
    _temp_kwargs_order: dict, temporary variable to pass kwargs of order type
    _internal_acc_methods: list, holds all internal methods of account to avoid over-patching
    _is_order_manager_monkey_patch: bool, set to True if account is patched
````   
  
Order management is a  pure Python implementation. Therefore, in contrast to FXAccount and FXStrategy (which are based 
on a Cython implementation), all attributes can be manipulated directly.      

------------------------
### method: trapeza.account.order_management
The following functions are available directly from FXAccount object (or derived from base class BaseAccount) after 
being patched by [monkey_patch](#monkey_patch) (only object and not entire class gets patched).  
See [Order Management](order_management.md) for further information.  
#### monkey_patch()
Monkey patch account. Patched methods are then accessible by class instance directly, e.g. account.stop_loss().  
Attributes and methods introduced from monkey_patch additionally to account instance attributes:  
````
   _order_heap, _is_order_manager_heap, _is_setup_order_called, _is_tear_down_order_called, _tick, __reset,
   _temp_kwargs_order, _internal_acc_methods, patch, _check_is_patchable, _exec_order_manager_heap, 
   _push_order_to_order_heap, _remove_order_from_order_heap, setup_order_method, tear_down_order_method, tick,
   _check_inverted_dict_current_data, _reset,
   if only_basic=False:
        stop_loss, trailing_stop_loss, start_buy, buy_limit, sell_limit
````  
If only_basic=True, then only basic functionality without any order types will be patched. Otherwise order types
i.e. stop_loss, sell_limit, etc. will be patched.  
  
Expired orders get cleaned up automatically.  
  
__monkey_patch(account, only_basic=False)__  
````
:param account: account instance of trapeza.account.BaseAccount or derived subclass
:param only_basic: bool, default=False
                   if True, then only basic functionality but no order types will be patched
                   if False, then additional order types are patched
                        --> !!! order types assume trapeza.account.FXAccount or at least same call signatures !!!
:return: None
````
#### stop_loss()
Places a stop loss order. The stop loss order triggers a sell transaction when the current price drops under (or equal to) 
the stop price. The stop order expires automatically after _order_lifetime_duration_ time steps.  
_(This is a very simplified order model and does not account for lack of market liquidity or FOK/ IOC.)_  
__stop_loss(current_bid_rate, volume, stop_bid_price, base, quote, order_lifetime_duration, **kwargs)__  
````
:param current_bid_rate: float, current exchange rate of the tradable base|quote pair
:param volume: float, volume in base currency
:param stop_bid_price: float, exchange rate at which to place the stop loss
:param base: string, base currency (identifier/ tick symbol)
:param quote: string, quote currency (identifier/ tick symbol)
:param oder_lifetime_duration: int, duration in time steps after which the order expires (all time steps inclusive)
:param **kwars: key word arguments for trapeza.account.FXAccount.sell method

:return: float, see return value of trapeza.account.FXAccount.sell method
         -1: sell transaction not executed (yet), 
          0: sell transaction delayed due to processing_duration, 
          1: sell transaction executed
````
#### trailing_stop_loss()
Places a trailing stop loss. Same as [Stop Loss](#stop_loss), but the stop price will be adjusted in case the price 
increases. The stop price will not follow downward movements of the price, only upward movements. Therefore, trailing 
stop loss preserves the distance between market price and stop price whenever the market price raises. This distance can 
either be defined as an absolute value or percentage value.  
_(This is a very simplified order model and does not account for lack of market liquidity or FOK/ IOC.)_  
__trailing_stop_loss(current_bid_rate, volume, trailing_stop_bid_price, base, quote,
                       order_lifetime_duration, percent=True, **kwargs)__
````
:param current_bid_rate: float, current exchange rate of the tradable base|quote pair
:param volume: float, volume in base currency
:param trailing_stop_bid_price: float, exchange rate at which to place the initial trailing stop loss
:param base: string, base currency (identifier/ tick symbol)
:param quote: string, quote currency (identifier/ tick symbol)
:param oder_lifetime_duration: int, duration in time steps after which the order expires (all time steps inclusive)
:param percent: bool, default=True
                True: adjust percentage distance in case of upward movement, 
                False: adjust absolute distance in case of upward movement, 
:param **kwars: key word arguments for trapeza.account.FXAccount.sell method

:return: float, see return value of trapeza.account.FXAccount.sell method
         -1: sell transaction not executed (yet), 
          0: sell transaction delayed due to processing_duration, 
          1: sell transaction executed
````
#### start_buy()
Places a normal start_buy order. The start buy order triggers a buy transaction when the current price raises above 
(or equal to) the start buy price ([_ask price_](account.md#transaction-buy)). The buy limit order expires automatically after 
_order_lifetime_duration_ time steps. The last time step is inclusive, e.g. if _order_lifetime_duration=5_, then order 
is still executable in the 5th time step but not afterwards at the next time step.  
_(This is a very simplified order model and does not account for lack of market liquidity or FOK/ IOC.)_  
__start_buy(current_ask_rate, volume, start_ask_price, base, quote, order_lifetime_duration, **kwargs)__  
````
:param current_ask_rate: float, current exchange rate of the tradable base|quote pair
:param volume: float, volume in base currency
:param start_ask_price: float, exchange rate at which to place the start buy limit
:param base: string, base currency (identifier/ tick symbol)
:param quote: string, quote currency (identifier/ tick symbol)
:param oder_lifetime_duration: int, duration in time steps after which the order expires (all time steps inclusive)
:param **kwars: key word arguments for trapeza.account.FXAccount.buy method

:return: float, see return value of  trapeza.account.FXAccount.buy method
         -1: buy transaction not executed (yet), 
          0: buy transaction delayed due to processing_duration, 
          1: buy transaction executed
````
#### buy_limit()
Places a buy limit order. The buy limit order triggers a buy transaction when the current price drops under (or equal to) 
the buy limit price ([_ask price_](account.md#transaction-buy)). The buy limit order expires automatically after 
_order_lifetime_duration_ time steps. The last time step is inclusive, e.g. if _order_lifetime_duration=5_, then order 
is still executable in the 5th time step but not afterwards at the next time step.  
_(This is a very simplified order model and does not account for lack of market liquidity or FOK/ IOC.)_  
__buy_limit(current_ask_rate, volume, limit_ask_price, base, quote, order_lifetime_duration, **kwargs)__  
````
:param current_ask_rate: float, current exchange rate of the tradable base|quote pair
:param volume: float, volume in base currency
:param limit_ask_price: float, exchange rate at which to place the buy limit
:param base: string, base currency (identifier/ tick symbol)
:param quote: string, quote currency (identifier/ tick symbol)
:param oder_lifetime_duration: int, duration in time steps after which the order expires (all time steps inclusive)
:param **kwars: key word arguments for trapeza.account.FXAccount.buy method

:return: see return value of  trapeza.account.FXAccount.buy method
         -1: buy transaction not executed (yet), 
          0: buy transaction delayed due to processing_duration, 
          1: buy transaction executed
````
#### sell_limit()
Places a sell limit order. The sell limit order triggers a sell transaction when the current price raises above (or equal 
to) the sell limit price ([_bid price_](account.md#transaction-sell)). The sell limit order expires automatically after 
_order_lifetime_duration_ time steps. The last time step is inclusive, e.g. if _order_lifetime_duration=5_, then order 
is still executable in the 5th time step but not afterwards at the next time step.  
_(This is a very simplified order model and does not account for lack of market liquidity or FOK/ IOC.)_  
__sell_limit(current_bid_rate, volume, limit_bid_price, base, quote, order_lifetime_duration, **kwargs)__  
````
:param current_bid_rate: float, current exchange rate of the tradable base|quote pair
:param volume: float, volume in base currency
:param limit_bid_price: float, exchange rate at which to place the sell limit
:param base: string, base currency (identifier/ tick symbol)
:param quote: string, quote currency (identifier/ tick symbol)
:param oder_lifetime_duration: int, duration in time steps after which the order expires (all time steps inclusive)
:param **kwars: key word arguments for trapeza.account.FXAccount.sell method

:return: see return value of  trapeza.account.FXAccount.sell method
         -1: sell transaction not executed (yet), 
          0: sell transaction delayed due to processing_duration, 
          1: sell transaction executed
````
#### setup_order_method()
This function has to be called at the very start of every custom order type implementation, which shall be
patched onto account. Does basic setup routines of internal variables.  
See [Custom Order Type Implementation](order_management.md#implement-custom-order-types) for details.  
__setup_order_method(func, order_lifespan_arg_name, **kwargs)__  
````
:param func: function object,
             order function passed as object/ callable, which shall be patched onto account of BaseAccount/ derived 
             subclass, e.g. FXAccount. If called within custom order type implementation, this argument is the function 
             itself (comparable to self of Python classes).
:param order_lifespan_arg: str,
                           denoting variable name of custom order function which represents order lifespan duration.
                           Pass string name of variable which ever variable (argument used in custom order function) 
                           represents the lifetime duration of the corresponding order. Suppose one has two variables 
                           denoting some duration, e.g. lifespan of the order and processing duration of transaction due 
                           to processing time of the broker. To avoid confusion about those durations, one has to pass
                           the name of the variable to param:order_lifespan_arg_name, that denotes the lifespan of the 
                           order. This way, both durations can be distinguished and be processed the right way. Only 
                           pass the string name of the variable to param:order_lifespan_arg, not the actual value or
                           reference/ object/ variable. For the pre-implemented order types this would be 
                           'order_lifetime_duration'.
:param kwargs: all arguments of func as kwargs (except current_rate)
               arguments of func can be passed as "arg_1=arg_1, arg_2=arg_2, ..." as param:kwargs, except
               current_rate (which is always the first argument after self within func signature of custom order type
               implementation, see documentation 'order_management.md'; even though this is theoretically caught, 
               it's better to be safe than sorry....).
               If func has **kwargs then use "arg_1=arg_1, arg_2=arg_2, ..., **kwargs" as param:kwargs.

               example of custom oder type implementation:
                    call signature of custom order implementation: 
                        func(self, current_rate , base, quote, duration, **kwargs)
                        
                    --> param:kwargs: base=base, quote=quote, **kwargs
                    
                    when used inside custom order implementation: 
                        setup_order_method(func, 'duration', base=base, quote=quote, **kwargs)

               current_rate (which is always first argument after self within func signature of custom order type 
               implementation) must be excluded from param:kwargs

               !!! variable, which is denoted by param:order_lifespan_arg_name has to be included in param:kwargs !!!
:return: is_expired: bool,
                     True if order is expired (not due to processing, but due to lifespan of order), 
                     else False
````
#### tear_down_order_method()
This function has to be called at the very end of every custom implemented order function, which shall be patched onto 
account. Does basic teardown routines of internal variables (especially cleaning order management stack).  
See [Custom Order Type Implementation](order_management.md#implement-custom-order-types) for details.  
__tear_down_order_method(func, confirmation)__  
````
:param func: function object,
             order function passed as object/ callable, which shall be patched onto account of BaseAccount/ derived 
             subclass, e.g. FXAccount. If called within custom order type implementation, this argument is the function 
             itself (comparable to self of Python classes).
:param confirmation: int, {-1, 0, 1}
                     transaction confirmation (return value of transaction) implemented in BaseAccount/ derived 
                     subclass, e.g. if sell() was successful. See trapeza.account.fx_account.FXAccount for further 
                     information.
                     Every class that shall be patched has to implement this confirmation for every transaction method.
                     -1: ignored, not successful, not yet executed
                      0: delayed due to execution/ transaction time (e.g. processing time of broker) but in
                         transaction/ filling
                      1: successful (it is absolutely important, to stick to at least this convention)
:return: confirmation: int,
                       just passes param:confirmation through
````
#### patch()
Patches methods onto account of BaseAccount or derived subclass.  
Patched methods are then directly accessible from patched instance (e.g. account.stop_loss()).  
Functions must follow the same structure as:
````python
def sample_patch_func(self, current_rate, *args, **kwargs):
    is_expired = self.setup_order_method(sample_patch_func, 'order_lifetime_name', ...)  # see setup_order_method()
    # order_lifetime is contained in *args, but it's string name has to be explicitly passed to
    # setup_order_method as well
    # ... custom_code ...
    confirmation = self.transaction(*args, *kwargs)    # see monkey_patch(), transaction method of account to be patched
    return self.tear_down_order_method(sample_patch_func, confirmation)
````  
- Make sure, that function calls [self.tear_down_order_method(func, confirmation)](#tear_down_order_method) before exit and
  self.setup_order_method at entry.    
- Furthermore, function must have base and quote as arguments.  
- !!! do not change position of param:current_rate !!!  
- __This method is used to patch single functions, which are not already patched by [monkey_patch()](#monkey_patch).__  
  
__patch(list_funcs, safe_patch=True)__  
````
:param list_funcs: list of methods (func objects/ callables)
                   eacht method function must follow signature:
                        confirmation = method(self, current_rate, *args, **kwargs)
                   see documentation 'order_management.md'
                   e.g. self.stop_loss(self, current_rate, volume, stop_bid_price, base, quote, **kwargs)
:param safe_patch: bool, default = True
                   If set to True, patch() checks if method contained in param:list_func already exists in account of
                   BaseAccount or derived subclass and throws AccountError. If False, over-patching is possible.
:return: None
````
#### tick()
Counts internal clock up by one time unit if clock=None. Else internal clock is set to param:clock.  
Executes oder management stack and internal transaction stack ([exec_heap](#exec_heap-list-of-lists)) of account after 
new clock time is set.  
__Overrides account.tick(), see monkey_patch() function. Basically, the user does not have to bother with this function 
unless any customization shall be implemented!__  
[Order management stack](order_management.md#inner-workings-the-order-stack) is cleaned automatically from expired 
orders during stack execution.  
__tick(dict_current_data=None, *args_base_tick, **kwargs_base_tick)__  
````
:param dict_current_data: dict or None, default=None
                          Python dictionary containing price data as 
                          {(base, quote): current_rate, (base, quote): current_rate, ...} or None.
                          If None, order management is not executed (orders may expire at next tick without getting 
                          triggered in the meantime) but only transactions (which also can be delayed by processing 
                          time) on the execution stack of the account get exectuted (oder management stack skipped for 
                          this time step, see documentation 'order_management.md').
                          Keys of param:dict_current_data, that are not listed within the order management stack
                          (list of all open orders which trade a base|quote pair), are ignored. This is in contrast to 
                          the execution stack (which contains transactions instead of orders), which will throw an 
                          exception if price data is incomplete (with regards to the contained base|quote prices).
:param args_base_tick: args
                       args supplied to original unpatched underlying original account.tick() method.
                       Original unpatched account.tick() (which is re-patched as account._tick(), see monkey_patch()) 
                       is called first before order management is executed. During call to original unpatched 
                       account.tick()/ respective account._tick() *args_base_tick is passed through and supplied to 
                       account._tick(). args_base_tick is dependent on concrete implementation of original unpatched 
                       account.tick() (in case of FXAccount: FXAccount.tick(clock=None) with param:clock as args).
:param kwargs_base_tick: kwargs dict
                         kwargs dict supplied to unpatched underlyin original account.tick().
                         Original unpatched account.tick() (which is re-patched as account._tick(), see monkey_patch()) 
                         is called first before order management is executed. During call to original unpatched 
                         account.tick()/ respective account._tick() **kwargs_base_tick is passed through and supplied 
                         to account._tick(). kwargs_base_tick is dependent on concrete implementation of original 
                         unpatched account.tick() (in case of FXAccount: FXAccount.tick(clock=None) with param:clock as 
                         kwargs)
:return: int, current (freshly set) clock time
````
#### _reset()   (order_management)
When patching with [monkey_patch()](#monkey_patch), unpatched original underlying [FXAccount._reset()](#_reset) will be 
renamed to _FXAccount.\_\_reset()_ to avoid name mangling. The original [FXAccount._reset()](#_reset) method gets called 
first, afterwards all [internals of order management](#attributes-trapezaaccountorder_management) get reset.  
__\_reset()__
````
:return: None
````    

------------------------
## FXStrategy
For further details see the [documentation - FXStrategy](strategy.md) or the [docstring and implementation](../trapeza/strategy/fx_strategy.pyx#L779).      

------------------------
### class: trapeza.strategy.FXStrategy()
- Ties (multiple) accounts and a strategy decision function to one logical unit, such that strategy decision function
  only has to implement trading logic without caring about e.g. tick handling (cf. time discrete state machine/
  stacked automaton logic implementation by FXAccount) or evaluation of trades and result parsing.  
- Strategy decision function must take on following call signature:  
    strategy_func(accounts, price_data_point, reference_currency, volume_data_point, fxstrategy, **strategy_kwargs)  
- FXStrategy.run() handles ticking and parses output into an appropriate format for subsequent analysis. 
  Internal reset is done BEFORE each run, but not after!  
- Base class: [BaseStrategy](../trapeza/strategy/base_strategy.py), Cython implementation: [cFXStrategy](../trapeza/strategy/fx_strategy.pxd).  
  
  
__trapeza.strategy.FXStrategy(name, accounts, strategy_func, lookback, tick_func=None, suppress_ticking=False, suppress_evaluation=False, 
                 ignore_type_checking=False, **kwargs)__
````
:param name: str,
             unique name for strategy object
:param accounts: list of FXAccounts (or other objects derived from BaseAccount) or single FXAccount
:param strategy_func: function, 
                      with call signature:
                          strategy_func(accounts, price_data_point, reference_currency, volume_data_point,
                                        fxstrategy, **strategy_kwargs)

                          accounts: list of account objects, 
                                    see :param:accounts, in the same order as :param:accounts passed during __init__ of
                                    FXStrategy
                          price_data_point: dict with tuple(str, str) as dict keys and float, list of floats or array of 
                                            floats as dict values.
                                            Dictionary of base and quote currency pair and their respective
                                            exchange rate in FX direct notation base|quote
                                            {(base, quote): [current_rate],
                                             (base, quote): [current_rate],
                                              ...}
                                             or
                                            {(base, quote): [rate_-2, rate_-1, current_rate],
                                             (base, quote): [rate_-2, rate_-1, current_rate],
                                             ...}
                                            --> dependent on size of parameter lookback (whether to prepend historic 
                                                data to each call to strategy decision function for each loop within 
                                                FXStrategy.run())
                          reference_currency: str,
                                              reference currency in which results shall be quoted
                          volume_data_point: None or dict,
                                             if dict, then in the same format as price_data_point (e.g. with
                                             volume data instead of exchange rate data)
                          fxstrategy: FXStrategy class object (same object, to which strategy decision function is 
                                      passed to; kind of similar to an enclosure) used for adding signals within
                                      strategy_func via fxstrategy.add_signal(signal_str)
                          strategy_kwargs: dict,
                                           additional custom key word arguments. kwargs of strategy_func can be passed 
                                           to param:kwargs. Must be contained with identical names in param:kwargs, 
                                           which are passed to FXStrategy.__init__(), see param:kwargs.
                                           Similar names to kwargs of tick_func are not allowed.

                    Ticking can either be done within strategy_func or otherwise is done in self.run() (auto-detection, 
                    also refer to param:suppress_ticking; auto-detection whether accounts have been patched by 
                    trapeza.account.order_management.monkey_patch()). Strategy_func does not have to return any values, 
                    values are fetched by FXStrategy.evaluate(). Just make sure, ticking of accounts happens before
                    FXStrategy.evaluate() such that effect of transactions on accounts has taken place

                    Regarding the strategy decision function's parameter 'accounts', a list of accounts is always
                    passed to strategy decision function during FXStrategy.run(). Even if a single account is passed
                    to FXStrategy.__init__, strategy decision function will receive a list of account(s).
:param lookback: int,
                 number of time steps which to be prepended as historic data at every loop passed to strategy decision 
                 function as price_data_point (see above).
                 Start time step of backtest simulation run is located at index=lookback (zero-based indexing) regarding 
                 data passed to FXStrategy.run(). See documentation 'strategy.md'.
:param tick_func: func or None, default=None
                  custom function for ticking accounts.

                  Default=None, which means internal default function is applied: account.tick() or account.tick(data) 
                  of FXAccount (handles both cases automatically, cf. patch by 
                  trapeza.account.order_management.monkey_patch()).

                  Ignored if param:suppress_ticking is set to True.

                  Must have following call signature:
                    tick_func(self, data_point, **kwargs)

                    self: placeholder in function signature to access self of this class
                    data_point: same as price_data_point in param:strategy_func
                                !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                Make sure custom tick_func accepts this data format
                                !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    kwargs: additionally kwargs arguments,
                            kwargs of tick_func can be passed to param:kwargs. Must be contained with identical
                            names in param:kwargs, which are passed to FXStrategy.__init__(), see param:kwargs.
                            Similar names to kwargs of strategy_func are not allowed.

                  Volume data is not passed to ticking. Sell and buy volumes are set by account.sell() and
                  account.buy(). 
:param suppress_ticking: bool, default=False,
                         If set to True, ticking of accounts (internal time-state machine) is ignored (at least
                         self.run() does not try to tick accounts automatically, accounts still can be ticked from 
                         inside param:strategy_func),
                         If set to False, accounts will be ticked according to param:tick_func.
:param suppress_evaluation: bool, default=False,
                            If set to False, accounts will be evaluated via FXStrategy.evaluate at every time
                            step (iteration) during FXStrategy.run.
                            If set to True, accounts will not be automatically evaluated at each time step
                            (iteration) of FXStrategy.run, but the user has to do this manually, which is
                            especially handy when implementing custom looping behaviour (see documentation
                            'strategy.md').
:param kwargs: additional key word arguments,
               kwargs can be supplied for param:tick_func (if function is passed as argument to
               FXStrategy.__init__()) and for param:strategy_func all in one.
               Auto-splits kwargs into kwargs of param:tick_func and param:strategy_func as long as both do
               not use same argument names! See documentation 'strategy.md'
:param ignore_type_checking: bool,
                             if True, type checking is omitted, which increases performance but is less safe
````    

------------------------
### attribute: trapeza.strategy.FXStrategy
#### name: str
Unique name of FXStrategy object.
#### accounts: list
List of accounts tied to FXStrategy object.  
Getter returns [ProtectedList](account.md#retrieving-complete-depot) which is immutable.  
Use a complete list of accounts as Setter. Single list entries cannot be manipulated 
(can only be overwritten by complete list).
#### lookback: int
Lookback size defining the number of time steps as historic data passed to strategy decision function besides current 
time step when performing backtest simulation step by step (looping through each time step within price data).
#### suppress_ticking: bool
Status whether to suppress automatic ticking of accounts within FXStrategy.run().
#### strategy_kwargs: dict
Dictionary of keyword arguments used for strategy decision function. See FXStrategy.\_\_init__() param:strategy_func.  
Getter returns [ProtectedDict](account.md#retrieving-complete-depot) which is immutable.  
Use a complete dictionary of keyword arguments as Setter. Single dict entries cannot be manipulated 
(can only be overwritten by complete dict).
#### tick_kwargs: dict
Dictionary of keyword arguments used for (alternative) ticking function (if param:tick_func is not None). 
See FXStrategy.\_\_init__() param:tick_func.    
Getter returns [ProtectedDict](account.md#retrieving-complete-depot) which is immutable.    
Use a complete dictionary of keyword arguments as Setter. Single dict entries cannot be manipulated 
(can only be overwritten by complete dict).  
#### strategy_func: func
Callable function object of the strategy decision function. See FXStrategy.\_\_init__() param:strategy_func.
#### tick_method: func
Callable function object of (alternative) ticking function. See FXStrategy.\_\_init__() param:tick_func.
#### \_init_accs_depots: list
List of the initial depot statuses (dicts) of accounts when passed to FXStrategy.\_\_init__(). See [FXAccount - depot status](account.md#depot-status).  
Getter returns [ProtectedDict](account.md#retrieving-complete-depot) which is immutable. See [FXAccount - depot status](account.md#depot-status) 
for details on how each dict is structured. List of dicts.  
Use a complete list of dicts for all accounts' new initial depot statuses as Setter. Single list entries cannot be manipulated 
(can only be overwritten by complete list of dicts).  
#### \_accs_clocks: list
List of ints representing the accounts' initial internal clock time when passed to FXStrategy.\_\_init__().  
Getter returns [ProtectedList](account.md#retrieving-complete-depot) which is immutable.  
Use a complete list of ints (for all accounts' new initial clock status) as Setter. Single list entries cannot be manipulated 
(can only be overwritten by complete list of ints).
#### positions: dict
Getter returns [ProtectedDict](account.md#retrieving-complete-depot) which is immutable.  
See [FXStrategy - Positions](strategy.md#__positions__).  
Use a complete dict as Setter. Single dict entries cannot be manipulated (can only be overwritten by complete dict).
#### merged_positions: dict
Getter returns [ProtectedDict](account.md#retrieving-complete-depot) which is immutable.  
See [FXStrategy - Merged Positions](strategy.md#__merged_positions__).  
Use a complete dict as Setter. Single dict entries cannot be manipulated (can only be overwritten by complete dict).
#### total_balances: list
Getter returns [ProtectedList](account.md#retrieving-complete-depot) which is immutable.  
See [FXStrategy - Total Balances](strategy.md#__total_balances__).  
Use a complete list as Setter. Single list entries cannot be manipulated (can only be overwritten by complete list).
#### merged_total_balances: list
Getter returns [ProtectedList](account.md#retrieving-complete-depot) which is immutable.  
See [FXStrategy - Merged Total Balances](strategy.md#__merged_total_balances__).  
Use a complete list as Setter. Single list entries cannot be manipulated (can only be overwritten by complete list).
#### signals: list
Getter returns [ProtectedList](account.md#retrieving-complete-depot) which is immutable.  
See [FXStrategy - Signals](strategy.md#__signals__) and [FXStrategs - Adding Signals](strategy.md#adding-signals).  
Use a complete list as Setter. Single list entries cannot be manipulated (can only be overwritten by complete list).
#### \_ignore_type_checking: bool
Whether type checking is ignored or not, can only be set at \_\_init__().      

------------------------
### method: trapeza.strategy.FXStrategy
#### run()
Runs strategy_func with supplied data. Data is fed in via looping through every time step of data.  
Reference currency is used to calculate total balances of accounts after executing strategy_func.  
Internal reset is done BEFORE each run, but not after!  
__run(price_data, reference_currency, volume_data=None)__  
````
:param price_data: dict with:
             keys: currency pair as tuple of strings: (currency_0, currency_1)
             values: list of floats as exchange rates per time step t or array of floats

             {(base, quote): [rate_0, rate_1, ..., rate_t],
              (base, quote): [rate_0, rate_1, ..., rate_t],
              ...}

             All values (list or array of floats) of all keys (currency pairs) have to be of same length.

             If historic data shall be used by strategy_func (cf. lookback), historic data has to be prepended
             manually to param:data, e.g.:
                 lookback = 3

                 {(base, quote): [rate_-3, rate_-2, rate_-1, rate_0, rate_1, ... rate_t],
                  (base, quote): [rate_-3, rate_-2, rate_-1, rate_0, rate_1, ... rate_t],
                  ...}

             Data will be looped through one by one (time frame specified by lookback and current time step t
             [size: lookback + 1] will be passed to strategy decition function at each loop). 
             Start time step is at index=lookback.

             Exchange rate is expressed in FX direct notation convention (direct notation, not volume notation)
             1.2 EUR|USD --> 1.2 USD for 1 EUR

             None values allowed (if handling via accounts during account.tick(data_point_t) is ensured).

             if accounts are patched with trapeza.account.order_management:
                if None value occurs, order management is not executed (orders may expire at next tick) but only
                those transactions that are on the internal execution stack (FXAccount.exec_heap) (e.g. due to 
                processing time of broker), see documentation 'account.md' for further information about the internal 
                heap to model the stack automaton logic. 
                Keys of param:data, that are not listed on either the internal execution stack (see 'account.md' 
                regarding stack automaton logic) or the order stack (list of all open orders annotated 
                with base|quote pair that are placed via an order type (i.e. stop_loss) which was patched onto the 
                account via trapeza.account.order_management, see 'order_management.md' for details), are ignored.
:param reference_currency: str,
                           reference currency in which to calculate total balances of each account assigned to 
                           FXStrategy at __init__.
:param volume_data: None or dict, default=None,
                    If None, then None value is also passed to strategy decision function.
                    If dict, then dict must be in the same format as param:price_data. Instead of price data,
                        place volume data (or whatever data shall be used within strategy decision function) as
                        values. Keys (tuple of strings, which annotate e.g. currencies) do not have to be
                        identical to keys of param:price_data. No cross volume checking applied, which means
                        that e.g. ('BTC', 'EUR') does not have to match up against inversed ('EUR', 'BTC'). Data input
                        at own caution. Lookbacks are handled in the same manner as in param:price_data.
:return: reference_currency: str,
                             see param:reference_currency
         self.signals: list of lists,
                       see documentation 'strategy.md'
         self.positions: dict,
                         see documentation 'strategy.md'
         self.merged_positions: dict,
                                see documentation 'strategy.md'
         self.total_balances: list of lists,
                              see documentation 'strategy.md'
         self.merged_total_balances: list,
                                     see documentation 'strategy.md'
````
#### add_signal()
This method is meant to be used inside the strategy decision function only, which is passed as param:strategy_func to 
FXStrategy.\_\_init__():  
  
Adds signal of specific account for current time step t, which is then automatically processed and appended to overall 
result when FXStrategy.evaluate() is called with append_to_results=True (inside FXStrategy.run()). 
Gets cleaned for next time step every time FXStrategy.evaluate() is called (inside FXStrategy.run()), which happens at 
every iteration through data supplied to FXStrategy.run() method.  
Just call add_signal within strategy_func every time a new signal is generated at specific time step t
(be cautious when calling evaluate() method manually, as this cleans all current signals without evaluating/
appending them to results!).  
Inside the strategy decision function _strategy_func_, call this method as '_fxstrategy.add_signal(acc, some_str)_' with 
_fx_strategy_ and _acc_ as specific list entry of _accs_ being arguments of the strategy_func call signature (keyword argument), see 
strategy_func call signature explained at FXStrategy.\_\_init__(). _acc_ in list _accs_ is in the same order as the 
list of accounts passed to FXStrategy at \_\_init__.  
See [FXStrategy - Adding Signals](strategy.md#adding-signals) for further information on how to add signals and see 
[FXDashboard](visualization.md#defining-symbols-to-visualize-signals) on how to structure the string argument in order 
to get properly visualized signals when FXDashboard is used.  
__add_signal(acc, signal)__  
````
:param acc: FXAccount object (or other Account object inherited from BaseAccount) which to add signal for,
            acc is one of the accounts contained in accs of strategy_func keyword arguments in its call signature,
            acc in accs (keyword argument of strategy_func) is in the same order as the list of accounts passed to 
            FXStrategy at __init__.
:param signal: str,
               unique string describing trading signal, such as 'buy', 'sell', 'short', 'long'. Signals are
               used for visualization in subsequent backtesting analysis. See documentation 'visualization.md' on how 
               this string has to be structured in order to get visualized properly
:return: None
````
#### reset()
Resets internal storage of FXStrategy (attributes [positions](#positions-dict), [merged_positions](#merged_positions-dict), 
[total_balances](#total_balances-list), [merged_total_balances](#merged_total_balances-list) and [signals](#signals-list)) 
and storage of accounts (attribute [depot](#depot-dict)). Restores account depot status to the same
status as of time of calling FXStrategy.\_\_init__(), keeps kwargs saved. Same applies to account.clock of each account 
contained within FXStrategy (attribute [accounts](#accounts-list)) as they are restored to the same status as of the 
time of initialization of FXStrategy. Total reset as if FXStrategy was freshly initialized.  
Internal reset is done BEFORE each run, but not after!  
__reset()__  
````
:return: None
````
#### evaluate()
Evaluate accounts regarding position sizes and total balances (expressed in reference currency), as well as all 
signals, which have been appended via FXStrategy.add_signal() method since last call to FXStrategy.evaluate().  
This method gets called at every iteration during looping through data supplied to FXStrategy.run().  
This method can either be called from outside (e.g. to get an overview of the current status) or from inside the strategy 
decision function.  
If called from inside, e.g. when implementing [custom looping logic](strategy.md#the-customized-way-user-defined-ticking):  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
Ticking and adding signals have to be done before evaluation! Ticking is not included in evaluate!  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
  
Further:  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
Clean signals, which are temporarily appended via FXStrategy.add_signal for time step t, so be careful when  
manually calling this method (when append_to_results=True).  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
  
__Usually, this method is not needed if the user does not do some fancy custom logic implementation. See 
[FXStrategy - Ticking Behavior and Evaluation](strategy.md#ticking-behavior-and-evaluation) for further information.__
  
__evaluate(data_frame, reference_currency, append_to_results)__
````
:param data_frame: dict
                   dict key: string tuple, (base, quote)
                   dict value: float, list of floats or array of floats as exchange_rates
                               if list or array: may contain historic data prepended according to parameter lookback
                                                 (see :param:lookback at FXStrategy.__init__()),
                                                 all lists/ arrays must have same length
                   
                   If called from self.run(), this is handled automatically.
                   If called manually, make sure data_frame is consistent in size (time steps).

                   e.g.:
                   {(base, quote): current_rate,
                    (base, quote): current_rate,
                     ...}
                   or
                   {(base, quote): [rate_-2, rate_-1, current_rate],
                    (base, quote): [rate_-2, rate_-1, current_rate],
                     ...}
                   --> dependent on size of parameter lookback (whether to prepend historic data to each call
                       to strategy decision function for each loop within FXStrategy.run()), see :param:lookback at
                       FXStrategy.__init__().
:param reference_currency: str,
                           reference currency to bill total balances in
:param append_to_results: bool,
                          if True: appends results to self.signals, self.positions, self.merged_positions, 
                                   self.total_balances and self.merged_total_balances such that backtesting engine can 
                                   use results for subsequent analysis
                          if False: does not append results to internal storage, self.evaluate() method does
                                    not take any effects on internal/ final results but just returns the current view/ 
                                    status
:return: signals: list of lists,
                  [[sig_acc_0, sig_acc_0, ...],
                   [sig_acc_1, sig_acc_1, ...], ...]
                  each list contains signals of each account at time step t,
                  cf. attribute 'signals' of FXStrategy, but only containing current time step t
         positions: list of dicts per account
                    [dict_acc_0, dict_acc_1, ...]
                    with dict_acc_x = {currency: amount, currency: amount, ...} (currency as string, amount as float)
                    each dict contains all positions and corresponding volume per account positions and corresponding 
                    amount/ volume per account at time step t
                    cf. attribute 'positions' of FXStrategy, but only containing current time step t
         total_balances: list of floats,
                         [tot_bal_acc_0, tot_bal_acc_1, ...]
                         total balance of each account in param:reference_currency at time step t
                         cf. attribute 'total_balances' of FXStrategy, but only containing current time step t
````    

------------------------
## FXEngine
For further details see the [documentation - FXEngine](engine.md) or the [docstring and implementation](../trapeza/engine/fx_engine.py#L26-L275).      

------------------------ 
### class: trapeza.engine.FXEngine()
- This class is a backtest automation designed for use with FXStrategy.    
- FXEngine takes in multiple strategies and evaluates them on the supplied data.  
- Base class: [BaseEngine](../trapeza/engine/base_engine.py), pure Python implementation.
  
The overall goal of this backtesting framework is to retrieve statistically analyzable results. Therefore, the
backtest engine slices coherent and consecutive time series sub-portions from the supplied data and executes a
trading strategy (supplied as FXStrategy class object). The backtest engine uses different window sizes and
different start indices for slicing time series sub-portion. Thereby, strategies can be tested for dependencies of
their performance with respect to time-being-invested and to different market phases (if they are captured
within the supplied data).  
  
__trapeza.engine.FXEngine(name_id, strategies, cache_dir='__system__', loc_decision_functions=False,
                 clean_cache_artifacts=False, ignore_type_checking=False, n_jobs=-1)__  
````
:param name_id: str,
                unique name of Engine object
:param strategies: list of FXStrategy objects (or objects derived from trapeza.strategy.base_strategy.BaseStrategy),
                   strategy must have unique names (no two strategies with same name attribute FXStrategy.name),
                   strategy names must not contain any other strategy's name as substring
:param cache_dir: str,
                  specifies path to directory, in which to store a temporary directory (gets cleaned up and
                  deleted afterwards) used as cache
                  '__system__' directs to platform specific default directory of Python tempfile package in
                               which the directory trapeza/FXEngine/self.name is created to store all
                               temporary files
                               under Windows: C:/Users/username/AppData/Local/Temp/trapeza/FXEngine/[param:name]
                  '__cache__' directs to 'package_path/__cache__/' where a new temporary sub-directory named by 
                              param:name will be created to store all temporary files. 
                              'package_path' is the path, where the trapeza package is located (e.g. in a venv).
                  or use user-specific path at which a new temporary sub-directory is created with the name given by 
                              param:name to store all temporary files
:param loc_decision_functions: bool,
                               if True, line of codes (source file) of decision function of each strategy is
                                        retrieved for later use e.g. in (custom) dashboard visualization.
                                        Source code of strategy decision function will be written to a temporary
                                        file (see temporary file structure in documentation 'engine.md').
                               if False, does not try to read line of codes of decision function

                               !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                               Be careful using this option, as it exposes code to temporary directory, which
                               might be readable for third parties (even though tempfile library is used).
                               !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
:param clean_cache_artifacts: bool,
                              if True, files and subdirectories in param:cache_dir are deleted before object is 
                              initiated. Usually, there shouldn't be any residue temporary folders left within
                              param:cache_dir, as this class cleans everything up automatically. In some cases,
                              e.g. if the running Python process is interrupted by exception or killed by user,
                              temporary files and subdirectory may remain unintentionally in param:cache_dir.
                              To avoid cluttering, param:clean_cache_artifacts can be set to True in order to
                              clean up those unintentionally left temporary files and subdirectories from
                              previously interrupted Python processes (which ran this class).
                              !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                              Be careful not to unintentionally delete any temporary files from other programs.
                              This can be avoided by setting param:cache_dir appropriately. Check the temporary
                              directory from time to time in order to avoid the disk getting filled up.
                              !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
:param ignore_type_checking: bool,
                             if True, type checking of input arguments is suppressed which increases performance
                             if False, explicit type checking is done. If types do not comply, an exception is
                             thrown and clean up routine (see FXEngine.close()) is invoked automatically to clean
                             up all temporary files created by this class object. This might decrease
                             performance but increases safety.
:param n_jobs: int,
               number of jobs running in parallel during self.run(), implementation based on joblib
               -1: run maximal number of jobs in parallel (either max number of cores of max number of jobs
                   if the latter is less than max number of cores)
               0: run in single process, no extra process will be spawned for parallelization
               int > 0: distinct number of cores
````  
_Do not forget to call FXEngine.close() at the end of the script._      

------------------------
### attribute: trapeza.engine.FXEngine 
FXEngine is a pure Python implementation (in contrast to FXAccount and FXStrategy which are implemented via Cython).  
Therefore, in contrast to FXAccount and FXStrategy, all attributes can be manipulated directly.  
#### strategies: list
List of strategies tied to FXEngine object.
#### name: str
Unique name of FXEngine object.
#### lookbacks: dict
Dictionary {strategy.name: strategy.lookback}. Dictionary contains all names of all strategies as dict keys and their 
respective [lookback](strategy.md#lookback) as dict values.
#### reference_currency: str
Name of reference currency used at [FXEngine.run()](#run---fxengine).
#### _tmp_dir: str
Path of temporary directory used as cache for FXEngine object.
#### run_register: dict of dicts
See [FXEngine - run_register](engine.md#retrieving-results).  
Dictionary of dictionaries:
````
dict of dicts,
{strategy_name_0: {window_size_0: [start_index_0, start_index_1, ...],
                   window_size_1: [start_index_0, start_index_1, ...],
                   ...
                  },
 strategy_name_1: {window_size_0: [start_index_0, start_index_1, ...],
                   window_size_1: [start_index_0, start_index_1, ...],
                   ...
                  },
 ...
}

strategy_name_x: str
window_size_x: int
start_index_x: int
````
#### metrics: list
List of metrics (as name strings) used for analysis. In particular, those are the keys of [metric_dict](#analyze) passed 
to FXEngine.analyze() (name strings of [standard metrics](engine.md#analyze-the-engine) not included).
#### standard_analysis_results: dict of dicts
See [FXEngine - Results regarding Metrics](engine.md#results-regarding-metrics).  
Holds analysis results of pre-implemented standard metrics.  
Results are annualized to yearly and assume daily frequency in data.  
````
standard_metric_dict = {'total_rate_of_return': _std_tot_rate_of_ret, 'volatility': _std_annual_vola,
                         'expected_rate_of_return': _std_annual_exp_rate_of_ret,
                         'expected_rate_of_log_return': _std_exp_rate_of_log_ret,
                         'sharpe_ratio': _std_annual_sharpe, 'sortino_ratio': _std_annual_sortino,
                         'value_at_risk': _std_var, 'max_drawdown': _std_max_drawdown}
````  
#### analysis_results: dict of dicts
See [FXEngine - Results regarding Metrics](engine.md#results-regarding-metrics).  
Holds analysis results of metrics passed as [metric_dict](#analyze) to FXEngine.analyze().  
   

-----------  
When no metrics supplied to [FXEngine.analyze()](#analyze), i.e. _metric_dict=None_, then pre-implemented metrics are used 
(_which are nonetheless different to [standard_analysis_results](#standard_analysis_results-dict-of-dicts)_).  
Results are NOT annualized (except: downside_risk and expected_rate_of_return 
are calculated against 0.0001172968 which is the log summing up to 3% p.a.).  
````
metric_dict = {'total_rate_of_return': metric.total_rate_of_return, 'volatility': metric.volatility,
               'downside_risk': _downside_risk, 'value_at_risk': metric.value_at_risk,
               'expected_rate_of_return': _expected_rate_of_return, 'sharpe_ratio': metric.sharpe_ratio,
               'sortino_ratio': metric.sortino_ratio, 'max_drawdown': _std_max_drawdown}
````
#### price_data: dict
Data used at [FXEngine.run()](#run---fxengine) are stored in this attribute. See [FXEngine - Input Data Format](engine.md#input-data-format).
#### volume_data: dict
Data used at [FXEngine.run()](#run---fxengine) are stored in this attribute. See [FXEngine - Input Data Format](engine.md#input-data-format).
#### n_jobs: int
Number of cores for multiprocessing, see [_n\_jobs_](#class-trapezaenginefxengine).
#### _ignore_type_checking: bool
Whether type checking is ignored or not. See [_\_ignore\_type\_checking_](#class-trapezaenginefxengine) for details.
#### is_loaded_from_file: bool
Whether engine object was loaded from file or is freshly instantiated.
#### has_run: bool
Whether [FXEngine.run()](#run---fxengine) has been called or not. This is checked at [FXEngine.analyze()](#analyze).  
FXEngine.run() has to be called before invoking FXEngine.analyze().
#### is_analyzed: bool
Whether [FXEngine.analyze()](#analyze) was called or not.
#### Further private attributes
Usually, the user does not need to bother with the following class attributes:  
- _from_child_: bool, controls logic regarding multi-processing  
- _\_finalizer_: weakref.finalize, controls clean up routine  
      

------------------------
### method: trapeza.engine.FXEngine
#### run()   (FXEngine)
Performs random sampling of input data into different sized windows, runs all strategies on sampled data and dumps 
results into temporary directory FXEngine._tmp_dir. Strategies of FXEngine are reset after run(), e.g. clocks and depots 
of accounts (contained within each strategy) as well as all results such as signals, positions, merged_positions, 
total_balances, merged_balances of strategies. Results are emptied out, depots and clocks are restored to the same 
status as of the time as they (accounts) were assigned to FXStrategy. For accessing results see 
[FXEngine - Retrieving Results](engine.md#retrieving-results).    
__run(price_data, reference_currency, volume_data=None,
            min_run_length=None, max_run_length=None, max_total_runs=None, run_through=True)__  
````
:param price_data: dict, market prices, see section 'Input Data Format' of 'engine.md'
                   (if (currency_1, currency_2) and its inverse pair (currency_2, currency_1) are contained in 
                   price_data, then check is applied, whether their dict values match up the inverse relation - this
                   is done in the underlying FXStrategy object).
                   Same data format as provided to FXStrategy.run()
:param reference_currency: str, reference currency to calculate total_balance of accounts and strategies
:param volume_data: dict or None, market volumes, see section 'Input Data Format' of 'engine.md'
                    (if (currency_1, currenc_y2) and its inverse pair (currency_2, currency_1) are contained in 
                    volume_data, then no (!) check is applied, whether their dict values match up the inverse relation)
                    If None, then None will be passed to underlying strategies.
                    Same data format as param:price_data.
                    default: None
:param min_run_length: None or int,
                       Minimal window size (minimal sub-sample length: min_run_length + lookback)
                       If None, min_run_length = 1.
                       default: None
:param max_run_length: None or int > min_run_length,
                       Maximum window size (maximal sub-sample length: max_run_length + lookback)
                       If max_run_length > len(price_data[currency_1, currency_2])-lookback, then max_run_length is clipped 
                       to meet the condition.
                       If None: set to len(price_data[currency_1, currency_2])-lookback (maximal possible window length)
                       default: None
:param max_total_runs: None, int or 'all_once',
                       Maximum number of runs per each strategy (!) contained in FXEnine.
                       If None: number of runs unconstrained, runs all possible sub-samples which can be drawn given 
                                minimal and maximal window sizes (runs all possible start indices for each window size) per
                                each strategy (!) in FXEngine.
                       If int: number of runs (per each strategy (!) in FXEngine) is close but never greater than 
                               max_total_runs, some window lengths might be omitted (chooses randomly, which sub-samples 
                               to run).
                       If 'all_once: Each possible window size (defined by min_run_length and max_run_length) will be run 
                                     exactly once (per each strategy (!) in FXEngine).
                       default: None
:param run_through: bool,
                    if True: performs a run over the total data (time steps), independent from other settings
                    if False: does not perform a run over total data but only above specified settings
                    default: True
                    does not affect above settings and is just an addtional run (per each strategy (!) in FXEngine)
                    
:return: dict,
         dict keys: tuple (strategy.name, window_size, start_index), 
                    see sections above regarding naming convention of window_size and start_index (which factors in 
                    lookback sizes)
         dict values: [start_balance, end_balance],
                      start_balance: total_balance of the strategy at start, in reference_currency
                      end_balance: total_balance of the strategy after run, in reference_currency
````
#### analyze()
Performs analysis by calculating a given set of metrics over all simulation runs performed by [FXEngine.run()](#run---fxengine) 
and can be used to retrieve statistical insights. In order to analyse results, FXEngine.run() has to be called 
beforehand, otherwise an exception is raised. Currently, metrics can only be evaluated on ['merged_total_balances'](strategy.md#__merged_total_balances__) 
of each strategy contained in FXEngine.  
See [FXEngine - Analyze the Engine](engine.md#analyze-the-engine) for further information, e.g. structure of supplied 
metrics dict. Results are stored in attributes [FXEngine.standard_analysis_results](#standard_analysis_results-dict-of-dicts) 
and [FXEngine.analysis_results](#analysis_results-dict-of-dicts).  
__analyze(metric_dict=None)__  
````
:param metric_dict: None or dict, default=None
                    dict: {metric_name: metric_fnc},
                          metric_name: str
                          metric_fnc: function object,
                                      metric_fnc must follow the function signature:
                                      float = metric_fnc(list_of_floats)
                                      where list_of_floats is FXStrategy.merged_total_balances
                    None: only pre-implemented metrics will be analyzed:
                          {'total_rate_of_return': # total rate of return in percent, not annualized
                           'volatility': # volatility based on log returns and normal distribution assumption, not annualized
                           'downside_risk': # downside risk, risk-free: 3%, normal distribution assumption
                           'value_at_risk': # value at risk at 95% confidence based on log returns and normal distribution 
                                              assumptions, 'historic simulation' used as computation method
                           'expected_rate_of_return': # expected rate of excess return per time step, risk-free: 3%, based on 
                                                        log return and normal distribution assumption
                           'sharpe_ratio': # sharpe ratio, risk-free: 3%, based on percentage return and normal distribution 
                                             assumption, not annualized
                           'sortino_ratio': # sortino ratio, risk-free: 3%, based on percentage return and normal distribution 
                                              assumption, not annualized
                           'max_drawdown': # max drawdown
                          }

:returns: None
````
#### load_results()
After FXEngine.run(), results of every simulation run are stored as temporary file within the temporary sub-directory 
under FXEngine._tmp_dir. This method is a convenience function for loading results from those temporary files.  
See [FXEngine - Results from Runs](engine.md#results-from-runs) and [FXEngine - Temporary Directory](engine.md#temporary-directory) 
for further information about naming convention of temporary files (which hold the results from simulation runs) and 
[FXStrategy - Retrieving Results](strategy.md#retrieving-results) regarding the data structure of results.  
__load_result(strategy_name, window_size=None, start_index=None)__  
````
:param strategy_name: str, name of strategy to inspect
:param window_size: None or int, default=None
                    if int: window size (number of simulation steps) of sub-sample
                    if None: largest window size in FXEngine.run_register                 
:param start_index: None or int, default=None
                    if int: start index of sub-sample (step at which strategy execution starts - lookback, see above for 
                            details), index at where data slicing starts and therefore factors in lookback value (that 
                            is why lookback has to be substracted from the actual start of the strategy execution time 
                            step, as data slicing starts factors in the lookback)
                    if None: minimal start_index in self.run_register or 'runthrough' (prefered if present in 
                             FXEngine.run_register) 

:returns: dict,
          keys: string, 'signals', 'positions', 'merged_positions', 'total_balances' or 'merged_total_balances'
          values: list, values of FXStrategy.signals, FXStrategy.positions, etc. of the run over the sub-sample 
                  specified by arguments window_size and start_index
````
#### close()   (FXEngine)
Closes Engine and deconstructs temporary directory. It is recommended to use this function explicitly, but nevertheless 
this routine should also execute whenever the class object is garbage collected.  
__close()__  
````
:return: None
````
#### save()
Saves current FXEngine class object. A new sub-directory {pth_dir}/{FXEngine.name}_temp/ will be created, in which all 
temporary files under FXEngine._tmp_dir will be copied into (this includes all results from FXEngine.run() as well as 
files that e.g. store lookback configuration, etc.). The FXEngine object (self) will be stored as pickle file directly
under the {pth_dir} directory as {FXEngine.name}.pkl.  
  
Do not rename any of those files and directories manually in order not to break loading routine via FXEngine.load()!
All internal states of the FXEngine will be saved accordingly and are re-loadable via FXEngine.load().  
  
DO ONLY USE UNIQUE NAME_IDs AT FXEngine.\_\_init__() AT SAME pth_dir!!!  
Otherwise, previously saved engines under pth_dir with same engine names will be overwritten!  
  
If the user wants to reduce the file size of object.pkl, then set FXEngine.price_data=None and 
FXEngine.volume_data=None. Otherwise, data will also be stored within FXEngine object. This is only recommended
if data is persistently accessible (as analysis and backtest results only make sense for a given data set!).  
__save(pth_dir)__  
````
:param pth_dir: str,
                directory where to store object.pkl (containing FXEngine object) and where to create a new
                sub-directory {pth_dir}/temp/ to copy all temporary files
:return: None
````
#### load()
Loads FXEngine object, which was saved via FXEngine.save().  
Use exact same name_id for FXEngine.\_\_init__ and pth_dir, which were used when calling FXEngine.save().    
  
If user has cleaned internal data storage of the FXEngine object before saving (in order to decrease file size),
then manually (!!!) re-load data into FXEngine.price_data=prices and FXEngine.volume_data=volumes.  
__load(pth_dir)__  
````
:param pth_dir: str,
                directory where object.pkl (containing FXEngine object) was stored and where
                sub-directory {pth_dir}/temp/ was created with all copied temporary files
:return: None
````
#### reset()   (FXEngine)
Resets engine. Cleans up temporary directory and resets each strategy. Resets internal storage of last analysis results.  
__reset(delete_dir=False, clean_lookbacks=False, clean_loc_decision_funcs=False)__  
````
:param delete_dir: bool, default=False,
                   if True, entire temporary directory is deleted
                   if False, only files within temporary directory (cached results) are deleted, but
                             temporary directory remains and is usable to cache results again
                   exception is not raised if no temporary directory is available
:param clean_lookbacks: bool, default=False,
                        if True, also deletes file, which stores lookback values
                        if False, keeps file storing lookback values
                        use param:clean_lookback=False if engines has to be cleaned up for next run
                        use param:clean_lookback=True if engines is going to be closed
                        exception is not raised if no lookbacks in temporary directory
:param clean_loc_decision_funcs: bool, default=False,
                                 If True, also deletes file, which stores loc_decision_functions values.
                                 If False, keeps file storing loc_decision_functions values.
                                 Use param:clean_loc_decision_funcs=False if engines has to be cleaned up for next run.
                                 Use param:clean_loc_decision_funcs=True if engines is going to be closed.
                                 Exception is not raised if no loc_decision_funcs in temporary dictionary.
                                 loc_decision_func (loc='lines of codes') is the source code of the strategy decision 
                                 function which is only retrieved if loc_decision_functions=True at FXEngine.__init__(), 
                                 (see FXEngine.__init__()).
:return: None
````
#### register_strategies()
Registers new strategies to Engine. This resets the complete engine object, deletes all existing strategies and assigns 
the new strategies to the engine object. This method also handles the strategies' lookback values automatically. All 
temporary results will be cleaned up, the attributes _FXEngine.standard_analysis_results_ and 
_FXEngine.analysis_results_ will be reset as well. Even though registering new strategies is possible, it is highly 
recommended using the constructor of FXEngine to bind strategies to the engine object.

All class attributes will be reset or overwritten except:
    - self._ignore_type_checking,  
    - self._tmp_dir,  
    - self._is_loaded_from_file,  
    - self.n_jobs,  
    - self.name  
  
__register_strategies(strategies, loc_decision_functions=False)__  
````
:param strategies: list of strategies or single strategy object (has to inherit from base class BaseStrategy)
:param loc_decision_funcsion: bool, whether to retrieve source code of strategies, see section about instantiation for further
                              details
                              
:return: None
````
#### _clean_artifacts()
Uses shutil.rmtree(cache_dir) to delete sub-directories and files of cache_dir (cache_dir itself will not be deleted).  
__clean\_artifacts(clean_dir)__  
````
:param clean_dir: str, path to parent directory where FXEngine defaults to create new temporary director for
                  each __init__, absolute full path
:return: None
````
#### run_strategy()
__Normally, the user does not need to call this function explicitly. This is done automatically by [FXEngine.run()](#run---fxengine).__  
  
__See [documentation - FXEngine](engine.md) for further details regarding e.g. naming convention of start_index and 
window_length.__  
  
Runs single strategy with one data sample/ set (data partition) and dumps results into temporary directory FXEngine._tmp_dir.  
Start index takes lookback size of strategy into account such that the actual simulation starts from
start_index + lookback (lookback provides historic data to strategy decision function for implementing custom
trade logic). Start index gives the start point for slicing data into respective samples, therefore start index
has to factor in lookback. The actual strategy execution starts at start_index + lookback.
Opposed to start index, window length does not factor in lookback but just gives the total number
of time steps per simulation run.  
Data is sliced according to \[start_index: start_index + window_length + lookback].
If a runthrough on the entire data frame is enforced (by setting param:run_through=True in self.run()), then
start index is set to 'runthrough', whereas start index normally is an integer.  
This functions dumps the results into temporary files.
  
__run_strategy(strategy_index, reference_currency, window_size, start_index)__  
````
:param strategy_index: int,
                       index number within FXEngine.strategies in order to select strategy, which shall be run.
                       Ordering is the same as in the list of strategies which has been passed to FXEngine.__init__()
:param reference_currency: str,
                           reference currency in which to calculate total balances of all accounts
:param window_size: int,
                    sampling window size of respective price_data_partition and volume_data_partition, which are
                    fed into strategy.run(), indexing with respect to complete data sets FXEngine.price_data and
                    FXEngine.volume_data
:param start_index: int or 'runthrough',
                    start index of respective price_data_partition and volume_data_partition, which are fed
                    into strategy.run(), indexing with respect to complete data sets FXEngine.price_data and
                    FXEngine.volume_data
                    if start_index='runthrough', start_index is internally set to 0
:return: strategy.merged_total_balances[0]: int,
                                            start balance/ total value of strategy in param:reference_currency at start 
                                            of backtest simulation
         strategy.merged_total_balances[-1]: int,
                                             final balance/ total value of strategy in param:reference_currency at the 
                                             end of backtest simulation
````
#### analyze_strategy()
__Normally, the user does not need to call this function explicitly. This is done automatically by [FXEngine.analyze()](#analyze).__  
  
__See [documentation - FXEngine](engine.md) for further details regarding e.g. naming convention of start_index and 
window_length.__  
  
Loops through temporary files which contain results of strategy_name and analyzes metric_dict.
Callables listed in metric_dict must have following call signature:
````
float = metric_fnc(merged_total_balances),
merged_total_balances: see trapeza.strategy.fx_strategy.FXStrategy class
````    
__analyze_strategy(strategy_name, file_name, window_size, start_index, std_metric_dict, metric_dict)__  
````
:param strategy_name: str,
                      name of strategy, which is listed within FXEngine.strategies
:param file_name: str,
                  filename of temporary file, which was created during FXEngine.run() and which shall be analyzed
                  with respect to param:metric_dict
:param window_size: int,
                    window size of param:file_name temporary file, see FXEngine.run()
:param start_index: int or 'runthrough',
                    start index at which data is sliced into sample, which was used for param:file_name
                    temporary file during FXEngine.run(), see FXEngine.run() 
:param std_metric_dict: dict,
                       {metric_name: metric_fnc}, metric_name as str, metric_fnc as callable function
                       standard pre-implemented metrics, see FXEngine.analyze
:param metric_dict: dict,
                    {metric_name: metric_fnc}, metric_name as str, metric_fnc as callable function
                    metric_fnc call signature:
                        float = metric_fnc(merged_total_balances)
                        merged_total_balances: list, see trapeza.strategy.fx_strategy.FXStrategy class
                    custom metrics, see FXEngine.analyze
:return: strategy_name, window_size, start_index, metric_result, dict_type,
         strategy_name: see param:strategy_name
         window_size: see param:window_size
         start_index: see param:start_index
         std_metric_result: dict,
                            key: metric_name from param:metric_dict
                            value: computed value of metric_fnc from param:metric_dict
                            pre-implemented metrics
         metric_result: dict,
                        key: metric_name from param:metric_dict
                        value: computed value of metric_fnc from param:metric_dict
                        custom metrics
````    

------------------------
## FXDashboard
For further details see the [documentation - Visualization](visualization.md) or the [docstring and implementation](../trapeza/dashboard/fx_dashboard.py#L30-L153).      

------------------------
### class: trapeza.dashboard.FXDashboard()
- _trapeza_ is not primarily designed for visualization purposes. Nonetheless, to make visualization 
  easier, _trapeza_ provides the _FXDashboard_ class.  
- _FXDashboard_ can visualize all the runs performed by _FXEngine_ (stochastic sub-sampling),   
- can visualize trading signals appended via _FXStrategy.add_signal()_ (see [FXStrategy - Signals](strategy.md#adding-signals))  
- and can visualize user-defined metrics (see [FXEngine - Metrics](engine.md#results-regarding-metrics)).  
- Regarding metrics, _FXDashboard_ not only visualizes single metrics, but also their distribution w.r.t. to the various 
  stochastically sub-sampled uns of _FXStrategy_.  
- Symbols for visualizing trading signals can be customized.  
- _FXEngine_ may contain multiple _FXStrategy_ objects (different strategies to be compared against each other), 
  _FXDashboard_ will handle those multiple strategies as well.  
- Base class: [BaseDashboard](../trapeza/dashboard/base_dashboard.py), pure Python implementation.  
  
___________  
_FXDashboard_ implements a browser-based dashboard via the external Python Package _Dash_. To access the dashboard, 
_FXDashboard_ launches a local dash server, which can be accessed via any browser.    
___________    
__trapeza.dashboard.FXDashboard(fx_engine, signal_symbols=None, date_time=None,
                 overview_page_layout='simple', strategy_page_layout='full')__  
````
:param fx_engine: FXEngine object
:param signal_symbols: dict,
                       key: str, will be pattern matched against FXStrategy's signals, see section below
                       value: tuple, specifing the symbol
:param date_time: {None, 1D list, 1D numpy array, pandas dataframe, pandas series}, default: None
                  dates to be used on the x-axis
:param overview_page_layout: {'simple', 'full'}, default: 'simple'
                             layout of the overview page
                             simple: only results/ metrics from the run with the larges window length will be
                                     displayed (maximal number of time steps, see FXEngine for further details)
                             full: results/ metrics from all runs will be displayed
:param strategy_page_layout: {'simple', 'full'}, default: 'full'
                             layout of each strategy page, each strategy contained in fx_engine is visualized on separate pages
                             simple: simple histograms
                             full: stacked histograms per window length of runs generated during FXEngine.run()
                                   overlaid with approximated probability density function
````    

------------------------
### attribute: trapeza.dashboard.FXDashboard
FXDashboard is a pure Python implementation (in contrast to FXAccount and FXStrategy which are implemented via Cython).  
Therefore, in contrast to FXAccount and FXStrategy, all attributes can be manipulated directly. 
#### engine: object
FXEngine object (or any class object derived from base class 'BaseEngine').
#### app: object
dash.Dash() object.
#### signal_symbols: dict or None
Argument _signal_symbols_ passed to FXDashboard.\_\_init__().See [_signal_symbols_ at FXDashboard.\_\_init__()](#class-trapezadashboardfxdashboard).
#### overview_page_layout: str
'simple' or 'full', see parameters at [FXDashboard.\_\_init()__](#class-trapezadashboardfxdashboard).
#### strategy_page_layout: str
'simple' or 'full', see parameters at [FXDashboard.\_\_init()__](#class-trapezadashboardfxdashboard).
#### Further Privat Attributes
Usually, the user does not need to bother with the following class attributes:  
- _len_data_: length of data supplied to FXEngine (number of time steps contained in [FXEngine.price_data](#price_data-dict))
- _palette_: dict, dict keys: ticker symbols in [FXEngine.price_data](#price_data-dict), dict values: color code  
- _\_xx_: parsed datetimes, see parameters at [FXDashboard.\_\_init()__](#class-trapezadashboardfxdashboard)  
      

------------------------
### method: trapeza.dashboard.FXDashboard
#### run()
Launches Dash app server and opens port for web browser. Port is displayed in console. For further information please 
refer to the Dash library.  
  
FXDashboard provides an "EXIT-page" with an exit button, which stops the Dash app server and enables to proceed
with any following code in the main control flow (where FXEngine.close() should be explicitly called at the end).  
__run(debug=False)__  
````
:param debug: bool, default=False
              True: _Dash_ application will be launched in debug mode, which in general is easier to debug and enables 
                    hot reloading. This might trigger multiple re-calculations and should be turned off, if hot 
                    reloading is not necessary. See [https://dash.plotly.com/layout](https://dash.plotly.com/layout) for 
                    more details.  
              False: Dash application will be launched normallly without hot reloading. Preferred, no re-computations.   
````
#### max_window_min_start_index()
Get max window and min start index from FXEngine.run_register[strategy_name].  
__max_window_min_start_index(strategy_name)__  
````
:param strategy_name: str, name of strategy contained in underlying FXEngine object
:return: _max_window: int (can also be string)
         _min_index: int or 'runthrough'
````
#### Drawing Methods
Besides providing an interactive dashboard, _FXDashboard_ also provides access to each of the figures displayed in the 
dashboard. Therefore, each figure can be retrieved via _FXDashboard_'s methods and can either be returned as [Plotly 
graphic object](https://plotly.com/python/graph-objects/) or as a single [Plotly figure](https://plotly.com/python/creating-and-updating-figures/).   
For further details, please see the implementation: [fx_dashboard.py](/../trapeza/trapeza/dashboard/fx_dashboard.py).  
  
Following drawing methods are available:  
- __draw_volumes(from_index=0, to_index=-1, return_type='fig')__  
  ````
  :param from_index: int,
                     start index, from which to start plotting volume data
  :param to_index: int,
                   end index (not included), to which to end plotting volume data
  :param return_type: str, {'fig', 'go'}
                      'go': list of plotly graphical objects, each key in FXEngine.volume_data (see FXEngine for
                            data format) is turned into a separate go.Bar object and enlisted in return value
                      'fig': a single plotly figure will be returned, which additionally updates figure style
  :return: see return_type
  ````
- __draw_underlyings(from_index=0, to_index=-1, return_type='fig')__  
  ````
  :param from_index: int,
                     start index, from which to start plotting price data
  :param to_index: int,
                   end index (not included), to which to end plotting price data
  :param return_type: str, {'fig', 'go'}
                      'go': list of plotly graphical objects, each key in FXEngine.price_data (see FXEngine for
                            data format) is turned into a separate go.Scatter object and enlisted in return value
                      'fig': a single plotly figure will be returned, which additionally updates figure style and
                             adds in volume data from self.draw_volumes() if FXEngine.volume_data is not None
  :return: see return_type
  ````
- __draw_strategy_signals(str_strategy, win_len, start_ind, return_type='fig')__  
  ````
  :param str_strategy: str, strategy name
  :param win_len: int, window length (see FXEngine)
  :param start_ind: int or 'runthrough' (see FXEngine)
  :param return_type: str, {'fig', 'go'}
                      'go': list of plotly graphical objects go.Scatter, wherein each go.Scatter contains each
                            unique set of the total set of all signals of given strategy of FXEngine
                      'fig': a single plotly figure will be returned, which additionally updates figure style
  :return: see return_type
  ````
- __draw_strategy_performance(str_strategy, win_len, start_ind, return_type='fig')__  
  ````
  :param str_strategy: str, strategy name
  :param win_len: int, window length (see FXEngine)
  :param start_ind: int or 'runthrough' (see FXEngine)
  :param return_type: str, {'fig', 'go'}
                      'go': list of plotly graphical objects go.Scatter, wherein each go.Scatter contains the
                            strategy performance (indexed to 100 base points) time series of given strategy in
                            FXEngine
                      'fig': a single plotly figure will be returned, which additionally updates figure style and
                             adds high water marks, horizontal line at 100 base points and max drawdown
  :return: see return_type
  ````
- __draw_strategy_composition(str_strategy, \_win_len, \_start_index, return_type='fig')__  
  ````
  :param str_strategy: str, strategy name
  :param _win_len: int, window length (see FXEngine)
  :param _start_index: int or 'runthrough' (see FXEngine)
  :param return_type: str, {'fig', 'go'}
                      'go': list of plotly graphical objects go.Scatter, wherein each go.Scatter contains the
                            time series of each merged position of given strategy of FXEngine
                      'fig': a single plotly figure will be returned, which additionally updates figure style and
                             adds total strategy value (sum of all positions) on top as line
  :return: see return_type
  ````
- __draw_strategy_metric_yield(str_strategy, str_metric, return_type='fig')__  
  ````
  :param str_strategy: str, strategy name
  :param str_metric: str, key describing metric in FXEngine.standard_analysis_results dictionary !!!
  :param return_type: str, {'fig', 'go'}
                      'go': list of plotly graphical objects, with
                          at index [0]: go.Scatter for min value of all runs for each window length of
                                        FXEngine.run()
                          at index [1]: go.Scatter for max value of all runs for each window length of
                                        FXEngine.run()
                          at index [2]: go.Scatter for average value over all runs for each window length of
                                        FXEngine.run()
                      'fig': a single plotly figure will be returned, which additionally updates figure style and
                             dotted line for annual compounded 3% value visualized for reference
  :return: see return_type
  ````
- __draw_strategy_metric_stacked_hist(str_strategy, str_metric, res_dict, return_type='fig')__  
  ````
  :param str_strategy: str, strategy name
  :param str_metric: str, key describing metric in param:res_dict, which is either standardized
                     FXEngine.standard_analysis_result dict or custom FXEngine.analysis_result dict
  :param res_dict: dict,
                   either standardized FXEngine.standard_analysis_result dict
                   or custom FXEngine.analysis_result dict
  :param return_type: str, {'fig', 'go'}
                      'go': list of plotly graphical objects, with stacked histograms for each metric in
                            param:res_dict for each window length of FXEngine.run() (see FXEngine.run() for
                            further details)
                      'fig': a single plotly figure will be returned, which additionally updates figure style,
                             approximated probability density function, ruge plot and vertical line at mean values
  :return: see return_type
  ````
- __draw_strategy_metric_simple_hist(str_strategy, str_metric, res_dict, return_type='fig')__  
  ````
  :param str_strategy: str, strategy name
  :param str_metric: str, key describing metric in param:res_dict, which is either standardized
                     FXEngine.standard_analysis_result dict or custom FXEngine.analysis_result dict
  :param res_dict: dict,
                   either standardized FXEngine.standard_analysis_result dict
                   or custom FXEngine.analysis_result dict
  :param return_type: str, {'fig', 'go'}
                      'go': list of plotly graphical objects, with simple histograms for each metric in
                            param:res_dict over all window lengths of FXEngine.run() (see FXEngine.run() for
                            further details)
                      'fig': a single plotly figure will be returned, which additionally updates figure style
                             and vertical line at mean values
  :return: see return_type
  ````
      

------------------------
## Context
Settings regarding arithmetics in _trapeza_ can be controlled via _trapeza.context_.  
See [Arithmetics](arithmetics.md) regarding arithmetics options and why this is important to consider (e.g. [Floating Point Error](arithmetics.md#floating-point-error)).  
__Context has to be set before anything (!) else from _trapeza_.  
Context variables have to be set via context.[...]=[...] and must not be imported directly. Instead, import context. 
Else only a reference will be set__ _(this is very Python specific..., see second example)_: see [Arithmetics - How to Set the Backend](arithmetics.md#how-to-set-the-backend).  
  
The following context options are available:  
- __trapeza.context.ARITHMETICS__: str, {_'FLOAT', 'DECIMAL', 'LIBMPDEC_FAST' (uses ryu dtoa), 'LIBMPDEC' (uses dtoa of Cpython)_}  
- __trapeza.context.ARBITRARY_DECIMAL_PRECISION__: int, number of digits  
- __trapeza.context.ARBITRARY_QUANTIZE_SIZE__: int, number of decimal places after radix point  
- __trapeza.context.ROUNDING__: str, 
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {_ROUND_CEILING, ROUND_FLOOR, ROUND_HALF_EVEN, 
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ROUND_HALF_TOWARDS_ZERO, ROUND_HALF_AWAY_FROM_ZERO_}  
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; and {_ROUND_UP, ROUND_DOWN, ROUND_05UP: only for DECIMAL and LIBMPDEC_}   
  
Default settings:
````
trapeza.context.ARITHMETICS = 'LIBMPDEC_FAST'  
trapeza.context.ARBITRARY_DECIMAL_PRECISION = 28   (same as Python's decimal module)  
trapeza.context.ARBITRARY_QUANTIZE_SIZE = 10  
trapeza.context.ROUNDING = 'ROUND_HALF_EVEN'    (also called banker's rounding) 
````    

------------------------
## Metrics
See [Documentation - Metrics](metric.md).  
  
Available metrics:  
  - [total_rate_of_return](metric.md#total_rate_of_return)  
  - [volatility](metric.md#volatility)  
  - [downside_risk](metric.md#downside_risk)  
  - [value_at_risk](metric.md#value_at_risk)  
  - [sharpe_ratio](metric.md#sharpe_ratio)  
  - [sortino_ratio](metric.md#sortino_ratio)  
  - [max_drawdown](metric.md#max_drawdown)  
  - [high_water_marks](metric.md#high_water_marks)  
  - [drawdown](metric.md#drawdown)  
  - [_returns](metric.md#_returns)  
  - [continuous_lower_partial_moment](metric.md#continuous_lower_partial_moment)  
  - [kde_lower_partial_moment](metric.md#kde_lower_partial_moment)  
  - [expected_rate_of_return](metric.md#expected_rate_of_return)  
      

------------------------
## Exceptions
_trapeza_ has a series of custom exception types. See [Documentation - Exceptions](exceptions.md) for further information.  
Exceptions are accessible via _trapeza.exception_.  
### CoverageError
Raised when coverage violation occurs on account level, e.g. position drops into negative range (debiting). Dependent 
whether coverage checks are turned on at account level. See [Account - Coverage](account.md#coverage).  
### AccountingError
Raised when any inconsistencies occur in accounting/ calculation, e.g. trying to perform arithmetic operations on 
different currencies (e.g. 1€ + 1$ without providing an exchange rate).
### PositionNAError
Raised when position (asset or currency) is not listed within an account's depot. See [Account - Depot](account.md#depot-status).
### AccountError
Raised when action cannot be performed on account object due to wrong account class type. This is usual the case if account 
does not inherit from the base class [BaseAccount](main_concepts.md#base-classes).
### StrategyError
Raised when action cannot be performed on strategy object due to wrong strategy class type. This is usual the case if strategy 
does not inherit from the base class [BaseStrategy](main_concepts.md#base-classes).
### OperatingError
Raised when any internal operation or transaction cannot be executed due to unknown logic error.  
    
  
  
---  
[Back to Top](#api)