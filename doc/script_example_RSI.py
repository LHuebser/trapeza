# we have to manipulate context first before doing anything else
from trapeza import context
context.ARBITRARY_QUANTIZE_SIZE = 3     # restrict to three after decimal places

from trapeza.account import FXAccount
from trapeza.strategy import FXStrategy
from trapeza.engine import FXEngine
from trapeza.dashboard import FXDashboard
from trapeza import metric

import numpy as np
from matplotlib import pyplot as plt
# noinspection PyPackageRequirements
import seaborn as sns
from pandas_datareader import data
import yfinance as yfin
import time


yfin.pdr_override()     # patch pandas-datareader as it sometimes has problems when yahoo makes changes to its website


# noinspection PyProtectedMember
def rsi(x, reference):
    # calculate differences/ absolute changes per time step -> this is the same as calculating stock returns
    differences = metric._returns(x, reference)

    # calculate average of positive gains
    avg_up = np.sum(differences[differences > 0]) / len(differences)

    # calculate average of negative gains
    avg_down = np.abs(np.sum(differences[differences < 0]) / len(differences))

    return avg_up / (avg_up + avg_down)


# noinspection DuplicatedCode,PyUnusedLocal
def rsi_strategy(accounts, price_data_pt, reference_currency, volume_data_pt, fxstrategy, rsi_method, rsi_reference,
                 tick_symbol):
    # first calculate RSI
    rsi_signal = rsi_method(price_data_pt[tick_symbol, reference_currency], rsi_reference)

    # read current asset position and cash position
    asset = accounts[0].position(tick_symbol)   # in number of stocks
    cash = accounts[0].position(reference_currency)     # in reference currency (€)

    current_price = price_data_pt[tick_symbol, reference_currency][-1]

    # make trading decision and add signal
    if rsi_signal > 0.8 and asset > 0:    # over-bought, only sell if we have a valid position size
        # we sell proportional to the signal strength
        strength = (rsi_signal-0.8) / 0.2
        # we have to round to the next integral number of stocks
        sell_volume = np.floor(asset*strength)
        accounts[0].sell(sell_volume, current_price, tick_symbol, reference_currency)
        fxstrategy.add_signal(accounts[0], 'sell_{}_{}'.format(tick_symbol, reference_currency))
    elif rsi_signal < 0.2 and cash > 70:  # over-sold, only buy if we have enough cash to cover fees
        # we buy proportional to the signal strength
        strength = (0.2-rsi_signal) / 0.2
        # we have to round the number of stocks to integral number
        # we hold out 70€ to be sure that we can cover fees
        buy_volume = np.floor((strength * (cash-70)) / current_price)
        accounts[0].buy(buy_volume, current_price, tick_symbol, reference_currency)
        fxstrategy.add_signal(accounts[0], 'buy_{}_{}'.format(tick_symbol, reference_currency))


# noinspection PyUnusedLocal
def rsi_strategy_bias(accounts, price_data_pt, reference_currency, volume_data_pt, fxstrategy, rsi_method,
                      rsi_reference, tick_symbol):
    # first calculate RSI
    rsi_signal = rsi_method(price_data_pt[tick_symbol, reference_currency], rsi_reference)

    # read current asset position and cash position
    asset = accounts[0].position(tick_symbol)   # in number of stocks
    cash = accounts[0].position(reference_currency)     # in reference currency (€)

    current_price = price_data_pt[tick_symbol, reference_currency][-1]

    # make trading decision and add signal
    # noinspection DuplicatedCode
    if rsi_signal > 0.8 and asset > 0:    # over-bought, only sell if we have a valid position size
        # we sell proportional to the signal strength
        strength = (rsi_signal-0.8) / 0.27  # --> !
        # we have to round to the next integral number of stocks
        sell_volume = np.floor(asset*strength)
        accounts[0].sell(sell_volume, current_price, tick_symbol, reference_currency)
        fxstrategy.add_signal(accounts[0], 'sell_{}_{}'.format(tick_symbol, reference_currency))
    elif rsi_signal < 0.2 and cash > 70:  # over-sold, only buy if we have enough cash to cover fees
        # we buy proportional to the signal strength
        strength = (0.2-rsi_signal) / 0.2
        # we have to round the number of stocks to integral number
        # we hold out 70€ to be sure that we can cover fees
        buy_volume = np.floor((strength * (cash-70)) / current_price)
        accounts[0].buy(buy_volume, current_price, tick_symbol, reference_currency)
        fxstrategy.add_signal(accounts[0], 'buy_{}_{}'.format(tick_symbol, reference_currency))


# noinspection PyUnusedLocal
def buy_and_hold_strategy(accounts, price_data_pt, reference_currency, volume_data_pt, fxstrategy, tick_symbol):
    cash = accounts[0].position(reference_currency)  # in reference currency (€)
    current_price = price_data_pt[tick_symbol, reference_currency][-1]
    if cash > 70:
        buy_volume = np.floor((cash - 70) / current_price)
        accounts[0].buy(buy_volume, current_price, tick_symbol, reference_currency)
        fxstrategy.add_signal(accounts[0], 'buy_{}_{}'.format(tick_symbol, reference_currency))


# noinspection PyUnusedLocal
def fee(volume, price, base, quote, processing_duration):
    # calculate volume in currency (quote)
    cash_equivalent_volume = volume * price

    fee_amount = 0.0025 * cash_equivalent_volume + 4.9
    fee_amount = min([fee_amount, 69.9])

    return fee_amount, quote, 0


def build_data(ticker_symbols, quote, start_data, end_date):
    _price_data = dict()
    _dates = False

    for _ticker in ticker_symbols:
        ticker_price_data = data.DataReader(_ticker, start=start_data, end=end_date,
                                            data_source='yahoo')['Close'].reset_index().drop_duplicates('Date', 'last')
        if _dates is False:
            _dates = ticker_price_data['Date']
        ticker_price_data = ticker_price_data['Close'].to_numpy()
        _price_data[_ticker, quote] = ticker_price_data

    return _dates, _price_data


if __name__ == "__main__":
    # get the data
    ticker = 'ADS.DE'
    dates, price_data = build_data([ticker], 'EUR', '2019-05-12', '2021-05-12')
    dates = dates.to_numpy()

    # init account object and deposit 5,000€
    acc = FXAccount('EUR', fee_buy=fee, fee_sell=fee, ignore_type_checking=True)
    acc.deposit(5000, 'EUR')

    # init strategy object
    strat = FXStrategy('rsi-14-pct', acc, rsi_strategy, 14,
                       rsi_method=rsi, rsi_reference='pct_return', tick_symbol=ticker, ignore_type_checking=True)

    # run strategy
    start_time = time.perf_counter()
    strat.run(price_data, 'EUR')
    end_time = time.perf_counter()
    print('Elapsed time for single strategy execution: {}'.format(end_time-start_time))

    # visualize results
    # underlying
    plt.figure()
    plt.plot(dates, price_data[ticker, 'EUR'], color='black')
    plt.title('Underlying')
    plt.xticks(rotation=90)
    plt.show()
    # total value of strategy
    plt.figure()
    plt.plot(dates[14:], strat.merged_total_balances, label='strategy', color='blue')
    plt.ylabel('EUR')
    plt.legend()
    plt.xticks(rotation=90)
    # total value of strategy performance, price data performance
    plt.figure()
    plt.plot(dates[14:], (np.array(strat.merged_total_balances)/5000)*100, label='strategy', color='blue')
    plt.ylabel('% indexed to strategy start [t_0=14]')
    plt.plot(dates, (price_data[ticker, 'EUR']/price_data[ticker, 'EUR'][14])*100, label='ads|EUR', color='black')
    plt.legend(loc='upper right')
    plt.xticks(rotation=90)
    # positions
    plt.figure()
    plt.stackplot(dates[14:],
                  strat.merged_positions['EUR'],
                  np.array(strat.merged_positions[ticker])*np.array(price_data[ticker, 'EUR'][14:]),
                  baseline='zero', labels=['EUR', 'ads'], colors=['yellow', 'indigo'])
    plt.legend()
    plt.xticks(rotation=90)
    # price data, signals, rsi
    _, ax1 = plt.subplots()
    ax1.plot(dates, price_data[ticker, 'EUR'], label='ads|EUR', color='black')
    buy_indices = [i + 14 for i in range(len(strat.signals)) for s in strat.signals[i][0] if 'buy' in s]
    sell_indices = [i + 14 for i in range(len(strat.signals)) for s in strat.signals[i][0] if 'sell' in s]
    ax1.scatter(dates[buy_indices], price_data[ticker, 'EUR'][buy_indices], label='buy', color='green', s=20)
    ax1.scatter(dates[sell_indices], price_data[ticker, 'EUR'][sell_indices], label='sell', color='red', s=20)
    ax1.set_ylabel('EUR', color='black')
    plt.legend(loc='upper left')
    plt.xticks(rotation=90)
    ax2 = ax1.twinx()
    rsi_vals = np.array([rsi(price_data[ticker, 'EUR'][i-15:i], 'pct_return')*100 for i in range(15, len(dates)+1)])
    ax2.plot(dates[14:], rsi_vals,
             label='rsi', color='grey', linestyle=':')
    ax2.fill_between(dates[14:], rsi_vals, y2=20, where=rsi_vals <= 20, interpolate=True, facecolor='green', alpha=0.25)
    ax2.fill_between(dates[14:], rsi_vals, y2=80, where=rsi_vals >= 80, interpolate=True, facecolor='red', alpha=0.25)
    ax2.hlines([80, 20], colors=['grey', 'grey'], linestyles=['-.', '-.'], xmin=dates[14], xmax=dates[-1])
    ax2.set_ylabel('%', color='grey')
    plt.legend(loc='upper right')
    plt.xticks(rotation=90)

    # perform stochastic analysis
    # first get more data
    dates, price_data = build_data([ticker], 'EUR', '1999-05-12', '2021-05-12')

    # reset strategy
    strat.reset()

    # init an engine object
    engine = FXEngine('rsi_engine', strat, ignore_type_checking=True, cache_dir='__cache__')

    # run the engine
    start_time = time.perf_counter()
    engine.run(price_data, 'EUR', min_run_length=60, max_total_runs=100)

    # analyze the engine
    engine.analyze()
    end_time = time.perf_counter()
    print('Elapsed time for 100 simulation runs and metrics calculations: {}'.format(end_time-start_time))

    # we are interested in the total return
    total_returns = [engine.standard_analysis_results['rsi-14-pct'][w, s]['total_rate_of_return']
                     for w in engine.run_register['rsi-14-pct'].keys()
                     for s in engine.run_register['rsi-14-pct'][w]]
    window_lengths = [w for w in engine.run_register['rsi-14-pct'].keys() for _ in engine.run_register['rsi-14-pct'][w]]

    # visualize
    # histogram and density
    plt.figure()
    sns.distplot(total_returns, hist=True, kde=True, kde_kws={'linewidth': 3})
    plt.title('Distribution of total return')
    plt.ylabel('Density')
    plt.xlabel('Total return')
    print('Average annualized total return: {} %'.format(np.average(total_returns)*100))
    # yield curve
    plt.figure()
    plt.scatter(window_lengths, total_returns, s=15)
    plt.title('Total return over window length')
    plt.xlabel('Window length (sample length for strategy execution)')
    plt.ylabel('Total return')
    plt.show()

    # benchmark other strategies
    strat_bias = FXStrategy('rsi-bias', acc, rsi_strategy_bias, 14,
                            rsi_method=rsi, rsi_reference='pct_return', tick_symbol=ticker, ignore_type_checking=True)
    strat_log = FXStrategy('rsi-14-log', acc, rsi_strategy_bias, 14,
                           rsi_method=rsi, rsi_reference='log_return', tick_symbol=ticker, ignore_type_checking=True)
    strat_buy_and_hold = FXStrategy('buy-and_hold', acc, buy_and_hold_strategy, 14, tick_symbol=ticker)

    engine_new = FXEngine('benchmark_engine', [strat, strat_bias, strat_log, strat_buy_and_hold], cache_dir='__cache__',
                          ignore_type_checking=True, n_jobs=-1)

    start_time = time.perf_counter()
    engine_new.run(price_data, 'EUR', min_run_length=60, max_total_runs=300)
    engine_new.analyze()
    end_time = time.perf_counter()
    print('Elapsed time to benchmark strategies over 300 simulations: {}'.format(end_time-start_time))

    dash = FXDashboard(engine_new, {'buy': ('triangle-up', 'Green'), 'sell': ('triangle-down', 'Red')}, dates, 'full')
    dash.run(debug=False)
    engine.close()
    engine_new.close()
