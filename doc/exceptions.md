Go back: [Table of Content](table_of_content.md)  
- [Exceptions](#exceptions)
  - [Introduction](#introduction)
  - [CoverageError](#coverageerror)
  - [AccountingError](#accountingerror)
  - [PositionNAError](#positionnaerror)
  - [AccountError](#accounterror)
  - [StrategyError](#strategyerror)
  - [OperatingError](#operatingerror)  
     
  
---------
# Exceptions
_trapeza_ has a series of custom exception types.  
________________________________  
## Introduction
For better debugging, _trapeza_ has a series of custom exception types, which are located at _trapeza.exception.py_.

## CoverageError
Raised when coverage violation occurs on account level, e.g. position drops into negative range (debiting). Dependent 
whether coverage checks are turned on at account level. See [Account - Coverage](account.md#coverage).  
## AccountingError
Raised when any inconsistencies occur in accounting/ calculation, e.g. trying to perform arithmetic operations on 
different currencies (e.g. 1€ + 1$ without providing an exchange rate).
## PositionNAError
Raised when position (asset or currency) is not listed within an account's depot. See [Account - Depot](account.md#depot-status).
## AccountError
Raised when action cannot be performed on account object due to wrong account class type. This is usual the case if account 
does not inherit from the base class [BaseAccount](main_concepts.md#base-classes).
## StrategyError
Raised when action cannot be performed on strategy object due to wrong strategy class type. This is usual the case if strategy 
does not inherit from the base class [BaseStrategy](main_concepts.md#base-classes).
## OperatingError
Raised when any internal operation or transaction cannot be executed due to unknown logic error.  
  
  
---  
[Back to Top](#exceptions)