from trapeza.account import FXAccount


if __name__ == "__main__":
    print('------------instantiate------------')
    account = FXAccount('USD')

    # #################################################################
    print('------------deposit------------')
    account.deposit(100, 'USD')  # deposit 100$ onto account
    print(account.depot)  # check account's depot
    # >>> {'USD': 100}

    # #################################################################
    account.deposit(100, 'EUR')  # deposit 100€
    print(account.position('EUR'))  # check account's €-position size
    # >>> 100
    print(account.depot)
    # >>> {'USD': 100, 'EUR': 100}  # two positions in depot now

    # #################################################################
    account.deposit(100, 'APPL')  # deposit 100 shares of Apple stocks
    print(account.depot)
    # >>> {'USD': 100, 'EUR': 100, 'APPL': 100}

    # #################################################################
    print('------------withdraw------------')
    print(account.depot)
    # >>> {'USD': 100, 'EUR': 100, 'APPL': 100}

    # #################################################################
    account.withdraw(50, 'EUR')
    account.withdraw(90, 'APPL')

    print(account.depot)
    # >>> {'USD': 100, 'EUR': 50, 'APPL': 10}

    # #################################################################
    print('------------buy------------')
    print(account.depot)
    # >>> {'USD': 100, 'EUR': 50, 'APPL': 10}

    account.buy(10, 0.82, 'USD', 'EUR')  # buy 10$ and give €s in exchange at an ask price of 0.82[€/$]
    print(account.depot)
    # >>> {'USD': 110, 'EUR': 41.8, 'APPL': 10}

    account.buy(1, 104.78, 'APPL', 'USD')  # buy one Apple stock at an ask price of 104.78$ (104.78[$/APPL])
    print(account.depot)
    # >>> {'USD': 5.22 'EUR': 41.8, 'APPL': 11}

    # #################################################################
    print('------------sell------------')
    print(account.depot)
    # >>> {'USD': 5.22 'EUR': 41.8, 'APPL': 11}

    account.sell(3, 0.82, 'USD', 'EUR')  # sell 3$ and receive €s at a bid price of 0.82[€/$]
    print(account.depot)    # 1$ is worth 0.82€ --> 3$ is worth 2.46€
    # >>> {'USD': 2.22, 'EUR': 44.26, 'APPL': 11}

    account.sell(2, 104.78, 'APPL', 'USD')  # sell two Apple stocks at a bid price of 104.78$ (104.78[$/APPL])
    print(account.depot)
    # >>> {'USD': 211.78, 'EUR': 44.26, 'APPL': 9}

    # #################################################################
    print('------------transfer------------')
    print(account.depot)
    # >>> {'USD': 211.78, 'EUR': 44.26, 'APPL': 9}

    counter_account = FXAccount('EUR')                  # counterpart's account can be in any reference currency

    account.transfer(counter_account, 100, 'USD')       # transfer 100 USD, no automatic conversion into counterpart's reference currency
    print(account.depot)
    # >>> {'USD': 111.78, 'EUR': 44.26, 'APPL': 9}
    print(counter_account.depot)
    # >>> {'USD': 100}

    account.transfer(counter_account, 1, 'APPL')        # transfer 1 share of Apple stocks
    print(account.depot)
    # >>> {'USD': 111.78, 'EUR': 44.26, 'APPL': 8}
    print(counter_account.depot)
    # >>> {'USD': 100, 'APPL': 1}

    # #################################################################
    print('------------collect------------')
    print(account.depot)
    # >>> {'USD': 111.78, 'EUR': 44.26, 'APPL': 8}
    print(counter_account.depot)
    # >>> {'USD': 100, 'APPL': 1}

    # exchange some USD at the counterpart's side
    counter_account.sell(50, 0.82, 'USD', 'EUR')
    print(counter_account.depot)
    # >>> {'USD': 50, 'EUR': 41, 'APPL': 1}

    account.collect(counter_account, 20, 'EUR')         # collect 20€
    print(account.depot)
    # >>> {'USD': 111.78, 'EUR': 64.26, 'APPL': 8}
    print(counter_account.depot)
    # >>> {'USD': 50, 'EUR': 21, 'APPL': 1}

    # #################################################################
    print('------------total balance------------')
    print(account.depot)
    # >>> {'USD': 111.78, 'EUR': 64.26, 'APPL': 8}

    exchange_rates = {('USD', 'EUR'): 0.82,
                      ('APPL', 'USD'): 104.78}

    total_balance = account.total_balance(exchange_rates=exchange_rates, reference_currency='USD')

    print(total_balance)
    # >>> 1028.3858536585
