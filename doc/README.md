# Welcome to the Documentation of _trapeza_!  
    
Go to [__Table of Content__](table_of_content.md).    
  
Go to [__Examples__](examples.md).  
  
Go to [__API Reference__](api_reference.md).  
  
Go to [__trapeza's README__](../README.md).