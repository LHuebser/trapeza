Go back: [Table of Content](table_of_content.md)  
- [Arithmetics](#arithmetics)  
  - [Floating Point Error](#floating-point-error)
  - [Precision and Quantization](#precision-and-quantization)
    - [ARBITRARY_DECIMAL_PRECISION](#arbitrary_decimal_precision)
    - [ARBITRARY_QUANTIZE_SIZE](#arbitrary_quantize_size)
  - [Backends](#backends)
    - [LIBMPDEC_FAST](#libmpdec_fast)
    - [LIBMPDEC](#libmpdec)
    - [DECIMAL](#decimal)
    - [FLOAT](#float)
    - [emyg dtoa](#emyg-dtoa)
    - [Comparison](#comparison)
  - [Rounding Modes](#rounding-modes)
  - [How to Set the Backend](#how-to-set-the-backend)
  - [Known Bogus Behavior](#known-bogus-behavior)  
  
  
---------  
# Arithmetics
_trapeza_ provides different approaches to handling floating point vs. decimal math. Arithmetics settings are controlled 
via _trapeza.context_.
________________________________  
## Floating Point Error
````python
print(1.1 * 3)
# >>> 3.3000000000000003
````  
The above example shows the floating point error, which occurs in nearly every programming language. This is not specific 
to Python but the way how computers represent floating point numbers. Simply speaking, not every decimal number can 
be represented by floating point binary representation.  
There are several ways on how to deal with this in order to avoid error accumulation. One way would be to use fixed point 
math (which comes with other inherent problems and is [currently not implemented](../README.md#future-development)).   
Another way of dealing with this is by using decimal math software packages. _trapeza_ implements a Cython-based C-wrapper 
around [mpdecimal](https://www.bytereef.org/mpdecimal/index.html) and can also use Python's decimal module (which is just 
a Python wrapper for mpdecimal).  
## Precision and Quantization
'Precision' is generally controlled via two parameters in _trapeza_:  
  - ARBITRARY_DECIMAL_PRECISION: Controls the total number of significant digits.  
  - ARBITRARY_QUANTIZE_SIZE: Controls the maximal number of decimal places (digits after the radix point).  
  
These options are only available for ['DECIMAL'](#decimal), ['LIBMPDEC'](#libmpdec) and ['LIBMPDEC_FAST'](#libmpdec_fast).  
They can be controlled via _trapeza.context.ARBITRARY_DECIMAL_PRECISION_ and _trapeza.context.ARBITRARY_QUANTIZE_SIZE_.  
### ARBITRARY_DECIMAL_PRECISION
The total number of significant digits is the total number of digits in a number except leading and trailing 0s:  
5.1 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -> precision: 2  
5.12 &nbsp;&nbsp;&nbsp; -> precision: 3  
0.12 &nbsp;&nbsp;&nbsp; -> precision: 2  
0.123 &nbsp; -> precision: 3  
0.012 &nbsp; -> precision: 2  
0.0123 -> precision: 3  
0.102 &nbsp; -> precision: 3  
0.120 &nbsp; -> precision: 2  
0.0120 -> precision: 2  
### ARBITRARY_QUANTIZE_SIZE
This controls the total number of digits after the radix point (also called decimal places) except trailing 0s:  
5.1 &nbsp;&nbsp;&nbsp; -> decimal places: 1  
5.12 &nbsp;&nbsp; -> decimal places: 2  
0.012 &nbsp; -> decimal places: 3  
0.0120 -> decimal places: 3  
0.0012 -> decimal places: 4  
  
This setting is useful in order to prevent inflation within decimal places:  
````
1.16 * 1.16 = 1.3456
````  
Multiplication of two decimal numbers increases the number of decimal places. In order to avoid that the number of 
decimal places grows inflationary during calculations (which costs performance and memory and might lead to inaccuracies), 
the total number of decimal places can be restricted.  
## Backends
Arithmetic calculations can be performed as floating point or as decimal math. This can be controlled via _trapeza.context.ARITHMETICS_.  
### LIBMPDEC_FAST
This uses a Cython-based wrapper around the C library libmpdec (mpdec). In order to perform decimal math with libmpdec, 
input numbers have to be converted to strings. Within this option, the _ryu dtoa algorithm_ is used. _trapeza_ wraps the 
C++ implementation via Cython. This dtoa operation is the most time-consuming step. _ryu dtoa_ is currently one of the 
fastest algorithms for this task.  
  
Nonetheless, in rare cases the last digit in the decimal places might be slightly off due to _ryu dtoa_. This only occurs 
in really rare cases! In general, the computational speed weighs off those rare cases.    
### LIBMPDEC
Like ['LIBMPDEC_FAST'](#libmpdec_fast) but uses CPython's (this is the underlying C-implementation of Python) _dtoa algorithm_.  

Cpython's _dtoa_ is more stable than _ryu dtoa_ but also a lot slower! As mentioned above, instabilities in _ryu dtoa_ are 
extremely rare.  
### DECIMAL  
Uses Python's decimal module and _dtoa_ method. Input numbers are first converted via str()-method and are then converted 
via Python's decimal module to decimal number. This guarantees the most accuracy and stability but is comparably slow.  
As mentioned above, instabilities in _ryu dtoa_ are extremely rare.
### FLOAT
Performs normal floating point math. This is by far the fastest but also the most inaccurate option.  
___
### emyg dtoa
A Cython-based wrapper of the _emyg dtoa_ algorithm (C-implementation) is located at _trapeza.arithmetics.dtoa.pyx_. 
This method is almost as fast as _ryu dtoa_ but more unstable and restricted to precision of 15-17 digits.  
This method is __not used anymore in _trapeza___.
___
### Comparison  
| Backend       | Speed     | Accuracy                         | dtoa           | Implementation |
| ------------- | --------- | -------------------------------- | -------------- | -------------- |
| LIBMPDEC_FAST | very fast | inaccurate in extreme rare cases | ryu            | underlying C   |
| LIBMPDEC      | fast      | accurate                         | CPython's dtoa | underlying C   |
| DECIMAL       | slow      | 'very' accurate                  | Python's dtoa  | pure Python    |
| FLOAT         | fastest   | inaccurate                       | -              | Cython C       |
## Rounding Modes
Rounding is only applied for decimal math (DECIMAL, LIBMPDEC_FAST and LIBMPDEC). Rounding appears whenever the 
quantization or precision is exceeded.  
  
The following rounding methods are implemented:  
- ROUND_CEILING  
- ROUND_FLOOR  
- ROUND_HALF_EVEN  
- ROUND_HALF_TOWARDS_ZERO  
- ROUND_HALF_AWAY_FROM_ZERO  
  
__Additional, only in DECIMAL, LIBMPDEC_FAST and LIBMPDEC__ (which is currently all decimal math backends but might be
important if other backends are implemented in [the future](../README.md#future-development)):  
- ROUND_UP  
- ROUND_DOWN  
- ROUND_05UP  
  
## How to Set the Backend
__Context has to be set before anything (!) else from _trapeza_.  
Context variables have to be set via context.[...]=[...] and must not be imported directly. Instead, import context. 
Else only a reference will be set__ _(this is very Python specific..., see second example)_:
````python
from trapeza import context
context.ARITHMETICS = 'LIBMPDEC_FAST'
context.ARBITRARY_DECIMAL_PRECISION = 28
context.ARBITRARY_QUANTIZE_SIZE = 10
context.ROUNDING = 'ROUND_HALF_EVEN'

# continue with user code
from trapeza.account import FXAccount
acc = FXAccount('EUR')
````  
  
__This will NOT work__ _(only a reference to context is altered...)_:
````python
from trapeza.context import ARITHMETICS
ARITHMETICS = 'FLOAT'   # this sets the local variable ARITHMETICS but not the variable in trapeza.context

# no changes have been made!
from trapeza.account import FXAccount
acc = FXAccount('EUR')
# all arithmetics are still done in the default value of trapeza.context.ARITHMETICS instead of the 
# desired 'FLOAT' option!
````  
_(This is somehow a Python quirk...)_    
  
The following context options are available:  
- trapeza.context.ARITHMETICS: str  
- trapeza.context.ARBITRARY_DECIMAL_PRECISION: int  
- trapeza.context.ARBITRARY_QUANTIZE_SIZE: int  
- trapeza.context.ROUNDING: str  
  
Default settings:
````
trapeza.context.ARITHMETICS = 'LIBMPDEC_FAST'  
trapeza.context.ARBITRARY_DECIMAL_PRECISION = 28   (same as Python's decimal module)  
trapeza.context.ARBITRARY_QUANTIZE_SIZE = 10  
trapeza.context.ROUNDING = 'ROUND_HALF_EVEN'    (also called banker's rounding) 
````
## Known Bogus Behavior
- No overflow checking implemented.  
- Be careful if input precision is greater than _trapeza.context.ARBITRARY_DECIMAL_PRECISION_ or 
  _trapeza.context.ARBITRARY_QUANTIZE_SIZE_. This leads to premature precision loss before even any calculations have 
  taken place.  
- In rare cases, there still might occur inaccuracies: _1.00001 \*  3.00001 == 3.0000400001_. Those very rare cases 
  occur in all backend implementations (even in Python's decimal module). This can be somehow mitigated by restricting 
  _ARBITRARY_QUANTIZE_SIZE_ which cuts off those unwanted excess decimal places (e.g. ARBITRARY_QUANTIZE_SIZE=6). 
  Nonetheless, do not trust the results to the last decimal place for a 100%!  
  __The user is always responsible by himself to check his results. The author of this software and accompanying 
  materials makes no representation or warranties with respect to the accuracy, applicability, fitness, or completeness 
  of the contents. Therefore, if you wish to apply this software or ideas contained in this software, you are taking 
  full responsibility for your action.__  
  
  
---  
[Back to Top](#arithmetics)