Go back: [Table of Content](table_of_content.md)  
- [Visualization](#visualization)
  - [Visualization via external libraries](#visualization-via-external-libraries)
  - [FXDashboard](#fxdashboard)
    - [Instantiate Dashboard Object](#instantiate-dashboard-object)
    - [Defining Symbols to visualize Signals](#defining-symbols-to-visualize-signals)
    - [Defining Symbol Shape and Color](#defining-symbol-shape-and-color)
    - [Passing Date Times](#passing-date-times)
    - [Page Layouts](#page-layouts)
    - [Volume Data](#volume-data)
    - [Run and open the Dashboard](#run-and-open-the-dashboard)
    - [Close the Dashboard](#close-the-dashboard)
    - [Visualizing Metrics](#visualizing-metrics)
    - [Returning plots and figures](#returning-plots-and-figures)
    - [Custom Implementations](#custom-implementations)
  

---------  
# Visualization
_trapeza_ is not primarily designed for visualization purposes. Visualization of _FXStrategy_ and _FXEngine_ results can 
be visualized with external Python packages, e.g. [matplotlib](https://matplotlib.org/). Because _FXEngine_'s results 
are structured in a bit more complicated way (nested data structure), _trapeza_ implements a _FXDashboard_ class, which 
can be used to get a quite rudimentary dashboard visualization.  
________________________________  
## Visualization via external libraries  
All results of _FXStrategy_ and _FXEngine_ can be visualized via external libraries.  
Please see the following documentation on how the results are structured:  
  - [FXStrategy - Retrieving Results](strategy.md#retrieving-results)  
  - [FXEngine - Retrieving Results](engine.md#retrieving-results)  
    
  
Visualization of _FXStrategy_ is pretty straight forward:
  - with [matplotlib](https://matplotlib.org/): [Examples - RSI](https://gitlab.com/LHuebser/trapeza/-/blob/master/doc/script_example_RSI.py#L157-L169)  
  - with [quantstats](https://github.com/ranaroussi/quantstats): [Examples - SMAC](https://gitlab.com/LHuebser/trapeza/-/blob/master/doc/script_example_smac.py#L376-L379)  
  
e.g. from the [RSI example](script_example_RSI.py):  
````python
from trapeza.strategy import FXStrategy
from matplotlib import pyplot as plt

# ...

# init strategy object
strat = FXStrategy('rsi-14-pct', acc, rsi_strategy, 14,
                   rsi_method=rsi, rsi_reference='pct_return', tick_symbol=ticker, ignore_type_checking=True)

# run strategy
strat.run(price_data, 'EUR')

# total value of strategy
plt.figure()
plt.plot(dates[14:], strat.merged_total_balances, label='strategy', color='blue')
plt.ylabel('EUR')
plt.legend()
plt.xticks(rotation=90)
# total value of strategy performance, price data performance
plt.figure()
plt.plot(dates[14:], (np.array(strat.merged_total_balances)/5000)*100, label='strategy', color='blue')
plt.ylabel('% indexed to strategy start [t_0=14]')
plt.plot(dates, (price_data[ticker, 'EUR']/price_data[ticker, 'EUR'][14])*100, label='ads|EUR', color='black')
plt.legend(loc='upper right')
plt.xticks(rotation=90)
````  
See [RSI - Performance Analysis](examples.md#performance-analysis) for resulting figures.  
  
The _FXEngine_ performs multiple runs over a randomly sampled set of sub-windows of the initial data provided. Each result 
of those runs is stored in _FXEngine_, which leads to a slightly more complicated and nested data structure. Nonetheless, 
those results can be visualized 'by hand':  
  - [Examples - RSI](https://gitlab.com/LHuebser/trapeza/-/blob/master/doc/script_example_RSI.py#L218-L238)  
  
e.g. from the [RSI example](script_example_RSI.py):   
````python
# ...

# init an engine object
engine = FXEngine('rsi_engine', strat, ignore_type_checking=True, cache_dir='__cache__')

# run the engine
engine.run(price_data, 'EUR', min_run_length=60, max_total_runs=100)

# analyze the engine
engine.analyze()

# visualize
total_returns = [engine.standard_analysis_results['rsi-14-pct'][w, s]['total_rate_of_return']
                 for w in engine.run_register['rsi-14-pct'].keys()
                 for s in engine.run_register['rsi-14-pct'][w]]
window_lengths = [w for w in engine.run_register['rsi-14-pct'].keys() for _ in engine.run_register['rsi-14-pct'][w]]
````  
  
_FXDashboard_ is an alternative to visualizing _FXEngine_'s results 'by hand' and takes care of parsing results into the 
right format.  
## FXDashboard
As already mentioned, _trapeza_ is not primarily designed for visualization purposes. Nonetheless, to make visualization 
easier, _trapeza_ provides the _FXDashboard_ class. _FXDashboard_ can visualize all the runs performed by _FXEngine_ 
(stochastic sub-sampling), can visualize trading signals appended via _FXStrategy.add_signal()_ (see [FXStrategy - Signals](strategy.md#adding-signals)) 
and can visualize user-defined metrics (see [FXEngine - Metrics](engine.md#results-regarding-metrics)). Regarding metrics, 
_FXDashboard_ not only visualizes single metrics, but also their distribution w.r.t. to the various stochastically sub-sampled 
runs of _FXStrategy_. Symbols for visualizing trading signals can be customized. _FXEngine_ may contain multiple _FXStrategy_ 
objects (different strategies to be compared against each other), _FXDashboard_ will handle those multiple strategies as 
well.  
  
___________  
_FXDashboard_ implements a browser-based dashboard via the external Python Package _Dash_. To access the dashboard, 
_FXDashboard_ launches a local dash server, which can be accessed via any browser.    
___________    
  
[<img src="media/dashboard_landing_page.png" alt="dashboard" width="1050"/>](media/dashboard_landing_page.png)  
The dashboard consists of an overview page, which shows all strategies contained by the _FXEngine_ object. It shows the 
total value of each strategy, the performance (indexed to 100 points at t=0) of each strategy as well as some key metrics.  
Furthermore, the dashboard has separate pages per each strategy. Each of those pages consists of a subpage for all relevant 
metrics and a subpage, which shows each of the runs performed by the stochastic sub-sampled backtests from _FXEngine_. 
The naming convention of each of those runs is '\[start_index]_\[window_length]', where start index is the index from 
where the data slicing starts and therefore includes the lookback, and where window_length is the total number of time 
steps executed by the strategy for this sub-sample. For further details on the naming convention see: [FXEngine - How 
Sub-Sampling works](engine.md#how-sub-sampling-works).

### Instantiate Dashboard Object
When instantiating a _FXDashboard_ object, a _FXEngine_ object (which in turn holds multiple strategies) has to be passed 
to the constructor:  
````python
from trapeza.dashboard import FXDashboard

dash = FXDashboard(engine)
````
Instantiating a _FXDashboard_ object just makes the object accessible, but does not start the local _Dash_ server, nor 
starts any computations/ operations.  
  
The full signature looks as follows:
````
def __init__(self, fx_engine, signal_symbols=None, date_time=None,
             overview_page_layout='simple', strategy_page_layout='full')

fx_engine: FXEngine object
signal_symbols: dict,
                key: str, will be pattern matched against FXStrategy's signals, see section below
                value: tuple, specifing the symbol
date_time: {None, 1D list, 1D numpy array, pandas dataframe, pandas series}, default: None
           dates to be used on the x-axis
overview_page_layout: {'simple', 'full'}, default: 'simple'
                      layout of the overview page
                      simple: only results/ metrics from the run with the larges window length will be
                              displayed (maximal number of time steps, see FXEngine for further details)
                      full: results/ metrics from all runs will be displayed
strategy_page_layout: {'simple', 'full'}, default: 'full'
                      layout of each strategy page, each strategy contained in fx_engine is visualized on separate pages
                      simple: simple histograms
                      full: stacked histograms per window length of runs generated during FXEngine.run()
                            overlaid with approximated probability density function
````   
### Defining Symbols to visualize Signals
The user can define which symbols shall be used to visualize the trading signals (appended via [_FXStrategy.add_signal()_](strategy.md#adding-signals)):     
From the [RSI example](script_example_RSI.py):   
````python
dash = FXDashboard(engine_new, {'buy': ('triangle-up', 'Green'), 'sell': ('triangle-down', 'Red')})
````
A Python dictionary is used for specifying symbols. The keys of this dictionary will be used to be pattern matched against 
_FXStrategy_'s signals, the dictionary values specify the respective symbols via tuples.  
  
The easiest way is using exact matches via _signal_symbols_ dictionary keys and _FXStrategy_'s signals. This way, signals 
can be visualized unambiguously on the correct asset chart line (used in the above example).  
If no exact match can be found, then _FXDashboard_ will perform a substring matching.  
This is done in two steps:  
  1. _FXDashboard_ searches if any of the currency names, stock ticker symbols, asset names, etc., which are used as 
     keys in the [_price data dictionary_](strategy.md#input-data-format) (basically the Python dictionary, which 
     contains exchanges rates/ prices), is contained in any of the [signals](strategy.md#retrieving-results). If it finds 
     a substring match, this symbol will be visualized on the respective chart line of the very asset.  
  2. _FXDashboard_ searches for any substring matches of _signal_symbols_ dictionary keys within each _FXStrategy_'s 
     symbols. If it finds a match, the respective signal will be visualized with the symbol defined via the tuple in 
     corresponding _signal_symbols_ dictionary value and will be placed on the asset chart line found in the previous
     step.  
  
_____
To avoid, that symbols will wrongly be placed multiple times onto the visualization charts, _FXStrategy_'s signals should 
not contain multiple substrings of currencies/ stock ticker symbols (or whatever used as keys in the [price data dictionary](engine.md#input-data-format) 
passed to _FXEngine_), nor multiple substrings of _signal_symbols_ dictionary keys (only one exact substring match).  
Recommendation: only use one currency/ stock ticker symbol and only one substring of _signal_symbols_ keys in _FXStrategy_'s 
signal strings.
_____  
### Defining Symbol Shape and Color
The appearance of each symbol is defined via a tuple in _signal_symbols_ (dictionary values).  
  
A full list of _Plotly/ Dash_ shapes: [https://plotly.com/python/marker-style/#custom-marker-symbols](https://plotly.com/python/marker-style/#custom-marker-symbols)  
A full list of color names: [https://htmlcolorcodes.com/color-names/](https://htmlcolorcodes.com/color-names/)  
### Passing Date Times
For a more comprehensive visualization, date times can be passed to _FXDashboard_, which will be used as tick labels for 
the x-axis.  
Date times can be passed as 1D List, 1D numpy arrays or pandas dataframes/ series. The length must be the same as the number 
of price data points per each asset (same length as each list contained in [price data dictionary](engine.md#input-data-format) 
passed to _FXEngine_).  
  
_Plotly/ Dash_ is a bit tricky regarding the date formats and does not support all formats. Please see 
[https://plotly.com/chart-studio-help/date-format-and-time-series/](https://plotly.com/chart-studio-help/date-format-and-time-series/) 
for details.  
### Page Layouts
The dashboard consists of an overview page and a page per each strategy contained in _FXEngine_. Those pages can be 
customized to some extent by using the following keyword arguments in the constructor of _FXDashboard_:  
__overview_page_layout__:  
[<img src="media/dashboard_overview.jpg" alt="dashboard" width="350"/>](media/dashboard_overview.jpg)
[<img src="media/dashboard_overview_full.jpg" alt="dashboard" width="350"/>](media/dashboard_overview_full.jpg)  
left: 'simple', right: 'full'  
_overview_page_layout_ keyword argument is specified by either the string 'simple' or 'full', default is 'simple': 
  - 'simple': only results (and respective metrics) of the longest run will be displayed  
  - 'full': results (and respective metrics) of all runs will be displayed  
  
__strategy_page_layout__:  
[<img src="media/dashboard_strategy.jpg" alt="dashboard" width="350"/>](media/dashboard_strategy.jpg)
[<img src="media/dashboard_strategy_simple.jpg" alt="dashboard" width="350"/>](media/dashboard_strategy_simple.jpg)  
left: 'full', right: 'simple'  
Because _FXEngine_ does not only one backtesting run and then calculates a single metric, but instead performs multiple, 
stochastic sub-sampled backtests and therefore calculates metrics per each of those runs. This means, that metrics are not 
only represented by a single number but by corresponding distribution (over all runs). _FXDashboard_ takes this into account 
within its visualization.  
_FXDashboard_ visualizes all metrics which were passed to the [_FXEngine.analyze()_ method](engine.md#analyze-the-engine). See [Visualizing Metrics](#visualizing-metrics).  
_strategy_page_layout_ keyword argument is specified by either the string 'full' or 'simple', default is 'full':  
  - 'simple': only simple histograms regarding distribution of metrics over all runs  
  - 'full': stacked histograms per each window lengths (summarizes all runs of a particular window length) overlaid with 
     an approximated probability density function  
    

Additionally, _FXDashboard_ has a sub-page named 'Runs' per each strategy, which visualizes each run of _FXEngine_.  
[<img src="media/dashboard_strategy_run.jpg" alt="dashboard" width="650"/>](media/dashboard_strategy_run.jpg)  
The naming convention for the dropdown menu (to select which run shall be visualized) is as follows:
_\[start_index]_\[window_length]_ with: 
  - start_index: the start index where data slicing starts and therefore factors in the lookback size. The following holds  
    _time step at which strategy execution starts = start_index + lookback size_  
  - window_length: the number of steps that the strategy executed  
  
The data slice is calculated as: _data\[start_index: start_index + window_length + lookback]_  
The 'Runs' sub-page cannot be customized further.  
  
_\*sample images generated via the README example [script_readme_examples.py](script_readme_examples.py)_
### Volume Data
If volume data was passed to the _FXEngine.run()_ method, then this volume data will be also visualized.  
See [FXEngine - Run the Engine](engine.md#run-the-engine) for further details.
### Run and open the Dashboard
Before opening the dashboard, _FXEngine_ first has to invoke its _run()_ and _analyze()_ method, which will compute all 
necessary backtest results. Opening the interactive dashboard is then pretty straightforward by just calling _FXDashboard_'s 
_run()_ method. This will launch a _Dash_ (Flask) server on the local host (e.g. http://127.0.0.1:8050/), which can be opened with 
any browser. The concrete address should be displayed in the terminal.  
````python
# first run and analyze the engine
engine.run(price_data, 'EUR', min_run_length=60, max_total_runs=300)
engine.analyze()

# instantiate a new dashboard and assign previously analyzed engine object
dash = FXDashboard(engine, {'buy': ('triangle-up', 'Green'), 'sell': ('triangle-down', 'Red')}, dates, 'full')

# open the dashboard
dash.run(debug=False)
# do not forget to close the engine afterwards
engine.close()
````  
This should output something at the terminal similar to:  
````
Dash is running on http://127.0.0.1:8050/

 * Serving Flask app "script_readme_examples" (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:8050/ (Press CTRL+C to quit)
````
The _FXDashboard.run()_ has an additional keyword argument _debug_, which can be either True or False:  
  - True: _Dash_ application will be launched in debug mode, which in general is easier to debug and enables hot reloading.
    This might trigger multiple re-calculations and should be turned off, if hot reloading is not necessary. See 
    [https://dash.plotly.com/layout](https://dash.plotly.com/layout) for more details.  
  - False: _Dash_ application will be launched normally without hot reloading. Preferred, no re-computations.   
  
Default is False.
### Close the Dashboard
When opened in the browser, there is an Exit-button at the right top-corner. When pressing this buttons, the dashboard server 
will be stopped and the browser site can safely be closed.  

____
Always hit the Exit-button before closing the browser!  
Do not forget to call [_FXEngine.close()_](engine.md#close-the-engine) at the end of your code!
____  
### Visualizing Metrics
All metrics, which were passed to [_FXEngine.analyze()_](engine.md#analyze-the-engine) will be visualized at each 
strategy's page described as in the chapter [Page Layouts](#page-layouts). Further, see: [available metrics](metric.md).
### Returning plots and figures
Besides providing an interactive dashboard, _FXDashboard_ also provides access to each of the figures displayed in the 
dashboard. Therefore, each figure can be retrieved via _FXDashboard_'s methods and can either be returned as [Plotly 
graphic object](https://plotly.com/python/graph-objects/) or as a single [Plotly figure](https://plotly.com/python/creating-and-updating-figures/).   
For further details, please see the implementation: [fx_dashboard.py](/../trapeza/trapeza/dashboard/fx_dashboard.py).  
### Custom Implementations
_FXDashboard_ is designed to work in conjunction with _FXEngine_, but basically any user-defined class can be used, which 
inherits from _trapeza.engine.base_engine.BaseEngine_ base class and implements the same data structure regarding storing 
results as _FXEngine_.   
Furthermore, _FXDashboard_ itself is derived from the base class _trapeza.dashboard.base_dashboard.BaseDashboard_.

  
  
---  
[Back to Top](#visualization)