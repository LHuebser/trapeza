import numpy as np
from matplotlib import pyplot as plt
from pandas_datareader import data
from trapeza.account import FXAccount
from trapeza.strategy import FXStrategy
from trapeza.engine import FXEngine
from trapeza.dashboard import FXDashboard

import yfinance as yfin


yfin.pdr_override()     # patch pandas-datareader as it sometimes has problems when yahoo makes changes to its website


if __name__ == "__main__":
    print('First Example: Account')
    acc = FXAccount('EUR')
    friend_account = FXAccount('USD')

    acc.deposit(100, 'EUR', 5)
    print(acc.depot)
    acc.transfer(friend_account, 50, 'EUR', payer_processing_duration=5, payee_processing_duration=10)
    print(acc.depot)
    acc.tick()
    print(acc.depot)
    acc.tick(5)
    print(acc.depot)
    friend_account.tick(11)
    print(friend_account.depot)
    print('----------------------------------------------------------------------------------------------')

    print('Second Example: Strategy')
    btc_eur = data.DataReader('BTC-EUR', start='2020-06-01', end='2021-05-12',
                              data_source='yahoo')['Close'].reset_index().drop_duplicates('Date', 'last')
    eur_usd = data.DataReader('EURUSD=X', start='2020-06-01', end='2021-05-12',
                              data_source='yahoo')['Close'].reset_index().drop_duplicates('Date', 'last')
    btc_usd = data.DataReader('BTC-USD', start='2020-06-01', end='2021-05-12',
                              data_source='yahoo')['Close'].reset_index().drop_duplicates('Date', 'last')
    currency_dates = eur_usd['Date'].to_numpy()
    crypto_dates = btc_eur['Date'].to_numpy()
    common_dates = np.sort(np.array(list(set(currency_dates).intersection(set(crypto_dates)))))
    btc_eur = btc_eur[btc_eur['Date'].isin(common_dates)]['Close'].to_numpy()
    btc_usd = btc_usd[btc_usd['Date'].isin(common_dates)]['Close'].to_numpy()
    eur_usd = eur_usd[eur_usd['Date'].isin(common_dates)]['Close'].to_numpy()
    price_data = {('BTC', 'EUR'): btc_eur, ('EUR', 'USD'): eur_usd, ('BTC', 'USD'): btc_usd}

    def avg_strategy(accounts, price_data_point, reference_currency, volume_data_point, fxstrategy):
        avg_10_today = np.sum(price_data_point['BTC', 'EUR'][-10:]) / 10
        avg_50_today = np.sum(price_data_point['BTC', 'EUR'][-50:]) / 50
        avg_10_yesterday = np.sum(price_data_point['BTC', 'EUR'][-11:-1]) / 10
        avg_50_yesterday = np.sum(price_data_point['BTC', 'EUR'][-51:-1]) / 50
        price_today = price_data['BTC', 'EUR'][-1]

        if avg_10_yesterday < avg_50_yesterday and avg_10_today > avg_50_today:
            accounts[0].buy(1, price_today, 'BTC', 'EUR')
            fxstrategy.add_signal(accounts[0], 'buy')

        if avg_10_yesterday > avg_50_yesterday and avg_10_today < avg_50_today:
            accounts[0].sell(1, price_today, 'BTC', 'EUR')
            fxstrategy.add_signal(accounts[0], 'sell')

    acc.deposit(1_000_000, 'EUR')
    acc.deposit(2, 'BTC')
    strategy = FXStrategy('my_awesome_strategy', acc, avg_strategy, lookback=51)
    strategy.run(price_data, 'USD')
    print(strategy.merged_total_balances)
    print(len(price_data['BTC', 'EUR']), len(strategy.merged_total_balances))


    def align_yaxis(ax1, v1, ax2, v2):
        """adjust ax2 ylimit so that v2 in ax2 is aligned to v1 in ax1"""
        _, y1 = ax1.transData.transform((0, v1))
        _, y2 = ax2.transData.transform((0, v2))
        inv = ax2.transData.inverted()
        _, dy = inv.transform((0, 0)) - inv.transform((0, y1 - y2))
        miny, maxy = ax2.get_ylim()
        ax2.set_ylim(miny + dy, maxy + dy)

    fig, ax1 = plt.subplots()
    ax1.plot(common_dates[51:], np.array(strategy.merged_total_balances) / strategy.merged_total_balances[0],
             label='strategy', color='orange')
    buy_label = False
    sell_label = False
    for i, signals in enumerate(strategy.signals):
        if 'buy' in signals[0]:
            if buy_label is False:
                ax1.scatter(common_dates[51+i], strategy.merged_total_balances[i] / strategy.merged_total_balances[0],
                            color='green', label='buy', s=50)
                buy_label = True
            else:
                ax1.scatter(common_dates[51 + i], strategy.merged_total_balances[i] / strategy.merged_total_balances[0],
                            color='green', s=50)
        if 'sell' in signals[0]:
            if sell_label is False:
                ax1.scatter(common_dates[51 + i], strategy.merged_total_balances[i] / strategy.merged_total_balances[0],
                            color='red', label='sell', s=50)
                sell_label = True
            else:
                ax1.scatter(common_dates[51 + i], strategy.merged_total_balances[i] / strategy.merged_total_balances[0],
                            color='red', s=50)
    plt.legend(loc='upper left')
    ax2 = ax1.twinx()
    ax2.plot(common_dates, btc_eur / btc_eur[51], label='BTC|EUR')
    ax1.set_ylabel('Strategy merged total balance indexed')
    ax2.set_ylabel('BTC|EUR indexed to start date of strategy')
    plt.legend(loc='upper right')
    align_yaxis(ax1, 1, ax2, 1)
    plt.tight_layout()

    fig, ax1 = plt.subplots()
    avg_10 = [np.sum(btc_eur[i-10:i])/10 for i in range(10, len(common_dates)+1)]
    avg_50 = [np.sum(btc_eur[i - 50:i]) / 50 for i in range(50, len(common_dates)+1)]
    ax1.plot(common_dates[9:], avg_10, label='avg10', color='orange', linestyle=':')
    ax1.plot(common_dates[49:], avg_50, label='avg50', color='green', linestyle='--')
    plt.legend(loc='upper right')
    ax2 = ax1.twinx()
    ax2.plot(common_dates, btc_eur, label='BTC|EUR')
    buy_label, sell_label = False, False
    for i, signals in enumerate(strategy.signals):
        if 'buy' in signals[0]:
            if buy_label is False:
                ax2.scatter(common_dates[51+i], btc_eur[51+i], color='green', label='buy', s=50)
                buy_label = True
            else:
                ax2.scatter(common_dates[51 + i], btc_eur[51 + i], color='green', s=50)
        if 'sell' in signals[0]:
            if sell_label is False:
                ax2.scatter(common_dates[51 + i], btc_eur[51+i], color='red', label='sell', s=50)
                sell_label = True
            else:
                ax2.scatter(common_dates[51 + i], btc_eur[51 + i], color='red', s=50)
    plt.legend(loc='upper left')
    plt.show()
    print('----------------------------------------------------------------------------------------------')

    print('Third Example: Engine')
    engine = FXEngine('backtesting', strategy, n_jobs=-1)  # n_jobs: use all available cores
    engine.run(price_data, 'USD', min_run_length=5, max_run_length=50, max_total_runs=200)

    def custom_metric(x):
        return np.var(x)

    engine.analyze({'my_metric': custom_metric})
    print(engine.standard_analysis_results)
    print(engine.analysis_results)

    dash = FXDashboard(engine)
    dash.run(debug=False)
    engine.close()
