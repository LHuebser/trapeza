Go back: [Table of Content](table_of_content.md)  
- [Installation](#installation)
  - [Installation via pip](#installation-via-pip)
  - [Build from Source](#build-from-source)
    - [Build mpdecimal](#build-mpdecimal)
    - [Re-compile via Cython](#re-compile-via-cython)
    - [Build .pyd/.so inplace](#build-pydso-inplace)
    - [pip install from Local Built](#pip-install-from-local-built)
    
 
-----
# Installation  
_trapeza_ can be installed:  
 1. via pip install, or
 2. build from source (and then pip install from local built)  
  
The recommended way of installing _trapeza_ is using [pip install](#installation-via-pip).  
  
Building from source can be done in two ways:  
 1. For development purpose: In this case the section [Build mpdecimal](#build-mpdecimal) can be skipped! _trapeza_ comes 
    with pre-compiled versions of mpdecimal.  
 2. Problems with mpdecimal: When problems during installation occur, then follow [Build from Source](#build-from-source) 
    and also consider the section [Build mpdecimal](#build-mpdecimal) (this is very likely for MacOs as currently mpdecimal
    is only pre-compiled for Windows and Linux).  
  
  
When building from source, _trapeza_ relies on Cython to compile .pyx files into C and C++ files. Cython requires a 
C compiler:  
    __Windows:__ MinGW is a popular option as an open source compiler. Alternatively Microsoft's Visual C Compiler (VCC) 
                can be used.  
    __Linux:__ gcc is usually available or can be easily fetched via sudo apt-get install build-essential.  
    __MacOS:__ gcc can be retrieved via Apple's XCode.  
See Cython for [further information](https://docs.cython.org/en/latest/src/quickstart/install.html).  
__WHEN INSTALLING FROM SOURCE, MAKE SURE _CYTHON_ IS INSTALLED!__
  
In the following, it is assumed, that a default Python3 environment is configured and _trapeza_ shall be installed herein,  
and that the latest version of pip is available.  
For further information about virtual environments and pip, please follow [venv](https://docs.python.org/3/library/venv.html), 
[conda env](https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html) and 
[pip](https://pip.pypa.io/en/stable/installing/).  
_pip install routine was tested under Python3.6 and Python3.8._  
  
___  
Building from source is only necessary if problems with [mpdecimal](https://www.bytereef.org/mpdecimal/index.html) (on 
which _trapeza_ relies on for decimal math, see [Floating Point Error](arithmetics.md#floating-point-error)) occur or for 
active development on the _trapeza_ package.  
Make sure to [_pip install -e ._](#pip-install-from-local-built) after re-compiling!   
___  
  
## Installation via pip
Installation of the latest release via pip is the preferred way:
````
pip install trapeza
````  
All [requirements](../requirements.txt) will install automatically during _pip install_ if they are not already satisfied.  
If you lack permissions, _trapeza_ can be installed into the user directory via:
````
pip install --user trapeza
````  
It is also possible, to clone the _trapeza_ repository and run 
````
(optional: cd some_download_folder/trapeza)
pip install -e .
````  
from the root folder of _trapeza_ where _setup.py_ is located as well.  
## Build from Source
_trapeza_ relies on [mpdecimal](https://www.bytereef.org/mpdecimal/index.html) for decimal math arithmetics (vs floating 
point arithmetics). In order for Cython to compile properly, mpdecimal has to be pre-built. _trapeza_ already comes with 
a ready-to-use version of mpdecimal (pre-compiled and wrapped by trapeza.arithmetics.libmpdec), such that the user normally 
does not have to re-compile mpdecimal separately.  
  
Nevertheless, it is possible that problems occur due to mpdecimal. In this case, the setup.py will try to build trapeza 
without mpdecimal during the 'pip install'-routine, which means that 'LIBMPDEC' and 'LIBMPDEC_FAST' will not be available 
as arithmetic backends (see [arithmetics][arithmetics.md]).  
  
Alternatively, to enable mpdecimal in case that problems occurred, _trapeza_ and mpdecimal can be built from source.  
If mpdecimal does not cause any problems, then re-building mpdecimal is not necessary to build _trapeza_ form source and 
one can proceed to [Re-compile via Cython](#re-compile-via-cython).  
  
___  
Make sure to [_pip install -e ._](#pip-install-from-local-built) after re-compiling!    
___  
  
### Build mpdecimal
___
_trapeza_ has a pre-compiled version of _mpdecimal_ for Windows and Unix. In general, there should not be any problems 
using those pre-built versions of _mpdecimal_.  
  
The steps in this section are only necessary if _mpdecimal_ makes trouble during installation. 
In this case, follow the instructions below. Else, you can directly proceed to the next [section](#re-compile-via-cython) 
to build _trapeza_ from source. 
___  
  
_trapeza_ comes with a full copy of mpdecimal's source.  
Git clone or download _trapeza_'s source from: [https://gitlab.com/LHuebser/trapeza](https://gitlab.com/LHuebser/trapeza).  
If one follows the steps beneath, make sure to use this version of mpdecimal.  
  
In all cases, make sure that the re-compiled files are properly placed into _trapeza_'s directory and that they replace 
the old (original) files. This is especially true, if you do not follow beneath instructions but other compiling instructions 
(e.g. those from mpdecimal or brew (when using MacOS) directly) which compile their files into different directories. It 
is recommended to stick to beneath instructions and to pay close attentions to the path into which you should navigate (_cd_) 
into before executing the command instructions. The compiled files are placed into [trapeza/trapeza/src/win](../trapeza/src/win) (for Windows) 
or into [trapeza/trapeza/src/linux](../trapeza/src/linux) (for Linux and MacOS) depending on your platform (make sure, if you follow other 
compilation instructions to replace the compiled files in those directories with your fresh re-compiled files).  
#### Windows
Navigate to _some_download_folder/trapeza/trapeza/src/win/mpdecimal-2.5.1/vcbuild_. The directory _vcbuild_ contains a number of .bat 
executable files.  
1. Run vcdistclean.bat.bat (double-click).
2. Run vcbuild64.bat (double-click).  
mpdecimal is now ready to be included for compilation by Cython.
#### Linux
Navigate to _some_download_folder/trapeza/trapeza/src/linux/mpdecimal-2.5.1_, where configure, install-sh, etc. files are located.
````
cd some_download_folder/trapeza/trapeza/src/linux/mpdecimal-2.5.1

autoconf

dos2unix *

./configure

make

sudo make install
````  
autoconf and dos2unix might need to be fetched via sudo apt-get install.  
autoconf is used to make the configure file bash executable. dos2unix is used for file formatting. \r\n and \n might got 
mixed up during download.  
#### MacOS
The installation instructions for [Linux](#linux) should also work for MacOS.

___  
_MacOS has not been tested yet. If you use brew for installing mpdecimal, you probably need to replace trapeza's 
compiled files with the fresh re-compiled files properly. The pre-compiled files (which are to be replaced by the 
re-compiled ones) are located under 'trapeza/trapeza/src/linux/mpdecimal-2.5.1/. Installation for MacOS has not been tested yet 
such that this is only a suggestion/ assumption._  
___  
  
  
### Re-compile via Cython
___  
__MAKE SURE _CYTHON_ IS INSTALLED!__  
_It might be useful to use Python's 'setup clean' before proceeding with the next step._  
_This step can be done either with or without re-building mpdecimal library. A pre-compiled version of mpdecimal is included 
in _trapeza_ and only needs to be re-built if problems occurred during normal installation._  
___  
  
  
Navigate to the root folder of trapeza where the setup.py is located. In order to re-compile the files via Cython:  
````
cd some_download_folder/trapeza
python setup.py build_ext --cython (--inplace for dev, can be both used at once or separately)
````  
Make sure to [_pip install -e ._](#pip-install-from-local-built) after re-compiling!  
### Build .pyd/.so inplace
Sometimes it is handy, to place the .pyd or .so files into the same directory as the .pyx and .c/ .cpp files. This is 
especially the case for development:
````
python setup.py build_ext --inplace
````   
This avoids updating the version of _trapeza_, which is placed in site-packages of the user's venv via pip. 
Therefore, code is executable from the local repository/ directory, which is easier during the development process. 
The --inplace flags places all .pyd and .so files into the same directory structure as their .c/.cpp (and respective .pyx) 
files. This step is usually only necessary for active development on the _trapeza_ package.  
When building from source and when [pip install from Local Built](#pip-install-from-local-built) will be done, then this 
step (_'Build .pyd/.so inplace'_) can be omitted.  
  
Make sure to [_pip install -e ._](#pip-install-from-local-built) after re-compiling!  
### pip install from Local Built
If the successful build from the previous steps shall be made available environment-wide, then navigate to the root of 
_trapeza_ (where the setup.py is located, but if you followed above steps, you should be already there...) and run:
````
pip install -e .     (note the dot at the end)
````  
  
_trapeza_ should now be ready to be used in your projects.

  
  
---  
[Back to Top](#installation)