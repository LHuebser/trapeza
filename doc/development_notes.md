Go back: [Table of Content](table_of_content.md)
- [Development Notes](#development-notes)
  - [Structure of _trapeza_'s Main Concepts](#structure-of-trapezas-main-concepts)
  - [Compiling](#compiling)
  - [Profiling and Testing](#profiling-and-testing)
  - [Versioning](#versioning)
  - [Package Data in setup.py](#package-data-in-setuppy)
  - [Tools](#tools)
  - [ToDos](#todos)
  - [Changelog](#changelog)
  - [Contribution](#contribution)


---------
# Development Notes
________________________________
## Structure of _trapeza_'s Main Concepts
The main concepts _FXAccount_, _FXStrategy_, _FXEngine_ and _FXDashboard_ are all encapsulated in separate classes.
Each of this classes inherits from corresponding base classes:
  - trapeza.account.base_account.BaseAccount
  - trapeza.strategy.base_strategy.BaseStrategy
  - trapeza.engine.base_engine.BaseEngine
  - trapeza.dashboard.base_dashboard.BaseDashboard

Each of those base classes defines an interface and class properties. This is especially useful, when the user wants
to define own classes and implementations. As long as custom implementations inherit from those base classes, the above
concepts should be swappable in a plug-and-play manner without any problems.

Additionally, _FXAccount_ and _FXStrategy_ are based on a Cython implementation, which transpiles into C/ C++. This is
done for performance reasons. The Cython implementation are implemented as separate classes. _FXAccount_ and _FXStrategy_
are only thin wrappers around those Cython implementation (cFXAccount and cFXStrategy), which additionally implement some
type checking.
Therefore, those two concepts (classes) do not only inherit from their base classes, but also from the Cython implementations
(cFXAccount and cFXStrategy), which contain the main part of the compute logic.
The Cython implementations are split into a .pxd definition file and a .pxy implementation file.
Furthermore, the heap logic of _FXAccount_ is implemented as a separate Cython class in [execution_heap.pyx](../trapeza/account/execution_heap.pyx)
and its definition file [execution_heap.pxd](../trapeza/account/execution_heap.pxd)

The arithmetic logic in _trapeza_ is also implemented via Cython and heavily relies on external libraries (e.g.
[mpdecimal](https://www.bytereef.org/mpdecimal/index.html)). Each [arithmetic backend](arithmetics.md) has its own
implementation .pyx file and definition .pxd file. All backend implementations are placed under the directory 'trapeza/trapeza/arithmetics'.
The [arithmetics.pyx](../trapeza/arithmetics/arithmetics.pyx) file is the central interface file, which handles
which backend will be used according to the [context](arithmetics.md) and also handles all rounding methods.

## Compiling
For compilation see [installation.md](installation.md) and the [setup.py](../setup.py) file.
For development, it is recommended to compile inplace with the '--inplace' flag.

When any code changes are applied, _trapeza_ has to be re-compiled before changes take effect (do not forget this before
running e.g. unit tests...).

___
Make sure to [_pip install -e ._](#pip-install-from-local-built) after re-compiling!.
___

When actively developing on _trapeza_, make sure to re-compile _trapeza_ in order to have changes take effect.
_trapeza_ is built with Cython. See [Installation](installation.md) for further details. Make sure to use the _--cython_
compile specifier. It is recommended to also use _--inplace_. Make sure to call _pip install -e ._ after each re-compilation
(cd to root of trapeza first).

Re-compile after any code changes. Also provide generated .c and .cpp files. The .pyd files do not have to be provided.

## Profiling and Testing
All unit tests are placed in the directory 'trapeza/trapeza/tests'. Unit tests are currently poorly documented and structured
and should be re-written in the near future...

All profiling scripts are placed in the directory 'trapeza/trapeza/profiling'. Profiling is useful to know where optimization
is really necessary and effective.
When profiling Cython-based code, some additionally compile flags have to be set in the corresponding Python files at the
very top:
```
# cython: profile=True
# cython: linetrace=True
# cython: binding=True
# distutils: define_macros=CYTHON_TRACE_NOGIL=1
```
Do not forget to re-compile. For further information see the [Cython Documentation](https://cython.readthedocs.io/en/latest/src/tutorial/profiling_tutorial.html).
Do not forget to remove the compile flags and re-compile afterwards, as profiling has significant impact on performance.

Files with the prefix 'mem_' do memory profiling instead of time profiling.

## Versioning
The version number is set in [_version.py](../_version.py).
If a new version of _trapeza_ shall be released, change the version in this file accordingly. Version number only has to
be changed in this central file.

If a new development status is reached, then in [setup.py](../setup.py) change the _classifier_ within _def setup_call()_
accordingly.

That should be everything needed to change versioning information in _trapeza_.

## Package Data in setup.py
All .pxd files in 'trapeza/arithmetics' are also made available as package data in the [setup.py](../setup.py) file.

This is especially useful, as _trapeza_ implements Cython bindings for [mpdecimal](https://www.bytereef.org/mpdecimal/index.html),
[emyg_dtoa](https://github.com/miloyip/dtoa-benchmark/blob/master/src/emyg/emyg_dtoa.c) and [ryu_dtoa](https://github.com/ulfjack/ryu),
which can be useful for others.
## Tools
The following tools are used during development:

__radon__:
- radon cc account.py -a -s
- radon mi account.py -s -m

__coverage__:
- coverage erase
- coverage run -m py.test test_FXAccount.py
- coverage report -m    (-i)
- coverage html     (-i)

__memory profiling__:
- memory_profiler [https://pypi.org/project/memory-profiler/](https://pypi.org/project/memory-profiler/)

## ToDos
- Try build and install process on MacOS. Delete .pyd files from wheels.
- Typo-Check documentation and check all links.
- __Re-write unit tests with clearer structure and more comments__
- __Add unit tests:__
    - Unit test kwargs in strategy decision function and tick function (kwargs usage).
    - Unit test path mangling in FXEngine cache.
    - Unit test FXDashboard date_time input types.
    - Unit test if collect and transfer really throws trapeza.exception.AccountError.
- __FXAccount.total_balance: assume exchange_rate=0 if currency not in data (is this really a good idea?)__
- __Write benchmarks for metrics and backtesting in comparison to other Python packages regarding performance and accuracy__
  Possible packages to benchmark against:
    - https://www.backtrader.com/docu/analyzers/analyzers/
    - https://github.com/quantopian/empyrical
    - https://github.com/quantopian/pyfolio
    - https://pypi.org/project/pyfinance/
    - https://github.com/quantopian/zipline
    - https://pypi.org/project/pyfinance/
    - https://github.com/ematvey/pybacktest
    - https://github.com/cuemacro/finmarketpy
- __Add an example where price data is generated dynamically inside the strategy decision function, e.g. when modelling
  option or turbo bull prices (which change depending on the underyling) such that those prices do not have to be passed
  within the price_data_dict argument supplied to FXStrategy.run() but are generated dynamically inside the strategy
  decision function.__
- __Implement a delay element at order management between hitting limit price and actual order execution (price), e.g.
  there's a difference between stop loss and stop loss limit, which are currently the same.__
- __FXEngine: sometime not all cores are used by joblib! --> os.environ["OPENBLAS_MAIN_FREE"] = "1" ?__
- __Account for stock shares: Based on FXAccount, but only allow integers (math.floor) for share volumes (respectively use
  kwarg to additionally specify if float/ ratio share volumes are allowed).__
- __Model dividends and ex-dividend prices.__
- Overlay additional concept: market simulation regarding liquidity and best orders. Conduct a market simulation
  (e.g. implemented as stand-alone Python function) in order to find current market price of a stock/ currency, then
  use this market price to feed into transaction method e.g. FXAccount.sell(). Currently, prices are deterministically
  dictated to transaction methods like FXAccount.sell(). For small volumes this should not affect backtesting. But for
  larger volumes, which might have a significant impact on market prices, this will affect backtesting for sure, which
  makes backtesting less reliable. In short: model stock liquidity before making any transactions:
    - models market's liquidity and prices (incl. volatility)
    - takes order books of multiple accounts (volumes and prices)
    - outputs prices and liquid volumes per prices and per time step (complies with time discrete account model), which
      can be fed into account orders, which then take care of filling orders
    - does not get patched onto account
    - implementation via callbacks
- emin and emax in libmpdec (wrapper for mpdecimal C-library) are currently left at default (this should work without
  any troubles), customization would be good.
- Quite a lot of todos in the implementation files regarding further (micro) optimization and further ideas and concepts.
- Check docstring of metric.py and documentation in metric.md
- Make interface of expected_rate_of_return to be similar to total_rate_of_return (computation on raw inputs) in
  [metric.py](../trapeza/metric.py).
- extend examples, e.g. risk models (i.e. via sklearn), usage of other financial Python packages in conjunction with
  _trapeza_, etc.
- Implement win-loss metric in trapeza.metrics.
- Add further FX market specific mechanisms, e.g. roll-overs and margin trading
- Improve visualization in FXEngine
- Implementation and integration of fixed point arithmetics as alternative to 'FLOAT', 'DECIMAL',
  'LIBMPDEC_FAST' (uses ryu dtoa), 'LIBMPDEC' (uses dtoa of CPython) when setting trapeza.context.ARITHMETICS
  See todo in trapeza.arithmetics for adding rounding methods when implementing fixed point arithmetics.
  --> Currently, there is no urgent need for this as fixed point math can be emulated via
  trapeza.context.ARBITRARY_QUANTIZE_SIZE setting. Only reason for implementing fixed point math separately would be
  runtime performance...
- Implement more order types ([Order Management](order_management.md)).
- Implement more [metrics](metrics.md): tail value at risk, bias ratio, win-loss-ratio, average absolute deviation,
  calmar ratio, long short exposure, sector exposure, etc. See comments in [metric.py](../trapeza/metric.py).
- See comments in [metric.py](../trapeza/metric.py) for further implementation details regarding factoring in inflation,
  factoring in time value of money, chebyshev distribution, etc.
- Improve computation time of FXEngine.analyze().
- Develop own fixed point arithmetics: common libraries suffer from overflow problems. This can be mitigated by a
  custom data type, which splits integral and fractional part into two long long data types. This way overflows do not
  occur as likely as in common libraries.
- Data parser to turn pandas and numpy data more easily into _trapeza_'s dict type price data. See [strategy.md#input-data-format]
  regarding the dict data structure for market information.

## Changelog
### 0.0.9 & 0.0.10 & 0.0.11
- Fixed missing setup.py
### 0.0.8
- Fix import bug py_dtoa_ryu
### 0.0.7
- Place src into package namespace. This somehow pollutes the project, but setuptools is not working otherwise.
- Call ctypes.CDLL to load libmpdec DLLs for Windows.
- Fix import bug py_dtoa_ryu
### 0.0.6
- Since Python 3.7 and higher import behavior of DLLs has changed. libmpdec's DLLs are now placed into trapeza/arithmetics
  during setup.py
- Fix import bug of py_dtoa_ryu (from trapeza.arithmetics.dtoa) in trapeza.utils
- Add keyword argument 'suppress_evaluation' to the constructor of FXStrategy, which suppresses automatic evaluation of
  accounts during each time step (iteration) of FXStrategy.run. This is especially handy when implementing custom
  loops in the strategy decision function, which is passed to FXStrategy. Adapted api_reference.md and strategy.md (documentation)
  accordingly.
- Change MANIFEST.in to exclude test and profiling files.
- Fix bug regarding paths in FXEngine. FXEngine had problems distinguishing relative and absolute paths.
### 0.0.5
- Bug fix context settings and import problems
### 0.0.4
- Bug fix context settings and import problems
### 0.0.3
- Make sub-modules available from top module _trapeza_
### 0.0.2
- Use trapeza.arithmetics.dtoa.py_dtoa_ryu() instead of frepr in trapeza.utils, which reduces the number of dependencies and is faster
- Do memory profiling for trapeza.arithmetics.dtoa.py_dtoa_ryu(), no memory leaks detected.
- Fix bug in setup.py, which caused installation fails for >Python3.6. This was caused by missing '_extra_objects_' argument
  for distutils.core.extension. This leads to unresolved external references under Windows (missing library link for
  compilation). This only affects Windows. MacOs is still untested.

## Contribution
Contribution and suggestions for improvements and future features are warmly welcome!



---
[Back to Top](#development-notes)